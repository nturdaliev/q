DO
$$
BEGIN
  IF NOT EXISTS(SELECT column_name
                FROM information_schema.columns
                WHERE table_schema = 'public' AND table_name = 'questionnaire_questionnaire' AND column_name = 'copyright')
  THEN
    ALTER TABLE questionnaire_questionnaire ADD COLUMN copyright CHARACTER VARYING;
  ELSE
    RAISE NOTICE 'Already exists';
  END IF;
END
$$;

CREATE TABLE IF NOT EXISTS questionnaire_email_template
(
  id INTEGER PRIMARY KEY NOT NULL,
  name VARCHAR NOT NULL,
  content TEXT NOT NULL,
  active BOOLEAN DEFAULT true NOT NULL
);
INSERT INTO public.questionnaire_email_template (id, name, content, active) VALUES (1, 'First Template', '<p>Hello {{client.firstname}}</p>

<p>&nbsp;</p>
<p>Your email address is {{client.email}}</p>
<p>You can log in with the following link {{ loginLink }}.</p>
<p>&nbsp;</p>
<p>Kind Regards,</p>
<p>&nbsp;</p>

<p>Nursultan Turdaliev</p>', true);
INSERT INTO public.questionnaire_email_template (id, name, content, active) VALUES (2, 'Second Template', '<p>Hello {{client.firstname}}</p>

<p>&nbsp;</p>
<p>Your email address is {{client.email}}</p>
<p>&nbsp;</p>
<p>Kind Regards,</p>
<p>&nbsp;</p>
<p>Nursultan Turdaliev</p>', true);
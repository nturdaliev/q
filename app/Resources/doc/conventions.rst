Security Conventions

to ensure the dynamic structure used within this application, please maintain this conventions:

a user credentials is made out of two or three parts and capitalized.
-  Those granting privileges to models, forms, etc. consist of three parts, eg. ROLE_LOCATION_VIEW, ROLE_UNIT_EDIT, ROLE_<ENTITY>_<ACTION>
-  Those granting privilege to a part of the application, like signing in, consist of two parts, eg. ROLE_USER

a form service id is written like:
    factory: <BUNDLE_PREFIX>.form.<ENTITY>
    type: <BUNDLE_PREFIX>.form.<ENTITY>.type
    handler: <BUNDLE_PREFIX>.form.<ENTITY>.handler

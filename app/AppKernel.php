<?php

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Propel\PropelBundle\PropelBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new JMS\I18nRoutingBundle\JMSI18nRoutingBundle(),
            new JMS\TranslationBundle\JMSTranslationBundle(),
            new Bazinga\Bundle\JsTranslationBundle\BazingaJsTranslationBundle(),
            new Bazinga\Bundle\PropelEventDispatcherBundle\BazingaPropelEventDispatcherBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Ivory\OrderedFormBundle\IvoryOrderedFormBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new Axtion\Bundle\UtilitiesBundle\AxtionUtilitiesBundle(),
            new Axtion\Bundle\FrontendBundle\AxtionFrontendBundle(),
            new Axtion\Bundle\UserBundle\AxtionUserBundle(),
            new Axtion\Bundle\QuestionnaireBundle\AxtionQuestionnaireBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new \Ivory\CKEditorBundle\IvoryCKEditorBundle(),
            new SC\DatetimepickerBundle\SCDatetimepickerBundle(),
            new Burgov\Bundle\KeyValueFormBundle\BurgovKeyValueFormBundle(),
            new Axtion\Bundle\SliderBundle\AxtionSliderBundle(),
            new SunCat\MobileDetectBundle\MobileDetectBundle(),
            new Axtion\Bundle\AssessmentBundle\AxtionAssessmentBundle(),
            new Axtion\Bundle\HelpBundle\AxtionHelpBundle(),
            new FM\ElfinderBundle\FMElfinderBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}

/**
 * Created by tunu on 4/21/2016.
 */
/*jshint strict: true */
/*jslint browser: true*/
/*global $, jQuery, alert, Translator, console,CKEDITOR*/
jQuery(function () {
    "use strict";
    (function ($) {
        $.widget('axtion.paginatedTab', {
            _create: function () {
                this.init();
                this.configBtnNext();
                this.configBtnPrev();
            },
            init: function () {
                this.tabList = this.element.find('ul.nav-tabs>li');
                this.tabContents = this.element.find('.tab-content>div');
                this.nextBtn = this.element.find('.btn-next');
                this.prevBtn = this.element.find('.btn-prev');
            },
            configBtnNext: function () {
                var self = this, activeTab;
                self.nextBtn.on('click', function () {
                    activeTab = self.tabList.filter('.active');
                    activeTab.next().find('a').trigger('click');
                });
            },
            configBtnPrev: function () {
                var self = this, activeTab;
                self.prevBtn.on('click', function () {
                    activeTab = self.tabList.filter('.active');
                    activeTab.prev().find('a').trigger('click');
                });
            }
        });
    }(jQuery));
});
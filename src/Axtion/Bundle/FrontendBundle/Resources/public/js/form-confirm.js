/**
 * Created by tunu on 4/21/2016.
 */
/*jshint strict: true */
/*jslint browser: true*/
/*global $, jQuery, alert, Translator, console,CKEDITOR*/
jQuery(function () {
    "use strict";
    (function ($) {
        $.widget('axtion.formConfirm', {
            _create: function () {
                var self = this;
                self.message = self.element.data('message');
                self.element.submit(function (event) {
                    if (!confirm(self.message)) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                });
            }
        });
        $('.form-confirm').formConfirm();
    }(jQuery));
});
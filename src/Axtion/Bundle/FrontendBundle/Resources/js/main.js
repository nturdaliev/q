/**
 * Created by tunu on 4/22/2016.
 */
jQuery(function () {
    "use strict";
    (function ($) {
        $('.axtion-datatable').DataTable({
            paging: false,
            scrollY: 400,
            language: {
                processing: "Bezig...",
                lengthMenu: "_MENU_ resultaten weergeven",
                zeroRecords: "Geen resultaten gevonden",
                info: "_START_ tot _END_ van _TOTAL_ resultaten",
                infoEmpty: "Geen resultaten om weer te geven",
                infoFiltered: " (gefilterd uit _MAX_ resultaten)",
                infoPostFix: "",
                search: "Zoeken:",
                emptyTable: "Geen resultaten aanwezig in de tabel",
                infoThousands: ".",
                loadingRecords: "Een moment geduld aub - bezig met laden...",
                paginate: {
                    first: "Eerste",
                    last: "Laatste",
                    next: "Volgende",
                    previous: "Vorige"
                }
            }
        });
    }(jQuery));
});
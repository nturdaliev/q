<?php

namespace Axtion\Bundle\FrontendBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class HomeController
 *
 * @package Axtion\Bundle\FrontendBundle\Controller
 * @Route("/")
 */
class HomeController extends Controller
{
    /**
     * @Route("/", name="frontend_home_index")
     * @Template()
     *
     * @return array Template variables
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/panel/dashboard", name="frontend_home_dashboard")
     * @Security("has_role('ROLE_USER')")
     * @Template()
     */
    public function dashboardAction()
    {
        return array();
    }
}

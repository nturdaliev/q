<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 9/7/2015
 * Time: 11:41 AM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\Handler;


use Axtion\Bundle\QuestionnaireBundle\Propel\Dependency;
use Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuery;
use Axtion\Bundle\UtilitiesBundle\Form\Handler\BaseHandler;
use Persistent;

/**
 * Class DependencyHandler
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Handler
 */
class DependencyHandler extends BaseHandler
{
    /**
     * @param Persistent $obj
     * @return bool
     */
    public function process(Persistent $obj)
    {
        $this->form->setData($obj);

        $this->form->handleRequest($this->request);
        /** @var Dependency $obj */
        $obj = $this->form->getData();

        $submitButton = $this->form->has('submit') ? $this->getForm()->get('submit') : false;
        if ($this->form->isValid()) {
            if ($submitButton && $submitButton->isClicked()) {
                $this->onSuccess($obj);
            }

            return true;
        }

        return false;
    }

    /**
     * @param Persistent | Dependency $obj
     * @throws \Exception
     * @throws \PropelException
     */
    protected function onSuccess(Persistent $obj)
    {
        $dependency = DependencyQuery::create();
        $dependency->filterByQuestionId($obj->getQuestionId());
        $dependency->filterByValue($obj->getValue());
        $dependency = $dependency->findOneOrCreate();

        if(!$dependency->isNew()){
        }
        $dependency->setDependentQuestions($obj->getDependentQuestions());
        $dependency->save();
    }
}
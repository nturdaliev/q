<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 4/20/2016
 * Time: 10:53 AM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\Handler;


use Axtion\Bundle\QuestionnaireBundle\Email\EmailSender;
use Axtion\Bundle\QuestionnaireBundle\Model\Invite;
use Axtion\Bundle\UtilitiesBundle\Form\Handler\BaseHandler;
use Persistent;

/**
 * Class InviteHandler
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Handler
 */
class InviteHandler extends BaseHandler
{
    /**
     * @var EmailSender $emailSender
     */
    private $emailSender;

    /**
     * InviteHandler constructor.
     * @param \Symfony\Component\Form\FormInterface $form
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Axtion\Bundle\QuestionnaireBundle\Email\EmailSender $emailSender
     */
    public function __construct(\Symfony\Component\Form\FormInterface $form, \Symfony\Component\HttpFoundation\Request $request, EmailSender $emailSender)
    {
        parent::__construct($form, $request);
        $this->emailSender = $emailSender;
    }


    /**
     * @param \Persistent | Invite $obj
     */
    protected function onSuccess(Persistent $obj)
    {
        $this->emailSender->send($obj);
    }
}
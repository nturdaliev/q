<?php

namespace Axtion\Bundle\QuestionnaireBundle\Form\Handler;

use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeQuery;
use Axtion\Bundle\UtilitiesBundle\Form\Handler\SaveHandler;
use Persistent;

/**
 * Class QuestionHandler
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Handler
 */
class QuestionHandler extends SaveHandler
{
    /**
     * {@inheritdoc}
     */
    public function process(Persistent $obj/** @var Question $obj */)
    {
        if (!is_null($formData = $this->request->get('question_form')) && array_key_exists('questionType', $formData) && !is_null($questionTypeId = $formData['questionType'])) {
            $obj->setQuestionType(QuestionTypeQuery::create()->findPk($questionTypeId));
        }

        return parent::process($obj);
    }
}
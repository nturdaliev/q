<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 4/20/2016
 * Time: 8:47 AM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\Handler;


use Axtion\Bundle\UtilitiesBundle\Form\Handler\BaseHandler;
use Persistent;

/**
 * Class EmailTemplateHandler
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Handler
 */
class EmailTemplateHandler extends BaseHandler
{

    /**
     * @param \Persistent $obj
     */
    protected function onSuccess(Persistent $obj)
    {
        $obj->save();
    }
}
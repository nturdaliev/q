<?php

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;

use Axtion\Bundle\QuestionnaireBundle\Form\EventListener\AddButtonSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FieldTypeType
 * @package Axtion\Bundle\QuestionnaireBundle\Form
 */
class FieldTypeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
            ->addEventSubscriber(new AddButtonSubscriber());
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            array(
                'data_class'   => 'Axtion\Bundle\QuestionnaireBundle\Propel\FieldType',
                'label_format' => 'field_type.%name%',
            )
        );
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'axtion_questionnaire_field_type_type';
    }
}

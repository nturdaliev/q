<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 9/3/2015
 * Time: 1:26 PM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;


use Axtion\Bundle\QuestionnaireBundle\Form\DataTransformer\IdToQuestionTransformer;
use Axtion\Bundle\QuestionnaireBundle\Propel\Dependency;
use Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionGroup;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionGroupQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Questionnaire;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery;
use Axtion\Bundle\QuestionnaireBundle\Services\AbstractDependencyValue;
use Criteria;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DependencyType
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Type
 */
class DependencyType extends AbstractType
{
    /**
     * @var Questionnaire
     */
    private $questionnaire = null;
    private $container;

    /**
     * @inheritDoc
     */
    function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->setQuestionnaire();
    }

    private function setQuestionnaire()
    {
        $this->questionnaire = $this->container->get('request')->get('questionnaire');
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //Questionnaire is required
        if (!$this->questionnaire) {
            throw new \Exception("You should initialize questionnaire!");
        }

        $query = QuestionGroupQuery::create()->filterByQuestionnaire($this->questionnaire);
        $builder->add(
            'questionGroup',
            'model',
            array(
                'class'    => 'Axtion\Bundle\QuestionnaireBundle\Propel\QuestionGroup',
                'property' => 'name',
                'query'    => $query,
                'mapped'   => false,
                'required' => false,
            )
        )
            ->addEventListener(
                FormEvents::PRE_SUBMIT,
                function (FormEvent $event) use ($builder) {
                    $form = $event->getForm();
                    $data = $event->getData();
                    if (!array_key_exists('questionGroup', $data) || !isset($data['questionGroup']) || sizeof($data['questionGroup']) == 0) {
                        return;
                    }
                    $questionGroup = QuestionGroupQuery::create()->findOneById($data['questionGroup']);
                    if (!$questionGroup instanceof QuestionGroup) {
                        return;
                    }


                    /** @var array $questions */
                    $questions = QuestionQuery::create()
                        ->filterByQuestionGroup($questionGroup)
                        ->useQuestionTypeQuery()
                        ->filterByMayHaveDependency(true)
                        ->endUse()
                        ->orderByName()
                        ->find()
                        ->toKeyValue("Id", "Name");
                    if (sizeof($questions) == 0) {
                        return;
                    }
                    $builder->add(
                        'question',
                        'choice',
                        array(
                            'auto_initialize' => false,
                            'required'        => true,
                            'choices'         => $questions,
                            'expanded'        => true,
                            'multiple'        => false,
                            'mapped'          => true,
                            'choice_label'    => function ($allChoices, $currentChoiceKey) {
                                return strip_tags($currentChoiceKey);
                            },
                            'error_bubbling'  => false,
                        )
                    );
                    $field = $builder->get('question');
                    $field->addModelTransformer(new IdToQuestionTransformer());
                    $form->add($field->getForm());

                    if (!array_key_exists('question', $data) || !isset($data['question']) || $data['question'] == "") {
                        return;
                    }
                    $question = QuestionQuery::create()->findOneById($data['question']);
                    if (!$question instanceof Question) {
                        return;
                    }

                    $fieldTypeName = $question->getQuestionType()->getFieldType()->getName();

                    /** @var AbstractDependencyValue $dependencyValue */
                    $dependencyValue = $this->container->get('axtion__questionnaire.dependency.value.'.ucfirst($fieldTypeName));
                    $dependencyValue->setQuestion($question);

                    $choices = $dependencyValue->getChoices();
                    $form->add(
                        'value',
                        'choice',
                        array(
                            'label'          => '',
                            'expanded'       => true,
                            'multiple'       => false,
                            'required'       => true,
                            'choices'        => $choices,
                            'error_bubbling' => false,
                        )
                    );


                    if (!array_key_exists('value', $data) || !isset($data['value']) || $data['value'] == "") {
                        return;
                    }


                    $allQuestions = QuestionQuery::create()
                        ->filterByQuestionnaire($this->questionnaire)
                        ->filterByQuestionGroup($questionGroup)
                        ->filterById($question->getId(), Criteria::NOT_EQUAL)
                        ->filterByParentId(null)
                        ->orderByName()
                        ->find()
                        ->toKeyValue('Id', 'Name');
                    $dependency = DependencyQuery::create()
                        ->filterByQuestion($question)
                        ->filterByValue($data['value'])
                        ->findOne();
                    $dependentQuestions = null;
                    if ($dependency) {
                        $dependentQuestions = array_keys($dependency->getDependentQuestions()->toKeyValue("Id"));
                    }
                    if (!array_key_exists('submit', $data)) {
                        $data["dependentQuestions"] = array_key_exists("dependentQuestions", $data) ? $data["dependentQuestions"] : $dependentQuestions;
                        $event->setData($data);
                    }
                    $builder->add(
                        'dependentQuestions',
                        'choice',
                        array(
                            'auto_initialize' => false,
                            'multiple'        => true,
                            'expanded'        => true,
                            'by_reference'    => false,
                            'choices'         => $allQuestions,
                            'error_bubbling'  => false,
                        )
                    );
                    $field = $builder->get('dependentQuestions');
                    $field->addModelTransformer(new IdToQuestionTransformer());
                    $form->add($field->getForm());

                    $form->add('submit', 'submit');
                }
            );
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'      => 'Axtion\Bundle\QuestionnaireBundle\Propel\Dependency',
                'csrf_protection' => false,
                'label_format'    => 'dependency.%name%',
            )
        );
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'dependency_form';
    }
}
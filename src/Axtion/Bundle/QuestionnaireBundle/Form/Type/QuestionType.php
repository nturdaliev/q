<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 17-Jun 15
 * Time: 14:12
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;

use Axtion\Bundle\QuestionnaireBundle\Form\EventListener\AddButtonSubscriber;
use Axtion\Bundle\QuestionnaireBundle\Form\EventListener\AddQuestionConfigurationSubscriber;
use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionGroupQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Questionnaire;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeQuery;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class QuestionType
 *
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Type
 */
class QuestionType extends AbstractType
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @inheritDoc
     */
    public function __construct(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'name',
            'ckeditor',
            array(
                'config_name' => 'question',
            )
        )
            ->add(
                'help',
                'ckeditor',
                array(
                    'config_name' => 'question',
                )
            )
            ->add(
                'questionType',
                'grid_question_type',
                array(
                    'class'        => 'Axtion\Bundle\QuestionnaireBundle\Propel\QuestionType',
                    'query'        => QuestionTypeQuery::create()->useI18nQuery('en')->orderByName(Criteria::ASC)->endUse(),
                    'choice_label' => 'name',
                    'placeholder'  => 'question_type.nothing_selected',
                    'required'     => false,
                )
            )
            ->addEventListener(
                FormEvents::POST_SET_DATA,
                function (FormEvent $event) {
                    $question = $event->getData();
                    if (!$question instanceof Question) {
                        return;
                    }
                    $questionnaire = $question->getQuestionnaire();
                    if (!$questionnaire instanceof Questionnaire) {
                        return;
                    }
                    $query = QuestionGroupQuery::create()->filterByQuestionnaire($questionnaire);

                    $form = $event->getForm();
                    $form->add(
                        'question_group',
                        'model',
                        array(
                            'class' => 'Axtion\Bundle\QuestionnaireBundle\Propel\QuestionGroup',
                            'query' => $query,
                        )
                    );
                }
            )
            ->addEventSubscriber(new AddButtonSubscriber())
            ->addEventSubscriber(new AddQuestionConfigurationSubscriber());
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'   => 'Axtion\Bundle\QuestionnaireBundle\Propel\Question',
                'label_format' => 'question.%name%',
            )
        );
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'question_form';
    }
}
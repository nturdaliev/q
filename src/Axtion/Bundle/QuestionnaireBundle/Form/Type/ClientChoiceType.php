<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 4/20/2016
 * Time: 4:59 PM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;


use Axtion\Bundle\QuestionnaireBundle\Form\DataTransformer\ClientTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ClientChoiceType
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Type
 */
class ClientChoiceType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new ClientTransformer());
    }

    /**
     * @inheritDoc
     */
    public function getParent()
    {
        return 'choice';
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'client_choice';
    }
}
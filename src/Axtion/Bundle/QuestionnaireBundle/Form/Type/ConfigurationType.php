<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/24/2015
 * Time: 1:53 PM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ConfigurationType
 * @package Axtion\Bundle\AssessmentBundle\Form\Type
 */
class ConfigurationType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Axtion\Bundle\QuestionnaireBundle\Propel\Configuration',
            )
        );
    }


    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'configuration_key',
                'model',
                array(
                    'class'       => 'Axtion\Bundle\QuestionnaireBundle\Propel\ConfigurationKey',
                    'property' => 'name',
                )
            )
            ->add('value');
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'configuration_form';
    }
}
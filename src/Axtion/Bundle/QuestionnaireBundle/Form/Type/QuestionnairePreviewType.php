<?php

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;

use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionGroupQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Questionnaire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class QuestionnairePreviewType
 *
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Type
 */
class QuestionnairePreviewType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

            /** @var Questionnaire $data */
            $data = $event->getData();
            if (!$data) {
                return;
            }
            $form = $event->getForm();
            $questionGroups = $data->getQuestionGroups(QuestionGroupQuery::create()->orderBySortableRank());
            $form->add(
                'question_groups',
                'collection',
                array(
                    'options' => array('label' => false),
                    'type'    => 'question_group_preview',
                    'data'    => $questionGroups,
                )
            );
        });
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'Axtion\Bundle\QuestionnaireBundle\Propel\Questionnaire'));
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'questionnaire_preview_form';
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 4/21/2016
 * Time: 2:00 PM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;


use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionGroup;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class QuestionGroupPreviewType
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Type
 */
class QuestionGroupPreviewType extends AbstractType
{
    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'Axtion\Bundle\QuestionnaireBundle\Propel\QuestionGroup'));
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var QuestionGroup $data */
            $data = $event->getData();
            if (!$data) {
                return;
            }
            $questions = $data->getQuestions(QuestionQuery::create()->orderBySortableRank());
            $form      = $event->getForm();
            $form->add(
                'questions',
                'collection',
                array(
                    'options' => array('label' => false),
                    'type'    => 'question_preview_form',
                    'data'    => $questions,
                )
            );
        });
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'question_group_preview';
    }
}
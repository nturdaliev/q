<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 4/20/2016
 * Time: 10:50 AM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;


use Axtion\Bundle\QuestionnaireBundle\Form\DataTransformer\EmailTemplateTransformer;
use Axtion\Bundle\QuestionnaireBundle\Model\Invite;
use Axtion\Bundle\QuestionnaireBundle\Propel\EmailTemplateQuery;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class InviteType
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Type
 */
class InviteType extends AbstractType
{
    /**
     * @var Router $router
     */
    private $router;

    /**
     * @inheritDoc
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Axtion\Bundle\QuestionnaireBundle\Model\Invite',
            'attr'       => array(
                'novalidate' => 'novalidate',
            ),
            'label_format'=>'invite.%name%'
        ));
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $emailTemplates = EmailTemplateQuery::create()->filterByActive(true)->find()->toKeyValue('PrimaryKey', 'Name');

        $builder->add('resend', 'checkbox')
                ->add('emailTemplate', 'choice', array('choices' => $emailTemplates))
                ->add('clients', 'client_choice', array('attr' => array('class' => 'hidden'), 'multiple' => true));

        $builder->get('emailTemplate')->addModelTransformer(new EmailTemplateTransformer());

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($builder) {
            $data = $event->getData();
            if (!$data || !array_key_exists('clients', $data)) {
                return;
            }

            $form = $event->getForm();
            $form->add('clients', 'client_choice', array(
                'multiple' => true,
                'choices'  => array_combine(array_values($data['clients']), array_values($data['clients'])),
            ));
        });
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'questionnaire_invite';
    }
}
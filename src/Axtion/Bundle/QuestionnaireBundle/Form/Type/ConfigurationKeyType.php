<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/24/2015
 * Time: 2:39 PM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ConfigurationKeyType
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Type
 */
class ConfigurationKeyType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Axtion\Bundle\QuestionnaireBundle\Propel\ConfigurationKey',
            )
        );
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name');
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'configuration_key_form';
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/12/2015
 * Time: 10:47 AM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class QuestionGroupType
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Type
 */
class QuestionGroupType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name');
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'   => 'Axtion\Bundle\QuestionnaireBundle\Propel\QuestionGroup',
                'label_format' => 'question_group.%name%',
            )
        );
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'question_group_form';
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 10/6/2015
 * Time: 11:48 AM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class GoodBadType
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Type
 */
class GoodBadType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'left_label'  => '',
                'right_label' => '',
            )
        );
    }

    /**
     * @inheritDoc
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['left_label'] = $options['left_label'];
        $view->vars['right_label'] = $options['right_label'];
        $view->vars['attr']['class'] = 'good-bad-slider';
        parent::finishView($view, $form, $options);
    }
    /**
     * @inheritDoc
     */
    public function getParent()
    {
        return 'slider';
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'good_bad';
    }
}
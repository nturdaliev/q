<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/24/2015
 * Time: 1:50 PM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DeviceType
 * @package Axtion\Bundle\AssessmentBundle\Form\Type
 */
class DeviceType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Axtion\Bundle\QuestionnaireBundle\Propel\Device',
            )
        );
    }


    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('code')
            ->add(
                'configurations',
                'collection',
                array(
                    'type'         => 'configuration_form',
                    'by_reference' => false,
                    'allow_add'    => true,
                    'allow_delete' => true,
                )
            );
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'device_form';
    }
}
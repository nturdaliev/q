<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 7/2/2015
 * Time: 10:29 AM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;

use Axtion\Bundle\QuestionnaireBundle\Form\EventListener\AddOptionsSubscriber;
use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class QuestionPreviewType
 *
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Type
 */
class QuestionPreviewType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventSubscriber(new AddOptionsSubscriber());
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'      => 'Axtion\Bundle\QuestionnaireBundle\Propel\Question',
                'label_format'    => 'question.%name%',
                'csrf_protection' => false,
            )
        );
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'question_preview_form';
    }
}
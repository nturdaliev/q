<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 7/3/2015
 * Time: 8:57 AM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OptionType
 *
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Type
 */
class FieldOptionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('field_type', 'model', array('class' => 'Axtion\Bundle\QuestionnaireBundle\Propel\FieldType'))
            ->add('name')
            ->add(
                'field_option_tab',
                'model',
                array(
                    'class'        => 'Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionTab',
                    'choice_label' => 'name',
                )
            )
            ->add('submit', 'submit', array('label' => 'button.submit'));
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'        => 'Axtion\Bundle\QuestionnaireBundle\Propel\FieldOption',
                'validation_groups' => array('form'),
                'label_format'      => 'field_option.%name%',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'field_option_form';
    }
}
<?php

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;

use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class GridType
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Type
 */
class GridType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'form';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'grid';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event) {
                $form = $event->getForm();

                /** @var Question[] $subQuestions */
                $subQuestions = $form->getData();

                /** @var \PropelObjectCollection|Question[] $subQuestions */
                if (!($subQuestions instanceof \PropelObjectCollection)) {
                    return;
                }

                $options = $subQuestions->getFirst()->getParent()->createOptions();
                /** @var Question $sub * */
                foreach ($subQuestions->toArray() as $index => $sub) {
                    $child = ($sub['Id']) ? $sub['Id'] : 'dummy'.$index;
                    $form->add($child, 'choice', array('label' => $sub['Name'], 'choices' => $options['choices'], 'expanded' => true));
                }
            }

        );
    }
}
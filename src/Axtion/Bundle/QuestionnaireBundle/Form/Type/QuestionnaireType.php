<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 17-Jun 15
 * Time: 11:47
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;

use Axtion\Bundle\QuestionnaireBundle\Form\EventListener\AddButtonSubscriber;
use Axtion\Bundle\QuestionnaireBundle\Propel\Questionnaire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class QuestionnaireType
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Type
 */
class QuestionnaireType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('status')
            ->add('theme', null, array('label' => 'theme.theme', 'required' => true))
            ->add('copyright')
            ->add('welcome', 'ckeditor')
            ->add('goodbye', 'ckeditor')
            ->add(
                'questionGroups',
                'collection',
                array(
                    'by_reference' => false,
                    'type'         => new QuestionGroupType(),
                    'allow_add'    => true,
                    'allow_delete' => true,
                )
            )
            ->addEventSubscriber(new AddButtonSubscriber());
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'   => 'Axtion\Bundle\QuestionnaireBundle\Propel\Questionnaire',
                'label_format' => 'questionnaire.%name%',
            )
        );
    }


    /**
     *�{@inheritdoc}
     */
    public function getName()
    {
        return 'questionnaire_form';
    }


}
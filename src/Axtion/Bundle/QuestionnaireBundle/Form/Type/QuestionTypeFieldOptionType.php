<?php

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;

use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionQuery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class QuestionTypeFieldOptionType
 * @package Axtion\Bundle\QuestionnaireBundle\Form
 */
class QuestionTypeFieldOptionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'field_option',
            'model',
            array(
                'class' => 'Axtion\Bundle\QuestionnaireBundle\Propel\FieldOption',
                'query' => FieldOptionQuery::create()->orderByName(\Criteria::ASC),
            )
        )
            ->add('form_options', null, array('required' => false))
            ->add('hidden', 'checkbox', array('required' => false,'label'=>'field_option.hidden'))
            ->add('default_value')
            ->add(
                'array_level',
                'choice',
                array(
                    'choices'  => array(
                        'attr'          => 'attr',
                        'pickerOptions' => 'pickerOptions',
                        'gridOptions'   => 'gridOptions',
                    ),
                    'required' => false,
                )
            );

    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeFieldOption',
                'label_format'=>'field_option.%name%'
            )
        );

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'axtion_questionnaire_question_type_field_option_type';
    }
}

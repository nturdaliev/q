<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 23-Jun 15
 * Time: 11:55
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;

use Axtion\Bundle\QuestionnaireBundle\Form\EventListener\AddButtonSubscriber;
use Axtion\Bundle\QuestionnaireBundle\Propel\Status;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class StatusType
 *
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Type
 */
class StatusType extends AbstractType
{
    /**
     * @var array Locales
     */
    protected $locales;

    /**
     * Constructor
     *
     * @param array $locales Locales
     */
    public function __construct(array $locales)
    {
        $this->locales = $locales;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('statusI18ns', 'translation_collection', array(
                'label'     => false,
                'languages' => $this->locales,
                'options'   => array(
                    'data_class'   => 'Axtion\Bundle\QuestionnaireBundle\Propel\StatusI18n',
                    'label'        => false,
                    'columns'      => array(
                        'name'        => array(
                            'label' => 'status.name',
                        ),
                    )
                )
            ))
            ->addEventSubscriber(new AddButtonSubscriber());
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'        => 'Axtion\Bundle\QuestionnaireBundle\Propel\Status',
                'validation_groups' => array('form'),
                'label_format'      => 'status.%name%',
            )
        );
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'status_form';
    }
}
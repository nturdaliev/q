<?php

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

/**
 * Class GridQuestionTypeType
 * @package Axtion\Bundle\QuestionnaireBundle\Form
 */
class GridQuestionTypeType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function getParent()
    {
        return 'model';
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'grid_question_type';
    }
}

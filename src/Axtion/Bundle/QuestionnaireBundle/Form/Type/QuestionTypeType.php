<?php

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class QuestionTypeType
 *
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Type
 */
class QuestionTypeType extends AbstractType
{
    /**
     * @var array Locales
     */
    protected $locales;

    /**
     * Constructor
     *
     * @param array $locales Locales
     */
    public function __construct(array $locales)
    {
        $this->locales = $locales;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('field_type', 'model', array('class' => 'Axtion\Bundle\QuestionnaireBundle\Propel\FieldType'))
            ->add(
                'questionTypeI18ns',
                'translation_collection',
                array(
                    'label'     => false,
                    'languages' => $this->locales,
                    'options'   => array(
                        'data_class' => 'Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeI18n',
                        'label'      => false,
                        'columns'    => array(
                            'name'        => array(
                                'label' => 'question_type.name',
                            ),
                            'description' => array(
                                'label' => 'question_type.description',
                            ),
                        ),
                    ),
                )
            )
            ->add('icon', 'text')
            ->add('hasSubQuestions', null, array('required' => false))
            ->add('may_have_dependency', null, array('required' => false))
            ->add(
                'question_type_field_options',
                'collection',
                array(
                    'type'         => new QuestionTypeFieldOptionType(),
                    'allow_add'    => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                )
            )
            ->add('submit', 'submit', array('label' => 'button.submit'));
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'   => 'Axtion\Bundle\QuestionnaireBundle\Propel\QuestionType',
                'label_format' => 'question_type.%name%',
            )
        );
    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'question_type_form';
    }


}

<?php

namespace Axtion\Bundle\QuestionnaireBundle\Form\Type;

use Axtion\Bundle\QuestionnaireBundle\Form\EventListener\AddButtonSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class EmailTemplateType
 * @package Axtion\Bundle\QuestionnaireBundle\Form\Type
 */
class EmailTemplateType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text')
                ->add('content', 'ckeditor', array(
                    'config_name' => 'email_template',
                ))
                ->add('active')
                ->addEventSubscriber(new AddButtonSubscriber());;
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Axtion\Bundle\QuestionnaireBundle\Propel\EmailTemplate',
            'attr'       => array('novalidate' => true),
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'questionnaire_email_template';
    }
}

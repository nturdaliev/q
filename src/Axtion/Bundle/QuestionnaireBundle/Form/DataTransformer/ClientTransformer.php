<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 9/3/2015
 * Time: 2:36 PM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\DataTransformer;


use Axtion\Bundle\AssessmentBundle\Propel\ClientQuery;
use PropelCollection;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class IdToQuestionTransformer
 * @package Axtion\Bundle\QuestionnaireBundle\Form\DataTransformer
 */
class ClientTransformer implements DataTransformerInterface
{

    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        if (is_null($value) || is_array($value)) {
            return $value;
        }
        $result = $value->toKeyValue('Id', 'Name');

        return $result;
    }

    /**
     * @param mixed $value
     * @return null
     */
    public function reverseTransform($value)
    {
        if (is_null($value)) {
            return null;
        }
        elseif (is_array($value)) {
            if (sizeof($value) > 0) {
                return ClientQuery::create()->findPks($value);
            }
            else {
                return new PropelCollection();
            }
        }
        $emailTemplate = ClientQuery::create()->findPk($value);

        return $emailTemplate;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 7/9/2015
 * Time: 9:02 AM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\EventListener;


use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeFieldOptionQuery;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddQuestionConfigurationSubscriber
 * @package Axtion\Bundle\QuestionnaireBundle\Form\EventListener
 */
class AddQuestionConfigurationSubscriber implements EventSubscriberInterface
{

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2'))
     *
     * @return array The event names to listen to
     *
     * @api
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {

        $form = $event->getForm();

        /** @var Question $question */
        $question = $event->getData();

        if (!$question instanceof Question) {
            return;
        }

        if (is_null($questionType = $question->getQuestionType())) {
            return;
        }

        $query = QuestionTypeFieldOptionQuery::create()->filterByHidden(false);

        foreach ($questionType->getQuestionTypeFieldOptions($query) as $option) {
            $options = array('required' => false, 'label' => 'option.'.$option->getFieldOption()->getName());

            if (!is_null($option->getFormOptions())) {
                $options = array_merge_recursive($options, json_decode($option->getFormOptions(), true));
            }

            $form->add('option'.$option->getFieldOption()->getName(), $option->getFieldOption()->getFieldType()->getName(), $options);
        }

        if ($questionType->getHasSubquestions()) {
            $form->add(
                'subQuestions',
                'collection',
                array(
                    'options'      => array('label' => false),
                    'type'         => 'sub_question_form',
                    'allow_add'    => true,
                    'allow_delete' => true,
                    'prototype'    => true,
                    'by_reference' => false,
                )
            );
        }
    }
}
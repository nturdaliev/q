<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 7/9/2015
 * Time: 10:53 AM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\EventListener;


use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddOptionsSubscriber
 *
 * @package Axtion\Bundle\QuestionnaireBundle\Form\EventListener
 */
class AddOptionsSubscriber implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();

        /** @var Question $question */
        $question = $event->getData();

        if ($question instanceof Question && $question->getQuestionType() instanceof QuestionType) {
            $questionType = $question->getQuestionType();

            if ($questionType->getHasSubquestions()) {
                $form->add('subQuestions', $questionType->getFieldType()->getName(),array('label'=>$question->getName(),'help'=>$question->getHelp()));
                return;
            }

            $form->add('dummy', $questionType->getFieldType()->getName(), $question->createOptions());
        }
    }
}
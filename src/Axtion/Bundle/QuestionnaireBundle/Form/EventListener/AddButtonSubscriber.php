<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 7/9/2015
 * Time: 9:39 AM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Form\EventListener;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddButtonSubscriber
 * @package Axtion\Bundle\QuestionnaireBundle\Form\EventListener
 */
class AddButtonSubscriber implements EventSubscriberInterface
{

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::POST_SET_DATA => 'postSetData');
    }

    /**
     * @param FormEvent $event
     */
    public function postSetData(FormEvent $event)
    {

        $form = $event->getForm();

        $model = $form->getData();
        if (is_null($model)) {
            return;
        }

        $options = array('label_format' => 'button.%name%', 'attr' => array('class' => 'btn-primary'));

        if ($model->isNew()) {
            $form->add('create_add_new', 'submit', $options);
            $options['attr']['class'] = 'btn-primary';
            $form->add('create_return_list', 'submit', $options);
        } else {
            $form->add('id', 'hidden');
            $form->add('save_return_list', 'submit', $options);
        }
    }


}

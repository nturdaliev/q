<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 4/20/2016
 * Time: 5:04 PM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Email;

use Axtion\Bundle\AssessmentBundle\Propel\Assessment;
use Axtion\Bundle\AssessmentBundle\Propel\Client;
use Axtion\Bundle\AssessmentBundle\Propel\ClientQuery;
use Axtion\Bundle\AssessmentBundle\Propel\Status;
use Axtion\Bundle\AssessmentBundle\Propel\StatusQuery;
use Axtion\Bundle\QuestionnaireBundle\Model\Invite;
use Axtion\Bundle\QuestionnaireBundle\Propel\Questionnaire;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;


/**
 * Class EmailSender
 * @package Axtion\Bundle\QuestionnaireBundle\Email
 */
class EmailSender
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var \Twig_Environment
     */
    private $twigEnvironment;

    /**
     * @var array
     */
    private $invitedClientArray;

    /**
     * @var string
     */
    private $from;
    /**
     * @var \Axtion\Bundle\AssessmentBundle\Propel\Status
     */
    private $status;
    /**
     * @var Questionnaire
     */
    private $questionnaire;
    /**
     * @var \Twig_Template
     */
    private $template;
    /**
     * @var \Symfony\Component\Routing\Router
     */
    private $router;
    /**
     * @var array
     */
    private $extraParams;

    /**
     * @inheritDoc
     */
    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twigEnvironment, Router $router, $from)
    {
        $this->mailer             = $mailer;
        $this->twigEnvironment    = $twigEnvironment;
        $this->invitedClientArray = [];
        $this->status             = StatusQuery::create()->findOneBySlug(Status::OPEN);
        $this->from               = $from;
        $this->router             = $router;
        $this->initExtraParams();
    }

    private function initExtraParams()
    {
        $this->extraParams['loginLink'] = $this->router->generate('assessment_security_login', array(), UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /**
     * @param $obj Invite
     */
    public function send($obj)
    {
        $this->config($obj);
        $invitedClients = $this->getInvitedClients($obj);

        $questionnaire = $obj->getQuestionnaire();
        /** @var Client $client */
        foreach ($obj->getClients() as $client) {
            if ($this->sendInvitation($client)) {
                $this->createAssessment($client, $this->questionnaire);
            }
        }
        if ($obj->isResend() && $invitedClients) {
            foreach ($invitedClients as $invitedClient) {
                $this->sendInvitation($invitedClient);
            }
        }
    }

    /**
     * @param \Axtion\Bundle\QuestionnaireBundle\Model\Invite $invite
     * @throws \Exception
     */
    private function config(Invite $invite)
    {
        $this->questionnaire = $invite->getQuestionnaire();
        $this->template      = $this->twigEnvironment->createTemplate($invite->getEmailTemplate()->getContent());
    }

    /**
     * @param \Axtion\Bundle\QuestionnaireBundle\Model\Invite $invite
     * @return array|mixed|null|\PropelObjectCollection
     * @throws \PropelException
     */
    private function getInvitedClients(Invite $invite)
    {
        $assessments = $invite->getQuestionnaire()->getAssessments();
        return ClientQuery::create()->filterByAssessment($assessments)->find();
    }

    /**
     * @param $client Client
     * @return bool
     * @throws \Exception
     */
    private function sendInvitation(Client $client)
    {
        if ($this->isNotInvited($client)) {

            $body    = $this->template->render(array_merge(array('client' => $client), $this->extraParams));
            $message = $this->createMessage($client, $body);

            if ($this->mailer->send($message)) {
                $this->invitedClientArray[] = $client->getId();
                return true;
            }
        }
        return false;
    }

    /**
     * @param $client Client
     * @return mixed
     */
    private function isNotInvited(Client $client)
    {
        return !in_array($client->getId(), $this->invitedClientArray);
    }

    /**
     * @param \Axtion\Bundle\AssessmentBundle\Propel\Client $client
     * @param $body
     * @return \Swift_Message
     */
    private function createMessage(Client $client, $body)
    {
        $message = \Swift_Message::newInstance();
        $message->setFrom($this->from);
        $message->setTo($client->getEmail());
        $message->setBody($body, 'text/html');
        return $message;
    }

    /**
     * @param \Axtion\Bundle\AssessmentBundle\Propel\Client $client
     * @param \Axtion\Bundle\QuestionnaireBundle\Propel\Questionnaire $questionnaire
     * @throws \Exception
     * @throws \PropelException
     * @internal param \Axtion\Bundle\AssessmentBundle\Propel\Status $status
     */
    private function createAssessment(Client $client, Questionnaire $questionnaire)
    {
        $assessment = new Assessment();
        $assessment->setClient($client)
                   ->setQuestionnaire($questionnaire)
                   ->setStatus($this->status);
        $assessment->save();
    }
}
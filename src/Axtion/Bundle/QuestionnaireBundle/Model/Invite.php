<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 4/20/2016
 * Time: 10:48 AM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Model;

use Axtion\Bundle\AssessmentBundle\Propel\Client;
use Axtion\Bundle\QuestionnaireBundle\Propel\EmailTemplate;
use Axtion\Bundle\QuestionnaireBundle\Propel\Questionnaire;
use Exception;
use ObjectKey;
use Persistent;
use PropelPDO;


/**
 * Class Invite
 * @package Axtion\Bundle\QuestionnaireBundle\Model
 */
class Invite implements Persistent
{
    private $id;
    /**
     * @var bool $resend
     */
    private $resend;

    /**
     * @var EmailTemplate $emailTemplate
     */
    private $emailTemplate;

    /**
     * @var Questionnaire $questionnaire
     */
    private $questionnaire;
    
    /** @var  Client[] */
    private $clients;
    
    /**
     * @return EmailTemplate
     */
    public function getEmailTemplate()
    {
        return $this->emailTemplate;
    }

    /**
     * @param EmailTemplate $emailTemplate
     * @return Invite
     */
    public function setEmailTemplate($emailTemplate)
    {
        $this->emailTemplate = $emailTemplate;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isResend()
    {
        return $this->resend;
    }

    /**
     * @param boolean $resend
     * @return Invite
     */
    public function setResend($resend)
    {
        $this->resend = $resend;
        return $this;
    }

    /**
     * getter for the object primaryKey.
     *
     * @return int the object primaryKey as an Object
     */
    public function getPrimaryKey()
    {
        return $this->id;
    }

    /**
     * Sets the PrimaryKey for the object.
     *
     * @param mixed $primaryKey The new PrimaryKey object or string (result of PrimaryKey.toString()).
     *
     * @return void
     * @throws Exception, This method might throw an exceptions
     */
    public function setPrimaryKey($primaryKey)
    {
        // TODO: Implement setPrimaryKey() method.
    }

    /**
     * Returns whether the object has been modified, since it was
     * last retrieved from storage.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        // TODO: Implement isModified() method.
    }

    /**
     * Has specified column been modified?
     *
     * @param string $col
     *
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        // TODO: Implement isColumnModified() method.
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean True, if the object has never been persisted.
     */
    public function isNew()
    {
        // TODO: Implement isNew() method.
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and Peers.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        // TODO: Implement setNew() method.
    }

    /**
     * Resets (to false) the "modified" state for this object.
     *
     * @return void
     */
    public function resetModified()
    {
        // TODO: Implement resetModified() method.
    }

    /**
     * Whether this object has been deleted.
     *
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        // TODO: Implement isDeleted() method.
    }

    /**
     * Specify whether this object has been deleted.
     *
     * @param boolean $b The deleted state of this object.
     *
     * @return void
     */
    public function setDeleted($b)
    {
        // TODO: Implement setDeleted() method.
    }

    /**
     * Deletes the object.
     *
     * @param PropelPDO $con
     *
     * @return void
     * @throws Exception
     */
    public function delete(PropelPDO $con = null)
    {
        // TODO: Implement delete() method.
    }

    /**
     * Saves the object.
     *
     * @param PropelPDO $con
     *
     * @return void
     * @throws Exception
     */
    public function save(PropelPDO $con = null)
    {
        // TODO: Implement save() method.
    }

    /**
     * @param Questionnaire $questionnaire
     * @return Invite
     */
    public function setQuestionnaire($questionnaire)
    {
        $this->questionnaire = $questionnaire;
        return $this;
    }

    /**
     * @return Questionnaire
     */
    public function getQuestionnaire()
    {
        return $this->questionnaire;
    }

    /**
     * @param \Axtion\Bundle\AssessmentBundle\Propel\Client[] $clients
     * @return Invite
     */
    public function setClients($clients)
    {
        $this->clients = $clients;
        return $this;
    }

    /**
     * @return \Axtion\Bundle\AssessmentBundle\Propel\Client[]
     */
    public function getClients()
    {
        return $this->clients;
    }
}
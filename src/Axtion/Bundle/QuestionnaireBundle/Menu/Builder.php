<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 25/06/2015
 * Time: 11:42
 */

namespace Axtion\Bundle\QuestionnaireBundle\Menu;

use Axtion\Bundle\AssessmentBundle\Propel\Client;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionnaireQuery;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Translation\DataCollectorTranslator;

/**
 * Class Builder
 * @package Axtion\Bundle\QuestionnaireBundle\Menu
 */
class Builder extends ContainerAware
{
    /**
     * @var DataCollectorTranslator
     */
    protected $translator;

    /**
     * @var \Knp\Menu\ItemInterface;
     */
    protected $menu;

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return ItemInterface
     */
    public function breadcrumb(FactoryInterface $factory, array $options)
    {
        $this->menu       = $factory->createItem(
            'root',
            array(
                'childrenAttributes' => array(
                    'class' => 'breadcrumb',
                ),
            )
        );
        $this->translator = $this->container->get('translator');

        $request = $this->container->get('request');
        $route   = $request->get('_route');
        $this->configure($route);

        return $this->menu;
    }

    /**
     * @param $route
     */
    public function configure($route)
    {
        $this->addHome();
        switch ($route) {
            case 'questionnaire_questionnaire_index':
                $this->addQuestionnaires('');
                break;
            case 'questionnaire_questionnaire_new':
                $this->addQuestionnaires();
                $this->addQuestionnaireNew();
                break;
            case 'questionnaire_questionnaire_edit':
                $this->addQuestionnaires();
                $this->addQuestionnaireEdit();
                break;
            case 'questionnaire_questionnaire_show':
                $this->addQuestionnaires();
                $this->addQuestionnaireShow(true);
                break;
            case 'questionnaire_questionnaire_preview':
                $this->addQuestionnaires();
                $this->addQuestionnairePreview();
                break;
            case 'questionnaire_question_edit':
                $this->addQuestionnaires();
                $this->addQuestionnaireShow(false);
                $this->addQuestionEdit();
                break;
            case 'questionnaire_question_new':
                $this->addQuestionnaires();
                $this->addQuestionnaireShow(false);
                $this->addQuestionNew();
                break;
            case 'questionnaire_question_type_index':
                $this->addQuestionTypes('');
                break;
            case 'questionnaire_question_type_edit':
                $this->addQuestionTypes();
                $this->addQuestionTypeEdit();
                break;
            case 'questionnaire_question_type_new':
                $this->addQuestionTypes();
                $this->addQuestionTypeNew();
                break;
            case 'questionnaire_status_index':
                $this->addStatuses('');
                break;
            case 'questionnaire_status_new':
                $this->addStatuses();
                $this->addStatusNew();
                break;
            case 'questionnaire_status_edit':
                $this->addStatuses();
                $this->addStatusEdit();
                break;
            case 'questionnaire_question_index':
                $this->addQuestionIndex('');
                break;
            case 'questionnaire_question_show':
                $this->addQuestionIndex();
                $this->addQuestionShow();
                break;
            case 'assessment_client_index':
                $this->addClientIndex('');
                break;
            case 'assessment_client_new':
                $this->addClientIndex();
                $this->addClientNew();
                break;
            case 'assessment_client_edit':
                $this->addClientIndex();
                $this->addClientEdit();
                break;
            case 'questionnaire_questionnaire_manage_clients':
                $this->addQuestionnaires();
                $this->addQuestionnaireManageClients();
                break;
            case 'questionnaire_patient_index':
                $this->addQuestionnaires('');
                break;
            case 'assessment_assessment_fill':
                $this->addQuestionnaireFill();
                break;
            case 'assessment_assessment_show':
                $this->clientQuestionnaireShow();
                break;
            case 'questionnaire_questionnaire_assessments':
                $this->addQuestionnaires();
                $this->addAssessmentsIndex();
                break;
            case 'assessment_response_index':
                $this->addQuestionnaires();
                $this->addResponseIndex();
                break;
            case 'questionnaire_email_template_index':
                $this->addEmailTemplateIndex();
                break;
            case 'questionnaire_email_template_edit':
                $this->addEmailTemplateEdit();
                break;
            case 'questionnaire_email_template_new':
                $this->addEmailTemplateNew();
                break;
            default:
                break;
        }
    }

    /**
     * @param string $route
     * @return ItemInterface
     */
    public function addHome()
    {
        $route = 'frontend_home_dashboard';
        if ($this->getUser() instanceof Client) {
            $route = 'assessment_assessment_index';
        }

        return $this->menu->addChild(
            $this->translator->trans('home'),
            array(
                'route' => $route,
            )
        );
    }

    /**
     * @return mixed|void
     */
    public function getUser()
    {
        if (!$this->container->has('security.token_storage')) {
            throw new \LogicException('The SecurityBundle is not registered in your application.');
        }

        if (null === $token = $this->container->get('security.token_storage')->getToken()) {
            return;
        }

        if (!is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return;
        }

        return $user;
    }

    /**
     * @param string $route
     * @return ItemInterface
     */
    private function addQuestionnaires($route = 'questionnaire_questionnaire_index')
    {
        return $this->menu->addChild(
            $this->translator->trans('questionnaire.questionnaires'),
            array(
                'route' => $route,
            )
        );
    }

    /**
     * @return ItemInterface
     */
    private function addQuestionnaireNew()
    {
        return $this->menu->addChild($this->translator->trans('questionnaire.new'));
    }

    /**
     * @return ItemInterface
     */
    private function addQuestionnaireEdit()
    {
        return $this->menu->addChild($this->translator->trans('questionnaire.edit'));
    }

    /**
     * @param $disabled
     * @return ItemInterface
     */
    private function addQuestionnaireShow($disabled)
    {
        if ($disabled) {
            return $this->menu->addChild($this->translator->trans('questionnaire.questionnaire'));
        }
        else {
            $id            = $this->container->get('request')->get('id');
            $questionnaire = QuestionnaireQuery::create()->findOneById($id);
            if (is_null($questionnaire)) {
                return $this->menu;
            }

            return $this->menu->addChild(
                $this->translator->trans('questionnaire.show'),
                array(
                    'route'           => 'questionnaire_questionnaire_show',
                    'routeParameters' => array('id' => $questionnaire->getId()),
                )
            );
        }
    }

    /**
     * @return ItemInterface
     */
    private function addQuestionnairePreview()
    {
        return $this->menu->addChild($this->translator->trans('questionnaire.preview'));
    }

    /**
     * @return ItemInterface
     */
    private function addQuestionEdit()
    {
        return $this->menu->addChild($this->translator->trans('question.edit'));
    }

    /**
     * @return ItemInterface
     */
    private function addQuestionNew()
    {
        return $this->menu->addChild($this->translator->trans('question.new'));
    }

    /**
     * @param string $route
     * @return ItemInterface
     */
    private function addQuestionTypes($route = 'questionnaire_question_type_index')
    {
        return $this->menu->addChild(
            $this->translator->trans('question_type.question_types'),
            array(
                'route' => $route,
            )
        );
    }

    /**
     * @return ItemInterface
     */
    private function addQuestionTypeEdit()
    {
        return $this->menu->addChild($this->translator->trans('question_type.question_type'));
    }

    /**
     * @return ItemInterface
     */
    private function addQuestionTypeNew()
    {
        return $this->menu->addChild($this->translator->trans('question_type.new'));
    }

    /**
     * @param string $route
     * @return ItemInterface
     */
    private function addStatuses($route = 'questionnaire_status_index')
    {
        return $this->menu->addChild(
            $this->translator->trans('status.statuses'),
            array(
                'route' => $route,
            )
        );
    }

    /**
     * @return ItemInterface
     */
    private function addStatusNew()
    {
        return $this->menu->addChild($this->translator->trans('status.new'));
    }

    /**
     * @return ItemInterface
     */
    private function addStatusEdit()
    {
        return $this->menu->addChild($this->translator->trans('status.edit'));
    }

    /**
     * @param string $route
     * @return ItemInterface
     */
    private function addQuestionIndex($route = 'questionnaire_question_index')
    {
        return $this->menu->addChild(
            $this->translator->trans('question.questions'),
            array(
                'route' => $route,
            )
        );
    }

    /**
     * @return ItemInterface
     */
    private function addQuestionShow()
    {
        return $this->menu->addChild($this->translator->trans('question.show'));
    }

    /**
     * @param string $route
     * @return ItemInterface
     */
    private function addClientIndex($route = 'assessment_client_index')
    {
        return $this->menu->addChild(
            $this->translator->trans('client.clients'),
            array(
                'route' => $route,
            )
        );
    }

    /**
     * @return ItemInterface
     */
    private function addClientNew()
    {
        return $this->menu->addChild(
            $this->translator->trans('client.new')
        );
    }

    /**
     * @return ItemInterface
     */
    private function addClientEdit()
    {
        return $this->menu->addChild(
            $this->translator->trans('client.edit')
        );
    }

    /**
     * @return ItemInterface
     */
    private function addQuestionnaireManageClients()
    {
        return $this->menu->addChild(
            $this->translator->trans('questionnaire.manage_clients')
        );
    }

    /**
     * @return ItemInterface
     */
    private function addQuestionnaireFill()
    {
        return $this->menu->addChild(
            $this->translator->trans('client.questionnaire.fill')
        );
    }

    /**
     * @return ItemInterface
     */
    private function clientQuestionnaireShow()
    {
        return $this->menu->addChild(
            $this->translator->trans('questionnaire.show')
        );
    }

    /**
     * @return \Knp\Menu\ItemInterface
     */
    private function addAssessmentsIndex()
    {
        return $this->menu->addChild(
            $this->translator->trans('assessment.assessments')
        );
    }

    /**
     * @return \Knp\Menu\ItemInterface
     */
    private function addResponseIndex()
    {
        return $this->menu->addChild(
            $this->translator->trans('response.responses')
        );
    }

    /**
     * @return \Knp\Menu\ItemInterface
     */
    private function addEmailTemplateIndex()
    {
        return $this->menu->addChild(
            $this->translator->trans('emailTemplate.emailTemplates')
        );
    }

    /**
     * @return \Knp\Menu\ItemInterface
     */
    private function addEmailTemplateEdit()
    {
        $this->menu->addChild(
            $this->translator->trans('emailTemplate.emailTemplates'),
            array('route' => 'questionnaire_email_template_index')
        );

        return $this->menu->addChild(
            $this->translator->trans('emailTemplate.edit')
        );

    }

    /**
     * @return \Knp\Menu\ItemInterface
     */
    private function addEmailTemplateNew()
    {
        $this->menu->addChild(
            $this->translator->trans('emailTemplate.emailTemplates'),
            array('route' => 'questionnaire_email_template_index')
        );

        return $this->menu->addChild(
            $this->translator->trans('emailTemplate.new')
        );

    }
}
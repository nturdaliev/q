<?php

namespace Axtion\Bundle\QuestionnaireBundle\Event;

use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Class LocaleListener
 * @package Axtion\Bundle\QuestionnaireBundle\Event
 */
class LocaleListener
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param GenericEvent $event Event
     */
    public function postHydrate(GenericEvent $event)
    {
        /** @var QuestionType $questionType */
        $questionType = $event->getSubject();

        if (!($questionType instanceof \Persistent) || !method_exists($questionType, 'setLocale')) {
            return;
        }

        $questionType->setLocale($this->container->getParameter('locale'));
    }
}
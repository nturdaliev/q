<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 7/3/2015
 * Time: 8:36 AM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Controller;

use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOption;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class FieldOptionController
 *
 * @package Axtion\Bundle\QuestionnaireBundle\Controller
 *
 * @Route("/panel/field_option")
 * @Security("has_role('ROLE_DEVELOPER')")
 */
class FieldOptionController extends Controller
{

    /**
     * @Route("/", name="questionnaire_field_option_index")
     * @Template()
     */
    public function indexAction()
    {
        return array('options' => FieldOptionQuery::create()->find());
    }

    /**
     * @Route("/new", name="questionnaire_field_option_new")
     * @Template()
     */
    public function newAction()
    {
        $option = new FieldOption();
        $handler = $this->get('axtion_questionnaire.form.handler.field_option');
        if ($handler->process($option)) {
            $this->addFlash('success', $this->get('translator')->trans('field_option.success.new', array('%field_option%' => $option->getName())));

            return $this->redirectToRoute('questionnaire_field_option_index');
        }

        return array('form' => $handler->getForm()->createView());
    }

    /**
     * @Route("/{id}/edit", name="questionnaire_field_option_edit")
     * @ParamConverter("option", options={""})
     * @Template()
     * @param FieldOption $option
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(FieldOption $option)
    {
        $handler = $this->get('axtion_questionnaire.form.handler.field_option');

        if ($handler->process($option)) {
            $this->addFlash('success', $this->get('translator')->trans('field_option.success.edit', array('%field_option%' => $option->getName())));

            return $this->redirectToRoute('questionnaire_field_option_index');
        }

        return array('form' => $handler->getForm()->createView());

    }

    /**
     * @Route("/{id}/delete", name="questionnaire_field_option_delete")
     * @ParamConverter("option", options={""})
     * @param FieldOption $option
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(FieldOption $option)
    {
        try {
            $option->delete();
            $this->addFlash('success', $this->get('translator')->trans('field_option.success.delete', array('%field_option%' => $option->getName())));
        } catch (\Exception $e) {
            $this->addFlash('danger', $this->get('translator')->trans('field_option.failure.delete', array('%field_option%' => $option->getName())));
        }

        return $this->redirectToRoute('questionnaire_field_option_index');
    }
}
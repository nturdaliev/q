<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 17-Jun 15
 * Time: 11:31
 */

namespace Axtion\Bundle\QuestionnaireBundle\Controller;

use Axtion\Bundle\AssessmentBundle\Form\Type\ClientFilterType;
use Axtion\Bundle\AssessmentBundle\Propel\Assessment;
use Axtion\Bundle\AssessmentBundle\Propel\AssessmentQuery;
use Axtion\Bundle\AssessmentBundle\Propel\Client;
use Axtion\Bundle\AssessmentBundle\Propel\ClientPeer;
use Axtion\Bundle\AssessmentBundle\Propel\ClientQuery;
use Axtion\Bundle\AssessmentBundle\Propel\Status;
use Axtion\Bundle\AssessmentBundle\Propel\StatusQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Questionnaire;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionnaireQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery;
use Axtion\Bundle\UserBundle\Propel\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class QuestionnaireController
 *
 * @package Axtion\Bundle\QuestionnaireBundle\Controller
 * @Route("/panel/questionnaire")
 * @Security("has_role('ROLE_USER')")
 */
class QuestionnaireController extends Controller
{
    /**
     * @Route("/", name="questionnaire_questionnaire_index")
     * @Template
     *
     * @return array Template variables
     */
    public function indexAction()
    {
        /** @var QuestionnaireQuery $query */
        $query = QuestionnaireQuery::create()->filterForGrid($this->container->getParameter('locale'));

        if (!$this->isGranted('ROLE_ADMIN')) {
            $query = $query->filterByUser($this->getUser());
        }

        $paginator      = $this->get('knp_paginator');
        $page           = $this->get('request')->query->getInt('page', 1);
        $questionnaires = $paginator->paginate($query, $page);

        return array('questionnaires' => $questionnaires);
    }

    /**
     * @Route("/{id}/show/", name="questionnaire_questionnaire_show", requirements={"id"="\d+"})
     * @Security("has_role('ROLE_ADMIN') or (has_role('ROLE_USER') and questionnaire.ofUser(user))")
     * @ParamConverter("questionnaire", options={""})
     * @Template
     *
     * @param Questionnaire $questionnaire Questionnaire
     * @return array Template variables
     */
    public function showAction(Questionnaire $questionnaire)
    {
        return array('questionnaire' => $questionnaire);
    }

    /**
     * @Route("/new", name="questionnaire_questionnaire_new")
     * @Template
     *
     * @return array Template variables
     */
    public function newAction()
    {
        $questionnaire = new Questionnaire();
        $questionnaire->setUser($this->getUser());

        $handler = $this->get('axtion_questionnaire.form.handler.questionnaire');
        if ($handler->process($questionnaire)) {
            $this->addFlash('success', $this->get('translator')->trans('questionnaire.success.new', array('%questionnaire_name%' => $questionnaire->getName())));

            if ($handler->getForm()->get('create_return_list')->isClicked()) {
                return $this->redirectToRoute('questionnaire_questionnaire_index');
            }
            else {
                return $this->redirectToRoute('questionnaire_question_new', array('id' => $questionnaire->getId()));
            }
        }

        return array('form' => $handler->getForm()->createView(), 'questionnaire' => $questionnaire);
    }

    /**
     * @Route("/{id}/edit/", name="questionnaire_questionnaire_edit", requirements={"id"="\d+"})
     * @Security("has_role('ROLE_ADMIN') or (has_role('ROLE_USER') and questionnaire.ofUser(user))")
     * @ParamConverter("questionnaire", options={""})
     * @Template
     *
     * @param Questionnaire $questionnaire Questionnaire
     * @return array Template variables
     */
    public function editAction(Questionnaire $questionnaire)
    {
        $handler = $this->get('axtion_questionnaire.form.handler.questionnaire');
        if ($handler->process($questionnaire)) {
            $this->addFlash('success', $this->get('translator')->trans('questionnaire.success.edit', array('%questionnaire_name%' => $questionnaire->getName())));

            return $this->redirectToRoute('questionnaire_questionnaire_index');
        }

        return array('form' => $handler->getForm()->createView(), 'questionnaire' => $questionnaire);
    }

    /**
     * @Route("/{id}/delete/", name="questionnaire_questionnaire_delete", requirements={"id"="\d+"})
     * @Security("has_role('ROLE_ADMIN') or (has_role('ROLE_USER') and questionnaire.ofUser(user))")
     * @ParamConverter("questionnaire", options={""})
     * @param Questionnaire $questionnaire Questionnaire
     * @return RedirectResponse Response
     */
    public function deleteAction(Questionnaire $questionnaire)
    {
        try {
            $questionnaire->delete();
            $this->addFlash('success', $this->get('translator')->trans('questionnaire.success.delete', array('%questionnaire_name%' => $questionnaire->getName())));
        } catch (\Exception $e) {
            $this->addFlash('danger', $this->get('translator')->trans('questionnaire.failure.delete', array('%questionnaire_name%' => $questionnaire->getName())));
        }

        return $this->redirectToRoute('questionnaire_questionnaire_index');
    }

    /**
     * @Route("/{id}/preview/", name="questionnaire_questionnaire_preview", requirements={"id"="\d+"})
     * @Security("has_role('ROLE_ADMIN') or (has_role('ROLE_USER') and questionnaire.ofUser(user))")
     * @ParamConverter("questionnaire", options={""})
     * @Template
     *
     * @param Questionnaire $questionnaire Questionnaire
     * @return RedirectResponse Response
     */
    public function previewAction(Questionnaire $questionnaire)
    {
        $form = $this->get('axtion_questionnaire.form.questionnaire_preview');
        $form->setData($questionnaire);

        return array('form' => $form->createView(), 'questionnaire' => $questionnaire);
    }


    /**
     * @Route("/{id}/manage", name="questionnaire_questionnaire_manage_clients", requirements={"id"="\d+"})
     * @Security("has_role('ROLE_ADMIN') or (has_role('ROLE_USER') and questionnaire.ofUser(user))")
     * @ParamConverter("questionnaire", options={""})
     * @Template()
     * @param Questionnaire $questionnaire
     * @return array
     */
    public function manageClientsAction(Questionnaire $questionnaire)
    {
        /** @var User $user */
        $user  = $this->getUser();
        $query = ClientQuery::create();
        if (!$this->isGranted('ROLE_ADMIN') || !$this->isGranted('ROLE_DEVELOPER')) {
            $query->filterByUser($user);
        }
        $clients     = $query->find();
        $form        = $this->createForm(new ClientFilterType());
        $assessments = AssessmentQuery::create()->filterByQuestionnaire($questionnaire)->select(array('ClientId'))->find();

        return array('clients' => $clients, 'questionnaire' => $questionnaire, 'form' => $form->createView(), 'assessments' => $assessments);
    }

    /**
     * @Route("/{id}/filter", name="questionnaire_questionnaire_filter_clients", requirements={"id"="\d+"}, options={"expose"=true})
     * @Security("has_role('ROLE_ADMIN') or (has_role('ROLE_USER') and questionnaire.ofUser(user))")
     * @ParamConverter("questionnaire", options={""})
     * @param Questionnaire $questionnaire
     * @return array
     */
    public function filterClientsAction(Questionnaire $questionnaire)
    {
        $user  = $this->getUser();
        $query = ClientQuery::create();
        if (!$this->isGranted('ROLE_ADMIN') || !$this->isGranted('ROLE_DEVELOPER')) {
            $query->filterByUser($user);
        }
        $data = $this->get('request')->request->get('client_filter_form');
        if (!is_null($data)) {
            $query->add(ClientPeer::FIRSTNAME, "%" . $data['search'] . "%", \Criteria::LIKE);
            $query->addOr(ClientPeer::LASTNAME, "%" . $data['search'] . "%", \Criteria::LIKE);
            $query->addOr(ClientPeer::EMAIL, "%" . $data['search'] . "%", \Criteria::LIKE);
            if ($data['is_active'] !== "") {
                $query->filterByIsActive($data['is_active']);
            }
        }
        $clients     = $query->find();
        $assessments = AssessmentQuery::create()->filterByQuestionnaire($questionnaire)->select(array('ClientId'))->find();

        return new JsonResponse($this->renderView('@AxtionQuestionnaire/Questionnaire/filter.html.twig', array('clients' => $clients, 'questionnaire' => $questionnaire, 'assessments' => $assessments)));
    }

    /**
     * @Route("/{id}/participating", name="questionnaire_questionnaire_participating", requirements={"id"="\d+"}, options={"expose"=true})
     * @Security("has_role('ROLE_ADMIN') or (has_role('ROLE_USER') and questionnaire.ofUser(user))")
     * @ParamConverter("questionnaire", options={""})
     * @param Request $request
     * @param Questionnaire $questionnaire
     * @return array
     */
    public function participatingAction(Request $request, Questionnaire $questionnaire)
    {
        $participating = $request->get('participating');
        if (!is_null($participating)) {
            $client = ClientQuery::create()->findOneById($participating['client_id']);
            if ($client instanceof Client) {
                $assessment = AssessmentQuery::create()->filterByQuestionnaire($questionnaire)->filterByClient($client)->findOne();
                if ($assessment) {
                    if ($participating['checked'] == 'true') {
                        return new JsonResponse('already exists');
                    }
                    else {
                        $assessment->delete();
                    }
                }
                else {
                    $status     = StatusQuery::create()->findOneBySlug(Status::OPEN);
                    $assessment = new Assessment();
                    $assessment->setClient($client)
                               ->setQuestionnaire($questionnaire)
                               ->setStatus($status);
                    $assessment->save();
                }

            }
        }

        return new JsonResponse('success');
    }

    /**
     * @Route("/{id}/assessments", name="questionnaire_questionnaire_assessments", requirements={"id"="\d+"})
     * @Security("has_role('ROLE_ADMIN') or (has_role('ROLE_USER') and questionnaire.ofUser(user))")
     * @ParamConverter("questionnaire", options={""})
     * @Template()
     * @param Questionnaire $questionnaire
     * @return array
     */
    public function assessmentsAction(Questionnaire $questionnaire)
    {
        return array('questionnaire' => $questionnaire);
    }
}
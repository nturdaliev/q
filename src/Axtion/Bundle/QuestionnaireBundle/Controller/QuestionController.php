<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 17-Jun 15
 * Time: 14:36
 */

namespace Axtion\Bundle\QuestionnaireBundle\Controller;

use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionGroupQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Questionnaire;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class QuestionController
 *
 * @package Axtion\Bundle\QuestionnaireBundle\Controller
 * @Route("/panel/question")
 * @Security("has_role('ROLE_USER')")
 */
class QuestionController extends Controller
{
    /**
     * @Route("/", name="questionnaire_question_index")
     * @Template
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @return array Template variables
     */
    public function indexAction()
    {
        $modelCriteria = QuestionQuery::create()->filterByParentId(null, \Criteria::ISNULL)->filterForGrid($this->container->getParameter('locale'));
        $page = $this->get('request')->query->getInt('page', 1);
        $questions = $this->get('knp_paginator')->paginate($modelCriteria, $page);

        return array('questions' => $questions);
    }

    /**
     * @Route("/{id}/show", name="questionnaire_question_show", requirements={"id"="\d+"})
     * @Security("has_role('ROLE_ADMIN') or (has_role('ROLE_USER') and question.ofUser(user))")
     * @ParamConverter("question", options={""})
     * @Template
     *
     * @param Question $question Question
     * @return array Template variables
     */
    public function showAction(Question $question)
    {
        $form = $this->get('axtion_questionnaire.form.question_preview');
        $form->setData($question);

        return array('question' => $question, 'form' => $form->createView());
    }

    /**
     * @Route("/{id}/edit", name="questionnaire_question_edit", requirements={"id"="\d+"})
     * @Security("has_role('ROLE_ADMIN') or (has_role('ROLE_USER') and question.ofUser(user))")
     * @ParamConverter("question", options={""})
     * @Template
     *
     * @param Question $question Question
     * @return array Template variables
     */
    public function editAction(Question $question)
    {
        $handler = $this->get('axtion_questionnaire.form.handler.question');
        $questionTypes = QuestionTypeQuery::create()->find();

        if ($handler->process($question)) {
            $this->addFlash('success', $this->get('translator')->trans('question.success.edit', array('%question_name%' => $question->getName())));

            return $this->redirectToRoute('questionnaire_questionnaire_show', array('id' => $question->getQuestionnaireId()));
        }

        return array('question' => $question, 'form' => $handler->getForm()->createView(), 'questionTypes' => $questionTypes);
    }

    /**
     * @Route("/questionnaire/{id}/new", name="questionnaire_question_new")
     * @Security("has_role('ROLE_ADMIN') or (has_role('ROLE_USER') and questionnaire.ofUser(user))")
     * @ParamConverter("questionnaire", options={""})
     * @Template
     *
     * @param Questionnaire $questionnaire Questionnaire
     * @return array Template variables
     */
    public function newAction(Questionnaire $questionnaire)
    {
        $question = new Question();
        $question->setQuestionnaire($questionnaire);

        $handler = $this->get('axtion_questionnaire.form.handler.question');
        if ($handler->process($question)) {
            $this->addFlash('success', $this->get('translator')->trans('question.success.new', array('%question_name%' => $question->getName())));

            if ($handler->getForm()->get('create_add_new')->isClicked()) {
                return $this->redirectToRoute('questionnaire_question_new', array('id' => $questionnaire->getId()));
            }

            return $this->redirectToRoute('questionnaire_questionnaire_show', array('id' => $question->getQuestionnaireId()));
        }

        return array('form' => $handler->getForm()->createView(), 'questionnaire' => $questionnaire, 'question' => $question);
    }

    /**
     * @Route("/{id}/delete", name="questionnaire_question_delete")
     * @ParamConverter("question", options={""})
     * @Security("has_role('ROLE_ADMIN') or (has_role('ROLE_USER') and question.ofUser(user))")
     *
     * @param Question $question Question
     * @return RedirectResponse Response
     */
    public function deleteAction(Question $question)
    {
        try {
            $question->delete();
            $this->addFlash('success', $this->get('translator')->trans('question.success.delete', array('%question_name%' => $question->getName())));
        } catch (\Exception $e) {
            $this->addFlash('danger', $this->get('translator')->trans('question.failure.delete', array('%question_name%' => $question->getName())));
        }

        return $this->redirectToRoute('questionnaire_questionnaire_show', array('id' => $question->getQuestionnaireId()));
    }


    /**
     * @Route("/updateSortableRanks", name="questionnaire_questions_update_sortable_ranks", options={"expose"="true"})
     * @Method("POST")
     * @ParamConverter("questionnaire", options={""})
     * @param Request $request
     * @return JsonResponse
     */
    public function updateSortableRanksAction(Request $request)
    {
        if (!$request->request->has('questions')) {
            return new JsonResponse(array('success' => false));
        }
        $questions = $request->request->get('questions');
        foreach ($questions as $question) {
            $questionFromDatabase = QuestionQuery::create()->findPk($question['id']);
            $questionFromDatabase->setSortableRank($question['sortable_rank']);
            $questionFromDatabase->save();
        }

        return new JsonResponse(array('success' => true));
    }

    /**
     * @Route("/template/{id}", name="questionnaire_question_template", options={"expose"=true}, requirements={"id"="\d+"})
     * @ParamConverter("question", options={"optional"=true})
     *
     * @param Request $request Request
     * @param Question $question Question
     * @return JsonResponse Response
     */
    public function templateAction(Request $request, Question $question = null)
    {
        $question = $this->populateQuestion($request, $question);
        $form = $this->get('axtion_questionnaire.form.question');
        $form->setData($question);

        $template = $this->renderView('AxtionQuestionnaireBundle:Question:template.html.twig', array('question' => $question, 'form' => $form->createView()));

        return new JsonResponse(array('template' => $template));
    }

    /**
     * @param Request $request Request
     * @param Question $question Question
     * @return Question
     */
    private function populateQuestion(Request $request, Question $question = null)
    {
        if (is_null($question)) {
            $question = new Question();
        }

        if (!is_null($formData = $request->get('question_form'))) {
            if (array_key_exists('name', $formData) && !is_null($name = $formData['name'])) {
                $question->setName($name);
            }

            if (array_key_exists('help', $formData) && !is_null($help = $formData['help'])) {
                $question->setHelp($help);
            }

            if (array_key_exists('questionType', $formData) && !is_null($questionTypeId = $formData['questionType']) && $formData['questionType']) {
                $questionType = QuestionTypeQuery::create()->findPk($questionTypeId);

                $question->setQuestionType($questionType);

                if ($questionType->getHasSubquestions()) {
                    $question->setSubQuestions($questionType->loadDummyQuestions($question));
                }
            }
            if (array_key_exists('question_group', $formData)) {
                $questionGroup = QuestionGroupQuery::create()->findOneById($formData['question_group']);
                if ($questionGroup) {
                    $question->setQuestionGroup($questionGroup);
                    $question->setQuestionnaire($questionGroup->getQuestionnaire());
                }
            }
        }

        return $question;
    }

    /**
     * @Route("/preview/{id}", name="questionnaire_question_preview", options={"expose"=true}, requirements={"id"="\d+"})
     * @ParamConverter("question", options={"optional"=true})
     *
     * @param Request $request Request
     * @param Question $question Question
     * @return JsonResponse Response
     */
    public function previewAction(Request $request, Question $question = null)
    {

        try {
            $question = $this->populateQuestion($request, $question);

            $form = $this->get('axtion_questionnaire.form.question');
            $form->setData($question);
            $form->handleRequest($request);

            $form = $this->get('axtion_questionnaire.form.question_preview');
            $form->setData($question);


            $preview = $this->renderView("AxtionQuestionnaireBundle:Question:preview.html.twig", array('question' => $question, 'form' => $form->createView()));
        } catch (\Exception $e) {
            $preview = $this->get('translator')->trans('question.empty_preview');
        }

        return new JsonResponse(array('preview' => $preview));
    }
}
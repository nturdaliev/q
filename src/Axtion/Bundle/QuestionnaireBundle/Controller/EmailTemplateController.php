<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 4/19/2016
 * Time: 5:00 PM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Controller;


use Axtion\Bundle\QuestionnaireBundle\Propel\EmailTemplate;
use Axtion\Bundle\QuestionnaireBundle\Propel\EmailTemplateQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/panel/emailTemplate")
 * Class EmailTemplateController
 * @package Axtion\Bundle\QuestionnaireBundle\Controller
 */
class EmailTemplateController extends Controller
{
    /**
     * @Route("/", name="questionnaire_email_template_index")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        $emailTemplates = EmailTemplateQuery::create()->find();
        return array('emailTemplates' => $emailTemplates);
    }

    /**
     * @Route("/new", name="questionnaire_email_template_new")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function newAction()
    {
        $emailTemplate = new EmailTemplate();
        $handler       = $this->get('axtion_questionnaire.form.handler.email_template');
        if ($handler->process($emailTemplate)) {
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('emailTemplate.success.new', array('%name%'=> $emailTemplate->getName())));
            if ($handler->getForm()->get('create_return_list')->isClicked()) {
                return $this->redirectToRoute('questionnaire_email_template_index');
            }
            else {
                return $this->redirectToRoute('questionnaire_email_template_new');
            }
        }
        return array('form' => $handler->getForm()->createView(), 'emailTemplate' => $emailTemplate);
    }


    /**
     * @Route("/{id}/edit", name="questionnaire_email_template_edit")
     * @ParamConverter("emailTemplate", options={""})
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     * @param \Axtion\Bundle\QuestionnaireBundle\Propel\EmailTemplate $emailTemplate
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(EmailTemplate $emailTemplate)
    {
        $handler = $this->get('axtion_questionnaire.form.handler.email_template');
        if ($handler->process($emailTemplate)) {
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('emailTemplate.success.edit', array('%name%'=> $emailTemplate->getName())));
            return $this->redirectToRoute('questionnaire_email_template_index');
        }
        return array('form' => $handler->getForm()->createView(), 'emailTemplate' => $emailTemplate);
    }

    /**
     * @Route("/{id}/delete", name="questionnaire_email_template_delete")
     * @ParamConverter("emailTemplate", options={""})
     * @Security("has_role('ROLE_ADMIN')")
     * @param \Axtion\Bundle\QuestionnaireBundle\Propel\EmailTemplate $emailTemplate
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(EmailTemplate $emailTemplate)
    {
        try {
            $emailTemplate->delete();
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('emailTemplate.success.delete', array('%name%'=> $emailTemplate->getName())));
            return $this->redirectToRoute('questionnaire_email_template_index');
        } catch (\Exception $e) {
            return $this->redirectToRoute('questionnaire_email_template_index');
        }

    }
}
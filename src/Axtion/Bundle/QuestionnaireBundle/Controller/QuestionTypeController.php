<?php

namespace Axtion\Bundle\QuestionnaireBundle\Controller;

use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionType;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class QuestionTypeController
 *
 * @package Axtion\Bundle\QuestionnaireBundle\Controller
 * @Route("/panel/question_type")
 * @Security("has_role('ROLE_DEVELOPER')")
 */
class QuestionTypeController extends Controller
{
    /**
     * @Route("/", name="questionnaire_question_type_index")
     * @Template()
     *
     * @return array Template variables
     */
    public function indexAction()
    {
        $modelCriteria = QuestionTypeQuery::create('q');
        $page = $this->get('request')->query->getInt('page', 1);
        $questionTypes = $this->get('knp_paginator')->paginate($modelCriteria, $page);

        return array('questionTypes' => $questionTypes);
    }

    /**
     * @Route("/{id}/edit", name="questionnaire_question_type_edit", requirements={"id"="\d+"})
     * @ParamConverter("questionType", options={""})
     * @Template()
     *
     * @param QuestionType $questionType Question type
     * @return array Template variables
     */
    public function editAction(QuestionType $questionType)
    {
        $handler = $this->get('axtion_questionnaire.form.handler.question_type');
        if ($handler->process($questionType)) {
            $this->addFlash('success', $this->get('translator')->trans('question_type.success.edit', array('%question_type_name%' => $questionType->getName())));

            return $this->redirectToRoute('questionnaire_question_type_index');
        }

        return array('form' => $handler->getForm()->createView());
    }

    /**
     * @Route("/new", name="questionnaire_question_type_new", requirements={"id"="\d+"})
     * @Template()
     */
    public function newAction()
    {
        $questionType = new QuestionType();
        $handler = $this->get('axtion_questionnaire.form.handler.question_type');
        if ($handler->process($questionType)) {
            $this->addFlash('success', $this->get('translator')->trans('question_type.success.new', array('%question_type_name%' => $questionType->getName())));

            return $this->redirectToRoute('questionnaire_question_type_index');
        }

        return array('form' => $handler->getForm()->createView());
    }

    /**
     * @Route("/{id}/delete", name="questionnaire_question_type_delete", requirements={"id"="\d+"})
     * @ParamConverter("questionType", options={""})
     * @param QuestionType $questionType
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(QuestionType $questionType)
    {
        try {
            $questionType->delete();
            $this->addFlash('success', $this->get('translator')->trans('question_type.success.delete', array('%question_type_name%' => $questionType->getName())));
        } catch (\Exception $e) {
            $this->addFlash('danger', $this->get('translator')->trans('question_type.failure.delete', array('%question_type_name%' => $questionType->getName())));
        }

        return $this->redirectToRoute('questionnaire_question_type_index');
    }
}

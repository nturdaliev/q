<?php

namespace Axtion\Bundle\QuestionnaireBundle\Controller;


use Axtion\Bundle\QuestionnaireBundle\Propel\Theme;
use Axtion\Bundle\QuestionnaireBundle\Propel\ThemeQuery;
use PropelException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/panel/theme")
 * @Security("has_role('ROLE_DEVELOPER')")
 * @package Axtion\Bundle\QuestionnaireBundle\Controller
 *
 * Class ThemeController
 * @package Axtion\Bundle\QuestionnaireBundle\Controller
 */
class ThemeController extends Controller
{
    /**
     * @Route("/", name="questionnaire_theme_index")
     * @Template()
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $theme = ThemeQuery::create()->find();

        return array('themes' => $theme);
    }

    /**
     * @Route("/new", name="questionnaire_theme_new")
     * @Template()
     */
    public function newAction()
    {
        $theme = new Theme();
        $handler = $this->get('axtion_questionnaire.form.handler.theme');
        if ($handler->process($theme)) {

            $this->get('session')->getFlashBag()->set('success', $this->get('translator')->trans('theme.success.new'));

            return $this->redirectToRoute('questionnaire_theme_index');
        }

        return array('form' => $handler->getForm()->createView());
    }

    /**
     * @Route("/edit/{id}", name="questionnaire_theme_edit", requirements={"id"="\d+"})
     * @ParamConverter("theme", options={""})
     * @Template()
     * @param Theme $theme
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Theme $theme)
    {

        $handler = $this->get('axtion_questionnaire.form.handler.theme');
        if ($handler->process($theme)) {

            $this->get('session')->getFlashBag()->set('success', $this->get('translator')->trans('theme.success.edit', array('%name%' => $theme->getName())));

            return $this->redirectToRoute('questionnaire_theme_index');
        }

        return array('form' => $handler->getForm()->createView());
    }

    /**
     * @Route("/delete/{id}", name="questionnaire_theme_delete", requirements={"id"="\d+"})
     * @ParamConverter("theme", options={""})
     * @param Theme $theme
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Theme $theme)
    {
        try {
            $theme->delete();
        } catch (PropelException $e) {
            $this->get('session')->getFlashBag()->set('success', $this->get('translator')->trans('theme.failure.delete', array('%name%' => $theme->getName())));
        }

        return $this->redirectToRoute('questionnaire_theme_index');
    }
}

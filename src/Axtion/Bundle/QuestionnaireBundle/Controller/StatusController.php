<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 23-Jun 15
 * Time: 11:44
 */

namespace Axtion\Bundle\QuestionnaireBundle\Controller;

use Axtion\Bundle\QuestionnaireBundle\Propel\Status;
use Axtion\Bundle\QuestionnaireBundle\Propel\StatusQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class StatusController
 *
 * @package Axtion\Bundle\QuestionnaireBundle\Controller
 * @Route("/panel/status")
 * @Security("has_role('ROLE_ADMIN')")
 */
class StatusController extends Controller
{

    /**
     * @Route("/", name="questionnaire_status_index")
     * @Template()
     *
     * @return array Template variables
     */
    public function indexAction()
    {
        return array('statuses' => StatusQuery::create()->find());
    }

    /**
     * @Route("/new", name="questionnaire_status_new")
     * @Template()
     * @return array Template variables
     */
    public function newAction()
    {
        $status = new Status();
        $handler = $this->get('axtion_questionnaire.form.handler.status');
        if ($handler->process($status)) {
            $this->addFlash('success', $this->get('translator')->trans('status.success.new', array('%status_name%' => $status->getName())));

            if ($handler->getForm()->get('create_add_new')->isClicked()) {
                return $this->redirectToRoute('questionnaire_status_new');
            } else {
                return $this->redirectToRoute('questionnaire_status_index');
            }
        }

        return array('form' => $handler->getForm()->createView());
    }

    /**
     * @Route("/{id}/edit", name="questionnaire_status_edit", requirements={"id"="\d+"})
     * @ParamConverter("status", class="Axtion\Bundle\QuestionnaireBundle\Propel\Status", options={"mapping"={"id":"id"}})
     * @Template()
     *
     * @param Status $status Status
     * @return array|RedirectResponse Template variables or response
     */
    public function editAction(Status $status)
    {
        $handler = $this->get('axtion_questionnaire.form.handler.status');
        if ($handler->process($status)) {
            $this->addFlash('success', $this->get('translator')->trans('status.success.edit', array('%status_name%' => $status->getName())));

            return $this->redirectToRoute('questionnaire_status_index');
        }

        return array('form' => $handler->getForm()->createView());
    }

    /**
     * @Route("/{id}/delete", name="questionnaire_status_delete", requirements={"id"="\d+"})
     * @ParamConverter("status", class="Axtion\Bundle\QuestionnaireBundle\Propel\Status", options={"mapping"={"id":"id"}})
     * @param Status $status Status
     * @return RedirectResponse Response
     */
    public function deleteAction(Status $status)
    {
        try {
            $status->delete();
            $this->addFlash('success', $this->get('translator')->trans('status.success.delete', array('%status_name%' => $status->getName())));
        } catch (\Exception $e) {
            $this->addFlash('danger', $this->get('translator')->trans('status.failure.delete', array('%status_name%' => $status->getName())));
        }

        return $this->redirectToRoute('questionnaire_status_index');
    }


}
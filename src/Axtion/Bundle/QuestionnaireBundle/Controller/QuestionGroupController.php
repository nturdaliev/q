<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 10/8/2015
 * Time: 3:07 PM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Controller;


use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionGroupQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * /**
 * Class QuestionController
 *
 * @package Axtion\Bundle\QuestionnaireBundle\Controller
 * @Route("/panel/question_group")
 * @Security("has_role('ROLE_USER')")
 * Class QuestionGroupController
 * @package Axtion\Bundle\QuestionnaireBundle\Controller
 */
class QuestionGroupController extends Controller
{

    /**
     * @Route("/sortable", name="questionnaire_question_group_sortable", options={"expose"="true"})
     * @Method("POST")
     * @ParamConverter("questionnaire", options={""})
     * @param Request $request
     * @return JsonResponse
     */
    public function sortableAction(Request $request)
    {
        if (!$request->request->has('ranks')) {
            return new JsonResponse(array('success' => false));
        }
        $ranks = $request->request->get('ranks');
        foreach ($ranks as $rank) {
            $questionGroup = QuestionGroupQuery::create()->findPk($rank['id']);
            $questionGroup->setSortableRank($rank['rank']);
            $questionGroup->save();
        }

        return new JsonResponse(array('success' => true));
    }

}
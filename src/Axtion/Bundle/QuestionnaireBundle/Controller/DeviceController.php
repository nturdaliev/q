<?php

namespace Axtion\Bundle\QuestionnaireBundle\Controller;

use Axtion\Bundle\QuestionnaireBundle\Form\Type\DeviceType;
use Axtion\Bundle\QuestionnaireBundle\Propel\Device;
use Axtion\Bundle\QuestionnaireBundle\Propel\DeviceQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/panel/device")
 * @Security("has_role('ROLE_ADMIN')")
 * Class DeviceController
 * @package Axtion\Bundle\AssessmentBundle\Controller
 */
class DeviceController extends Controller
{
    /**
     * @Route("/", name="questionnaire_device_index")
     * @Template()
     */
    public function indexAction()
    {
        $devices = DeviceQuery::create()->find();

        return array('devices' => $devices);
    }

    /**
     * @Route("/new", name="questionnaire_device_new")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $device = new Device();
        $form = $this->createForm(new DeviceType(), $device);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $device->save();
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/{id}/edit", name="questionnaire_device_edit")
     * @ParamConverter("device", options={""})
     * @Template()
     * @param Device $device
     * @return array
     */
    public function editAction(Device $device)
    {
        $form = $this->createForm(new DeviceType(), $device);
        $form->handleRequest($this->get('request'));
        if ($form->isValid()) {
            $device->save();
            return $this->redirectToRoute('questionnaire_device_index');
        }

        return array('form' => $form->createView());
    }
}


<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/24/2015
 * Time: 2:38 PM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Controller;


use Axtion\Bundle\QuestionnaireBundle\Form\Type\ConfigurationKeyType;
use Axtion\Bundle\QuestionnaireBundle\Propel\ConfigurationKey;
use Axtion\Bundle\QuestionnaireBundle\Propel\ConfigurationKeyQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/panel/configuration_key")
 * Class ConfigurationKeyController
 * @package Axtion\Bundle\QuestionnaireBundle\Controller
 */
class ConfigurationKeyController extends Controller
{
    /**
     * @Route("/", name="questionnaire_configuration_key_index")
     * @Template()
     */
    public function indexAction()
    {
        $keys = ConfigurationKeyQuery::create()->find();

        return array('keys' => $keys);
    }

    /**
     * @Route("/new", name="questionnaire_configuration_key_new")
     * @Template()
     * @return array
     */
    public function newAction()
    {
        $configurationKey = new ConfigurationKey();
        $form = $this->createForm(new ConfigurationKeyType(), $configurationKey);
        $form->handleRequest($this->get('request'));
        if ($form->isValid()) {
            $configurationKey->save();
        }

        return array('form' => $form->createView());
    }

}
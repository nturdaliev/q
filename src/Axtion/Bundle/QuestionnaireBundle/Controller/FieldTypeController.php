<?php

namespace Axtion\Bundle\QuestionnaireBundle\Controller;

use Axtion\Bundle\QuestionnaireBundle\Form\Type\FieldTypeType;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldType;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldTypeQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class FieldTypeController
 * @Route("/panel/field_type")
 * @Security("has_role('ROLE_DEVELOPER')")
 * @package Axtion\Bundle\QuestionnaireBundle\Controller
 */
class FieldTypeController extends Controller
{
    /**
     * @Route("/", name="questionnaire_field_type_index")
     * @Template()
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return array('fieldTypes' => FieldTypeQuery::create()->find());
    }

    /**
     * @Route("/new", name="questionnaire_field_type_new")
     * @Template()
     */
    public function newAction()
    {
        $fieldType = new FieldType();
        $form = $this->createForm(new FieldTypeType(), $fieldType);
        $form->handleRequest($this->get('request'));
        if ($form->isValid()) {
            $fieldType->save();
            $this->addFlash('success', $this->get('translator')->trans('field_type.success.new', array('%field_type_name%' => $fieldType->getName())));

            return $this->redirectToRoute('questionnaire_field_type_index');
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/{id}/edit", name="questionnaire_field_type_edit")
     * @ParamConverter("fieldType",options={""})
     * @Template()
     * @param FieldType $fieldType
     * @return array
     */
    public function editAction(FieldType $fieldType)
    {
        $form = $this->createForm(new FieldTypeType(), $fieldType);
        $form->handleRequest($this->get('request'));
        if ($form->isValid()) {
            $fieldType->save();
            $this->addFlash('success', $this->get('translator')->trans('field_type.success.edit', array('%field_type_name%' => $fieldType->getName())));

            return $this->redirectToRoute('questionnaire_field_type_index');
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/{id}/delete", name="questionnaire_field_type_delete")
     * @ParamConverter("fieldType", options={""})
     * @param FieldType $fieldType
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(FieldType $fieldType)
    {
        try {
            $fieldType->delete();
            $this->addFlash('success', $this->get('translator')->trans('field_type.success.delete', array('%field_type_name%' => $fieldType->getName())));
        } catch (\Exception $ignore) {
            $this->addFlash('danger', $this->get('translator')->trans('field_type.failure.delete', array('%field_type_name%' => $fieldType->getName())));
        }

        return $this->redirectToRoute('questionnaire_field_type_index');
    }
}

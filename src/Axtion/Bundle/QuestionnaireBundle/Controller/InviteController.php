<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 4/20/2016
 * Time: 11:00 AM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Controller;


use Axtion\Bundle\QuestionnaireBundle\Model\Invite;
use Axtion\Bundle\QuestionnaireBundle\Propel\Questionnaire;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/invite")
 * Class InviteController
 * @package Axtion\Bundle\QuestionnaireBundle\Controller
 */
class InviteController extends Controller
{
    /**
     * @Route("/{id}/questionnaire", name="questionnaire_invite_invite")
     * @ParamConverter("questionnaire", options={""})
     * @Template()
     * @param \Axtion\Bundle\QuestionnaireBundle\Propel\Questionnaire $questionnaire
     * @return array
     */
    public function inviteAction(Questionnaire $questionnaire)
    {
        $invite = new Invite();
        $invite->setQuestionnaire($questionnaire);
        $handler = $this->get('axtion_questionnaire.form.handler.invite');
        if ($handler->process($invite)) {
            $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('invite.success.send'));
            return $this->redirectToRoute('questionnaire_questionnaire_manage_clients', array('id' => $questionnaire->getId()));
        }
        return array('invitationForm' => $handler->getForm()->createView(),'questionnaire'=>$questionnaire);
    }
}
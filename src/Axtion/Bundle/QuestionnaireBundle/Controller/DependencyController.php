<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 9/3/2015
 * Time: 1:21 PM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Controller;


use Axtion\Bundle\QuestionnaireBundle\Propel\Dependency;
use Axtion\Bundle\QuestionnaireBundle\Propel\Questionnaire;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/panel")
 * @Security("has_role('ROLE_USER')")
 * @package Axtion\Bundle\QuestionnaireBundle\Controller
 */
class DependencyController extends Controller
{
    /**
     * @Route("/questionnaire/{id}/dependency/manage/", name="questionnaire_dependency_manage")
     * @ParamConverter("questionnaire", options={""})
     * @Template()
     * @param Questionnaire $questionnaire
     * @return array
     */
    public function manageAction(Questionnaire $questionnaire)
    {
        $dependency = new Dependency();
        /** @var \Axtion\Bundle\QuestionnaireBundle\Form\Handler\DependencyHandler $handler */
        $handler = $this->get('axtion_questionnaire.form.handler.dependency');
        if ($handler->process($dependency)) {
            $this->get('session')->getFlashBag()->set('success', $this->get('translator')->trans('dependency.success'));
            //return $this->redirectToRoute('questionnaire_questionnaire_show', array('id' => $questionnaire->getId()));
        }

        return array('form' => $handler->getForm()->createView(), 'questionnaire' => $questionnaire);
    }

    /**
     * @Route("/questionnaire/{id}/dependency/form", name="questionnaire_dependency_form", options={"expose"=true}, condition="request.isXmlHttpRequest()")
     * @ParamConverter("questionnaire", options={""})
     * @Template()
     * @param Questionnaire $questionnaire
     * @return array
     */
    public function formAction(Questionnaire $questionnaire)
    {
        $dependency = new Dependency();
        $handler = $this->get('axtion_questionnaire.form.handler.dependency');
        $handler->process($dependency);

        return array('form' => $handler->getForm()->createView(), 'questionnaire' => $questionnaire);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 9/14/2015
 * Time: 3:59 PM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Services;


use Axtion\Bundle\QuestionnaireBundle\Propel\Question;

/**
 * Class AbstractDependencyValue
 * @package Axtion\Bundle\QuestionnaireBundle\Services
 */
abstract class AbstractDependencyValue
{
    /** @var  Question */
    protected $question;


    /**
     * @param Question $question
     * @return DependencyValueChoice
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    abstract public function getChoices();

    protected function check()
    {
        if (!$this->question instanceof Question) {
            throw new \Exception('You need to set a question first');
        }
    }
}
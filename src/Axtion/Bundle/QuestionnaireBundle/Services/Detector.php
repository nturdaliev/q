<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/24/2015
 * Time: 4:05 PM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Services;


use Axtion\Bundle\QuestionnaireBundle\Propel\ConfigurationKey;
use Axtion\Bundle\QuestionnaireBundle\Propel\Device;
use Axtion\Bundle\QuestionnaireBundle\Propel\DeviceQuery;
use SunCat\MobileDetectBundle\DeviceDetector\MobileDetector;

/**
 * Class Detector
 * @package Axtion\Bundle\QuestionnaireBundle\Detector
 */
class Detector
{
    /** @var  Device */
    public $device;
    private $detector;

    /**
     * @inheritDoc
     */
    function __construct(MobileDetector $detector)
    {
        $this->detector = $detector;
        $this->configure();
    }

    private function configure()
    {
        $code = ConfigurationKey::DESKTOP;
        /** @var Device $device */
        if ($this->detector->isMobile()) {
            $code = ConfigurationKey::MOBILE;
        } elseif ($this->detector->isTablet()) {
            $code = ConfigurationKey::TABLET;
        }

        $this->device = DeviceQuery::create()->findOneByCode($code);
    }

    /**
     * @return MobileDetector
     */
    public function getDetector()
    {
        return $this->detector;
    }

    /**
     * @return Device
     */
    public function getDevice()
    {
        return $this->device;
    }
}
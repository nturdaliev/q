<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 9/14/2015
 * Time: 3:54 PM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Services;


/**
 * Class DependencyValue
 * @package Axtion\Bundle\QuestionnaireBundle\Services
 */
class DependencyValueChoice extends AbstractDependencyValue
{

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getChoices()
    {
        $this->check();

        return $this->question->createOptions()['choices'];
    }
}
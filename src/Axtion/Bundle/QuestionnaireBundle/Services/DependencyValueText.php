<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 9/14/2015
 * Time: 4:06 PM
 */

namespace Axtion\Bundle\QuestionnaireBundle\Services;


/**
 * Class DependencyValueText
 * @package Axtion\Bundle\QuestionnaireBundle\Services
 */
class DependencyValueText extends AbstractDependencyValue
{

    /**
     * @return array
     * @throws \Exception
     */
    public function getChoices()
    {
        $this->check();

        return array(
            'null' => 'Empty',
        );
    }
}
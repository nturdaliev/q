<?php

namespace Axtion\Bundle\QuestionnaireBundle\Twig\Extension;

use Axtion\Bundle\AssessmentBundle\Propel\Client;
use Axtion\Bundle\QuestionnaireBundle\Services\Detector;

/**
 * Class DeviceExtension
 * @package Axtion\Bundle\QuestionnaireBundle\Twig\Extension
 */
class DeviceExtension extends \Twig_Extension
{
    private $detector;

    /**
     * @inheritDoc
     */
    function __construct(Detector $detector)
    {
        $this->detector = $detector;
    }

    /**
     * @inheritDoc
     */
    public function getGlobals()
    {
        return array('device' => $this->detector->getDevice());
    }

    /**
     * @inheritDoc
     */
    public function getTests()
    {
        return [
            new \Twig_SimpleTest('client', function ($user) {
                return $user instanceof Client;
            }),
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'device';
    }
}

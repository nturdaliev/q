<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel;

use Axtion\Bundle\QuestionnaireBundle\Propel\om\BaseDependencyQuestionQuery;
use Criteria;

class DependencyQuestionQuery extends BaseDependencyQuestionQuery
{
    /**
     * @param array $keys
     * @return $this|\Criteria
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(DependencyQuestionPeer::DEPENDENCY_ID, $key[1], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(DependencyQuestionPeer::QUESTION_ID, $key[0], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }
}

<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel;

use Axtion\Bundle\QuestionnaireBundle\Propel\om\BaseConfiguration;

/**
 * Class Configuration
 * @package Axtion\Bundle\QuestionnaireBundle\Propel
 */
class Configuration extends BaseConfiguration
{
}

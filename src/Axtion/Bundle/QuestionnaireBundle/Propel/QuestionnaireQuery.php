<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel;

use Axtion\Bundle\QuestionnaireBundle\Propel\om\BaseQuestionnaireQuery;

/**
 * Class QuestionnaireQuery
 * @package Axtion\Bundle\QuestionnaireBundle\Propel
 */
class QuestionnaireQuery extends BaseQuestionnaireQuery
{
    /**
     * Filter for grid
     *
     * @param string $locale Locals
     * @return QuestionnaireQuery
     */
    public function filterForGrid($locale)
    {
        return $this
            ->orderByCreatedAt(\Criteria::DESC)
            ->useUserQuery()
            ->endUse()
            ->useStatusQuery()
            ->useI18nQuery($locale)
            ->endUse()
            ->endUse()
            ->withColumn('StatusI18n.Name', 'statusname')
            ->withColumn('User.DisplayName', 'username');
    }
}
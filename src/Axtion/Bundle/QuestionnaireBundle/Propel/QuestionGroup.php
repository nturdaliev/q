<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel;

use Axtion\Bundle\QuestionnaireBundle\Propel\om\BaseQuestionGroup;
use PropelPDO;

class QuestionGroup extends BaseQuestionGroup
{
    /**
     * @inheritDoc
     */
    public function getQuestions($criteria = null, PropelPDO $con = null)
    {
        $criteria = QuestionQuery::create()->orderBySortableRank();
        return parent::getQuestions($criteria, $con);
    }

}

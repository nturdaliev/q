<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel;

use Axtion\Bundle\QuestionnaireBundle\Propel\om\BaseConfigurationKey;

class ConfigurationKey extends BaseConfigurationKey
{

    const DESKTOP = 'desktop';
    const TABLET = 'tablet';
    const MOBILE = 'mobile';
}

<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'questionnaire_questionnaire' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Axtion.Bundle.QuestionnaireBundle.Propel.map
 */
class QuestionnaireTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Axtion.Bundle.QuestionnaireBundle.Propel.map.QuestionnaireTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('questionnaire_questionnaire');
        $this->setPhpName('Questionnaire');
        $this->setClassname('Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Questionnaire');
        $this->setPackage('src.Axtion.Bundle.QuestionnaireBundle.Propel');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('questionnaire_questionnaire_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('user_id', 'UserId', 'INTEGER', 'user_user', 'id', false, null, null);
        $this->addForeignKey('status_id', 'StatusId', 'INTEGER', 'questionnaire_status', 'id', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 100, null);
        $this->getColumn('name', false)->setPrimaryString(true);
        $this->addColumn('welcome', 'Welcome', 'LONGVARCHAR', false, null, null);
        $this->addColumn('goodbye', 'Goodbye', 'LONGVARCHAR', false, null, null);
        $this->addForeignKey('theme_id', 'ThemeId', 'INTEGER', 'questionnaire_theme', 'id', false, null, null);
        $this->addColumn('copyright', 'Copyright', 'VARCHAR', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Status', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Status', RelationMap::MANY_TO_ONE, array('status_id' => 'id', ), null, null);
        $this->addRelation('User', 'Axtion\\Bundle\\UserBundle\\Propel\\User', RelationMap::MANY_TO_ONE, array('user_id' => 'id', ), null, null);
        $this->addRelation('Theme', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Theme', RelationMap::MANY_TO_ONE, array('theme_id' => 'id', ), null, null);
        $this->addRelation('Assessment', 'Axtion\\Bundle\\AssessmentBundle\\Propel\\Assessment', RelationMap::ONE_TO_MANY, array('id' => 'questionnaire_id', ), null, null, 'Assessments');
        $this->addRelation('Question', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Question', RelationMap::ONE_TO_MANY, array('id' => 'questionnaire_id', ), null, null, 'Questions');
        $this->addRelation('QuestionGroup', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\QuestionGroup', RelationMap::ONE_TO_MANY, array('id' => 'questionnaire_id', ), null, null, 'QuestionGroups');
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' =>  array (
  'create_column' => 'created_at',
  'update_column' => 'updated_at',
  'disable_updated_at' => 'false',
),
        );
    } // getBehaviors()

} // QuestionnaireTableMap

<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'questionnaire_field_option' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Axtion.Bundle.QuestionnaireBundle.Propel.map
 */
class FieldOptionTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Axtion.Bundle.QuestionnaireBundle.Propel.map.FieldOptionTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('questionnaire_field_option');
        $this->setPhpName('FieldOption');
        $this->setClassname('Axtion\\Bundle\\QuestionnaireBundle\\Propel\\FieldOption');
        $this->setPackage('src.Axtion.Bundle.QuestionnaireBundle.Propel');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('questionnaire_field_option_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('field_type_id', 'FieldTypeId', 'INTEGER', 'questionnaire_field_type', 'id', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 32, null);
        $this->getColumn('name', false)->setPrimaryString(true);
        $this->addForeignKey('field_option_tab_id', 'FieldOptionTabId', 'INTEGER', 'questionnaire_field_option_tab', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('FieldType', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\FieldType', RelationMap::MANY_TO_ONE, array('field_type_id' => 'id', ), null, null);
        $this->addRelation('FieldOptionTab', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\FieldOptionTab', RelationMap::MANY_TO_ONE, array('field_option_tab_id' => 'id', ), null, null);
        $this->addRelation('SelectedOption', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\SelectedOption', RelationMap::ONE_TO_MANY, array('id' => 'option_id', ), null, null, 'SelectedOptions');
        $this->addRelation('QuestionTypeFieldOption', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\QuestionTypeFieldOption', RelationMap::ONE_TO_MANY, array('id' => 'option_id', ), null, null, 'QuestionTypeFieldOptions');
        $this->addRelation('Question', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Question', RelationMap::MANY_TO_MANY, array(), null, null, 'Questions');
        $this->addRelation('QuestionType', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\QuestionType', RelationMap::MANY_TO_MANY, array(), null, null, 'QuestionTypes');
    } // buildRelations()

} // FieldOptionTableMap

<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'questionnaire_question_group' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Axtion.Bundle.QuestionnaireBundle.Propel.map
 */
class QuestionGroupTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Axtion.Bundle.QuestionnaireBundle.Propel.map.QuestionGroupTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('questionnaire_question_group');
        $this->setPhpName('QuestionGroup');
        $this->setClassname('Axtion\\Bundle\\QuestionnaireBundle\\Propel\\QuestionGroup');
        $this->setPackage('src.Axtion.Bundle.QuestionnaireBundle.Propel');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('questionnaire_question_group_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, null, null);
        $this->getColumn('name', false)->setPrimaryString(true);
        $this->addForeignKey('questionnaire_id', 'QuestionnaireId', 'INTEGER', 'questionnaire_questionnaire', 'id', true, null, null);
        $this->addColumn('sortable_rank', 'SortableRank', 'INTEGER', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Questionnaire', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Questionnaire', RelationMap::MANY_TO_ONE, array('questionnaire_id' => 'id', ), null, null);
        $this->addRelation('Question', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Question', RelationMap::ONE_TO_MANY, array('id' => 'group_id', ), null, null, 'Questions');
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'sortable' =>  array (
  'rank_column' => 'sortable_rank',
  'use_scope' => 'true',
  'scope_column' => 'questionnaire_id',
),
        );
    } // getBehaviors()

} // QuestionGroupTableMap

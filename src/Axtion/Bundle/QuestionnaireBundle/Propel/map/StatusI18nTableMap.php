<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'questionnaire_status_i18n' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Axtion.Bundle.QuestionnaireBundle.Propel.map
 */
class StatusI18nTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Axtion.Bundle.QuestionnaireBundle.Propel.map.StatusI18nTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('questionnaire_status_i18n');
        $this->setPhpName('StatusI18n');
        $this->setClassname('Axtion\\Bundle\\QuestionnaireBundle\\Propel\\StatusI18n');
        $this->setPackage('src.Axtion.Bundle.QuestionnaireBundle.Propel');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('id', 'Id', 'INTEGER' , 'questionnaire_status', 'id', true, null, null);
        $this->addPrimaryKey('locale', 'Locale', 'VARCHAR', true, 5, 'en_US');
        $this->addColumn('name', 'Name', 'VARCHAR', true, 100, null);
        $this->getColumn('name', false)->setPrimaryString(true);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Status', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Status', RelationMap::MANY_TO_ONE, array('id' => 'id', ), 'CASCADE', null);
    } // buildRelations()

} // StatusI18nTableMap

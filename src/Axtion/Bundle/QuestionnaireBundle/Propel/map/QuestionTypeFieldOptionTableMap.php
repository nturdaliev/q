<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'questionnaire_question_type_field_option' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Axtion.Bundle.QuestionnaireBundle.Propel.map
 */
class QuestionTypeFieldOptionTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Axtion.Bundle.QuestionnaireBundle.Propel.map.QuestionTypeFieldOptionTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('questionnaire_question_type_field_option');
        $this->setPhpName('QuestionTypeFieldOption');
        $this->setClassname('Axtion\\Bundle\\QuestionnaireBundle\\Propel\\QuestionTypeFieldOption');
        $this->setPackage('src.Axtion.Bundle.QuestionnaireBundle.Propel');
        $this->setUseIdGenerator(false);
        $this->setIsCrossRef(true);
        // columns
        $this->addForeignPrimaryKey('question_type_id', 'QuestionTypeId', 'INTEGER' , 'questionnaire_question_type', 'id', true, null, null);
        $this->addForeignPrimaryKey('option_id', 'OptionId', 'INTEGER' , 'questionnaire_field_option', 'id', true, null, null);
        $this->addColumn('form_options', 'FormOptions', 'LONGVARCHAR', false, null, null);
        $this->addColumn('hidden', 'Hidden', 'BOOLEAN', true, null, false);
        $this->addColumn('default_value', 'DefaultValue', 'LONGVARCHAR', false, null, null);
        $this->addColumn('array_level', 'ArrayLevel', 'LONGVARCHAR', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('FieldOption', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\FieldOption', RelationMap::MANY_TO_ONE, array('option_id' => 'id', ), null, null);
        $this->addRelation('QuestionType', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\QuestionType', RelationMap::MANY_TO_ONE, array('question_type_id' => 'id', ), null, null);
    } // buildRelations()

} // QuestionTypeFieldOptionTableMap

<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'questionnaire_dependency_question' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Axtion.Bundle.QuestionnaireBundle.Propel.map
 */
class DependencyQuestionTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Axtion.Bundle.QuestionnaireBundle.Propel.map.DependencyQuestionTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('questionnaire_dependency_question');
        $this->setPhpName('DependencyQuestion');
        $this->setClassname('Axtion\\Bundle\\QuestionnaireBundle\\Propel\\DependencyQuestion');
        $this->setPackage('src.Axtion.Bundle.QuestionnaireBundle.Propel');
        $this->setUseIdGenerator(false);
        $this->setIsCrossRef(true);
        // columns
        $this->addForeignPrimaryKey('dependency_id', 'DependencyId', 'INTEGER' , 'questionnaire_dependency', 'id', true, null, null);
        $this->addForeignPrimaryKey('question_id', 'QuestionId', 'INTEGER' , 'questionnaire_question', 'id', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('DependentQuestion', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Question', RelationMap::MANY_TO_ONE, array('question_id' => 'id', ), null, null);
        $this->addRelation('DemandingDependency', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Dependency', RelationMap::MANY_TO_ONE, array('dependency_id' => 'id', ), null, null);
    } // buildRelations()

} // DependencyQuestionTableMap

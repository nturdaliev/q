<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'questionnaire_question_type' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Axtion.Bundle.QuestionnaireBundle.Propel.map
 */
class QuestionTypeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Axtion.Bundle.QuestionnaireBundle.Propel.map.QuestionTypeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('questionnaire_question_type');
        $this->setPhpName('QuestionType');
        $this->setClassname('Axtion\\Bundle\\QuestionnaireBundle\\Propel\\QuestionType');
        $this->setPackage('src.Axtion.Bundle.QuestionnaireBundle.Propel');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('questionnaire_question_type_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('field_type_id', 'FieldTypeId', 'INTEGER', 'questionnaire_field_type', 'id', false, null, null);
        $this->addColumn('has_subquestions', 'HasSubquestions', 'BOOLEAN', true, null, false);
        $this->addColumn('icon', 'Icon', 'VARCHAR', true, 30, null);
        $this->addColumn('may_have_dependency', 'MayHaveDependency', 'BOOLEAN', true, null, false);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('FieldType', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\FieldType', RelationMap::MANY_TO_ONE, array('field_type_id' => 'id', ), null, null);
        $this->addRelation('Question', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Question', RelationMap::ONE_TO_MANY, array('id' => 'question_type_id', ), null, null, 'Questions');
        $this->addRelation('DummyQuestion', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\DummyQuestion', RelationMap::ONE_TO_MANY, array('id' => 'question_type_id', ), null, null, 'DummyQuestions');
        $this->addRelation('QuestionTypeFieldOption', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\QuestionTypeFieldOption', RelationMap::ONE_TO_MANY, array('id' => 'question_type_id', ), null, null, 'QuestionTypeFieldOptions');
        $this->addRelation('QuestionTypeI18n', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\QuestionTypeI18n', RelationMap::ONE_TO_MANY, array('id' => 'id', ), 'CASCADE', null, 'QuestionTypeI18ns');
        $this->addRelation('FieldOption', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\FieldOption', RelationMap::MANY_TO_MANY, array(), null, null, 'FieldOptions');
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'event_dispatcher' =>  array (
),
            'i18n' =>  array (
  'i18n_table' => '%TABLE%_i18n',
  'i18n_phpname' => '%PHPNAME%I18n',
  'i18n_columns' => 'name,description',
  'i18n_pk_name' => NULL,
  'locale_column' => 'locale',
  'default_locale' => 'en',
  'locale_alias' => '',
),
        );
    } // getBehaviors()

} // QuestionTypeTableMap

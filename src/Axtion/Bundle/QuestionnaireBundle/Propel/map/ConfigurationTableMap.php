<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'questionnaire_configuration' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Axtion.Bundle.QuestionnaireBundle.Propel.map
 */
class ConfigurationTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Axtion.Bundle.QuestionnaireBundle.Propel.map.ConfigurationTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('questionnaire_configuration');
        $this->setPhpName('Configuration');
        $this->setClassname('Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Configuration');
        $this->setPackage('src.Axtion.Bundle.QuestionnaireBundle.Propel');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('questionnaire_configuration_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('value', 'Value', 'LONGVARCHAR', true, null, null);
        $this->addForeignKey('device_id', 'DeviceId', 'INTEGER', 'questionnaire_device', 'id', false, null, null);
        $this->addForeignKey('configuration_key_id', 'ConfigurationKeyId', 'INTEGER', 'questionnaire_configuration_key', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Device', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Device', RelationMap::MANY_TO_ONE, array('device_id' => 'id', ), null, null);
        $this->addRelation('ConfigurationKey', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\ConfigurationKey', RelationMap::MANY_TO_ONE, array('configuration_key_id' => 'id', ), null, null);
    } // buildRelations()

} // ConfigurationTableMap

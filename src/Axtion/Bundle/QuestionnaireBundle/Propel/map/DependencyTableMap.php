<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'questionnaire_dependency' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Axtion.Bundle.QuestionnaireBundle.Propel.map
 */
class DependencyTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Axtion.Bundle.QuestionnaireBundle.Propel.map.DependencyTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('questionnaire_dependency');
        $this->setPhpName('Dependency');
        $this->setClassname('Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Dependency');
        $this->setPackage('src.Axtion.Bundle.QuestionnaireBundle.Propel');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('questionnaire_dependency_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('question_id', 'QuestionId', 'INTEGER', 'questionnaire_question', 'id', true, null, null);
        $this->addColumn('value', 'Value', 'VARCHAR', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Question', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Question', RelationMap::MANY_TO_ONE, array('question_id' => 'id', ), null, null);
        $this->addRelation('DependencyQuestion', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\DependencyQuestion', RelationMap::ONE_TO_MANY, array('id' => 'dependency_id', ), null, null, 'DependencyQuestions');
        $this->addRelation('DependentQuestion', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Question', RelationMap::MANY_TO_MANY, array(), null, null, 'DependentQuestions');
    } // buildRelations()

} // DependencyTableMap

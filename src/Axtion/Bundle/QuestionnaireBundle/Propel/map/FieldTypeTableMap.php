<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'questionnaire_field_type' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Axtion.Bundle.QuestionnaireBundle.Propel.map
 */
class FieldTypeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Axtion.Bundle.QuestionnaireBundle.Propel.map.FieldTypeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('questionnaire_field_type');
        $this->setPhpName('FieldType');
        $this->setClassname('Axtion\\Bundle\\QuestionnaireBundle\\Propel\\FieldType');
        $this->setPackage('src.Axtion.Bundle.QuestionnaireBundle.Propel');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('questionnaire_field_type_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 32, null);
        $this->getColumn('name', false)->setPrimaryString(true);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('QuestionType', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\QuestionType', RelationMap::ONE_TO_MANY, array('id' => 'field_type_id', ), null, null, 'QuestionTypes');
        $this->addRelation('FieldOption', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\FieldOption', RelationMap::ONE_TO_MANY, array('id' => 'field_type_id', ), null, null, 'FieldOptions');
    } // buildRelations()

} // FieldTypeTableMap

<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'questionnaire_question' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Axtion.Bundle.QuestionnaireBundle.Propel.map
 */
class QuestionTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Axtion.Bundle.QuestionnaireBundle.Propel.map.QuestionTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('questionnaire_question');
        $this->setPhpName('Question');
        $this->setClassname('Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Question');
        $this->setPackage('src.Axtion.Bundle.QuestionnaireBundle.Propel');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('questionnaire_question_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('parent_id', 'ParentId', 'INTEGER', 'questionnaire_question', 'id', false, null, null);
        $this->addForeignKey('questionnaire_id', 'QuestionnaireId', 'INTEGER', 'questionnaire_questionnaire', 'id', true, null, null);
        $this->addForeignKey('question_type_id', 'QuestionTypeId', 'INTEGER', 'questionnaire_question_type', 'id', false, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, null, null);
        $this->getColumn('name', false)->setPrimaryString(true);
        $this->addColumn('help', 'Help', 'LONGVARCHAR', false, null, null);
        $this->addForeignKey('group_id', 'GroupId', 'INTEGER', 'questionnaire_question_group', 'id', false, null, null);
        $this->addColumn('sortable_rank', 'SortableRank', 'INTEGER', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('QuestionType', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\QuestionType', RelationMap::MANY_TO_ONE, array('question_type_id' => 'id', ), null, null);
        $this->addRelation('Questionnaire', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Questionnaire', RelationMap::MANY_TO_ONE, array('questionnaire_id' => 'id', ), null, null);
        $this->addRelation('Parent', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Question', RelationMap::MANY_TO_ONE, array('parent_id' => 'id', ), null, null);
        $this->addRelation('QuestionGroup', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\QuestionGroup', RelationMap::MANY_TO_ONE, array('group_id' => 'id', ), null, null);
        $this->addRelation('Response', 'Axtion\\Bundle\\AssessmentBundle\\Propel\\Response', RelationMap::ONE_TO_MANY, array('id' => 'question_id', ), null, null, 'Responses');
        $this->addRelation('SubQuestion', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Question', RelationMap::ONE_TO_MANY, array('id' => 'parent_id', ), null, null, 'SubQuestions');
        $this->addRelation('SelectedOption', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\SelectedOption', RelationMap::ONE_TO_MANY, array('id' => 'question_id', ), null, null, 'SelectedOptions');
        $this->addRelation('Dependency', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Dependency', RelationMap::ONE_TO_MANY, array('id' => 'question_id', ), null, null, 'Dependencies');
        $this->addRelation('DependencyQuestion', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\DependencyQuestion', RelationMap::ONE_TO_MANY, array('id' => 'question_id', ), null, null, 'DependencyQuestions');
        $this->addRelation('FieldOption', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\FieldOption', RelationMap::MANY_TO_MANY, array(), null, null, 'FieldOptions');
        $this->addRelation('DemandingDependency', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Dependency', RelationMap::MANY_TO_MANY, array(), null, null, 'DemandingDependencies');
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'sortable' =>  array (
  'rank_column' => 'sortable_rank',
  'use_scope' => 'true',
  'scope_column' => 'group_id',
),
            'timestampable' =>  array (
  'create_column' => 'created_at',
  'update_column' => 'updated_at',
  'disable_updated_at' => 'false',
),
        );
    } // getBehaviors()

} // QuestionTableMap

<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel;

use Axtion\Bundle\QuestionnaireBundle\Propel\om\BaseStatus;

class Status extends BaseStatus
{
    /**
     * Return status name
     *
     * @return string Name
     */
    public function __toString()
    {
        return (string)$this->getName();
    }
}

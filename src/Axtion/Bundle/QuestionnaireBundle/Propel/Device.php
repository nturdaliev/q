<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel;

use Axtion\Bundle\QuestionnaireBundle\Propel\om\BaseDevice;

/**
 * Class Device
 * @package Axtion\Bundle\QuestionnaireBundle\Propel
 */
class Device extends BaseDevice
{

    /**
     * @inheritDoc
     */
    public function __call($name, $params)
    {
        $name = str_replace('config_', '', $name);
        foreach ($this->getConfigurations() as $config) {
            if ($config->getConfigurationKey()->getName() === $name) {
                return $config->getValue();
            }
        }

        return null;
    }


}

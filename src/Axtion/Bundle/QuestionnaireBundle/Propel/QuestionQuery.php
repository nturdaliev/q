<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel;

use Axtion\Bundle\QuestionnaireBundle\Propel\om\BaseQuestionQuery;

class QuestionQuery extends BaseQuestionQuery
{
    /**
     * Filter for grid
     *
     * @param string $locale Locals
     * @return QuestionQuery
     */
    public function filterForGrid($locale)
    {
        return $this
            ->useQuestionnaireQuery()
            ->endUse()
            ->useQuestionTypeQuery()
                ->useI18nQuery($locale)
                ->endUse()
            ->endUse()
            ->withColumn('Questionnaire.Name', 'questionnairename')
            ->withColumn('QuestionTypeI18n.Name', 'questiontypename');
    }
}

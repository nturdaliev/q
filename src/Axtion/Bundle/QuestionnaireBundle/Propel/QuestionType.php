<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel;

use Axtion\Bundle\QuestionnaireBundle\Propel\om\BaseQuestionType;

/**
 * Class QuestionType
 * @package Axtion\Bundle\QuestionnaireBundle\Propel
 */
class QuestionType extends BaseQuestionType
{

    /**
     * Load dummy questions
     *
     * @param Question $question Question
     * @return \PropelObjectCollection|Question[] Questions loaded from dummy table
     */
    public function loadDummyQuestions(Question $question)
    {
        $collection = new \PropelObjectCollection();
        $collection->setModel('Question');

        foreach ($this->getDummyQuestions() as $dummyQuestion) {
            $convertedQuestion = $dummyQuestion->convertoQuestion();
            $convertedQuestion->setParent($question);
            $collection->append($convertedQuestion);
        }

        return $collection;
    }
}

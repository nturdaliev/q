<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel;

use Axtion\Bundle\QuestionnaireBundle\Propel\om\BaseQuestion;
use Axtion\Bundle\UserBundle\Propel\User;
use PropelException;
use PropelPDO;

/**
 * Class Question
 *
 * @package Axtion\Bundle\QuestionnaireBundle\Propel
 */
class Question extends BaseQuestion
{
    /**
     * @var array Question options
     */
    private $options = array();

    /**
     * If this questionnaire belongs to provided user
     *
     * @param User $user
     * @return bool
     */
    public function ofUser(User $user)
    {
        return $user === $this->getQuestionnaire()->getUser();
    }

    /**
     * Code to be run after object hydration
     *
     * @param $row
     * @param int $startcol
     * @param bool $rehydrate
     * @internal param PropelPDO $con
     */
    public function postHydrate($row, $startcol = 0, $rehydrate = false)
    {
        parent::postHydrate($row, $startcol, $rehydrate);
        $this->reloadOptions();
    }


    /**
     * Loads question options according to the question type.
     * First loads default options.
     * If it is not new loads selected options
     * if the question has no selected options yet loads default options.
     */
    private function reloadOptions()
    {
        $this->options = array();

        if (is_null($questionType = $this->getQuestionType())) {
            return;
        }

        $this->loadDefaultOptions($questionType);

        if ($this->isNew()) {
            return;
        }

        $this->loadSelectedOptions($questionType);
    }

    /**
     * This function loads default options of a question type.
     *
     * @param QuestionType $questionType Question type
     * @return bool
     * @throws PropelException
     */
    private function loadDefaultOptions(QuestionType $questionType)
    {
        foreach ($questionType->getQuestionTypeFieldOptions() as $defaultOption) {
            $this->options[$defaultOption->getFieldOption()->getName()] = array(
                'value' => json_decode($defaultOption->getDefaultValue(), true),
                'level' => $defaultOption->getArrayLevel(),
            );
        }
    }

    /**
     * This function loads selected options of a question from selected options table.
     *
     * @param QuestionType $questionType Question type
     * @return SelectedOption[]
     * @throws PropelException
     */
    private function loadSelectedOptions(QuestionType $questionType)
    {
        /** @var SelectedOption[] $userOptions */
        $userOptions = SelectedOptionQuery::create()
            ->filterByQuestion($this)
            ->useFieldOptionQuery()
            ->filterByQuestionType($questionType)
            ->endUse()
            ->find();

        foreach ($userOptions as $userOption) {
            $this->options[$userOption->getFieldOption()->getName()]['value'] = json_decode($userOption->getValue(), true);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function preDelete(PropelPDO $con = null)
    {
        $this->getSubQuestions()->delete();
        $this->getSelectedOptions()->delete();
        $this->getDependencyQuestions()->delete();
        $this->getDemandingDependencies()->delete();
        $this->getDependencies()->delete();

        return parent::preDelete($con);
    }

    /**
     * {@inheritdoc}
     */
    public function preSave(PropelPDO $con = null)
    {
        foreach ($this->getSubQuestions() as $subQuestion) {
            $subQuestion->setParent($this);
            $subQuestion->setQuestionnaire($this->getQuestionnaire());
        }

        return parent::preSave($con);
    }

    /**
     * {@inheritdoc}
     */
    public function postSave(PropelPDO $con = null)
    {
        parent::postSave($con);

        $this->getSelectedOptions()->delete();

        foreach ($this->options as $name => $option) {
            $selectedOption = new SelectedOption();
            $value = json_encode($option['value']);
            //Bad Code :)
            if (strpos($value, 'yyyy hh:ii')) {
                $value = 'dd\/MM\/yyyy hh\:ii';
            }
            $selectedOption->setValue($value);
            $selectedOption->setQuestion($this);
            $selectedOption->setFieldOption(FieldOptionQuery::create()->findOneByName($name));
            $selectedOption->save();
        }
    }

    /**
     * Declares an association between this object and a QuestionType object.
     *
     * @param QuestionType $questionType
     * @return Question The current object (for fluent API support)
     * @throws PropelException
     */
    public function setQuestionType(QuestionType $questionType = null)
    {
        if ($this->getQuestionType() === $questionType) {
            return $this;
        }

        parent::setQuestionType($questionType);
        $this->reloadOptions();

        return $this;
    }

    /**
     * This magic method returns question option
     *
     * @param $name
     * @return mixed
     * @throws PropelException
     */
    public function __get($name)
    {
        if (strpos($name, 'option') !== 0) {
            return null;
        }

        $option = str_replace('option', '', $name);

        if (!array_key_exists($option, $this->options)) {
            return null;
        }

        return $this->options[$option]['value'];
    }

    /**
     * This magic method sets question options value
     * @param $name
     * @param $value
     */
    function __set($name, $value)
    {
        if (strpos($name, 'option') !== 0) {
            return;
        }

        $option = str_replace('option', '', $name);

        if (!array_key_exists($option, $this->options)) {
            return;
        }

        $this->options[$option]['value'] = $value;
    }

    /**
     * Create options
     * @return array Options
     */
    public function createOptions()
    {

        $options = array(
            'label' => $this->getName(),
            'help'  => $this->getHelp(),
        );


        foreach ($this->options as $name => $option) {
            if (is_null($option['value'])) {
                continue;
            }

            if (is_null($option['level'])) {
                $options[$name] = $option['value'];
                continue;
            }

            $levels = array_reverse(explode('.', $option['level']));
            $total = count($levels);
            $result = array();

            for ($i = 0; $i < $total; $i++) {
                if ($i == 0) {
                    $result[$levels[$i]][$name] = $option['value'];
                    continue;
                }

                $result = array($levels[$i] => $result);
            }

            $options = array_merge_recursive($options, $result);
        }

        return $options;
    }


    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }


}


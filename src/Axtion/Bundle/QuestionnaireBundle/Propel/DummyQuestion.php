<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel;

use Axtion\Bundle\QuestionnaireBundle\Propel\om\BaseDummyQuestion;

class DummyQuestion extends BaseDummyQuestion
{
    /**
     * Convert dummy question to question
     *
     * @return Question
     */
    public function convertoQuestion()
    {
        $question = new Question();
        $question->setName($this->getName());
        $question->setHelp($this->getHelp());

        return $question;
    }
}

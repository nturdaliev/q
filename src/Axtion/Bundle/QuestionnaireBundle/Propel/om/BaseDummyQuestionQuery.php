<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Axtion\Bundle\QuestionnaireBundle\Propel\DummyQuestion;
use Axtion\Bundle\QuestionnaireBundle\Propel\DummyQuestionPeer;
use Axtion\Bundle\QuestionnaireBundle\Propel\DummyQuestionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionType;

/**
 * @method DummyQuestionQuery orderById($order = Criteria::ASC) Order by the id column
 * @method DummyQuestionQuery orderByQuestionTypeId($order = Criteria::ASC) Order by the question_type_id column
 * @method DummyQuestionQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method DummyQuestionQuery orderByHelp($order = Criteria::ASC) Order by the help column
 *
 * @method DummyQuestionQuery groupById() Group by the id column
 * @method DummyQuestionQuery groupByQuestionTypeId() Group by the question_type_id column
 * @method DummyQuestionQuery groupByName() Group by the name column
 * @method DummyQuestionQuery groupByHelp() Group by the help column
 *
 * @method DummyQuestionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method DummyQuestionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method DummyQuestionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method DummyQuestionQuery leftJoinQuestionType($relationAlias = null) Adds a LEFT JOIN clause to the query using the QuestionType relation
 * @method DummyQuestionQuery rightJoinQuestionType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the QuestionType relation
 * @method DummyQuestionQuery innerJoinQuestionType($relationAlias = null) Adds a INNER JOIN clause to the query using the QuestionType relation
 *
 * @method DummyQuestion findOne(PropelPDO $con = null) Return the first DummyQuestion matching the query
 * @method DummyQuestion findOneOrCreate(PropelPDO $con = null) Return the first DummyQuestion matching the query, or a new DummyQuestion object populated from the query conditions when no match is found
 *
 * @method DummyQuestion findOneByQuestionTypeId(int $question_type_id) Return the first DummyQuestion filtered by the question_type_id column
 * @method DummyQuestion findOneByName(string $name) Return the first DummyQuestion filtered by the name column
 * @method DummyQuestion findOneByHelp(string $help) Return the first DummyQuestion filtered by the help column
 *
 * @method array findById(int $id) Return DummyQuestion objects filtered by the id column
 * @method array findByQuestionTypeId(int $question_type_id) Return DummyQuestion objects filtered by the question_type_id column
 * @method array findByName(string $name) Return DummyQuestion objects filtered by the name column
 * @method array findByHelp(string $help) Return DummyQuestion objects filtered by the help column
 */
abstract class BaseDummyQuestionQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseDummyQuestionQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\DummyQuestion';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new DummyQuestionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   DummyQuestionQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return DummyQuestionQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof DummyQuestionQuery) {
            return $criteria;
        }
        $query = new DummyQuestionQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   DummyQuestion|DummyQuestion[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = DummyQuestionPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(DummyQuestionPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 DummyQuestion A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 DummyQuestion A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "id", "question_type_id", "name", "help" FROM "questionnaire_dummy_question" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new DummyQuestion();
            $obj->hydrate($row);
            DummyQuestionPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return DummyQuestion|DummyQuestion[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|DummyQuestion[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return DummyQuestionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(DummyQuestionPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return DummyQuestionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(DummyQuestionPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DummyQuestionQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(DummyQuestionPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(DummyQuestionPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DummyQuestionPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the question_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestionTypeId(1234); // WHERE question_type_id = 1234
     * $query->filterByQuestionTypeId(array(12, 34)); // WHERE question_type_id IN (12, 34)
     * $query->filterByQuestionTypeId(array('min' => 12)); // WHERE question_type_id >= 12
     * $query->filterByQuestionTypeId(array('max' => 12)); // WHERE question_type_id <= 12
     * </code>
     *
     * @see       filterByQuestionType()
     *
     * @param     mixed $questionTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DummyQuestionQuery The current query, for fluid interface
     */
    public function filterByQuestionTypeId($questionTypeId = null, $comparison = null)
    {
        if (is_array($questionTypeId)) {
            $useMinMax = false;
            if (isset($questionTypeId['min'])) {
                $this->addUsingAlias(DummyQuestionPeer::QUESTION_TYPE_ID, $questionTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($questionTypeId['max'])) {
                $this->addUsingAlias(DummyQuestionPeer::QUESTION_TYPE_ID, $questionTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DummyQuestionPeer::QUESTION_TYPE_ID, $questionTypeId, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DummyQuestionQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DummyQuestionPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the help column
     *
     * Example usage:
     * <code>
     * $query->filterByHelp('fooValue');   // WHERE help = 'fooValue'
     * $query->filterByHelp('%fooValue%'); // WHERE help LIKE '%fooValue%'
     * </code>
     *
     * @param     string $help The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DummyQuestionQuery The current query, for fluid interface
     */
    public function filterByHelp($help = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($help)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $help)) {
                $help = str_replace('*', '%', $help);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DummyQuestionPeer::HELP, $help, $comparison);
    }

    /**
     * Filter the query by a related QuestionType object
     *
     * @param   QuestionType|PropelObjectCollection $questionType The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 DummyQuestionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByQuestionType($questionType, $comparison = null)
    {
        if ($questionType instanceof QuestionType) {
            return $this
                ->addUsingAlias(DummyQuestionPeer::QUESTION_TYPE_ID, $questionType->getId(), $comparison);
        } elseif ($questionType instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DummyQuestionPeer::QUESTION_TYPE_ID, $questionType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByQuestionType() only accepts arguments of type QuestionType or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the QuestionType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return DummyQuestionQuery The current query, for fluid interface
     */
    public function joinQuestionType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('QuestionType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'QuestionType');
        }

        return $this;
    }

    /**
     * Use the QuestionType relation QuestionType object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeQuery A secondary query class using the current class as primary query
     */
    public function useQuestionTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinQuestionType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'QuestionType', '\Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   DummyQuestion $dummyQuestion Object to remove from the list of results
     *
     * @return DummyQuestionQuery The current query, for fluid interface
     */
    public function prune($dummyQuestion = null)
    {
        if ($dummyQuestion) {
            $this->addUsingAlias(DummyQuestionPeer::ID, $dummyQuestion->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

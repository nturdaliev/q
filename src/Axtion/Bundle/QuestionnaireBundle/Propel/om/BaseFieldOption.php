<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOption;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionPeer;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionTab;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionTabQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldType;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldTypeQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionType;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeFieldOption;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeFieldOptionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\SelectedOption;
use Axtion\Bundle\QuestionnaireBundle\Propel\SelectedOptionQuery;

abstract class BaseFieldOption extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\FieldOptionPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        FieldOptionPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the field_type_id field.
     * @var        int
     */
    protected $field_type_id;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the field_option_tab_id field.
     * @var        int
     */
    protected $field_option_tab_id;

    /**
     * @var        FieldType
     */
    protected $aFieldType;

    /**
     * @var        FieldOptionTab
     */
    protected $aFieldOptionTab;

    /**
     * @var        PropelObjectCollection|SelectedOption[] Collection to store aggregation of SelectedOption objects.
     */
    protected $collSelectedOptions;
    protected $collSelectedOptionsPartial;

    /**
     * @var        PropelObjectCollection|QuestionTypeFieldOption[] Collection to store aggregation of QuestionTypeFieldOption objects.
     */
    protected $collQuestionTypeFieldOptions;
    protected $collQuestionTypeFieldOptionsPartial;

    /**
     * @var        PropelObjectCollection|Question[] Collection to store aggregation of Question objects.
     */
    protected $collQuestions;

    /**
     * @var        PropelObjectCollection|QuestionType[] Collection to store aggregation of QuestionType objects.
     */
    protected $collQuestionTypes;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $questionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $questionTypesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selectedOptionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $questionTypeFieldOptionsScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [field_type_id] column value.
     *
     * @return int
     */
    public function getFieldTypeId()
    {

        return $this->field_type_id;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * Get the [field_option_tab_id] column value.
     *
     * @return int
     */
    public function getFieldOptionTabId()
    {

        return $this->field_option_tab_id;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return FieldOption The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = FieldOptionPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [field_type_id] column.
     *
     * @param  int $v new value
     * @return FieldOption The current object (for fluent API support)
     */
    public function setFieldTypeId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->field_type_id !== $v) {
            $this->field_type_id = $v;
            $this->modifiedColumns[] = FieldOptionPeer::FIELD_TYPE_ID;
        }

        if ($this->aFieldType !== null && $this->aFieldType->getId() !== $v) {
            $this->aFieldType = null;
        }


        return $this;
    } // setFieldTypeId()

    /**
     * Set the value of [name] column.
     *
     * @param  string $v new value
     * @return FieldOption The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = FieldOptionPeer::NAME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [field_option_tab_id] column.
     *
     * @param  int $v new value
     * @return FieldOption The current object (for fluent API support)
     */
    public function setFieldOptionTabId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->field_option_tab_id !== $v) {
            $this->field_option_tab_id = $v;
            $this->modifiedColumns[] = FieldOptionPeer::FIELD_OPTION_TAB_ID;
        }

        if ($this->aFieldOptionTab !== null && $this->aFieldOptionTab->getId() !== $v) {
            $this->aFieldOptionTab = null;
        }


        return $this;
    } // setFieldOptionTabId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->field_type_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->name = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->field_option_tab_id = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 4; // 4 = FieldOptionPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating FieldOption object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aFieldType !== null && $this->field_type_id !== $this->aFieldType->getId()) {
            $this->aFieldType = null;
        }
        if ($this->aFieldOptionTab !== null && $this->field_option_tab_id !== $this->aFieldOptionTab->getId()) {
            $this->aFieldOptionTab = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(FieldOptionPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = FieldOptionPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aFieldType = null;
            $this->aFieldOptionTab = null;
            $this->collSelectedOptions = null;

            $this->collQuestionTypeFieldOptions = null;

            $this->collQuestions = null;
            $this->collQuestionTypes = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(FieldOptionPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = FieldOptionQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(FieldOptionPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                FieldOptionPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aFieldType !== null) {
                if ($this->aFieldType->isModified() || $this->aFieldType->isNew()) {
                    $affectedRows += $this->aFieldType->save($con);
                }
                $this->setFieldType($this->aFieldType);
            }

            if ($this->aFieldOptionTab !== null) {
                if ($this->aFieldOptionTab->isModified() || $this->aFieldOptionTab->isNew()) {
                    $affectedRows += $this->aFieldOptionTab->save($con);
                }
                $this->setFieldOptionTab($this->aFieldOptionTab);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->questionsScheduledForDeletion !== null) {
                if (!$this->questionsScheduledForDeletion->isEmpty()) {
                    $pks = array();
                    $pk = $this->getPrimaryKey();
                    foreach ($this->questionsScheduledForDeletion->getPrimaryKeys(false) as $remotePk) {
                        $pks[] = array($pk, $remotePk);
                    }
                    SelectedOptionQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);
                    $this->questionsScheduledForDeletion = null;
                }

                foreach ($this->getQuestions() as $question) {
                    if ($question->isModified()) {
                        $question->save($con);
                    }
                }
            } elseif ($this->collQuestions) {
                foreach ($this->collQuestions as $question) {
                    if ($question->isModified()) {
                        $question->save($con);
                    }
                }
            }

            if ($this->questionTypesScheduledForDeletion !== null) {
                if (!$this->questionTypesScheduledForDeletion->isEmpty()) {
                    $pks = array();
                    $pk = $this->getPrimaryKey();
                    foreach ($this->questionTypesScheduledForDeletion->getPrimaryKeys(false) as $remotePk) {
                        $pks[] = array($pk, $remotePk);
                    }
                    QuestionTypeFieldOptionQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);
                    $this->questionTypesScheduledForDeletion = null;
                }

                foreach ($this->getQuestionTypes() as $questionType) {
                    if ($questionType->isModified()) {
                        $questionType->save($con);
                    }
                }
            } elseif ($this->collQuestionTypes) {
                foreach ($this->collQuestionTypes as $questionType) {
                    if ($questionType->isModified()) {
                        $questionType->save($con);
                    }
                }
            }

            if ($this->selectedOptionsScheduledForDeletion !== null) {
                if (!$this->selectedOptionsScheduledForDeletion->isEmpty()) {
                    SelectedOptionQuery::create()
                        ->filterByPrimaryKeys($this->selectedOptionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selectedOptionsScheduledForDeletion = null;
                }
            }

            if ($this->collSelectedOptions !== null) {
                foreach ($this->collSelectedOptions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->questionTypeFieldOptionsScheduledForDeletion !== null) {
                if (!$this->questionTypeFieldOptionsScheduledForDeletion->isEmpty()) {
                    QuestionTypeFieldOptionQuery::create()
                        ->filterByPrimaryKeys($this->questionTypeFieldOptionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->questionTypeFieldOptionsScheduledForDeletion = null;
                }
            }

            if ($this->collQuestionTypeFieldOptions !== null) {
                foreach ($this->collQuestionTypeFieldOptions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = FieldOptionPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . FieldOptionPeer::ID . ')');
        }
        if (null === $this->id) {
            try {
                $stmt = $con->query("SELECT nextval('questionnaire_field_option_id_seq')");
                $row = $stmt->fetch(PDO::FETCH_NUM);
                $this->id = $row[0];
            } catch (Exception $e) {
                throw new PropelException('Unable to get sequence id.', $e);
            }
        }


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(FieldOptionPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '"id"';
        }
        if ($this->isColumnModified(FieldOptionPeer::FIELD_TYPE_ID)) {
            $modifiedColumns[':p' . $index++]  = '"field_type_id"';
        }
        if ($this->isColumnModified(FieldOptionPeer::NAME)) {
            $modifiedColumns[':p' . $index++]  = '"name"';
        }
        if ($this->isColumnModified(FieldOptionPeer::FIELD_OPTION_TAB_ID)) {
            $modifiedColumns[':p' . $index++]  = '"field_option_tab_id"';
        }

        $sql = sprintf(
            'INSERT INTO "questionnaire_field_option" (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '"id"':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '"field_type_id"':
                        $stmt->bindValue($identifier, $this->field_type_id, PDO::PARAM_INT);
                        break;
                    case '"name"':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '"field_option_tab_id"':
                        $stmt->bindValue($identifier, $this->field_option_tab_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aFieldType !== null) {
                if (!$this->aFieldType->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aFieldType->getValidationFailures());
                }
            }

            if ($this->aFieldOptionTab !== null) {
                if (!$this->aFieldOptionTab->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aFieldOptionTab->getValidationFailures());
                }
            }


            if (($retval = FieldOptionPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collSelectedOptions !== null) {
                    foreach ($this->collSelectedOptions as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collQuestionTypeFieldOptions !== null) {
                    foreach ($this->collQuestionTypeFieldOptions as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = FieldOptionPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getFieldTypeId();
                break;
            case 2:
                return $this->getName();
                break;
            case 3:
                return $this->getFieldOptionTabId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['FieldOption'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['FieldOption'][$this->getPrimaryKey()] = true;
        $keys = FieldOptionPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getFieldTypeId(),
            $keys[2] => $this->getName(),
            $keys[3] => $this->getFieldOptionTabId(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aFieldType) {
                $result['FieldType'] = $this->aFieldType->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aFieldOptionTab) {
                $result['FieldOptionTab'] = $this->aFieldOptionTab->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSelectedOptions) {
                $result['SelectedOptions'] = $this->collSelectedOptions->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collQuestionTypeFieldOptions) {
                $result['QuestionTypeFieldOptions'] = $this->collQuestionTypeFieldOptions->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = FieldOptionPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setFieldTypeId($value);
                break;
            case 2:
                $this->setName($value);
                break;
            case 3:
                $this->setFieldOptionTabId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = FieldOptionPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setFieldTypeId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setName($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setFieldOptionTabId($arr[$keys[3]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(FieldOptionPeer::DATABASE_NAME);

        if ($this->isColumnModified(FieldOptionPeer::ID)) $criteria->add(FieldOptionPeer::ID, $this->id);
        if ($this->isColumnModified(FieldOptionPeer::FIELD_TYPE_ID)) $criteria->add(FieldOptionPeer::FIELD_TYPE_ID, $this->field_type_id);
        if ($this->isColumnModified(FieldOptionPeer::NAME)) $criteria->add(FieldOptionPeer::NAME, $this->name);
        if ($this->isColumnModified(FieldOptionPeer::FIELD_OPTION_TAB_ID)) $criteria->add(FieldOptionPeer::FIELD_OPTION_TAB_ID, $this->field_option_tab_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(FieldOptionPeer::DATABASE_NAME);
        $criteria->add(FieldOptionPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of FieldOption (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setFieldTypeId($this->getFieldTypeId());
        $copyObj->setName($this->getName());
        $copyObj->setFieldOptionTabId($this->getFieldOptionTabId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getSelectedOptions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelectedOption($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getQuestionTypeFieldOptions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addQuestionTypeFieldOption($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return FieldOption Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return FieldOptionPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new FieldOptionPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a FieldType object.
     *
     * @param                  FieldType $v
     * @return FieldOption The current object (for fluent API support)
     * @throws PropelException
     */
    public function setFieldType(FieldType $v = null)
    {
        if ($v === null) {
            $this->setFieldTypeId(NULL);
        } else {
            $this->setFieldTypeId($v->getId());
        }

        $this->aFieldType = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the FieldType object, it will not be re-added.
        if ($v !== null) {
            $v->addFieldOption($this);
        }


        return $this;
    }


    /**
     * Get the associated FieldType object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return FieldType The associated FieldType object.
     * @throws PropelException
     */
    public function getFieldType(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aFieldType === null && ($this->field_type_id !== null) && $doQuery) {
            $this->aFieldType = FieldTypeQuery::create()->findPk($this->field_type_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aFieldType->addFieldOptions($this);
             */
        }

        return $this->aFieldType;
    }

    /**
     * Declares an association between this object and a FieldOptionTab object.
     *
     * @param                  FieldOptionTab $v
     * @return FieldOption The current object (for fluent API support)
     * @throws PropelException
     */
    public function setFieldOptionTab(FieldOptionTab $v = null)
    {
        if ($v === null) {
            $this->setFieldOptionTabId(NULL);
        } else {
            $this->setFieldOptionTabId($v->getId());
        }

        $this->aFieldOptionTab = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the FieldOptionTab object, it will not be re-added.
        if ($v !== null) {
            $v->addFieldOption($this);
        }


        return $this;
    }


    /**
     * Get the associated FieldOptionTab object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return FieldOptionTab The associated FieldOptionTab object.
     * @throws PropelException
     */
    public function getFieldOptionTab(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aFieldOptionTab === null && ($this->field_option_tab_id !== null) && $doQuery) {
            $this->aFieldOptionTab = FieldOptionTabQuery::create()->findPk($this->field_option_tab_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aFieldOptionTab->addFieldOptions($this);
             */
        }

        return $this->aFieldOptionTab;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SelectedOption' == $relationName) {
            $this->initSelectedOptions();
        }
        if ('QuestionTypeFieldOption' == $relationName) {
            $this->initQuestionTypeFieldOptions();
        }
    }

    /**
     * Clears out the collSelectedOptions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return FieldOption The current object (for fluent API support)
     * @see        addSelectedOptions()
     */
    public function clearSelectedOptions()
    {
        $this->collSelectedOptions = null; // important to set this to null since that means it is uninitialized
        $this->collSelectedOptionsPartial = null;

        return $this;
    }

    /**
     * reset is the collSelectedOptions collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelectedOptions($v = true)
    {
        $this->collSelectedOptionsPartial = $v;
    }

    /**
     * Initializes the collSelectedOptions collection.
     *
     * By default this just sets the collSelectedOptions collection to an empty array (like clearcollSelectedOptions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelectedOptions($overrideExisting = true)
    {
        if (null !== $this->collSelectedOptions && !$overrideExisting) {
            return;
        }
        $this->collSelectedOptions = new PropelObjectCollection();
        $this->collSelectedOptions->setModel('SelectedOption');
    }

    /**
     * Gets an array of SelectedOption objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this FieldOption is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelectedOption[] List of SelectedOption objects
     * @throws PropelException
     */
    public function getSelectedOptions($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelectedOptionsPartial && !$this->isNew();
        if (null === $this->collSelectedOptions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelectedOptions) {
                // return empty collection
                $this->initSelectedOptions();
            } else {
                $collSelectedOptions = SelectedOptionQuery::create(null, $criteria)
                    ->filterByFieldOption($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelectedOptionsPartial && count($collSelectedOptions)) {
                      $this->initSelectedOptions(false);

                      foreach ($collSelectedOptions as $obj) {
                        if (false == $this->collSelectedOptions->contains($obj)) {
                          $this->collSelectedOptions->append($obj);
                        }
                      }

                      $this->collSelectedOptionsPartial = true;
                    }

                    $collSelectedOptions->getInternalIterator()->rewind();

                    return $collSelectedOptions;
                }

                if ($partial && $this->collSelectedOptions) {
                    foreach ($this->collSelectedOptions as $obj) {
                        if ($obj->isNew()) {
                            $collSelectedOptions[] = $obj;
                        }
                    }
                }

                $this->collSelectedOptions = $collSelectedOptions;
                $this->collSelectedOptionsPartial = false;
            }
        }

        return $this->collSelectedOptions;
    }

    /**
     * Sets a collection of SelectedOption objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selectedOptions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return FieldOption The current object (for fluent API support)
     */
    public function setSelectedOptions(PropelCollection $selectedOptions, PropelPDO $con = null)
    {
        $selectedOptionsToDelete = $this->getSelectedOptions(new Criteria(), $con)->diff($selectedOptions);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->selectedOptionsScheduledForDeletion = clone $selectedOptionsToDelete;

        foreach ($selectedOptionsToDelete as $selectedOptionRemoved) {
            $selectedOptionRemoved->setFieldOption(null);
        }

        $this->collSelectedOptions = null;
        foreach ($selectedOptions as $selectedOption) {
            $this->addSelectedOption($selectedOption);
        }

        $this->collSelectedOptions = $selectedOptions;
        $this->collSelectedOptionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelectedOption objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelectedOption objects.
     * @throws PropelException
     */
    public function countSelectedOptions(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelectedOptionsPartial && !$this->isNew();
        if (null === $this->collSelectedOptions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelectedOptions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSelectedOptions());
            }
            $query = SelectedOptionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByFieldOption($this)
                ->count($con);
        }

        return count($this->collSelectedOptions);
    }

    /**
     * Method called to associate a SelectedOption object to this object
     * through the SelectedOption foreign key attribute.
     *
     * @param    SelectedOption $l SelectedOption
     * @return FieldOption The current object (for fluent API support)
     */
    public function addSelectedOption(SelectedOption $l)
    {
        if ($this->collSelectedOptions === null) {
            $this->initSelectedOptions();
            $this->collSelectedOptionsPartial = true;
        }

        if (!in_array($l, $this->collSelectedOptions->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelectedOption($l);

            if ($this->selectedOptionsScheduledForDeletion and $this->selectedOptionsScheduledForDeletion->contains($l)) {
                $this->selectedOptionsScheduledForDeletion->remove($this->selectedOptionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	SelectedOption $selectedOption The selectedOption object to add.
     */
    protected function doAddSelectedOption($selectedOption)
    {
        $this->collSelectedOptions[]= $selectedOption;
        $selectedOption->setFieldOption($this);
    }

    /**
     * @param	SelectedOption $selectedOption The selectedOption object to remove.
     * @return FieldOption The current object (for fluent API support)
     */
    public function removeSelectedOption($selectedOption)
    {
        if ($this->getSelectedOptions()->contains($selectedOption)) {
            $this->collSelectedOptions->remove($this->collSelectedOptions->search($selectedOption));
            if (null === $this->selectedOptionsScheduledForDeletion) {
                $this->selectedOptionsScheduledForDeletion = clone $this->collSelectedOptions;
                $this->selectedOptionsScheduledForDeletion->clear();
            }
            $this->selectedOptionsScheduledForDeletion[]= clone $selectedOption;
            $selectedOption->setFieldOption(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this FieldOption is new, it will return
     * an empty collection; or if this FieldOption has previously
     * been saved, it will retrieve related SelectedOptions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in FieldOption.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelectedOption[] List of SelectedOption objects
     */
    public function getSelectedOptionsJoinQuestion($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelectedOptionQuery::create(null, $criteria);
        $query->joinWith('Question', $join_behavior);

        return $this->getSelectedOptions($query, $con);
    }

    /**
     * Clears out the collQuestionTypeFieldOptions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return FieldOption The current object (for fluent API support)
     * @see        addQuestionTypeFieldOptions()
     */
    public function clearQuestionTypeFieldOptions()
    {
        $this->collQuestionTypeFieldOptions = null; // important to set this to null since that means it is uninitialized
        $this->collQuestionTypeFieldOptionsPartial = null;

        return $this;
    }

    /**
     * reset is the collQuestionTypeFieldOptions collection loaded partially
     *
     * @return void
     */
    public function resetPartialQuestionTypeFieldOptions($v = true)
    {
        $this->collQuestionTypeFieldOptionsPartial = $v;
    }

    /**
     * Initializes the collQuestionTypeFieldOptions collection.
     *
     * By default this just sets the collQuestionTypeFieldOptions collection to an empty array (like clearcollQuestionTypeFieldOptions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initQuestionTypeFieldOptions($overrideExisting = true)
    {
        if (null !== $this->collQuestionTypeFieldOptions && !$overrideExisting) {
            return;
        }
        $this->collQuestionTypeFieldOptions = new PropelObjectCollection();
        $this->collQuestionTypeFieldOptions->setModel('QuestionTypeFieldOption');
    }

    /**
     * Gets an array of QuestionTypeFieldOption objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this FieldOption is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|QuestionTypeFieldOption[] List of QuestionTypeFieldOption objects
     * @throws PropelException
     */
    public function getQuestionTypeFieldOptions($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collQuestionTypeFieldOptionsPartial && !$this->isNew();
        if (null === $this->collQuestionTypeFieldOptions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collQuestionTypeFieldOptions) {
                // return empty collection
                $this->initQuestionTypeFieldOptions();
            } else {
                $collQuestionTypeFieldOptions = QuestionTypeFieldOptionQuery::create(null, $criteria)
                    ->filterByFieldOption($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collQuestionTypeFieldOptionsPartial && count($collQuestionTypeFieldOptions)) {
                      $this->initQuestionTypeFieldOptions(false);

                      foreach ($collQuestionTypeFieldOptions as $obj) {
                        if (false == $this->collQuestionTypeFieldOptions->contains($obj)) {
                          $this->collQuestionTypeFieldOptions->append($obj);
                        }
                      }

                      $this->collQuestionTypeFieldOptionsPartial = true;
                    }

                    $collQuestionTypeFieldOptions->getInternalIterator()->rewind();

                    return $collQuestionTypeFieldOptions;
                }

                if ($partial && $this->collQuestionTypeFieldOptions) {
                    foreach ($this->collQuestionTypeFieldOptions as $obj) {
                        if ($obj->isNew()) {
                            $collQuestionTypeFieldOptions[] = $obj;
                        }
                    }
                }

                $this->collQuestionTypeFieldOptions = $collQuestionTypeFieldOptions;
                $this->collQuestionTypeFieldOptionsPartial = false;
            }
        }

        return $this->collQuestionTypeFieldOptions;
    }

    /**
     * Sets a collection of QuestionTypeFieldOption objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $questionTypeFieldOptions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return FieldOption The current object (for fluent API support)
     */
    public function setQuestionTypeFieldOptions(PropelCollection $questionTypeFieldOptions, PropelPDO $con = null)
    {
        $questionTypeFieldOptionsToDelete = $this->getQuestionTypeFieldOptions(new Criteria(), $con)->diff($questionTypeFieldOptions);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->questionTypeFieldOptionsScheduledForDeletion = clone $questionTypeFieldOptionsToDelete;

        foreach ($questionTypeFieldOptionsToDelete as $questionTypeFieldOptionRemoved) {
            $questionTypeFieldOptionRemoved->setFieldOption(null);
        }

        $this->collQuestionTypeFieldOptions = null;
        foreach ($questionTypeFieldOptions as $questionTypeFieldOption) {
            $this->addQuestionTypeFieldOption($questionTypeFieldOption);
        }

        $this->collQuestionTypeFieldOptions = $questionTypeFieldOptions;
        $this->collQuestionTypeFieldOptionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related QuestionTypeFieldOption objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related QuestionTypeFieldOption objects.
     * @throws PropelException
     */
    public function countQuestionTypeFieldOptions(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collQuestionTypeFieldOptionsPartial && !$this->isNew();
        if (null === $this->collQuestionTypeFieldOptions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collQuestionTypeFieldOptions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getQuestionTypeFieldOptions());
            }
            $query = QuestionTypeFieldOptionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByFieldOption($this)
                ->count($con);
        }

        return count($this->collQuestionTypeFieldOptions);
    }

    /**
     * Method called to associate a QuestionTypeFieldOption object to this object
     * through the QuestionTypeFieldOption foreign key attribute.
     *
     * @param    QuestionTypeFieldOption $l QuestionTypeFieldOption
     * @return FieldOption The current object (for fluent API support)
     */
    public function addQuestionTypeFieldOption(QuestionTypeFieldOption $l)
    {
        if ($this->collQuestionTypeFieldOptions === null) {
            $this->initQuestionTypeFieldOptions();
            $this->collQuestionTypeFieldOptionsPartial = true;
        }

        if (!in_array($l, $this->collQuestionTypeFieldOptions->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddQuestionTypeFieldOption($l);

            if ($this->questionTypeFieldOptionsScheduledForDeletion and $this->questionTypeFieldOptionsScheduledForDeletion->contains($l)) {
                $this->questionTypeFieldOptionsScheduledForDeletion->remove($this->questionTypeFieldOptionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	QuestionTypeFieldOption $questionTypeFieldOption The questionTypeFieldOption object to add.
     */
    protected function doAddQuestionTypeFieldOption($questionTypeFieldOption)
    {
        $this->collQuestionTypeFieldOptions[]= $questionTypeFieldOption;
        $questionTypeFieldOption->setFieldOption($this);
    }

    /**
     * @param	QuestionTypeFieldOption $questionTypeFieldOption The questionTypeFieldOption object to remove.
     * @return FieldOption The current object (for fluent API support)
     */
    public function removeQuestionTypeFieldOption($questionTypeFieldOption)
    {
        if ($this->getQuestionTypeFieldOptions()->contains($questionTypeFieldOption)) {
            $this->collQuestionTypeFieldOptions->remove($this->collQuestionTypeFieldOptions->search($questionTypeFieldOption));
            if (null === $this->questionTypeFieldOptionsScheduledForDeletion) {
                $this->questionTypeFieldOptionsScheduledForDeletion = clone $this->collQuestionTypeFieldOptions;
                $this->questionTypeFieldOptionsScheduledForDeletion->clear();
            }
            $this->questionTypeFieldOptionsScheduledForDeletion[]= clone $questionTypeFieldOption;
            $questionTypeFieldOption->setFieldOption(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this FieldOption is new, it will return
     * an empty collection; or if this FieldOption has previously
     * been saved, it will retrieve related QuestionTypeFieldOptions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in FieldOption.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|QuestionTypeFieldOption[] List of QuestionTypeFieldOption objects
     */
    public function getQuestionTypeFieldOptionsJoinQuestionType($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = QuestionTypeFieldOptionQuery::create(null, $criteria);
        $query->joinWith('QuestionType', $join_behavior);

        return $this->getQuestionTypeFieldOptions($query, $con);
    }

    /**
     * Clears out the collQuestions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return FieldOption The current object (for fluent API support)
     * @see        addQuestions()
     */
    public function clearQuestions()
    {
        $this->collQuestions = null; // important to set this to null since that means it is uninitialized
        $this->collQuestionsPartial = null;

        return $this;
    }

    /**
     * Initializes the collQuestions collection.
     *
     * By default this just sets the collQuestions collection to an empty collection (like clearQuestions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initQuestions()
    {
        $this->collQuestions = new PropelObjectCollection();
        $this->collQuestions->setModel('Question');
    }

    /**
     * Gets a collection of Question objects related by a many-to-many relationship
     * to the current object by way of the questionnaire_selected_option cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this FieldOption is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param PropelPDO $con Optional connection object
     *
     * @return PropelObjectCollection|Question[] List of Question objects
     */
    public function getQuestions($criteria = null, PropelPDO $con = null)
    {
        if (null === $this->collQuestions || null !== $criteria) {
            if ($this->isNew() && null === $this->collQuestions) {
                // return empty collection
                $this->initQuestions();
            } else {
                $collQuestions = QuestionQuery::create(null, $criteria)
                    ->filterByFieldOption($this)
                    ->find($con);
                if (null !== $criteria) {
                    return $collQuestions;
                }
                $this->collQuestions = $collQuestions;
            }
        }

        return $this->collQuestions;
    }

    /**
     * Sets a collection of Question objects related by a many-to-many relationship
     * to the current object by way of the questionnaire_selected_option cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $questions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return FieldOption The current object (for fluent API support)
     */
    public function setQuestions(PropelCollection $questions, PropelPDO $con = null)
    {
        $this->clearQuestions();
        $currentQuestions = $this->getQuestions(null, $con);

        $this->questionsScheduledForDeletion = $currentQuestions->diff($questions);

        foreach ($questions as $question) {
            if (!$currentQuestions->contains($question)) {
                $this->doAddQuestion($question);
            }
        }

        $this->collQuestions = $questions;

        return $this;
    }

    /**
     * Gets the number of Question objects related by a many-to-many relationship
     * to the current object by way of the questionnaire_selected_option cross-reference table.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param boolean $distinct Set to true to force count distinct
     * @param PropelPDO $con Optional connection object
     *
     * @return int the number of related Question objects
     */
    public function countQuestions($criteria = null, $distinct = false, PropelPDO $con = null)
    {
        if (null === $this->collQuestions || null !== $criteria) {
            if ($this->isNew() && null === $this->collQuestions) {
                return 0;
            } else {
                $query = QuestionQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByFieldOption($this)
                    ->count($con);
            }
        } else {
            return count($this->collQuestions);
        }
    }

    /**
     * Associate a Question object to this object
     * through the questionnaire_selected_option cross reference table.
     *
     * @param  Question $question The SelectedOption object to relate
     * @return FieldOption The current object (for fluent API support)
     */
    public function addQuestion(Question $question)
    {
        if ($this->collQuestions === null) {
            $this->initQuestions();
        }

        if (!$this->collQuestions->contains($question)) { // only add it if the **same** object is not already associated
            $this->doAddQuestion($question);
            $this->collQuestions[] = $question;

            if ($this->questionsScheduledForDeletion and $this->questionsScheduledForDeletion->contains($question)) {
                $this->questionsScheduledForDeletion->remove($this->questionsScheduledForDeletion->search($question));
            }
        }

        return $this;
    }

    /**
     * @param	Question $question The question object to add.
     */
    protected function doAddQuestion(Question $question)
    {
        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$question->getFieldOptions()->contains($this)) { $selectedOption = new SelectedOption();
            $selectedOption->setQuestion($question);
            $this->addSelectedOption($selectedOption);

            $foreignCollection = $question->getFieldOptions();
            $foreignCollection[] = $this;
        }
    }

    /**
     * Remove a Question object to this object
     * through the questionnaire_selected_option cross reference table.
     *
     * @param Question $question The SelectedOption object to relate
     * @return FieldOption The current object (for fluent API support)
     */
    public function removeQuestion(Question $question)
    {
        if ($this->getQuestions()->contains($question)) {
            $this->collQuestions->remove($this->collQuestions->search($question));
            if (null === $this->questionsScheduledForDeletion) {
                $this->questionsScheduledForDeletion = clone $this->collQuestions;
                $this->questionsScheduledForDeletion->clear();
            }
            $this->questionsScheduledForDeletion[]= $question;
        }

        return $this;
    }

    /**
     * Clears out the collQuestionTypes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return FieldOption The current object (for fluent API support)
     * @see        addQuestionTypes()
     */
    public function clearQuestionTypes()
    {
        $this->collQuestionTypes = null; // important to set this to null since that means it is uninitialized
        $this->collQuestionTypesPartial = null;

        return $this;
    }

    /**
     * Initializes the collQuestionTypes collection.
     *
     * By default this just sets the collQuestionTypes collection to an empty collection (like clearQuestionTypes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initQuestionTypes()
    {
        $this->collQuestionTypes = new PropelObjectCollection();
        $this->collQuestionTypes->setModel('QuestionType');
    }

    /**
     * Gets a collection of QuestionType objects related by a many-to-many relationship
     * to the current object by way of the questionnaire_question_type_field_option cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this FieldOption is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param PropelPDO $con Optional connection object
     *
     * @return PropelObjectCollection|QuestionType[] List of QuestionType objects
     */
    public function getQuestionTypes($criteria = null, PropelPDO $con = null)
    {
        if (null === $this->collQuestionTypes || null !== $criteria) {
            if ($this->isNew() && null === $this->collQuestionTypes) {
                // return empty collection
                $this->initQuestionTypes();
            } else {
                $collQuestionTypes = QuestionTypeQuery::create(null, $criteria)
                    ->filterByFieldOption($this)
                    ->find($con);
                if (null !== $criteria) {
                    return $collQuestionTypes;
                }
                $this->collQuestionTypes = $collQuestionTypes;
            }
        }

        return $this->collQuestionTypes;
    }

    /**
     * Sets a collection of QuestionType objects related by a many-to-many relationship
     * to the current object by way of the questionnaire_question_type_field_option cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $questionTypes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return FieldOption The current object (for fluent API support)
     */
    public function setQuestionTypes(PropelCollection $questionTypes, PropelPDO $con = null)
    {
        $this->clearQuestionTypes();
        $currentQuestionTypes = $this->getQuestionTypes(null, $con);

        $this->questionTypesScheduledForDeletion = $currentQuestionTypes->diff($questionTypes);

        foreach ($questionTypes as $questionType) {
            if (!$currentQuestionTypes->contains($questionType)) {
                $this->doAddQuestionType($questionType);
            }
        }

        $this->collQuestionTypes = $questionTypes;

        return $this;
    }

    /**
     * Gets the number of QuestionType objects related by a many-to-many relationship
     * to the current object by way of the questionnaire_question_type_field_option cross-reference table.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param boolean $distinct Set to true to force count distinct
     * @param PropelPDO $con Optional connection object
     *
     * @return int the number of related QuestionType objects
     */
    public function countQuestionTypes($criteria = null, $distinct = false, PropelPDO $con = null)
    {
        if (null === $this->collQuestionTypes || null !== $criteria) {
            if ($this->isNew() && null === $this->collQuestionTypes) {
                return 0;
            } else {
                $query = QuestionTypeQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByFieldOption($this)
                    ->count($con);
            }
        } else {
            return count($this->collQuestionTypes);
        }
    }

    /**
     * Associate a QuestionType object to this object
     * through the questionnaire_question_type_field_option cross reference table.
     *
     * @param  QuestionType $questionType The QuestionTypeFieldOption object to relate
     * @return FieldOption The current object (for fluent API support)
     */
    public function addQuestionType(QuestionType $questionType)
    {
        if ($this->collQuestionTypes === null) {
            $this->initQuestionTypes();
        }

        if (!$this->collQuestionTypes->contains($questionType)) { // only add it if the **same** object is not already associated
            $this->doAddQuestionType($questionType);
            $this->collQuestionTypes[] = $questionType;

            if ($this->questionTypesScheduledForDeletion and $this->questionTypesScheduledForDeletion->contains($questionType)) {
                $this->questionTypesScheduledForDeletion->remove($this->questionTypesScheduledForDeletion->search($questionType));
            }
        }

        return $this;
    }

    /**
     * @param	QuestionType $questionType The questionType object to add.
     */
    protected function doAddQuestionType(QuestionType $questionType)
    {
        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$questionType->getFieldOptions()->contains($this)) { $questionTypeFieldOption = new QuestionTypeFieldOption();
            $questionTypeFieldOption->setQuestionType($questionType);
            $this->addQuestionTypeFieldOption($questionTypeFieldOption);

            $foreignCollection = $questionType->getFieldOptions();
            $foreignCollection[] = $this;
        }
    }

    /**
     * Remove a QuestionType object to this object
     * through the questionnaire_question_type_field_option cross reference table.
     *
     * @param QuestionType $questionType The QuestionTypeFieldOption object to relate
     * @return FieldOption The current object (for fluent API support)
     */
    public function removeQuestionType(QuestionType $questionType)
    {
        if ($this->getQuestionTypes()->contains($questionType)) {
            $this->collQuestionTypes->remove($this->collQuestionTypes->search($questionType));
            if (null === $this->questionTypesScheduledForDeletion) {
                $this->questionTypesScheduledForDeletion = clone $this->collQuestionTypes;
                $this->questionTypesScheduledForDeletion->clear();
            }
            $this->questionTypesScheduledForDeletion[]= $questionType;
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->field_type_id = null;
        $this->name = null;
        $this->field_option_tab_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collSelectedOptions) {
                foreach ($this->collSelectedOptions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collQuestionTypeFieldOptions) {
                foreach ($this->collQuestionTypeFieldOptions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collQuestions) {
                foreach ($this->collQuestions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collQuestionTypes) {
                foreach ($this->collQuestionTypes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aFieldType instanceof Persistent) {
              $this->aFieldType->clearAllReferences($deep);
            }
            if ($this->aFieldOptionTab instanceof Persistent) {
              $this->aFieldOptionTab->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collSelectedOptions instanceof PropelCollection) {
            $this->collSelectedOptions->clearIterator();
        }
        $this->collSelectedOptions = null;
        if ($this->collQuestionTypeFieldOptions instanceof PropelCollection) {
            $this->collQuestionTypeFieldOptions->clearIterator();
        }
        $this->collQuestionTypeFieldOptions = null;
        if ($this->collQuestions instanceof PropelCollection) {
            $this->collQuestions->clearIterator();
        }
        $this->collQuestions = null;
        if ($this->collQuestionTypes instanceof PropelCollection) {
            $this->collQuestionTypes->clearIterator();
        }
        $this->collQuestionTypes = null;
        $this->aFieldType = null;
        $this->aFieldOptionTab = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string The value of the 'name' column
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}

<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \EventDispatcherAwareModelInterface;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Axtion\Bundle\QuestionnaireBundle\Propel\DummyQuestion;
use Axtion\Bundle\QuestionnaireBundle\Propel\DummyQuestionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOption;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldType;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldTypeQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionType;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeFieldOption;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeFieldOptionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeI18n;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeI18nQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypePeer;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeQuery;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

abstract class BaseQuestionType extends BaseObject implements Persistent, EventDispatcherAwareModelInterface
{
    /**
     * Peer class name
     */
    const PEER = 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\QuestionTypePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        QuestionTypePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the field_type_id field.
     * @var        int
     */
    protected $field_type_id;

    /**
     * The value for the has_subquestions field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $has_subquestions;

    /**
     * The value for the icon field.
     * @var        string
     */
    protected $icon;

    /**
     * The value for the may_have_dependency field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $may_have_dependency;

    /**
     * @var        FieldType
     */
    protected $aFieldType;

    /**
     * @var        PropelObjectCollection|Question[] Collection to store aggregation of Question objects.
     */
    protected $collQuestions;
    protected $collQuestionsPartial;

    /**
     * @var        PropelObjectCollection|DummyQuestion[] Collection to store aggregation of DummyQuestion objects.
     */
    protected $collDummyQuestions;
    protected $collDummyQuestionsPartial;

    /**
     * @var        PropelObjectCollection|QuestionTypeFieldOption[] Collection to store aggregation of QuestionTypeFieldOption objects.
     */
    protected $collQuestionTypeFieldOptions;
    protected $collQuestionTypeFieldOptionsPartial;

    /**
     * @var        PropelObjectCollection|QuestionTypeI18n[] Collection to store aggregation of QuestionTypeI18n objects.
     */
    protected $collQuestionTypeI18ns;
    protected $collQuestionTypeI18nsPartial;

    /**
     * @var        PropelObjectCollection|FieldOption[] Collection to store aggregation of FieldOption objects.
     */
    protected $collFieldOptions;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    // event_dispatcher behavior

    const EVENT_CONSTRUCT = 'propel.construct';

    const EVENT_POST_HYDRATE = 'propel.post_hydrate';

    const EVENT_PRE_SAVE = 'propel.pre_save';

    const EVENT_POST_SAVE = 'propel.post_save';

    const EVENT_PRE_UPDATE = 'propel.pre_update';

    const EVENT_POST_UPDATE = 'propel.post_update';

    const EVENT_PRE_INSERT = 'propel.pre_insert';

    const EVENT_POST_INSERT = 'propel.post_insert';

    const EVENT_PRE_DELETE = 'propel.pre_delete';

    const EVENT_POST_DELETE = 'propel.post_delete';

    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcher
     */
    static private $dispatcher = null;

    // i18n behavior

    /**
     * Current locale
     * @var        string
     */
    protected $currentLocale = 'en';

    /**
     * Current translation objects
     * @var        array[QuestionTypeI18n]
     */
    protected $currentTranslations;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $fieldOptionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $questionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $dummyQuestionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $questionTypeFieldOptionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $questionTypeI18nsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->has_subquestions = false;
        $this->may_have_dependency = false;
    }

    /**
     * Initializes internal state of BaseQuestionType object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
        self::getEventDispatcher()->dispatch('propel.construct', new GenericEvent($this));
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [field_type_id] column value.
     *
     * @return int
     */
    public function getFieldTypeId()
    {

        return $this->field_type_id;
    }

    /**
     * Get the [has_subquestions] column value.
     *
     * @return boolean
     */
    public function getHasSubquestions()
    {

        return $this->has_subquestions;
    }

    /**
     * Get the [icon] column value.
     *
     * @return string
     */
    public function getIcon()
    {

        return $this->icon;
    }

    /**
     * Get the [may_have_dependency] column value.
     *
     * @return boolean
     */
    public function getMayHaveDependency()
    {

        return $this->may_have_dependency;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return QuestionType The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = QuestionTypePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [field_type_id] column.
     *
     * @param  int $v new value
     * @return QuestionType The current object (for fluent API support)
     */
    public function setFieldTypeId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->field_type_id !== $v) {
            $this->field_type_id = $v;
            $this->modifiedColumns[] = QuestionTypePeer::FIELD_TYPE_ID;
        }

        if ($this->aFieldType !== null && $this->aFieldType->getId() !== $v) {
            $this->aFieldType = null;
        }


        return $this;
    } // setFieldTypeId()

    /**
     * Sets the value of the [has_subquestions] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return QuestionType The current object (for fluent API support)
     */
    public function setHasSubquestions($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_subquestions !== $v) {
            $this->has_subquestions = $v;
            $this->modifiedColumns[] = QuestionTypePeer::HAS_SUBQUESTIONS;
        }


        return $this;
    } // setHasSubquestions()

    /**
     * Set the value of [icon] column.
     *
     * @param  string $v new value
     * @return QuestionType The current object (for fluent API support)
     */
    public function setIcon($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->icon !== $v) {
            $this->icon = $v;
            $this->modifiedColumns[] = QuestionTypePeer::ICON;
        }


        return $this;
    } // setIcon()

    /**
     * Sets the value of the [may_have_dependency] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return QuestionType The current object (for fluent API support)
     */
    public function setMayHaveDependency($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->may_have_dependency !== $v) {
            $this->may_have_dependency = $v;
            $this->modifiedColumns[] = QuestionTypePeer::MAY_HAVE_DEPENDENCY;
        }


        return $this;
    } // setMayHaveDependency()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->has_subquestions !== false) {
                return false;
            }

            if ($this->may_have_dependency !== false) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->field_type_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->has_subquestions = ($row[$startcol + 2] !== null) ? (boolean) $row[$startcol + 2] : null;
            $this->icon = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->may_have_dependency = ($row[$startcol + 4] !== null) ? (boolean) $row[$startcol + 4] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            // event_dispatcher behavior
            self::getEventDispatcher()->dispatch('propel.post_hydrate', new GenericEvent($this));


            return $startcol + 5; // 5 = QuestionTypePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating QuestionType object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aFieldType !== null && $this->field_type_id !== $this->aFieldType->getId()) {
            $this->aFieldType = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(QuestionTypePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = QuestionTypePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aFieldType = null;
            $this->collQuestions = null;

            $this->collDummyQuestions = null;

            $this->collQuestionTypeFieldOptions = null;

            $this->collQuestionTypeI18ns = null;

            $this->collFieldOptions = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(QuestionTypePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = QuestionTypeQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // event_dispatcher behavior
            self::getEventDispatcher()->dispatch('propel.pre_delete', new GenericEvent($this, array('connection' => $con)));

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                // event_dispatcher behavior
                self::getEventDispatcher()->dispatch('propel.post_delete', new GenericEvent($this, array('connection' => $con)));

                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(QuestionTypePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            // event_dispatcher behavior
            self::getEventDispatcher()->dispatch('propel.pre_save', new GenericEvent($this, array('connection' => $con)));

            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // event_dispatcher behavior
                self::getEventDispatcher()->dispatch('propel.pre_insert', new GenericEvent($this, array('connection' => $con)));

            } else {
                $ret = $ret && $this->preUpdate($con);
                // event_dispatcher behavior
                self::getEventDispatcher()->dispatch('propel.pre_update', new GenericEvent($this, array('connection' => $con)));

            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                    // event_dispatcher behavior
                    self::getEventDispatcher()->dispatch('propel.post_insert', new GenericEvent($this, array('connection' => $con)));

                } else {
                    $this->postUpdate($con);
                    // event_dispatcher behavior
                    self::getEventDispatcher()->dispatch('propel.post_update', new GenericEvent($this, array('connection' => $con)));

                }
                $this->postSave($con);
                // event_dispatcher behavior
                self::getEventDispatcher()->dispatch('propel.post_save', new GenericEvent($this, array('connection' => $con)));

                QuestionTypePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aFieldType !== null) {
                if ($this->aFieldType->isModified() || $this->aFieldType->isNew()) {
                    $affectedRows += $this->aFieldType->save($con);
                }
                $this->setFieldType($this->aFieldType);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->fieldOptionsScheduledForDeletion !== null) {
                if (!$this->fieldOptionsScheduledForDeletion->isEmpty()) {
                    $pks = array();
                    $pk = $this->getPrimaryKey();
                    foreach ($this->fieldOptionsScheduledForDeletion->getPrimaryKeys(false) as $remotePk) {
                        $pks[] = array($remotePk, $pk);
                    }
                    QuestionTypeFieldOptionQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);
                    $this->fieldOptionsScheduledForDeletion = null;
                }

                foreach ($this->getFieldOptions() as $fieldOption) {
                    if ($fieldOption->isModified()) {
                        $fieldOption->save($con);
                    }
                }
            } elseif ($this->collFieldOptions) {
                foreach ($this->collFieldOptions as $fieldOption) {
                    if ($fieldOption->isModified()) {
                        $fieldOption->save($con);
                    }
                }
            }

            if ($this->questionsScheduledForDeletion !== null) {
                if (!$this->questionsScheduledForDeletion->isEmpty()) {
                    foreach ($this->questionsScheduledForDeletion as $question) {
                        // need to save related object because we set the relation to null
                        $question->save($con);
                    }
                    $this->questionsScheduledForDeletion = null;
                }
            }

            if ($this->collQuestions !== null) {
                foreach ($this->collQuestions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->dummyQuestionsScheduledForDeletion !== null) {
                if (!$this->dummyQuestionsScheduledForDeletion->isEmpty()) {
                    DummyQuestionQuery::create()
                        ->filterByPrimaryKeys($this->dummyQuestionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->dummyQuestionsScheduledForDeletion = null;
                }
            }

            if ($this->collDummyQuestions !== null) {
                foreach ($this->collDummyQuestions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->questionTypeFieldOptionsScheduledForDeletion !== null) {
                if (!$this->questionTypeFieldOptionsScheduledForDeletion->isEmpty()) {
                    QuestionTypeFieldOptionQuery::create()
                        ->filterByPrimaryKeys($this->questionTypeFieldOptionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->questionTypeFieldOptionsScheduledForDeletion = null;
                }
            }

            if ($this->collQuestionTypeFieldOptions !== null) {
                foreach ($this->collQuestionTypeFieldOptions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->questionTypeI18nsScheduledForDeletion !== null) {
                if (!$this->questionTypeI18nsScheduledForDeletion->isEmpty()) {
                    QuestionTypeI18nQuery::create()
                        ->filterByPrimaryKeys($this->questionTypeI18nsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->questionTypeI18nsScheduledForDeletion = null;
                }
            }

            if ($this->collQuestionTypeI18ns !== null) {
                foreach ($this->collQuestionTypeI18ns as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = QuestionTypePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . QuestionTypePeer::ID . ')');
        }
        if (null === $this->id) {
            try {
                $stmt = $con->query("SELECT nextval('questionnaire_question_type_id_seq')");
                $row = $stmt->fetch(PDO::FETCH_NUM);
                $this->id = $row[0];
            } catch (Exception $e) {
                throw new PropelException('Unable to get sequence id.', $e);
            }
        }


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(QuestionTypePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '"id"';
        }
        if ($this->isColumnModified(QuestionTypePeer::FIELD_TYPE_ID)) {
            $modifiedColumns[':p' . $index++]  = '"field_type_id"';
        }
        if ($this->isColumnModified(QuestionTypePeer::HAS_SUBQUESTIONS)) {
            $modifiedColumns[':p' . $index++]  = '"has_subquestions"';
        }
        if ($this->isColumnModified(QuestionTypePeer::ICON)) {
            $modifiedColumns[':p' . $index++]  = '"icon"';
        }
        if ($this->isColumnModified(QuestionTypePeer::MAY_HAVE_DEPENDENCY)) {
            $modifiedColumns[':p' . $index++]  = '"may_have_dependency"';
        }

        $sql = sprintf(
            'INSERT INTO "questionnaire_question_type" (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '"id"':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '"field_type_id"':
                        $stmt->bindValue($identifier, $this->field_type_id, PDO::PARAM_INT);
                        break;
                    case '"has_subquestions"':
                        $stmt->bindValue($identifier, $this->has_subquestions, PDO::PARAM_BOOL);
                        break;
                    case '"icon"':
                        $stmt->bindValue($identifier, $this->icon, PDO::PARAM_STR);
                        break;
                    case '"may_have_dependency"':
                        $stmt->bindValue($identifier, $this->may_have_dependency, PDO::PARAM_BOOL);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aFieldType !== null) {
                if (!$this->aFieldType->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aFieldType->getValidationFailures());
                }
            }


            if (($retval = QuestionTypePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collQuestions !== null) {
                    foreach ($this->collQuestions as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collDummyQuestions !== null) {
                    foreach ($this->collDummyQuestions as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collQuestionTypeFieldOptions !== null) {
                    foreach ($this->collQuestionTypeFieldOptions as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collQuestionTypeI18ns !== null) {
                    foreach ($this->collQuestionTypeI18ns as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = QuestionTypePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getFieldTypeId();
                break;
            case 2:
                return $this->getHasSubquestions();
                break;
            case 3:
                return $this->getIcon();
                break;
            case 4:
                return $this->getMayHaveDependency();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['QuestionType'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['QuestionType'][$this->getPrimaryKey()] = true;
        $keys = QuestionTypePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getFieldTypeId(),
            $keys[2] => $this->getHasSubquestions(),
            $keys[3] => $this->getIcon(),
            $keys[4] => $this->getMayHaveDependency(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aFieldType) {
                $result['FieldType'] = $this->aFieldType->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collQuestions) {
                $result['Questions'] = $this->collQuestions->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDummyQuestions) {
                $result['DummyQuestions'] = $this->collDummyQuestions->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collQuestionTypeFieldOptions) {
                $result['QuestionTypeFieldOptions'] = $this->collQuestionTypeFieldOptions->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collQuestionTypeI18ns) {
                $result['QuestionTypeI18ns'] = $this->collQuestionTypeI18ns->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = QuestionTypePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setFieldTypeId($value);
                break;
            case 2:
                $this->setHasSubquestions($value);
                break;
            case 3:
                $this->setIcon($value);
                break;
            case 4:
                $this->setMayHaveDependency($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = QuestionTypePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setFieldTypeId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setHasSubquestions($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setIcon($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setMayHaveDependency($arr[$keys[4]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(QuestionTypePeer::DATABASE_NAME);

        if ($this->isColumnModified(QuestionTypePeer::ID)) $criteria->add(QuestionTypePeer::ID, $this->id);
        if ($this->isColumnModified(QuestionTypePeer::FIELD_TYPE_ID)) $criteria->add(QuestionTypePeer::FIELD_TYPE_ID, $this->field_type_id);
        if ($this->isColumnModified(QuestionTypePeer::HAS_SUBQUESTIONS)) $criteria->add(QuestionTypePeer::HAS_SUBQUESTIONS, $this->has_subquestions);
        if ($this->isColumnModified(QuestionTypePeer::ICON)) $criteria->add(QuestionTypePeer::ICON, $this->icon);
        if ($this->isColumnModified(QuestionTypePeer::MAY_HAVE_DEPENDENCY)) $criteria->add(QuestionTypePeer::MAY_HAVE_DEPENDENCY, $this->may_have_dependency);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(QuestionTypePeer::DATABASE_NAME);
        $criteria->add(QuestionTypePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of QuestionType (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setFieldTypeId($this->getFieldTypeId());
        $copyObj->setHasSubquestions($this->getHasSubquestions());
        $copyObj->setIcon($this->getIcon());
        $copyObj->setMayHaveDependency($this->getMayHaveDependency());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getQuestions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addQuestion($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDummyQuestions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDummyQuestion($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getQuestionTypeFieldOptions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addQuestionTypeFieldOption($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getQuestionTypeI18ns() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addQuestionTypeI18n($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return QuestionType Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return QuestionTypePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new QuestionTypePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a FieldType object.
     *
     * @param                  FieldType $v
     * @return QuestionType The current object (for fluent API support)
     * @throws PropelException
     */
    public function setFieldType(FieldType $v = null)
    {
        if ($v === null) {
            $this->setFieldTypeId(NULL);
        } else {
            $this->setFieldTypeId($v->getId());
        }

        $this->aFieldType = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the FieldType object, it will not be re-added.
        if ($v !== null) {
            $v->addQuestionType($this);
        }


        return $this;
    }


    /**
     * Get the associated FieldType object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return FieldType The associated FieldType object.
     * @throws PropelException
     */
    public function getFieldType(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aFieldType === null && ($this->field_type_id !== null) && $doQuery) {
            $this->aFieldType = FieldTypeQuery::create()->findPk($this->field_type_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aFieldType->addQuestionTypes($this);
             */
        }

        return $this->aFieldType;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Question' == $relationName) {
            $this->initQuestions();
        }
        if ('DummyQuestion' == $relationName) {
            $this->initDummyQuestions();
        }
        if ('QuestionTypeFieldOption' == $relationName) {
            $this->initQuestionTypeFieldOptions();
        }
        if ('QuestionTypeI18n' == $relationName) {
            $this->initQuestionTypeI18ns();
        }
    }

    /**
     * Clears out the collQuestions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return QuestionType The current object (for fluent API support)
     * @see        addQuestions()
     */
    public function clearQuestions()
    {
        $this->collQuestions = null; // important to set this to null since that means it is uninitialized
        $this->collQuestionsPartial = null;

        return $this;
    }

    /**
     * reset is the collQuestions collection loaded partially
     *
     * @return void
     */
    public function resetPartialQuestions($v = true)
    {
        $this->collQuestionsPartial = $v;
    }

    /**
     * Initializes the collQuestions collection.
     *
     * By default this just sets the collQuestions collection to an empty array (like clearcollQuestions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initQuestions($overrideExisting = true)
    {
        if (null !== $this->collQuestions && !$overrideExisting) {
            return;
        }
        $this->collQuestions = new PropelObjectCollection();
        $this->collQuestions->setModel('Question');
    }

    /**
     * Gets an array of Question objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this QuestionType is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Question[] List of Question objects
     * @throws PropelException
     */
    public function getQuestions($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collQuestionsPartial && !$this->isNew();
        if (null === $this->collQuestions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collQuestions) {
                // return empty collection
                $this->initQuestions();
            } else {
                $collQuestions = QuestionQuery::create(null, $criteria)
                    ->filterByQuestionType($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collQuestionsPartial && count($collQuestions)) {
                      $this->initQuestions(false);

                      foreach ($collQuestions as $obj) {
                        if (false == $this->collQuestions->contains($obj)) {
                          $this->collQuestions->append($obj);
                        }
                      }

                      $this->collQuestionsPartial = true;
                    }

                    $collQuestions->getInternalIterator()->rewind();

                    return $collQuestions;
                }

                if ($partial && $this->collQuestions) {
                    foreach ($this->collQuestions as $obj) {
                        if ($obj->isNew()) {
                            $collQuestions[] = $obj;
                        }
                    }
                }

                $this->collQuestions = $collQuestions;
                $this->collQuestionsPartial = false;
            }
        }

        return $this->collQuestions;
    }

    /**
     * Sets a collection of Question objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $questions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return QuestionType The current object (for fluent API support)
     */
    public function setQuestions(PropelCollection $questions, PropelPDO $con = null)
    {
        $questionsToDelete = $this->getQuestions(new Criteria(), $con)->diff($questions);


        $this->questionsScheduledForDeletion = $questionsToDelete;

        foreach ($questionsToDelete as $questionRemoved) {
            $questionRemoved->setQuestionType(null);
        }

        $this->collQuestions = null;
        foreach ($questions as $question) {
            $this->addQuestion($question);
        }

        $this->collQuestions = $questions;
        $this->collQuestionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Question objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Question objects.
     * @throws PropelException
     */
    public function countQuestions(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collQuestionsPartial && !$this->isNew();
        if (null === $this->collQuestions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collQuestions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getQuestions());
            }
            $query = QuestionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByQuestionType($this)
                ->count($con);
        }

        return count($this->collQuestions);
    }

    /**
     * Method called to associate a Question object to this object
     * through the Question foreign key attribute.
     *
     * @param    Question $l Question
     * @return QuestionType The current object (for fluent API support)
     */
    public function addQuestion(Question $l)
    {
        if ($this->collQuestions === null) {
            $this->initQuestions();
            $this->collQuestionsPartial = true;
        }

        if (!in_array($l, $this->collQuestions->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddQuestion($l);

            if ($this->questionsScheduledForDeletion and $this->questionsScheduledForDeletion->contains($l)) {
                $this->questionsScheduledForDeletion->remove($this->questionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Question $question The question object to add.
     */
    protected function doAddQuestion($question)
    {
        $this->collQuestions[]= $question;
        $question->setQuestionType($this);
    }

    /**
     * @param	Question $question The question object to remove.
     * @return QuestionType The current object (for fluent API support)
     */
    public function removeQuestion($question)
    {
        if ($this->getQuestions()->contains($question)) {
            $this->collQuestions->remove($this->collQuestions->search($question));
            if (null === $this->questionsScheduledForDeletion) {
                $this->questionsScheduledForDeletion = clone $this->collQuestions;
                $this->questionsScheduledForDeletion->clear();
            }
            $this->questionsScheduledForDeletion[]= $question;
            $question->setQuestionType(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this QuestionType is new, it will return
     * an empty collection; or if this QuestionType has previously
     * been saved, it will retrieve related Questions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in QuestionType.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Question[] List of Question objects
     */
    public function getQuestionsJoinQuestionnaire($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = QuestionQuery::create(null, $criteria);
        $query->joinWith('Questionnaire', $join_behavior);

        return $this->getQuestions($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this QuestionType is new, it will return
     * an empty collection; or if this QuestionType has previously
     * been saved, it will retrieve related Questions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in QuestionType.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Question[] List of Question objects
     */
    public function getQuestionsJoinParent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = QuestionQuery::create(null, $criteria);
        $query->joinWith('Parent', $join_behavior);

        return $this->getQuestions($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this QuestionType is new, it will return
     * an empty collection; or if this QuestionType has previously
     * been saved, it will retrieve related Questions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in QuestionType.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Question[] List of Question objects
     */
    public function getQuestionsJoinQuestionGroup($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = QuestionQuery::create(null, $criteria);
        $query->joinWith('QuestionGroup', $join_behavior);

        return $this->getQuestions($query, $con);
    }

    /**
     * Clears out the collDummyQuestions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return QuestionType The current object (for fluent API support)
     * @see        addDummyQuestions()
     */
    public function clearDummyQuestions()
    {
        $this->collDummyQuestions = null; // important to set this to null since that means it is uninitialized
        $this->collDummyQuestionsPartial = null;

        return $this;
    }

    /**
     * reset is the collDummyQuestions collection loaded partially
     *
     * @return void
     */
    public function resetPartialDummyQuestions($v = true)
    {
        $this->collDummyQuestionsPartial = $v;
    }

    /**
     * Initializes the collDummyQuestions collection.
     *
     * By default this just sets the collDummyQuestions collection to an empty array (like clearcollDummyQuestions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDummyQuestions($overrideExisting = true)
    {
        if (null !== $this->collDummyQuestions && !$overrideExisting) {
            return;
        }
        $this->collDummyQuestions = new PropelObjectCollection();
        $this->collDummyQuestions->setModel('DummyQuestion');
    }

    /**
     * Gets an array of DummyQuestion objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this QuestionType is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|DummyQuestion[] List of DummyQuestion objects
     * @throws PropelException
     */
    public function getDummyQuestions($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collDummyQuestionsPartial && !$this->isNew();
        if (null === $this->collDummyQuestions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDummyQuestions) {
                // return empty collection
                $this->initDummyQuestions();
            } else {
                $collDummyQuestions = DummyQuestionQuery::create(null, $criteria)
                    ->filterByQuestionType($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collDummyQuestionsPartial && count($collDummyQuestions)) {
                      $this->initDummyQuestions(false);

                      foreach ($collDummyQuestions as $obj) {
                        if (false == $this->collDummyQuestions->contains($obj)) {
                          $this->collDummyQuestions->append($obj);
                        }
                      }

                      $this->collDummyQuestionsPartial = true;
                    }

                    $collDummyQuestions->getInternalIterator()->rewind();

                    return $collDummyQuestions;
                }

                if ($partial && $this->collDummyQuestions) {
                    foreach ($this->collDummyQuestions as $obj) {
                        if ($obj->isNew()) {
                            $collDummyQuestions[] = $obj;
                        }
                    }
                }

                $this->collDummyQuestions = $collDummyQuestions;
                $this->collDummyQuestionsPartial = false;
            }
        }

        return $this->collDummyQuestions;
    }

    /**
     * Sets a collection of DummyQuestion objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $dummyQuestions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return QuestionType The current object (for fluent API support)
     */
    public function setDummyQuestions(PropelCollection $dummyQuestions, PropelPDO $con = null)
    {
        $dummyQuestionsToDelete = $this->getDummyQuestions(new Criteria(), $con)->diff($dummyQuestions);


        $this->dummyQuestionsScheduledForDeletion = $dummyQuestionsToDelete;

        foreach ($dummyQuestionsToDelete as $dummyQuestionRemoved) {
            $dummyQuestionRemoved->setQuestionType(null);
        }

        $this->collDummyQuestions = null;
        foreach ($dummyQuestions as $dummyQuestion) {
            $this->addDummyQuestion($dummyQuestion);
        }

        $this->collDummyQuestions = $dummyQuestions;
        $this->collDummyQuestionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related DummyQuestion objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related DummyQuestion objects.
     * @throws PropelException
     */
    public function countDummyQuestions(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collDummyQuestionsPartial && !$this->isNew();
        if (null === $this->collDummyQuestions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDummyQuestions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDummyQuestions());
            }
            $query = DummyQuestionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByQuestionType($this)
                ->count($con);
        }

        return count($this->collDummyQuestions);
    }

    /**
     * Method called to associate a DummyQuestion object to this object
     * through the DummyQuestion foreign key attribute.
     *
     * @param    DummyQuestion $l DummyQuestion
     * @return QuestionType The current object (for fluent API support)
     */
    public function addDummyQuestion(DummyQuestion $l)
    {
        if ($this->collDummyQuestions === null) {
            $this->initDummyQuestions();
            $this->collDummyQuestionsPartial = true;
        }

        if (!in_array($l, $this->collDummyQuestions->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddDummyQuestion($l);

            if ($this->dummyQuestionsScheduledForDeletion and $this->dummyQuestionsScheduledForDeletion->contains($l)) {
                $this->dummyQuestionsScheduledForDeletion->remove($this->dummyQuestionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	DummyQuestion $dummyQuestion The dummyQuestion object to add.
     */
    protected function doAddDummyQuestion($dummyQuestion)
    {
        $this->collDummyQuestions[]= $dummyQuestion;
        $dummyQuestion->setQuestionType($this);
    }

    /**
     * @param	DummyQuestion $dummyQuestion The dummyQuestion object to remove.
     * @return QuestionType The current object (for fluent API support)
     */
    public function removeDummyQuestion($dummyQuestion)
    {
        if ($this->getDummyQuestions()->contains($dummyQuestion)) {
            $this->collDummyQuestions->remove($this->collDummyQuestions->search($dummyQuestion));
            if (null === $this->dummyQuestionsScheduledForDeletion) {
                $this->dummyQuestionsScheduledForDeletion = clone $this->collDummyQuestions;
                $this->dummyQuestionsScheduledForDeletion->clear();
            }
            $this->dummyQuestionsScheduledForDeletion[]= clone $dummyQuestion;
            $dummyQuestion->setQuestionType(null);
        }

        return $this;
    }

    /**
     * Clears out the collQuestionTypeFieldOptions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return QuestionType The current object (for fluent API support)
     * @see        addQuestionTypeFieldOptions()
     */
    public function clearQuestionTypeFieldOptions()
    {
        $this->collQuestionTypeFieldOptions = null; // important to set this to null since that means it is uninitialized
        $this->collQuestionTypeFieldOptionsPartial = null;

        return $this;
    }

    /**
     * reset is the collQuestionTypeFieldOptions collection loaded partially
     *
     * @return void
     */
    public function resetPartialQuestionTypeFieldOptions($v = true)
    {
        $this->collQuestionTypeFieldOptionsPartial = $v;
    }

    /**
     * Initializes the collQuestionTypeFieldOptions collection.
     *
     * By default this just sets the collQuestionTypeFieldOptions collection to an empty array (like clearcollQuestionTypeFieldOptions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initQuestionTypeFieldOptions($overrideExisting = true)
    {
        if (null !== $this->collQuestionTypeFieldOptions && !$overrideExisting) {
            return;
        }
        $this->collQuestionTypeFieldOptions = new PropelObjectCollection();
        $this->collQuestionTypeFieldOptions->setModel('QuestionTypeFieldOption');
    }

    /**
     * Gets an array of QuestionTypeFieldOption objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this QuestionType is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|QuestionTypeFieldOption[] List of QuestionTypeFieldOption objects
     * @throws PropelException
     */
    public function getQuestionTypeFieldOptions($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collQuestionTypeFieldOptionsPartial && !$this->isNew();
        if (null === $this->collQuestionTypeFieldOptions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collQuestionTypeFieldOptions) {
                // return empty collection
                $this->initQuestionTypeFieldOptions();
            } else {
                $collQuestionTypeFieldOptions = QuestionTypeFieldOptionQuery::create(null, $criteria)
                    ->filterByQuestionType($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collQuestionTypeFieldOptionsPartial && count($collQuestionTypeFieldOptions)) {
                      $this->initQuestionTypeFieldOptions(false);

                      foreach ($collQuestionTypeFieldOptions as $obj) {
                        if (false == $this->collQuestionTypeFieldOptions->contains($obj)) {
                          $this->collQuestionTypeFieldOptions->append($obj);
                        }
                      }

                      $this->collQuestionTypeFieldOptionsPartial = true;
                    }

                    $collQuestionTypeFieldOptions->getInternalIterator()->rewind();

                    return $collQuestionTypeFieldOptions;
                }

                if ($partial && $this->collQuestionTypeFieldOptions) {
                    foreach ($this->collQuestionTypeFieldOptions as $obj) {
                        if ($obj->isNew()) {
                            $collQuestionTypeFieldOptions[] = $obj;
                        }
                    }
                }

                $this->collQuestionTypeFieldOptions = $collQuestionTypeFieldOptions;
                $this->collQuestionTypeFieldOptionsPartial = false;
            }
        }

        return $this->collQuestionTypeFieldOptions;
    }

    /**
     * Sets a collection of QuestionTypeFieldOption objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $questionTypeFieldOptions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return QuestionType The current object (for fluent API support)
     */
    public function setQuestionTypeFieldOptions(PropelCollection $questionTypeFieldOptions, PropelPDO $con = null)
    {
        $questionTypeFieldOptionsToDelete = $this->getQuestionTypeFieldOptions(new Criteria(), $con)->diff($questionTypeFieldOptions);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->questionTypeFieldOptionsScheduledForDeletion = clone $questionTypeFieldOptionsToDelete;

        foreach ($questionTypeFieldOptionsToDelete as $questionTypeFieldOptionRemoved) {
            $questionTypeFieldOptionRemoved->setQuestionType(null);
        }

        $this->collQuestionTypeFieldOptions = null;
        foreach ($questionTypeFieldOptions as $questionTypeFieldOption) {
            $this->addQuestionTypeFieldOption($questionTypeFieldOption);
        }

        $this->collQuestionTypeFieldOptions = $questionTypeFieldOptions;
        $this->collQuestionTypeFieldOptionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related QuestionTypeFieldOption objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related QuestionTypeFieldOption objects.
     * @throws PropelException
     */
    public function countQuestionTypeFieldOptions(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collQuestionTypeFieldOptionsPartial && !$this->isNew();
        if (null === $this->collQuestionTypeFieldOptions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collQuestionTypeFieldOptions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getQuestionTypeFieldOptions());
            }
            $query = QuestionTypeFieldOptionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByQuestionType($this)
                ->count($con);
        }

        return count($this->collQuestionTypeFieldOptions);
    }

    /**
     * Method called to associate a QuestionTypeFieldOption object to this object
     * through the QuestionTypeFieldOption foreign key attribute.
     *
     * @param    QuestionTypeFieldOption $l QuestionTypeFieldOption
     * @return QuestionType The current object (for fluent API support)
     */
    public function addQuestionTypeFieldOption(QuestionTypeFieldOption $l)
    {
        if ($this->collQuestionTypeFieldOptions === null) {
            $this->initQuestionTypeFieldOptions();
            $this->collQuestionTypeFieldOptionsPartial = true;
        }

        if (!in_array($l, $this->collQuestionTypeFieldOptions->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddQuestionTypeFieldOption($l);

            if ($this->questionTypeFieldOptionsScheduledForDeletion and $this->questionTypeFieldOptionsScheduledForDeletion->contains($l)) {
                $this->questionTypeFieldOptionsScheduledForDeletion->remove($this->questionTypeFieldOptionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	QuestionTypeFieldOption $questionTypeFieldOption The questionTypeFieldOption object to add.
     */
    protected function doAddQuestionTypeFieldOption($questionTypeFieldOption)
    {
        $this->collQuestionTypeFieldOptions[]= $questionTypeFieldOption;
        $questionTypeFieldOption->setQuestionType($this);
    }

    /**
     * @param	QuestionTypeFieldOption $questionTypeFieldOption The questionTypeFieldOption object to remove.
     * @return QuestionType The current object (for fluent API support)
     */
    public function removeQuestionTypeFieldOption($questionTypeFieldOption)
    {
        if ($this->getQuestionTypeFieldOptions()->contains($questionTypeFieldOption)) {
            $this->collQuestionTypeFieldOptions->remove($this->collQuestionTypeFieldOptions->search($questionTypeFieldOption));
            if (null === $this->questionTypeFieldOptionsScheduledForDeletion) {
                $this->questionTypeFieldOptionsScheduledForDeletion = clone $this->collQuestionTypeFieldOptions;
                $this->questionTypeFieldOptionsScheduledForDeletion->clear();
            }
            $this->questionTypeFieldOptionsScheduledForDeletion[]= clone $questionTypeFieldOption;
            $questionTypeFieldOption->setQuestionType(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this QuestionType is new, it will return
     * an empty collection; or if this QuestionType has previously
     * been saved, it will retrieve related QuestionTypeFieldOptions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in QuestionType.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|QuestionTypeFieldOption[] List of QuestionTypeFieldOption objects
     */
    public function getQuestionTypeFieldOptionsJoinFieldOption($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = QuestionTypeFieldOptionQuery::create(null, $criteria);
        $query->joinWith('FieldOption', $join_behavior);

        return $this->getQuestionTypeFieldOptions($query, $con);
    }

    /**
     * Clears out the collQuestionTypeI18ns collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return QuestionType The current object (for fluent API support)
     * @see        addQuestionTypeI18ns()
     */
    public function clearQuestionTypeI18ns()
    {
        $this->collQuestionTypeI18ns = null; // important to set this to null since that means it is uninitialized
        $this->collQuestionTypeI18nsPartial = null;

        return $this;
    }

    /**
     * reset is the collQuestionTypeI18ns collection loaded partially
     *
     * @return void
     */
    public function resetPartialQuestionTypeI18ns($v = true)
    {
        $this->collQuestionTypeI18nsPartial = $v;
    }

    /**
     * Initializes the collQuestionTypeI18ns collection.
     *
     * By default this just sets the collQuestionTypeI18ns collection to an empty array (like clearcollQuestionTypeI18ns());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initQuestionTypeI18ns($overrideExisting = true)
    {
        if (null !== $this->collQuestionTypeI18ns && !$overrideExisting) {
            return;
        }
        $this->collQuestionTypeI18ns = new PropelObjectCollection();
        $this->collQuestionTypeI18ns->setModel('QuestionTypeI18n');
    }

    /**
     * Gets an array of QuestionTypeI18n objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this QuestionType is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|QuestionTypeI18n[] List of QuestionTypeI18n objects
     * @throws PropelException
     */
    public function getQuestionTypeI18ns($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collQuestionTypeI18nsPartial && !$this->isNew();
        if (null === $this->collQuestionTypeI18ns || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collQuestionTypeI18ns) {
                // return empty collection
                $this->initQuestionTypeI18ns();
            } else {
                $collQuestionTypeI18ns = QuestionTypeI18nQuery::create(null, $criteria)
                    ->filterByQuestionType($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collQuestionTypeI18nsPartial && count($collQuestionTypeI18ns)) {
                      $this->initQuestionTypeI18ns(false);

                      foreach ($collQuestionTypeI18ns as $obj) {
                        if (false == $this->collQuestionTypeI18ns->contains($obj)) {
                          $this->collQuestionTypeI18ns->append($obj);
                        }
                      }

                      $this->collQuestionTypeI18nsPartial = true;
                    }

                    $collQuestionTypeI18ns->getInternalIterator()->rewind();

                    return $collQuestionTypeI18ns;
                }

                if ($partial && $this->collQuestionTypeI18ns) {
                    foreach ($this->collQuestionTypeI18ns as $obj) {
                        if ($obj->isNew()) {
                            $collQuestionTypeI18ns[] = $obj;
                        }
                    }
                }

                $this->collQuestionTypeI18ns = $collQuestionTypeI18ns;
                $this->collQuestionTypeI18nsPartial = false;
            }
        }

        return $this->collQuestionTypeI18ns;
    }

    /**
     * Sets a collection of QuestionTypeI18n objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $questionTypeI18ns A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return QuestionType The current object (for fluent API support)
     */
    public function setQuestionTypeI18ns(PropelCollection $questionTypeI18ns, PropelPDO $con = null)
    {
        $questionTypeI18nsToDelete = $this->getQuestionTypeI18ns(new Criteria(), $con)->diff($questionTypeI18ns);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->questionTypeI18nsScheduledForDeletion = clone $questionTypeI18nsToDelete;

        foreach ($questionTypeI18nsToDelete as $questionTypeI18nRemoved) {
            $questionTypeI18nRemoved->setQuestionType(null);
        }

        $this->collQuestionTypeI18ns = null;
        foreach ($questionTypeI18ns as $questionTypeI18n) {
            $this->addQuestionTypeI18n($questionTypeI18n);
        }

        $this->collQuestionTypeI18ns = $questionTypeI18ns;
        $this->collQuestionTypeI18nsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related QuestionTypeI18n objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related QuestionTypeI18n objects.
     * @throws PropelException
     */
    public function countQuestionTypeI18ns(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collQuestionTypeI18nsPartial && !$this->isNew();
        if (null === $this->collQuestionTypeI18ns || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collQuestionTypeI18ns) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getQuestionTypeI18ns());
            }
            $query = QuestionTypeI18nQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByQuestionType($this)
                ->count($con);
        }

        return count($this->collQuestionTypeI18ns);
    }

    /**
     * Method called to associate a QuestionTypeI18n object to this object
     * through the QuestionTypeI18n foreign key attribute.
     *
     * @param    QuestionTypeI18n $l QuestionTypeI18n
     * @return QuestionType The current object (for fluent API support)
     */
    public function addQuestionTypeI18n(QuestionTypeI18n $l)
    {
        if ($l && $locale = $l->getLocale()) {
            $this->setLocale($locale);
            $this->currentTranslations[$locale] = $l;
        }
        if ($this->collQuestionTypeI18ns === null) {
            $this->initQuestionTypeI18ns();
            $this->collQuestionTypeI18nsPartial = true;
        }

        if (!in_array($l, $this->collQuestionTypeI18ns->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddQuestionTypeI18n($l);

            if ($this->questionTypeI18nsScheduledForDeletion and $this->questionTypeI18nsScheduledForDeletion->contains($l)) {
                $this->questionTypeI18nsScheduledForDeletion->remove($this->questionTypeI18nsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	QuestionTypeI18n $questionTypeI18n The questionTypeI18n object to add.
     */
    protected function doAddQuestionTypeI18n($questionTypeI18n)
    {
        $this->collQuestionTypeI18ns[]= $questionTypeI18n;
        $questionTypeI18n->setQuestionType($this);
    }

    /**
     * @param	QuestionTypeI18n $questionTypeI18n The questionTypeI18n object to remove.
     * @return QuestionType The current object (for fluent API support)
     */
    public function removeQuestionTypeI18n($questionTypeI18n)
    {
        if ($this->getQuestionTypeI18ns()->contains($questionTypeI18n)) {
            $this->collQuestionTypeI18ns->remove($this->collQuestionTypeI18ns->search($questionTypeI18n));
            if (null === $this->questionTypeI18nsScheduledForDeletion) {
                $this->questionTypeI18nsScheduledForDeletion = clone $this->collQuestionTypeI18ns;
                $this->questionTypeI18nsScheduledForDeletion->clear();
            }
            $this->questionTypeI18nsScheduledForDeletion[]= clone $questionTypeI18n;
            $questionTypeI18n->setQuestionType(null);
        }

        return $this;
    }

    /**
     * Clears out the collFieldOptions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return QuestionType The current object (for fluent API support)
     * @see        addFieldOptions()
     */
    public function clearFieldOptions()
    {
        $this->collFieldOptions = null; // important to set this to null since that means it is uninitialized
        $this->collFieldOptionsPartial = null;

        return $this;
    }

    /**
     * Initializes the collFieldOptions collection.
     *
     * By default this just sets the collFieldOptions collection to an empty collection (like clearFieldOptions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initFieldOptions()
    {
        $this->collFieldOptions = new PropelObjectCollection();
        $this->collFieldOptions->setModel('FieldOption');
    }

    /**
     * Gets a collection of FieldOption objects related by a many-to-many relationship
     * to the current object by way of the questionnaire_question_type_field_option cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this QuestionType is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param PropelPDO $con Optional connection object
     *
     * @return PropelObjectCollection|FieldOption[] List of FieldOption objects
     */
    public function getFieldOptions($criteria = null, PropelPDO $con = null)
    {
        if (null === $this->collFieldOptions || null !== $criteria) {
            if ($this->isNew() && null === $this->collFieldOptions) {
                // return empty collection
                $this->initFieldOptions();
            } else {
                $collFieldOptions = FieldOptionQuery::create(null, $criteria)
                    ->filterByQuestionType($this)
                    ->find($con);
                if (null !== $criteria) {
                    return $collFieldOptions;
                }
                $this->collFieldOptions = $collFieldOptions;
            }
        }

        return $this->collFieldOptions;
    }

    /**
     * Sets a collection of FieldOption objects related by a many-to-many relationship
     * to the current object by way of the questionnaire_question_type_field_option cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $fieldOptions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return QuestionType The current object (for fluent API support)
     */
    public function setFieldOptions(PropelCollection $fieldOptions, PropelPDO $con = null)
    {
        $this->clearFieldOptions();
        $currentFieldOptions = $this->getFieldOptions(null, $con);

        $this->fieldOptionsScheduledForDeletion = $currentFieldOptions->diff($fieldOptions);

        foreach ($fieldOptions as $fieldOption) {
            if (!$currentFieldOptions->contains($fieldOption)) {
                $this->doAddFieldOption($fieldOption);
            }
        }

        $this->collFieldOptions = $fieldOptions;

        return $this;
    }

    /**
     * Gets the number of FieldOption objects related by a many-to-many relationship
     * to the current object by way of the questionnaire_question_type_field_option cross-reference table.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param boolean $distinct Set to true to force count distinct
     * @param PropelPDO $con Optional connection object
     *
     * @return int the number of related FieldOption objects
     */
    public function countFieldOptions($criteria = null, $distinct = false, PropelPDO $con = null)
    {
        if (null === $this->collFieldOptions || null !== $criteria) {
            if ($this->isNew() && null === $this->collFieldOptions) {
                return 0;
            } else {
                $query = FieldOptionQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByQuestionType($this)
                    ->count($con);
            }
        } else {
            return count($this->collFieldOptions);
        }
    }

    /**
     * Associate a FieldOption object to this object
     * through the questionnaire_question_type_field_option cross reference table.
     *
     * @param  FieldOption $fieldOption The QuestionTypeFieldOption object to relate
     * @return QuestionType The current object (for fluent API support)
     */
    public function addFieldOption(FieldOption $fieldOption)
    {
        if ($this->collFieldOptions === null) {
            $this->initFieldOptions();
        }

        if (!$this->collFieldOptions->contains($fieldOption)) { // only add it if the **same** object is not already associated
            $this->doAddFieldOption($fieldOption);
            $this->collFieldOptions[] = $fieldOption;

            if ($this->fieldOptionsScheduledForDeletion and $this->fieldOptionsScheduledForDeletion->contains($fieldOption)) {
                $this->fieldOptionsScheduledForDeletion->remove($this->fieldOptionsScheduledForDeletion->search($fieldOption));
            }
        }

        return $this;
    }

    /**
     * @param	FieldOption $fieldOption The fieldOption object to add.
     */
    protected function doAddFieldOption(FieldOption $fieldOption)
    {
        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$fieldOption->getQuestionTypes()->contains($this)) { $questionTypeFieldOption = new QuestionTypeFieldOption();
            $questionTypeFieldOption->setFieldOption($fieldOption);
            $this->addQuestionTypeFieldOption($questionTypeFieldOption);

            $foreignCollection = $fieldOption->getQuestionTypes();
            $foreignCollection[] = $this;
        }
    }

    /**
     * Remove a FieldOption object to this object
     * through the questionnaire_question_type_field_option cross reference table.
     *
     * @param FieldOption $fieldOption The QuestionTypeFieldOption object to relate
     * @return QuestionType The current object (for fluent API support)
     */
    public function removeFieldOption(FieldOption $fieldOption)
    {
        if ($this->getFieldOptions()->contains($fieldOption)) {
            $this->collFieldOptions->remove($this->collFieldOptions->search($fieldOption));
            if (null === $this->fieldOptionsScheduledForDeletion) {
                $this->fieldOptionsScheduledForDeletion = clone $this->collFieldOptions;
                $this->fieldOptionsScheduledForDeletion->clear();
            }
            $this->fieldOptionsScheduledForDeletion[]= $fieldOption;
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->field_type_id = null;
        $this->has_subquestions = null;
        $this->icon = null;
        $this->may_have_dependency = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collQuestions) {
                foreach ($this->collQuestions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDummyQuestions) {
                foreach ($this->collDummyQuestions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collQuestionTypeFieldOptions) {
                foreach ($this->collQuestionTypeFieldOptions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collQuestionTypeI18ns) {
                foreach ($this->collQuestionTypeI18ns as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFieldOptions) {
                foreach ($this->collFieldOptions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aFieldType instanceof Persistent) {
              $this->aFieldType->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        // i18n behavior
        $this->currentLocale = 'en';
        $this->currentTranslations = null;

        if ($this->collQuestions instanceof PropelCollection) {
            $this->collQuestions->clearIterator();
        }
        $this->collQuestions = null;
        if ($this->collDummyQuestions instanceof PropelCollection) {
            $this->collDummyQuestions->clearIterator();
        }
        $this->collDummyQuestions = null;
        if ($this->collQuestionTypeFieldOptions instanceof PropelCollection) {
            $this->collQuestionTypeFieldOptions->clearIterator();
        }
        $this->collQuestionTypeFieldOptions = null;
        if ($this->collQuestionTypeI18ns instanceof PropelCollection) {
            $this->collQuestionTypeI18ns->clearIterator();
        }
        $this->collQuestionTypeI18ns = null;
        if ($this->collFieldOptions instanceof PropelCollection) {
            $this->collFieldOptions->clearIterator();
        }
        $this->collFieldOptions = null;
        $this->aFieldType = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(QuestionTypePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

    // event_dispatcher behavior

    /**
     * @return \Symfony\Component\EventDispatcher\EventDispatcher
     */
    static public function getEventDispatcher()
    {
        if (null === self::$dispatcher) {
            self::setEventDispatcher(new EventDispatcher());
        }

        return self::$dispatcher;
    }

    /**
     * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    static public function setEventDispatcher(EventDispatcherInterface $eventDispatcher)
    {
        self::$dispatcher = $eventDispatcher;
    }

    // i18n behavior

    /**
     * Sets the locale for translations
     *
     * @param     string $locale Locale to use for the translation, e.g. 'fr_FR'
     *
     * @return    QuestionType The current object (for fluent API support)
     */
    public function setLocale($locale = 'en')
    {
        $this->currentLocale = $locale;

        return $this;
    }

    /**
     * Gets the locale for translations
     *
     * @return    string $locale Locale to use for the translation, e.g. 'fr_FR'
     */
    public function getLocale()
    {
        return $this->currentLocale;
    }

    /**
     * Returns the current translation for a given locale
     *
     * @param     string $locale Locale to use for the translation, e.g. 'fr_FR'
     * @param     PropelPDO $con an optional connection object
     *
     * @return QuestionTypeI18n */
    public function getTranslation($locale = 'en', PropelPDO $con = null)
    {
        if (!isset($this->currentTranslations[$locale])) {
            if (null !== $this->collQuestionTypeI18ns) {
                foreach ($this->collQuestionTypeI18ns as $translation) {
                    if ($translation->getLocale() == $locale) {
                        $this->currentTranslations[$locale] = $translation;

                        return $translation;
                    }
                }
            }
            if ($this->isNew()) {
                $translation = new QuestionTypeI18n();
                $translation->setLocale($locale);
            } else {
                $translation = QuestionTypeI18nQuery::create()
                    ->filterByPrimaryKey(array($this->getPrimaryKey(), $locale))
                    ->findOneOrCreate($con);
                $this->currentTranslations[$locale] = $translation;
            }
            $this->addQuestionTypeI18n($translation);
        }

        return $this->currentTranslations[$locale];
    }

    /**
     * Remove the translation for a given locale
     *
     * @param     string $locale Locale to use for the translation, e.g. 'fr_FR'
     * @param     PropelPDO $con an optional connection object
     *
     * @return    QuestionType The current object (for fluent API support)
     */
    public function removeTranslation($locale = 'en', PropelPDO $con = null)
    {
        if (!$this->isNew()) {
            QuestionTypeI18nQuery::create()
                ->filterByPrimaryKey(array($this->getPrimaryKey(), $locale))
                ->delete($con);
        }
        if (isset($this->currentTranslations[$locale])) {
            unset($this->currentTranslations[$locale]);
        }
        foreach ($this->collQuestionTypeI18ns as $key => $translation) {
            if ($translation->getLocale() == $locale) {
                unset($this->collQuestionTypeI18ns[$key]);
                break;
            }
        }

        return $this;
    }

    /**
     * Returns the current translation
     *
     * @param     PropelPDO $con an optional connection object
     *
     * @return QuestionTypeI18n */
    public function getCurrentTranslation(PropelPDO $con = null)
    {
        return $this->getTranslation($this->getLocale(), $con);
    }


        /**
         * Get the [name] column value.
         *
         * @return string
         */
        public function getName()
        {
        return $this->getCurrentTranslation()->getName();
    }


        /**
         * Set the value of [name] column.
         *
         * @param  string $v new value
         * @return QuestionTypeI18n The current object (for fluent API support)
         */
        public function setName($v)
        {    $this->getCurrentTranslation()->setName($v);

        return $this;
    }


        /**
         * Get the [description] column value.
         *
         * @return string
         */
        public function getDescription()
        {
        return $this->getCurrentTranslation()->getDescription();
    }


        /**
         * Set the value of [description] column.
         *
         * @param  string $v new value
         * @return QuestionTypeI18n The current object (for fluent API support)
         */
        public function setDescription($v)
        {    $this->getCurrentTranslation()->setDescription($v);

        return $this;
    }

}

<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Axtion\Bundle\QuestionnaireBundle\Propel\DummyQuestion;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOption;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldType;
use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionType;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeFieldOption;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeI18n;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypePeer;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeQuery;

/**
 * @method QuestionTypeQuery orderById($order = Criteria::ASC) Order by the id column
 * @method QuestionTypeQuery orderByFieldTypeId($order = Criteria::ASC) Order by the field_type_id column
 * @method QuestionTypeQuery orderByHasSubquestions($order = Criteria::ASC) Order by the has_subquestions column
 * @method QuestionTypeQuery orderByIcon($order = Criteria::ASC) Order by the icon column
 * @method QuestionTypeQuery orderByMayHaveDependency($order = Criteria::ASC) Order by the may_have_dependency column
 *
 * @method QuestionTypeQuery groupById() Group by the id column
 * @method QuestionTypeQuery groupByFieldTypeId() Group by the field_type_id column
 * @method QuestionTypeQuery groupByHasSubquestions() Group by the has_subquestions column
 * @method QuestionTypeQuery groupByIcon() Group by the icon column
 * @method QuestionTypeQuery groupByMayHaveDependency() Group by the may_have_dependency column
 *
 * @method QuestionTypeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method QuestionTypeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method QuestionTypeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method QuestionTypeQuery leftJoinFieldType($relationAlias = null) Adds a LEFT JOIN clause to the query using the FieldType relation
 * @method QuestionTypeQuery rightJoinFieldType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FieldType relation
 * @method QuestionTypeQuery innerJoinFieldType($relationAlias = null) Adds a INNER JOIN clause to the query using the FieldType relation
 *
 * @method QuestionTypeQuery leftJoinQuestion($relationAlias = null) Adds a LEFT JOIN clause to the query using the Question relation
 * @method QuestionTypeQuery rightJoinQuestion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Question relation
 * @method QuestionTypeQuery innerJoinQuestion($relationAlias = null) Adds a INNER JOIN clause to the query using the Question relation
 *
 * @method QuestionTypeQuery leftJoinDummyQuestion($relationAlias = null) Adds a LEFT JOIN clause to the query using the DummyQuestion relation
 * @method QuestionTypeQuery rightJoinDummyQuestion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DummyQuestion relation
 * @method QuestionTypeQuery innerJoinDummyQuestion($relationAlias = null) Adds a INNER JOIN clause to the query using the DummyQuestion relation
 *
 * @method QuestionTypeQuery leftJoinQuestionTypeFieldOption($relationAlias = null) Adds a LEFT JOIN clause to the query using the QuestionTypeFieldOption relation
 * @method QuestionTypeQuery rightJoinQuestionTypeFieldOption($relationAlias = null) Adds a RIGHT JOIN clause to the query using the QuestionTypeFieldOption relation
 * @method QuestionTypeQuery innerJoinQuestionTypeFieldOption($relationAlias = null) Adds a INNER JOIN clause to the query using the QuestionTypeFieldOption relation
 *
 * @method QuestionTypeQuery leftJoinQuestionTypeI18n($relationAlias = null) Adds a LEFT JOIN clause to the query using the QuestionTypeI18n relation
 * @method QuestionTypeQuery rightJoinQuestionTypeI18n($relationAlias = null) Adds a RIGHT JOIN clause to the query using the QuestionTypeI18n relation
 * @method QuestionTypeQuery innerJoinQuestionTypeI18n($relationAlias = null) Adds a INNER JOIN clause to the query using the QuestionTypeI18n relation
 *
 * @method QuestionType findOne(PropelPDO $con = null) Return the first QuestionType matching the query
 * @method QuestionType findOneOrCreate(PropelPDO $con = null) Return the first QuestionType matching the query, or a new QuestionType object populated from the query conditions when no match is found
 *
 * @method QuestionType findOneByFieldTypeId(int $field_type_id) Return the first QuestionType filtered by the field_type_id column
 * @method QuestionType findOneByHasSubquestions(boolean $has_subquestions) Return the first QuestionType filtered by the has_subquestions column
 * @method QuestionType findOneByIcon(string $icon) Return the first QuestionType filtered by the icon column
 * @method QuestionType findOneByMayHaveDependency(boolean $may_have_dependency) Return the first QuestionType filtered by the may_have_dependency column
 *
 * @method array findById(int $id) Return QuestionType objects filtered by the id column
 * @method array findByFieldTypeId(int $field_type_id) Return QuestionType objects filtered by the field_type_id column
 * @method array findByHasSubquestions(boolean $has_subquestions) Return QuestionType objects filtered by the has_subquestions column
 * @method array findByIcon(string $icon) Return QuestionType objects filtered by the icon column
 * @method array findByMayHaveDependency(boolean $may_have_dependency) Return QuestionType objects filtered by the may_have_dependency column
 */
abstract class BaseQuestionTypeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseQuestionTypeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\QuestionType';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new QuestionTypeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   QuestionTypeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return QuestionTypeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof QuestionTypeQuery) {
            return $criteria;
        }
        $query = new QuestionTypeQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   QuestionType|QuestionType[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = QuestionTypePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(QuestionTypePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 QuestionType A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 QuestionType A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "id", "field_type_id", "has_subquestions", "icon", "may_have_dependency" FROM "questionnaire_question_type" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new QuestionType();
            $obj->hydrate($row);
            QuestionTypePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return QuestionType|QuestionType[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|QuestionType[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return QuestionTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(QuestionTypePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return QuestionTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(QuestionTypePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionTypeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(QuestionTypePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(QuestionTypePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionTypePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the field_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFieldTypeId(1234); // WHERE field_type_id = 1234
     * $query->filterByFieldTypeId(array(12, 34)); // WHERE field_type_id IN (12, 34)
     * $query->filterByFieldTypeId(array('min' => 12)); // WHERE field_type_id >= 12
     * $query->filterByFieldTypeId(array('max' => 12)); // WHERE field_type_id <= 12
     * </code>
     *
     * @see       filterByFieldType()
     *
     * @param     mixed $fieldTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionTypeQuery The current query, for fluid interface
     */
    public function filterByFieldTypeId($fieldTypeId = null, $comparison = null)
    {
        if (is_array($fieldTypeId)) {
            $useMinMax = false;
            if (isset($fieldTypeId['min'])) {
                $this->addUsingAlias(QuestionTypePeer::FIELD_TYPE_ID, $fieldTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fieldTypeId['max'])) {
                $this->addUsingAlias(QuestionTypePeer::FIELD_TYPE_ID, $fieldTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionTypePeer::FIELD_TYPE_ID, $fieldTypeId, $comparison);
    }

    /**
     * Filter the query on the has_subquestions column
     *
     * Example usage:
     * <code>
     * $query->filterByHasSubquestions(true); // WHERE has_subquestions = true
     * $query->filterByHasSubquestions('yes'); // WHERE has_subquestions = true
     * </code>
     *
     * @param     boolean|string $hasSubquestions The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionTypeQuery The current query, for fluid interface
     */
    public function filterByHasSubquestions($hasSubquestions = null, $comparison = null)
    {
        if (is_string($hasSubquestions)) {
            $hasSubquestions = in_array(strtolower($hasSubquestions), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(QuestionTypePeer::HAS_SUBQUESTIONS, $hasSubquestions, $comparison);
    }

    /**
     * Filter the query on the icon column
     *
     * Example usage:
     * <code>
     * $query->filterByIcon('fooValue');   // WHERE icon = 'fooValue'
     * $query->filterByIcon('%fooValue%'); // WHERE icon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $icon The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionTypeQuery The current query, for fluid interface
     */
    public function filterByIcon($icon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($icon)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $icon)) {
                $icon = str_replace('*', '%', $icon);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionTypePeer::ICON, $icon, $comparison);
    }

    /**
     * Filter the query on the may_have_dependency column
     *
     * Example usage:
     * <code>
     * $query->filterByMayHaveDependency(true); // WHERE may_have_dependency = true
     * $query->filterByMayHaveDependency('yes'); // WHERE may_have_dependency = true
     * </code>
     *
     * @param     boolean|string $mayHaveDependency The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionTypeQuery The current query, for fluid interface
     */
    public function filterByMayHaveDependency($mayHaveDependency = null, $comparison = null)
    {
        if (is_string($mayHaveDependency)) {
            $mayHaveDependency = in_array(strtolower($mayHaveDependency), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(QuestionTypePeer::MAY_HAVE_DEPENDENCY, $mayHaveDependency, $comparison);
    }

    /**
     * Filter the query by a related FieldType object
     *
     * @param   FieldType|PropelObjectCollection $fieldType The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 QuestionTypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByFieldType($fieldType, $comparison = null)
    {
        if ($fieldType instanceof FieldType) {
            return $this
                ->addUsingAlias(QuestionTypePeer::FIELD_TYPE_ID, $fieldType->getId(), $comparison);
        } elseif ($fieldType instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(QuestionTypePeer::FIELD_TYPE_ID, $fieldType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFieldType() only accepts arguments of type FieldType or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FieldType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return QuestionTypeQuery The current query, for fluid interface
     */
    public function joinFieldType($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FieldType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FieldType');
        }

        return $this;
    }

    /**
     * Use the FieldType relation FieldType object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\FieldTypeQuery A secondary query class using the current class as primary query
     */
    public function useFieldTypeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFieldType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FieldType', '\Axtion\Bundle\QuestionnaireBundle\Propel\FieldTypeQuery');
    }

    /**
     * Filter the query by a related Question object
     *
     * @param   Question|PropelObjectCollection $question  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 QuestionTypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByQuestion($question, $comparison = null)
    {
        if ($question instanceof Question) {
            return $this
                ->addUsingAlias(QuestionTypePeer::ID, $question->getQuestionTypeId(), $comparison);
        } elseif ($question instanceof PropelObjectCollection) {
            return $this
                ->useQuestionQuery()
                ->filterByPrimaryKeys($question->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByQuestion() only accepts arguments of type Question or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Question relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return QuestionTypeQuery The current query, for fluid interface
     */
    public function joinQuestion($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Question');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Question');
        }

        return $this;
    }

    /**
     * Use the Question relation Question object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery A secondary query class using the current class as primary query
     */
    public function useQuestionQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinQuestion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Question', '\Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery');
    }

    /**
     * Filter the query by a related DummyQuestion object
     *
     * @param   DummyQuestion|PropelObjectCollection $dummyQuestion  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 QuestionTypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByDummyQuestion($dummyQuestion, $comparison = null)
    {
        if ($dummyQuestion instanceof DummyQuestion) {
            return $this
                ->addUsingAlias(QuestionTypePeer::ID, $dummyQuestion->getQuestionTypeId(), $comparison);
        } elseif ($dummyQuestion instanceof PropelObjectCollection) {
            return $this
                ->useDummyQuestionQuery()
                ->filterByPrimaryKeys($dummyQuestion->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDummyQuestion() only accepts arguments of type DummyQuestion or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DummyQuestion relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return QuestionTypeQuery The current query, for fluid interface
     */
    public function joinDummyQuestion($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DummyQuestion');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DummyQuestion');
        }

        return $this;
    }

    /**
     * Use the DummyQuestion relation DummyQuestion object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\DummyQuestionQuery A secondary query class using the current class as primary query
     */
    public function useDummyQuestionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDummyQuestion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DummyQuestion', '\Axtion\Bundle\QuestionnaireBundle\Propel\DummyQuestionQuery');
    }

    /**
     * Filter the query by a related QuestionTypeFieldOption object
     *
     * @param   QuestionTypeFieldOption|PropelObjectCollection $questionTypeFieldOption  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 QuestionTypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByQuestionTypeFieldOption($questionTypeFieldOption, $comparison = null)
    {
        if ($questionTypeFieldOption instanceof QuestionTypeFieldOption) {
            return $this
                ->addUsingAlias(QuestionTypePeer::ID, $questionTypeFieldOption->getQuestionTypeId(), $comparison);
        } elseif ($questionTypeFieldOption instanceof PropelObjectCollection) {
            return $this
                ->useQuestionTypeFieldOptionQuery()
                ->filterByPrimaryKeys($questionTypeFieldOption->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByQuestionTypeFieldOption() only accepts arguments of type QuestionTypeFieldOption or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the QuestionTypeFieldOption relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return QuestionTypeQuery The current query, for fluid interface
     */
    public function joinQuestionTypeFieldOption($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('QuestionTypeFieldOption');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'QuestionTypeFieldOption');
        }

        return $this;
    }

    /**
     * Use the QuestionTypeFieldOption relation QuestionTypeFieldOption object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeFieldOptionQuery A secondary query class using the current class as primary query
     */
    public function useQuestionTypeFieldOptionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinQuestionTypeFieldOption($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'QuestionTypeFieldOption', '\Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeFieldOptionQuery');
    }

    /**
     * Filter the query by a related QuestionTypeI18n object
     *
     * @param   QuestionTypeI18n|PropelObjectCollection $questionTypeI18n  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 QuestionTypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByQuestionTypeI18n($questionTypeI18n, $comparison = null)
    {
        if ($questionTypeI18n instanceof QuestionTypeI18n) {
            return $this
                ->addUsingAlias(QuestionTypePeer::ID, $questionTypeI18n->getId(), $comparison);
        } elseif ($questionTypeI18n instanceof PropelObjectCollection) {
            return $this
                ->useQuestionTypeI18nQuery()
                ->filterByPrimaryKeys($questionTypeI18n->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByQuestionTypeI18n() only accepts arguments of type QuestionTypeI18n or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the QuestionTypeI18n relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return QuestionTypeQuery The current query, for fluid interface
     */
    public function joinQuestionTypeI18n($relationAlias = null, $joinType = 'LEFT JOIN')
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('QuestionTypeI18n');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'QuestionTypeI18n');
        }

        return $this;
    }

    /**
     * Use the QuestionTypeI18n relation QuestionTypeI18n object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeI18nQuery A secondary query class using the current class as primary query
     */
    public function useQuestionTypeI18nQuery($relationAlias = null, $joinType = 'LEFT JOIN')
    {
        return $this
            ->joinQuestionTypeI18n($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'QuestionTypeI18n', '\Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeI18nQuery');
    }

    /**
     * Filter the query by a related FieldOption object
     * using the questionnaire_question_type_field_option table as cross reference
     *
     * @param   FieldOption $fieldOption the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   QuestionTypeQuery The current query, for fluid interface
     */
    public function filterByFieldOption($fieldOption, $comparison = Criteria::EQUAL)
    {
        return $this
            ->useQuestionTypeFieldOptionQuery()
            ->filterByFieldOption($fieldOption, $comparison)
            ->endUse();
    }

    /**
     * Exclude object from result
     *
     * @param   QuestionType $questionType Object to remove from the list of results
     *
     * @return QuestionTypeQuery The current query, for fluid interface
     */
    public function prune($questionType = null)
    {
        if ($questionType) {
            $this->addUsingAlias(QuestionTypePeer::ID, $questionType->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    // i18n behavior

    /**
     * Adds a JOIN clause to the query using the i18n relation
     *
     * @param     string $locale Locale to use for the join condition, e.g. 'fr_FR'
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'. Defaults to left join.
     *
     * @return    QuestionTypeQuery The current query, for fluid interface
     */
    public function joinI18n($locale = 'en', $relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $relationName = $relationAlias ? $relationAlias : 'QuestionTypeI18n';

        return $this
            ->joinQuestionTypeI18n($relationAlias, $joinType)
            ->addJoinCondition($relationName, $relationName . '.Locale = ?', $locale);
    }

    /**
     * Adds a JOIN clause to the query and hydrates the related I18n object.
     * Shortcut for $c->joinI18n($locale)->with()
     *
     * @param     string $locale Locale to use for the join condition, e.g. 'fr_FR'
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'. Defaults to left join.
     *
     * @return    QuestionTypeQuery The current query, for fluid interface
     */
    public function joinWithI18n($locale = 'en', $joinType = Criteria::LEFT_JOIN)
    {
        $this
            ->joinI18n($locale, null, $joinType)
            ->with('QuestionTypeI18n');
        $this->with['QuestionTypeI18n']->setIsWithOneToMany(false);

        return $this;
    }

    /**
     * Use the I18n relation query object
     *
     * @see       useQuery()
     *
     * @param     string $locale Locale to use for the join condition, e.g. 'fr_FR'
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'. Defaults to left join.
     *
     * @return    QuestionTypeI18nQuery A secondary query class using the current class as primary query
     */
    public function useI18nQuery($locale = 'en', $relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinI18n($locale, $relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'QuestionTypeI18n', 'Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeI18nQuery');
    }

}

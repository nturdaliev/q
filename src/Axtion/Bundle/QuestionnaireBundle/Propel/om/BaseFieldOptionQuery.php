<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOption;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionPeer;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionTab;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldType;
use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionType;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeFieldOption;
use Axtion\Bundle\QuestionnaireBundle\Propel\SelectedOption;

/**
 * @method FieldOptionQuery orderById($order = Criteria::ASC) Order by the id column
 * @method FieldOptionQuery orderByFieldTypeId($order = Criteria::ASC) Order by the field_type_id column
 * @method FieldOptionQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method FieldOptionQuery orderByFieldOptionTabId($order = Criteria::ASC) Order by the field_option_tab_id column
 *
 * @method FieldOptionQuery groupById() Group by the id column
 * @method FieldOptionQuery groupByFieldTypeId() Group by the field_type_id column
 * @method FieldOptionQuery groupByName() Group by the name column
 * @method FieldOptionQuery groupByFieldOptionTabId() Group by the field_option_tab_id column
 *
 * @method FieldOptionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method FieldOptionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method FieldOptionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method FieldOptionQuery leftJoinFieldType($relationAlias = null) Adds a LEFT JOIN clause to the query using the FieldType relation
 * @method FieldOptionQuery rightJoinFieldType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FieldType relation
 * @method FieldOptionQuery innerJoinFieldType($relationAlias = null) Adds a INNER JOIN clause to the query using the FieldType relation
 *
 * @method FieldOptionQuery leftJoinFieldOptionTab($relationAlias = null) Adds a LEFT JOIN clause to the query using the FieldOptionTab relation
 * @method FieldOptionQuery rightJoinFieldOptionTab($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FieldOptionTab relation
 * @method FieldOptionQuery innerJoinFieldOptionTab($relationAlias = null) Adds a INNER JOIN clause to the query using the FieldOptionTab relation
 *
 * @method FieldOptionQuery leftJoinSelectedOption($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelectedOption relation
 * @method FieldOptionQuery rightJoinSelectedOption($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelectedOption relation
 * @method FieldOptionQuery innerJoinSelectedOption($relationAlias = null) Adds a INNER JOIN clause to the query using the SelectedOption relation
 *
 * @method FieldOptionQuery leftJoinQuestionTypeFieldOption($relationAlias = null) Adds a LEFT JOIN clause to the query using the QuestionTypeFieldOption relation
 * @method FieldOptionQuery rightJoinQuestionTypeFieldOption($relationAlias = null) Adds a RIGHT JOIN clause to the query using the QuestionTypeFieldOption relation
 * @method FieldOptionQuery innerJoinQuestionTypeFieldOption($relationAlias = null) Adds a INNER JOIN clause to the query using the QuestionTypeFieldOption relation
 *
 * @method FieldOption findOne(PropelPDO $con = null) Return the first FieldOption matching the query
 * @method FieldOption findOneOrCreate(PropelPDO $con = null) Return the first FieldOption matching the query, or a new FieldOption object populated from the query conditions when no match is found
 *
 * @method FieldOption findOneByFieldTypeId(int $field_type_id) Return the first FieldOption filtered by the field_type_id column
 * @method FieldOption findOneByName(string $name) Return the first FieldOption filtered by the name column
 * @method FieldOption findOneByFieldOptionTabId(int $field_option_tab_id) Return the first FieldOption filtered by the field_option_tab_id column
 *
 * @method array findById(int $id) Return FieldOption objects filtered by the id column
 * @method array findByFieldTypeId(int $field_type_id) Return FieldOption objects filtered by the field_type_id column
 * @method array findByName(string $name) Return FieldOption objects filtered by the name column
 * @method array findByFieldOptionTabId(int $field_option_tab_id) Return FieldOption objects filtered by the field_option_tab_id column
 */
abstract class BaseFieldOptionQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseFieldOptionQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\FieldOption';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new FieldOptionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   FieldOptionQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return FieldOptionQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof FieldOptionQuery) {
            return $criteria;
        }
        $query = new FieldOptionQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   FieldOption|FieldOption[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FieldOptionPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(FieldOptionPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 FieldOption A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 FieldOption A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "id", "field_type_id", "name", "field_option_tab_id" FROM "questionnaire_field_option" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new FieldOption();
            $obj->hydrate($row);
            FieldOptionPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return FieldOption|FieldOption[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|FieldOption[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return FieldOptionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FieldOptionPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return FieldOptionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FieldOptionPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FieldOptionQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FieldOptionPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FieldOptionPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FieldOptionPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the field_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFieldTypeId(1234); // WHERE field_type_id = 1234
     * $query->filterByFieldTypeId(array(12, 34)); // WHERE field_type_id IN (12, 34)
     * $query->filterByFieldTypeId(array('min' => 12)); // WHERE field_type_id >= 12
     * $query->filterByFieldTypeId(array('max' => 12)); // WHERE field_type_id <= 12
     * </code>
     *
     * @see       filterByFieldType()
     *
     * @param     mixed $fieldTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FieldOptionQuery The current query, for fluid interface
     */
    public function filterByFieldTypeId($fieldTypeId = null, $comparison = null)
    {
        if (is_array($fieldTypeId)) {
            $useMinMax = false;
            if (isset($fieldTypeId['min'])) {
                $this->addUsingAlias(FieldOptionPeer::FIELD_TYPE_ID, $fieldTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fieldTypeId['max'])) {
                $this->addUsingAlias(FieldOptionPeer::FIELD_TYPE_ID, $fieldTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FieldOptionPeer::FIELD_TYPE_ID, $fieldTypeId, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FieldOptionQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FieldOptionPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the field_option_tab_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFieldOptionTabId(1234); // WHERE field_option_tab_id = 1234
     * $query->filterByFieldOptionTabId(array(12, 34)); // WHERE field_option_tab_id IN (12, 34)
     * $query->filterByFieldOptionTabId(array('min' => 12)); // WHERE field_option_tab_id >= 12
     * $query->filterByFieldOptionTabId(array('max' => 12)); // WHERE field_option_tab_id <= 12
     * </code>
     *
     * @see       filterByFieldOptionTab()
     *
     * @param     mixed $fieldOptionTabId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FieldOptionQuery The current query, for fluid interface
     */
    public function filterByFieldOptionTabId($fieldOptionTabId = null, $comparison = null)
    {
        if (is_array($fieldOptionTabId)) {
            $useMinMax = false;
            if (isset($fieldOptionTabId['min'])) {
                $this->addUsingAlias(FieldOptionPeer::FIELD_OPTION_TAB_ID, $fieldOptionTabId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fieldOptionTabId['max'])) {
                $this->addUsingAlias(FieldOptionPeer::FIELD_OPTION_TAB_ID, $fieldOptionTabId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FieldOptionPeer::FIELD_OPTION_TAB_ID, $fieldOptionTabId, $comparison);
    }

    /**
     * Filter the query by a related FieldType object
     *
     * @param   FieldType|PropelObjectCollection $fieldType The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 FieldOptionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByFieldType($fieldType, $comparison = null)
    {
        if ($fieldType instanceof FieldType) {
            return $this
                ->addUsingAlias(FieldOptionPeer::FIELD_TYPE_ID, $fieldType->getId(), $comparison);
        } elseif ($fieldType instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FieldOptionPeer::FIELD_TYPE_ID, $fieldType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFieldType() only accepts arguments of type FieldType or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FieldType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return FieldOptionQuery The current query, for fluid interface
     */
    public function joinFieldType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FieldType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FieldType');
        }

        return $this;
    }

    /**
     * Use the FieldType relation FieldType object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\FieldTypeQuery A secondary query class using the current class as primary query
     */
    public function useFieldTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFieldType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FieldType', '\Axtion\Bundle\QuestionnaireBundle\Propel\FieldTypeQuery');
    }

    /**
     * Filter the query by a related FieldOptionTab object
     *
     * @param   FieldOptionTab|PropelObjectCollection $fieldOptionTab The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 FieldOptionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByFieldOptionTab($fieldOptionTab, $comparison = null)
    {
        if ($fieldOptionTab instanceof FieldOptionTab) {
            return $this
                ->addUsingAlias(FieldOptionPeer::FIELD_OPTION_TAB_ID, $fieldOptionTab->getId(), $comparison);
        } elseif ($fieldOptionTab instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FieldOptionPeer::FIELD_OPTION_TAB_ID, $fieldOptionTab->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFieldOptionTab() only accepts arguments of type FieldOptionTab or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FieldOptionTab relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return FieldOptionQuery The current query, for fluid interface
     */
    public function joinFieldOptionTab($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FieldOptionTab');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FieldOptionTab');
        }

        return $this;
    }

    /**
     * Use the FieldOptionTab relation FieldOptionTab object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionTabQuery A secondary query class using the current class as primary query
     */
    public function useFieldOptionTabQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFieldOptionTab($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FieldOptionTab', '\Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionTabQuery');
    }

    /**
     * Filter the query by a related SelectedOption object
     *
     * @param   SelectedOption|PropelObjectCollection $selectedOption  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 FieldOptionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySelectedOption($selectedOption, $comparison = null)
    {
        if ($selectedOption instanceof SelectedOption) {
            return $this
                ->addUsingAlias(FieldOptionPeer::ID, $selectedOption->getOptionId(), $comparison);
        } elseif ($selectedOption instanceof PropelObjectCollection) {
            return $this
                ->useSelectedOptionQuery()
                ->filterByPrimaryKeys($selectedOption->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelectedOption() only accepts arguments of type SelectedOption or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelectedOption relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return FieldOptionQuery The current query, for fluid interface
     */
    public function joinSelectedOption($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelectedOption');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelectedOption');
        }

        return $this;
    }

    /**
     * Use the SelectedOption relation SelectedOption object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\SelectedOptionQuery A secondary query class using the current class as primary query
     */
    public function useSelectedOptionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelectedOption($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelectedOption', '\Axtion\Bundle\QuestionnaireBundle\Propel\SelectedOptionQuery');
    }

    /**
     * Filter the query by a related QuestionTypeFieldOption object
     *
     * @param   QuestionTypeFieldOption|PropelObjectCollection $questionTypeFieldOption  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 FieldOptionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByQuestionTypeFieldOption($questionTypeFieldOption, $comparison = null)
    {
        if ($questionTypeFieldOption instanceof QuestionTypeFieldOption) {
            return $this
                ->addUsingAlias(FieldOptionPeer::ID, $questionTypeFieldOption->getOptionId(), $comparison);
        } elseif ($questionTypeFieldOption instanceof PropelObjectCollection) {
            return $this
                ->useQuestionTypeFieldOptionQuery()
                ->filterByPrimaryKeys($questionTypeFieldOption->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByQuestionTypeFieldOption() only accepts arguments of type QuestionTypeFieldOption or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the QuestionTypeFieldOption relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return FieldOptionQuery The current query, for fluid interface
     */
    public function joinQuestionTypeFieldOption($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('QuestionTypeFieldOption');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'QuestionTypeFieldOption');
        }

        return $this;
    }

    /**
     * Use the QuestionTypeFieldOption relation QuestionTypeFieldOption object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeFieldOptionQuery A secondary query class using the current class as primary query
     */
    public function useQuestionTypeFieldOptionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinQuestionTypeFieldOption($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'QuestionTypeFieldOption', '\Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeFieldOptionQuery');
    }

    /**
     * Filter the query by a related Question object
     * using the questionnaire_selected_option table as cross reference
     *
     * @param   Question $question the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   FieldOptionQuery The current query, for fluid interface
     */
    public function filterByQuestion($question, $comparison = Criteria::EQUAL)
    {
        return $this
            ->useSelectedOptionQuery()
            ->filterByQuestion($question, $comparison)
            ->endUse();
    }

    /**
     * Filter the query by a related QuestionType object
     * using the questionnaire_question_type_field_option table as cross reference
     *
     * @param   QuestionType $questionType the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   FieldOptionQuery The current query, for fluid interface
     */
    public function filterByQuestionType($questionType, $comparison = Criteria::EQUAL)
    {
        return $this
            ->useQuestionTypeFieldOptionQuery()
            ->filterByQuestionType($questionType, $comparison)
            ->endUse();
    }

    /**
     * Exclude object from result
     *
     * @param   FieldOption $fieldOption Object to remove from the list of results
     *
     * @return FieldOptionQuery The current query, for fluid interface
     */
    public function prune($fieldOption = null)
    {
        if ($fieldOption) {
            $this->addUsingAlias(FieldOptionPeer::ID, $fieldOption->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

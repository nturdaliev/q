<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Axtion\Bundle\QuestionnaireBundle\Propel\Configuration;
use Axtion\Bundle\QuestionnaireBundle\Propel\ConfigurationKey;
use Axtion\Bundle\QuestionnaireBundle\Propel\ConfigurationPeer;
use Axtion\Bundle\QuestionnaireBundle\Propel\ConfigurationQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Device;

/**
 * @method ConfigurationQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ConfigurationQuery orderByValue($order = Criteria::ASC) Order by the value column
 * @method ConfigurationQuery orderByDeviceId($order = Criteria::ASC) Order by the device_id column
 * @method ConfigurationQuery orderByConfigurationKeyId($order = Criteria::ASC) Order by the configuration_key_id column
 *
 * @method ConfigurationQuery groupById() Group by the id column
 * @method ConfigurationQuery groupByValue() Group by the value column
 * @method ConfigurationQuery groupByDeviceId() Group by the device_id column
 * @method ConfigurationQuery groupByConfigurationKeyId() Group by the configuration_key_id column
 *
 * @method ConfigurationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ConfigurationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ConfigurationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ConfigurationQuery leftJoinDevice($relationAlias = null) Adds a LEFT JOIN clause to the query using the Device relation
 * @method ConfigurationQuery rightJoinDevice($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Device relation
 * @method ConfigurationQuery innerJoinDevice($relationAlias = null) Adds a INNER JOIN clause to the query using the Device relation
 *
 * @method ConfigurationQuery leftJoinConfigurationKey($relationAlias = null) Adds a LEFT JOIN clause to the query using the ConfigurationKey relation
 * @method ConfigurationQuery rightJoinConfigurationKey($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ConfigurationKey relation
 * @method ConfigurationQuery innerJoinConfigurationKey($relationAlias = null) Adds a INNER JOIN clause to the query using the ConfigurationKey relation
 *
 * @method Configuration findOne(PropelPDO $con = null) Return the first Configuration matching the query
 * @method Configuration findOneOrCreate(PropelPDO $con = null) Return the first Configuration matching the query, or a new Configuration object populated from the query conditions when no match is found
 *
 * @method Configuration findOneByValue(string $value) Return the first Configuration filtered by the value column
 * @method Configuration findOneByDeviceId(int $device_id) Return the first Configuration filtered by the device_id column
 * @method Configuration findOneByConfigurationKeyId(int $configuration_key_id) Return the first Configuration filtered by the configuration_key_id column
 *
 * @method array findById(int $id) Return Configuration objects filtered by the id column
 * @method array findByValue(string $value) Return Configuration objects filtered by the value column
 * @method array findByDeviceId(int $device_id) Return Configuration objects filtered by the device_id column
 * @method array findByConfigurationKeyId(int $configuration_key_id) Return Configuration objects filtered by the configuration_key_id column
 */
abstract class BaseConfigurationQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseConfigurationQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Configuration';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ConfigurationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ConfigurationQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ConfigurationQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ConfigurationQuery) {
            return $criteria;
        }
        $query = new ConfigurationQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Configuration|Configuration[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ConfigurationPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ConfigurationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Configuration A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Configuration A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "id", "value", "device_id", "configuration_key_id" FROM "questionnaire_configuration" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Configuration();
            $obj->hydrate($row);
            ConfigurationPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Configuration|Configuration[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Configuration[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ConfigurationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ConfigurationPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ConfigurationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ConfigurationPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ConfigurationQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ConfigurationPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ConfigurationPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConfigurationPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the value column
     *
     * Example usage:
     * <code>
     * $query->filterByValue('fooValue');   // WHERE value = 'fooValue'
     * $query->filterByValue('%fooValue%'); // WHERE value LIKE '%fooValue%'
     * </code>
     *
     * @param     string $value The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ConfigurationQuery The current query, for fluid interface
     */
    public function filterByValue($value = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($value)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $value)) {
                $value = str_replace('*', '%', $value);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ConfigurationPeer::VALUE, $value, $comparison);
    }

    /**
     * Filter the query on the device_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDeviceId(1234); // WHERE device_id = 1234
     * $query->filterByDeviceId(array(12, 34)); // WHERE device_id IN (12, 34)
     * $query->filterByDeviceId(array('min' => 12)); // WHERE device_id >= 12
     * $query->filterByDeviceId(array('max' => 12)); // WHERE device_id <= 12
     * </code>
     *
     * @see       filterByDevice()
     *
     * @param     mixed $deviceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ConfigurationQuery The current query, for fluid interface
     */
    public function filterByDeviceId($deviceId = null, $comparison = null)
    {
        if (is_array($deviceId)) {
            $useMinMax = false;
            if (isset($deviceId['min'])) {
                $this->addUsingAlias(ConfigurationPeer::DEVICE_ID, $deviceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deviceId['max'])) {
                $this->addUsingAlias(ConfigurationPeer::DEVICE_ID, $deviceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConfigurationPeer::DEVICE_ID, $deviceId, $comparison);
    }

    /**
     * Filter the query on the configuration_key_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConfigurationKeyId(1234); // WHERE configuration_key_id = 1234
     * $query->filterByConfigurationKeyId(array(12, 34)); // WHERE configuration_key_id IN (12, 34)
     * $query->filterByConfigurationKeyId(array('min' => 12)); // WHERE configuration_key_id >= 12
     * $query->filterByConfigurationKeyId(array('max' => 12)); // WHERE configuration_key_id <= 12
     * </code>
     *
     * @see       filterByConfigurationKey()
     *
     * @param     mixed $configurationKeyId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ConfigurationQuery The current query, for fluid interface
     */
    public function filterByConfigurationKeyId($configurationKeyId = null, $comparison = null)
    {
        if (is_array($configurationKeyId)) {
            $useMinMax = false;
            if (isset($configurationKeyId['min'])) {
                $this->addUsingAlias(ConfigurationPeer::CONFIGURATION_KEY_ID, $configurationKeyId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($configurationKeyId['max'])) {
                $this->addUsingAlias(ConfigurationPeer::CONFIGURATION_KEY_ID, $configurationKeyId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConfigurationPeer::CONFIGURATION_KEY_ID, $configurationKeyId, $comparison);
    }

    /**
     * Filter the query by a related Device object
     *
     * @param   Device|PropelObjectCollection $device The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ConfigurationQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByDevice($device, $comparison = null)
    {
        if ($device instanceof Device) {
            return $this
                ->addUsingAlias(ConfigurationPeer::DEVICE_ID, $device->getId(), $comparison);
        } elseif ($device instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ConfigurationPeer::DEVICE_ID, $device->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDevice() only accepts arguments of type Device or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Device relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ConfigurationQuery The current query, for fluid interface
     */
    public function joinDevice($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Device');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Device');
        }

        return $this;
    }

    /**
     * Use the Device relation Device object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\DeviceQuery A secondary query class using the current class as primary query
     */
    public function useDeviceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinDevice($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Device', '\Axtion\Bundle\QuestionnaireBundle\Propel\DeviceQuery');
    }

    /**
     * Filter the query by a related ConfigurationKey object
     *
     * @param   ConfigurationKey|PropelObjectCollection $configurationKey The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ConfigurationQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByConfigurationKey($configurationKey, $comparison = null)
    {
        if ($configurationKey instanceof ConfigurationKey) {
            return $this
                ->addUsingAlias(ConfigurationPeer::CONFIGURATION_KEY_ID, $configurationKey->getId(), $comparison);
        } elseif ($configurationKey instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ConfigurationPeer::CONFIGURATION_KEY_ID, $configurationKey->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByConfigurationKey() only accepts arguments of type ConfigurationKey or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ConfigurationKey relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ConfigurationQuery The current query, for fluid interface
     */
    public function joinConfigurationKey($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ConfigurationKey');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ConfigurationKey');
        }

        return $this;
    }

    /**
     * Use the ConfigurationKey relation ConfigurationKey object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\ConfigurationKeyQuery A secondary query class using the current class as primary query
     */
    public function useConfigurationKeyQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinConfigurationKey($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ConfigurationKey', '\Axtion\Bundle\QuestionnaireBundle\Propel\ConfigurationKeyQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Configuration $configuration Object to remove from the list of results
     *
     * @return ConfigurationQuery The current query, for fluid interface
     */
    public function prune($configuration = null)
    {
        if ($configuration) {
            $this->addUsingAlias(ConfigurationPeer::ID, $configuration->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

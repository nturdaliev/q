<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \EventDispatcherAwareModelInterface;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Axtion\Bundle\QuestionnaireBundle\Propel\Questionnaire;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionnaireQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Status;
use Axtion\Bundle\QuestionnaireBundle\Propel\StatusI18n;
use Axtion\Bundle\QuestionnaireBundle\Propel\StatusI18nQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\StatusPeer;
use Axtion\Bundle\QuestionnaireBundle\Propel\StatusQuery;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

abstract class BaseStatus extends BaseObject implements Persistent, EventDispatcherAwareModelInterface
{
    /**
     * Peer class name
     */
    const PEER = 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\StatusPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        StatusPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * @var        PropelObjectCollection|Questionnaire[] Collection to store aggregation of Questionnaire objects.
     */
    protected $collQuestionnaires;
    protected $collQuestionnairesPartial;

    /**
     * @var        PropelObjectCollection|StatusI18n[] Collection to store aggregation of StatusI18n objects.
     */
    protected $collStatusI18ns;
    protected $collStatusI18nsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    // i18n behavior

    /**
     * Current locale
     * @var        string
     */
    protected $currentLocale = 'en_US';

    /**
     * Current translation objects
     * @var        array[StatusI18n]
     */
    protected $currentTranslations;

    // event_dispatcher behavior

    const EVENT_CONSTRUCT = 'propel.construct';

    const EVENT_POST_HYDRATE = 'propel.post_hydrate';

    const EVENT_PRE_SAVE = 'propel.pre_save';

    const EVENT_POST_SAVE = 'propel.post_save';

    const EVENT_PRE_UPDATE = 'propel.pre_update';

    const EVENT_POST_UPDATE = 'propel.post_update';

    const EVENT_PRE_INSERT = 'propel.pre_insert';

    const EVENT_POST_INSERT = 'propel.post_insert';

    const EVENT_PRE_DELETE = 'propel.pre_delete';

    const EVENT_POST_DELETE = 'propel.post_delete';

    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcher
     */
    static private $dispatcher = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $questionnairesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $statusI18nsScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return Status The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = StatusPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            // event_dispatcher behavior
            self::getEventDispatcher()->dispatch('propel.post_hydrate', new GenericEvent($this));


            return $startcol + 1; // 1 = StatusPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Status object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(StatusPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = StatusPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collQuestionnaires = null;

            $this->collStatusI18ns = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(StatusPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = StatusQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // event_dispatcher behavior
            self::getEventDispatcher()->dispatch('propel.pre_delete', new GenericEvent($this, array('connection' => $con)));

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                // event_dispatcher behavior
                self::getEventDispatcher()->dispatch('propel.post_delete', new GenericEvent($this, array('connection' => $con)));

                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(StatusPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            // event_dispatcher behavior
            self::getEventDispatcher()->dispatch('propel.pre_save', new GenericEvent($this, array('connection' => $con)));

            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // event_dispatcher behavior
                self::getEventDispatcher()->dispatch('propel.pre_insert', new GenericEvent($this, array('connection' => $con)));

            } else {
                $ret = $ret && $this->preUpdate($con);
                // event_dispatcher behavior
                self::getEventDispatcher()->dispatch('propel.pre_update', new GenericEvent($this, array('connection' => $con)));

            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                    // event_dispatcher behavior
                    self::getEventDispatcher()->dispatch('propel.post_insert', new GenericEvent($this, array('connection' => $con)));

                } else {
                    $this->postUpdate($con);
                    // event_dispatcher behavior
                    self::getEventDispatcher()->dispatch('propel.post_update', new GenericEvent($this, array('connection' => $con)));

                }
                $this->postSave($con);
                // event_dispatcher behavior
                self::getEventDispatcher()->dispatch('propel.post_save', new GenericEvent($this, array('connection' => $con)));

                StatusPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->questionnairesScheduledForDeletion !== null) {
                if (!$this->questionnairesScheduledForDeletion->isEmpty()) {
                    QuestionnaireQuery::create()
                        ->filterByPrimaryKeys($this->questionnairesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->questionnairesScheduledForDeletion = null;
                }
            }

            if ($this->collQuestionnaires !== null) {
                foreach ($this->collQuestionnaires as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->statusI18nsScheduledForDeletion !== null) {
                if (!$this->statusI18nsScheduledForDeletion->isEmpty()) {
                    StatusI18nQuery::create()
                        ->filterByPrimaryKeys($this->statusI18nsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->statusI18nsScheduledForDeletion = null;
                }
            }

            if ($this->collStatusI18ns !== null) {
                foreach ($this->collStatusI18ns as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = StatusPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . StatusPeer::ID . ')');
        }
        if (null === $this->id) {
            try {
                $stmt = $con->query("SELECT nextval('questionnaire_status_id_seq')");
                $row = $stmt->fetch(PDO::FETCH_NUM);
                $this->id = $row[0];
            } catch (Exception $e) {
                throw new PropelException('Unable to get sequence id.', $e);
            }
        }


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(StatusPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '"id"';
        }

        $sql = sprintf(
            'INSERT INTO "questionnaire_status" (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '"id"':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = StatusPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collQuestionnaires !== null) {
                    foreach ($this->collQuestionnaires as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collStatusI18ns !== null) {
                    foreach ($this->collStatusI18ns as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = StatusPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Status'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Status'][$this->getPrimaryKey()] = true;
        $keys = StatusPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collQuestionnaires) {
                $result['Questionnaires'] = $this->collQuestionnaires->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collStatusI18ns) {
                $result['StatusI18ns'] = $this->collStatusI18ns->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = StatusPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = StatusPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(StatusPeer::DATABASE_NAME);

        if ($this->isColumnModified(StatusPeer::ID)) $criteria->add(StatusPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(StatusPeer::DATABASE_NAME);
        $criteria->add(StatusPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Status (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getQuestionnaires() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addQuestionnaire($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getStatusI18ns() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addStatusI18n($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Status Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return StatusPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new StatusPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Questionnaire' == $relationName) {
            $this->initQuestionnaires();
        }
        if ('StatusI18n' == $relationName) {
            $this->initStatusI18ns();
        }
    }

    /**
     * Clears out the collQuestionnaires collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Status The current object (for fluent API support)
     * @see        addQuestionnaires()
     */
    public function clearQuestionnaires()
    {
        $this->collQuestionnaires = null; // important to set this to null since that means it is uninitialized
        $this->collQuestionnairesPartial = null;

        return $this;
    }

    /**
     * reset is the collQuestionnaires collection loaded partially
     *
     * @return void
     */
    public function resetPartialQuestionnaires($v = true)
    {
        $this->collQuestionnairesPartial = $v;
    }

    /**
     * Initializes the collQuestionnaires collection.
     *
     * By default this just sets the collQuestionnaires collection to an empty array (like clearcollQuestionnaires());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initQuestionnaires($overrideExisting = true)
    {
        if (null !== $this->collQuestionnaires && !$overrideExisting) {
            return;
        }
        $this->collQuestionnaires = new PropelObjectCollection();
        $this->collQuestionnaires->setModel('Questionnaire');
    }

    /**
     * Gets an array of Questionnaire objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Status is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Questionnaire[] List of Questionnaire objects
     * @throws PropelException
     */
    public function getQuestionnaires($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collQuestionnairesPartial && !$this->isNew();
        if (null === $this->collQuestionnaires || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collQuestionnaires) {
                // return empty collection
                $this->initQuestionnaires();
            } else {
                $collQuestionnaires = QuestionnaireQuery::create(null, $criteria)
                    ->filterByStatus($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collQuestionnairesPartial && count($collQuestionnaires)) {
                      $this->initQuestionnaires(false);

                      foreach ($collQuestionnaires as $obj) {
                        if (false == $this->collQuestionnaires->contains($obj)) {
                          $this->collQuestionnaires->append($obj);
                        }
                      }

                      $this->collQuestionnairesPartial = true;
                    }

                    $collQuestionnaires->getInternalIterator()->rewind();

                    return $collQuestionnaires;
                }

                if ($partial && $this->collQuestionnaires) {
                    foreach ($this->collQuestionnaires as $obj) {
                        if ($obj->isNew()) {
                            $collQuestionnaires[] = $obj;
                        }
                    }
                }

                $this->collQuestionnaires = $collQuestionnaires;
                $this->collQuestionnairesPartial = false;
            }
        }

        return $this->collQuestionnaires;
    }

    /**
     * Sets a collection of Questionnaire objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $questionnaires A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Status The current object (for fluent API support)
     */
    public function setQuestionnaires(PropelCollection $questionnaires, PropelPDO $con = null)
    {
        $questionnairesToDelete = $this->getQuestionnaires(new Criteria(), $con)->diff($questionnaires);


        $this->questionnairesScheduledForDeletion = $questionnairesToDelete;

        foreach ($questionnairesToDelete as $questionnaireRemoved) {
            $questionnaireRemoved->setStatus(null);
        }

        $this->collQuestionnaires = null;
        foreach ($questionnaires as $questionnaire) {
            $this->addQuestionnaire($questionnaire);
        }

        $this->collQuestionnaires = $questionnaires;
        $this->collQuestionnairesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Questionnaire objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Questionnaire objects.
     * @throws PropelException
     */
    public function countQuestionnaires(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collQuestionnairesPartial && !$this->isNew();
        if (null === $this->collQuestionnaires || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collQuestionnaires) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getQuestionnaires());
            }
            $query = QuestionnaireQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByStatus($this)
                ->count($con);
        }

        return count($this->collQuestionnaires);
    }

    /**
     * Method called to associate a Questionnaire object to this object
     * through the Questionnaire foreign key attribute.
     *
     * @param    Questionnaire $l Questionnaire
     * @return Status The current object (for fluent API support)
     */
    public function addQuestionnaire(Questionnaire $l)
    {
        if ($this->collQuestionnaires === null) {
            $this->initQuestionnaires();
            $this->collQuestionnairesPartial = true;
        }

        if (!in_array($l, $this->collQuestionnaires->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddQuestionnaire($l);

            if ($this->questionnairesScheduledForDeletion and $this->questionnairesScheduledForDeletion->contains($l)) {
                $this->questionnairesScheduledForDeletion->remove($this->questionnairesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Questionnaire $questionnaire The questionnaire object to add.
     */
    protected function doAddQuestionnaire($questionnaire)
    {
        $this->collQuestionnaires[]= $questionnaire;
        $questionnaire->setStatus($this);
    }

    /**
     * @param	Questionnaire $questionnaire The questionnaire object to remove.
     * @return Status The current object (for fluent API support)
     */
    public function removeQuestionnaire($questionnaire)
    {
        if ($this->getQuestionnaires()->contains($questionnaire)) {
            $this->collQuestionnaires->remove($this->collQuestionnaires->search($questionnaire));
            if (null === $this->questionnairesScheduledForDeletion) {
                $this->questionnairesScheduledForDeletion = clone $this->collQuestionnaires;
                $this->questionnairesScheduledForDeletion->clear();
            }
            $this->questionnairesScheduledForDeletion[]= clone $questionnaire;
            $questionnaire->setStatus(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Status is new, it will return
     * an empty collection; or if this Status has previously
     * been saved, it will retrieve related Questionnaires from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Status.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Questionnaire[] List of Questionnaire objects
     */
    public function getQuestionnairesJoinUser($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = QuestionnaireQuery::create(null, $criteria);
        $query->joinWith('User', $join_behavior);

        return $this->getQuestionnaires($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Status is new, it will return
     * an empty collection; or if this Status has previously
     * been saved, it will retrieve related Questionnaires from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Status.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Questionnaire[] List of Questionnaire objects
     */
    public function getQuestionnairesJoinTheme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = QuestionnaireQuery::create(null, $criteria);
        $query->joinWith('Theme', $join_behavior);

        return $this->getQuestionnaires($query, $con);
    }

    /**
     * Clears out the collStatusI18ns collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Status The current object (for fluent API support)
     * @see        addStatusI18ns()
     */
    public function clearStatusI18ns()
    {
        $this->collStatusI18ns = null; // important to set this to null since that means it is uninitialized
        $this->collStatusI18nsPartial = null;

        return $this;
    }

    /**
     * reset is the collStatusI18ns collection loaded partially
     *
     * @return void
     */
    public function resetPartialStatusI18ns($v = true)
    {
        $this->collStatusI18nsPartial = $v;
    }

    /**
     * Initializes the collStatusI18ns collection.
     *
     * By default this just sets the collStatusI18ns collection to an empty array (like clearcollStatusI18ns());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initStatusI18ns($overrideExisting = true)
    {
        if (null !== $this->collStatusI18ns && !$overrideExisting) {
            return;
        }
        $this->collStatusI18ns = new PropelObjectCollection();
        $this->collStatusI18ns->setModel('StatusI18n');
    }

    /**
     * Gets an array of StatusI18n objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Status is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|StatusI18n[] List of StatusI18n objects
     * @throws PropelException
     */
    public function getStatusI18ns($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collStatusI18nsPartial && !$this->isNew();
        if (null === $this->collStatusI18ns || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collStatusI18ns) {
                // return empty collection
                $this->initStatusI18ns();
            } else {
                $collStatusI18ns = StatusI18nQuery::create(null, $criteria)
                    ->filterByStatus($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collStatusI18nsPartial && count($collStatusI18ns)) {
                      $this->initStatusI18ns(false);

                      foreach ($collStatusI18ns as $obj) {
                        if (false == $this->collStatusI18ns->contains($obj)) {
                          $this->collStatusI18ns->append($obj);
                        }
                      }

                      $this->collStatusI18nsPartial = true;
                    }

                    $collStatusI18ns->getInternalIterator()->rewind();

                    return $collStatusI18ns;
                }

                if ($partial && $this->collStatusI18ns) {
                    foreach ($this->collStatusI18ns as $obj) {
                        if ($obj->isNew()) {
                            $collStatusI18ns[] = $obj;
                        }
                    }
                }

                $this->collStatusI18ns = $collStatusI18ns;
                $this->collStatusI18nsPartial = false;
            }
        }

        return $this->collStatusI18ns;
    }

    /**
     * Sets a collection of StatusI18n objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $statusI18ns A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Status The current object (for fluent API support)
     */
    public function setStatusI18ns(PropelCollection $statusI18ns, PropelPDO $con = null)
    {
        $statusI18nsToDelete = $this->getStatusI18ns(new Criteria(), $con)->diff($statusI18ns);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->statusI18nsScheduledForDeletion = clone $statusI18nsToDelete;

        foreach ($statusI18nsToDelete as $statusI18nRemoved) {
            $statusI18nRemoved->setStatus(null);
        }

        $this->collStatusI18ns = null;
        foreach ($statusI18ns as $statusI18n) {
            $this->addStatusI18n($statusI18n);
        }

        $this->collStatusI18ns = $statusI18ns;
        $this->collStatusI18nsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related StatusI18n objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related StatusI18n objects.
     * @throws PropelException
     */
    public function countStatusI18ns(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collStatusI18nsPartial && !$this->isNew();
        if (null === $this->collStatusI18ns || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collStatusI18ns) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getStatusI18ns());
            }
            $query = StatusI18nQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByStatus($this)
                ->count($con);
        }

        return count($this->collStatusI18ns);
    }

    /**
     * Method called to associate a StatusI18n object to this object
     * through the StatusI18n foreign key attribute.
     *
     * @param    StatusI18n $l StatusI18n
     * @return Status The current object (for fluent API support)
     */
    public function addStatusI18n(StatusI18n $l)
    {
        if ($l && $locale = $l->getLocale()) {
            $this->setLocale($locale);
            $this->currentTranslations[$locale] = $l;
        }
        if ($this->collStatusI18ns === null) {
            $this->initStatusI18ns();
            $this->collStatusI18nsPartial = true;
        }

        if (!in_array($l, $this->collStatusI18ns->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddStatusI18n($l);

            if ($this->statusI18nsScheduledForDeletion and $this->statusI18nsScheduledForDeletion->contains($l)) {
                $this->statusI18nsScheduledForDeletion->remove($this->statusI18nsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	StatusI18n $statusI18n The statusI18n object to add.
     */
    protected function doAddStatusI18n($statusI18n)
    {
        $this->collStatusI18ns[]= $statusI18n;
        $statusI18n->setStatus($this);
    }

    /**
     * @param	StatusI18n $statusI18n The statusI18n object to remove.
     * @return Status The current object (for fluent API support)
     */
    public function removeStatusI18n($statusI18n)
    {
        if ($this->getStatusI18ns()->contains($statusI18n)) {
            $this->collStatusI18ns->remove($this->collStatusI18ns->search($statusI18n));
            if (null === $this->statusI18nsScheduledForDeletion) {
                $this->statusI18nsScheduledForDeletion = clone $this->collStatusI18ns;
                $this->statusI18nsScheduledForDeletion->clear();
            }
            $this->statusI18nsScheduledForDeletion[]= clone $statusI18n;
            $statusI18n->setStatus(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collQuestionnaires) {
                foreach ($this->collQuestionnaires as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collStatusI18ns) {
                foreach ($this->collStatusI18ns as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        // i18n behavior
        $this->currentLocale = 'en_US';
        $this->currentTranslations = null;

        if ($this->collQuestionnaires instanceof PropelCollection) {
            $this->collQuestionnaires->clearIterator();
        }
        $this->collQuestionnaires = null;
        if ($this->collStatusI18ns instanceof PropelCollection) {
            $this->collStatusI18ns->clearIterator();
        }
        $this->collStatusI18ns = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(StatusPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

    // i18n behavior

    /**
     * Sets the locale for translations
     *
     * @param     string $locale Locale to use for the translation, e.g. 'fr_FR'
     *
     * @return    Status The current object (for fluent API support)
     */
    public function setLocale($locale = 'en_US')
    {
        $this->currentLocale = $locale;

        return $this;
    }

    /**
     * Gets the locale for translations
     *
     * @return    string $locale Locale to use for the translation, e.g. 'fr_FR'
     */
    public function getLocale()
    {
        return $this->currentLocale;
    }

    /**
     * Returns the current translation for a given locale
     *
     * @param     string $locale Locale to use for the translation, e.g. 'fr_FR'
     * @param     PropelPDO $con an optional connection object
     *
     * @return StatusI18n */
    public function getTranslation($locale = 'en_US', PropelPDO $con = null)
    {
        if (!isset($this->currentTranslations[$locale])) {
            if (null !== $this->collStatusI18ns) {
                foreach ($this->collStatusI18ns as $translation) {
                    if ($translation->getLocale() == $locale) {
                        $this->currentTranslations[$locale] = $translation;

                        return $translation;
                    }
                }
            }
            if ($this->isNew()) {
                $translation = new StatusI18n();
                $translation->setLocale($locale);
            } else {
                $translation = StatusI18nQuery::create()
                    ->filterByPrimaryKey(array($this->getPrimaryKey(), $locale))
                    ->findOneOrCreate($con);
                $this->currentTranslations[$locale] = $translation;
            }
            $this->addStatusI18n($translation);
        }

        return $this->currentTranslations[$locale];
    }

    /**
     * Remove the translation for a given locale
     *
     * @param     string $locale Locale to use for the translation, e.g. 'fr_FR'
     * @param     PropelPDO $con an optional connection object
     *
     * @return    Status The current object (for fluent API support)
     */
    public function removeTranslation($locale = 'en_US', PropelPDO $con = null)
    {
        if (!$this->isNew()) {
            StatusI18nQuery::create()
                ->filterByPrimaryKey(array($this->getPrimaryKey(), $locale))
                ->delete($con);
        }
        if (isset($this->currentTranslations[$locale])) {
            unset($this->currentTranslations[$locale]);
        }
        foreach ($this->collStatusI18ns as $key => $translation) {
            if ($translation->getLocale() == $locale) {
                unset($this->collStatusI18ns[$key]);
                break;
            }
        }

        return $this;
    }

    /**
     * Returns the current translation
     *
     * @param     PropelPDO $con an optional connection object
     *
     * @return StatusI18n */
    public function getCurrentTranslation(PropelPDO $con = null)
    {
        return $this->getTranslation($this->getLocale(), $con);
    }


        /**
         * Get the [name] column value.
         *
         * @return string
         */
        public function getName()
        {
        return $this->getCurrentTranslation()->getName();
    }


        /**
         * Set the value of [name] column.
         *
         * @param  string $v new value
         * @return StatusI18n The current object (for fluent API support)
         */
        public function setName($v)
        {    $this->getCurrentTranslation()->setName($v);

        return $this;
    }

    // event_dispatcher behavior

    /**
     * @return \Symfony\Component\EventDispatcher\EventDispatcher
     */
    static public function getEventDispatcher()
    {
        if (null === self::$dispatcher) {
            self::setEventDispatcher(new EventDispatcher());
        }

        return self::$dispatcher;
    }

    /**
     * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    static public function setEventDispatcher(EventDispatcherInterface $eventDispatcher)
    {
        self::$dispatcher = $eventDispatcher;
    }

    // event_dispatcher behavior
    public function __construct()
    {
        parent::__construct();
        self::getEventDispatcher()->dispatch('propel.construct', new GenericEvent($this));
    }

}

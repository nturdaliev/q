<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Axtion\Bundle\QuestionnaireBundle\Propel\Dependency;
use Axtion\Bundle\QuestionnaireBundle\Propel\DependencyPeer;
use Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuestion;
use Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuestionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery;

abstract class BaseDependency extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\DependencyPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        DependencyPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the question_id field.
     * @var        int
     */
    protected $question_id;

    /**
     * The value for the value field.
     * @var        string
     */
    protected $value;

    /**
     * @var        Question
     */
    protected $aQuestion;

    /**
     * @var        PropelObjectCollection|DependencyQuestion[] Collection to store aggregation of DependencyQuestion objects.
     */
    protected $collDependencyQuestions;
    protected $collDependencyQuestionsPartial;

    /**
     * @var        PropelObjectCollection|Question[] Collection to store aggregation of Question objects.
     */
    protected $collDependentQuestions;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $dependentQuestionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $dependencyQuestionsScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [question_id] column value.
     *
     * @return int
     */
    public function getQuestionId()
    {

        return $this->question_id;
    }

    /**
     * Get the [value] column value.
     *
     * @return string
     */
    public function getValue()
    {

        return $this->value;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return Dependency The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = DependencyPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [question_id] column.
     *
     * @param  int $v new value
     * @return Dependency The current object (for fluent API support)
     */
    public function setQuestionId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->question_id !== $v) {
            $this->question_id = $v;
            $this->modifiedColumns[] = DependencyPeer::QUESTION_ID;
        }

        if ($this->aQuestion !== null && $this->aQuestion->getId() !== $v) {
            $this->aQuestion = null;
        }


        return $this;
    } // setQuestionId()

    /**
     * Set the value of [value] column.
     *
     * @param  string $v new value
     * @return Dependency The current object (for fluent API support)
     */
    public function setValue($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->value !== $v) {
            $this->value = $v;
            $this->modifiedColumns[] = DependencyPeer::VALUE;
        }


        return $this;
    } // setValue()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->question_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->value = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 3; // 3 = DependencyPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Dependency object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aQuestion !== null && $this->question_id !== $this->aQuestion->getId()) {
            $this->aQuestion = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(DependencyPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = DependencyPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aQuestion = null;
            $this->collDependencyQuestions = null;

            $this->collDependentQuestions = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(DependencyPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = DependencyQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(DependencyPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                DependencyPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aQuestion !== null) {
                if ($this->aQuestion->isModified() || $this->aQuestion->isNew()) {
                    $affectedRows += $this->aQuestion->save($con);
                }
                $this->setQuestion($this->aQuestion);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->dependentQuestionsScheduledForDeletion !== null) {
                if (!$this->dependentQuestionsScheduledForDeletion->isEmpty()) {
                    $pks = array();
                    $pk = $this->getPrimaryKey();
                    foreach ($this->dependentQuestionsScheduledForDeletion->getPrimaryKeys(false) as $remotePk) {
                        $pks[] = array($remotePk, $pk);
                    }
                    DependencyQuestionQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);
                    $this->dependentQuestionsScheduledForDeletion = null;
                }

                foreach ($this->getDependentQuestions() as $dependentQuestion) {
                    if ($dependentQuestion->isModified()) {
                        $dependentQuestion->save($con);
                    }
                }
            } elseif ($this->collDependentQuestions) {
                foreach ($this->collDependentQuestions as $dependentQuestion) {
                    if ($dependentQuestion->isModified()) {
                        $dependentQuestion->save($con);
                    }
                }
            }

            if ($this->dependencyQuestionsScheduledForDeletion !== null) {
                if (!$this->dependencyQuestionsScheduledForDeletion->isEmpty()) {
                    DependencyQuestionQuery::create()
                        ->filterByPrimaryKeys($this->dependencyQuestionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->dependencyQuestionsScheduledForDeletion = null;
                }
            }

            if ($this->collDependencyQuestions !== null) {
                foreach ($this->collDependencyQuestions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = DependencyPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . DependencyPeer::ID . ')');
        }
        if (null === $this->id) {
            try {
                $stmt = $con->query("SELECT nextval('questionnaire_dependency_id_seq')");
                $row = $stmt->fetch(PDO::FETCH_NUM);
                $this->id = $row[0];
            } catch (Exception $e) {
                throw new PropelException('Unable to get sequence id.', $e);
            }
        }


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(DependencyPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '"id"';
        }
        if ($this->isColumnModified(DependencyPeer::QUESTION_ID)) {
            $modifiedColumns[':p' . $index++]  = '"question_id"';
        }
        if ($this->isColumnModified(DependencyPeer::VALUE)) {
            $modifiedColumns[':p' . $index++]  = '"value"';
        }

        $sql = sprintf(
            'INSERT INTO "questionnaire_dependency" (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '"id"':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '"question_id"':
                        $stmt->bindValue($identifier, $this->question_id, PDO::PARAM_INT);
                        break;
                    case '"value"':
                        $stmt->bindValue($identifier, $this->value, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aQuestion !== null) {
                if (!$this->aQuestion->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aQuestion->getValidationFailures());
                }
            }


            if (($retval = DependencyPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collDependencyQuestions !== null) {
                    foreach ($this->collDependencyQuestions as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = DependencyPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getQuestionId();
                break;
            case 2:
                return $this->getValue();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Dependency'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Dependency'][$this->getPrimaryKey()] = true;
        $keys = DependencyPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getQuestionId(),
            $keys[2] => $this->getValue(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aQuestion) {
                $result['Question'] = $this->aQuestion->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collDependencyQuestions) {
                $result['DependencyQuestions'] = $this->collDependencyQuestions->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = DependencyPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setQuestionId($value);
                break;
            case 2:
                $this->setValue($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = DependencyPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setQuestionId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setValue($arr[$keys[2]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(DependencyPeer::DATABASE_NAME);

        if ($this->isColumnModified(DependencyPeer::ID)) $criteria->add(DependencyPeer::ID, $this->id);
        if ($this->isColumnModified(DependencyPeer::QUESTION_ID)) $criteria->add(DependencyPeer::QUESTION_ID, $this->question_id);
        if ($this->isColumnModified(DependencyPeer::VALUE)) $criteria->add(DependencyPeer::VALUE, $this->value);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(DependencyPeer::DATABASE_NAME);
        $criteria->add(DependencyPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Dependency (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setQuestionId($this->getQuestionId());
        $copyObj->setValue($this->getValue());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getDependencyQuestions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDependencyQuestion($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Dependency Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return DependencyPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new DependencyPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Question object.
     *
     * @param                  Question $v
     * @return Dependency The current object (for fluent API support)
     * @throws PropelException
     */
    public function setQuestion(Question $v = null)
    {
        if ($v === null) {
            $this->setQuestionId(NULL);
        } else {
            $this->setQuestionId($v->getId());
        }

        $this->aQuestion = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Question object, it will not be re-added.
        if ($v !== null) {
            $v->addDependency($this);
        }


        return $this;
    }


    /**
     * Get the associated Question object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Question The associated Question object.
     * @throws PropelException
     */
    public function getQuestion(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aQuestion === null && ($this->question_id !== null) && $doQuery) {
            $this->aQuestion = QuestionQuery::create()->findPk($this->question_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aQuestion->addDependencies($this);
             */
        }

        return $this->aQuestion;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('DependencyQuestion' == $relationName) {
            $this->initDependencyQuestions();
        }
    }

    /**
     * Clears out the collDependencyQuestions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Dependency The current object (for fluent API support)
     * @see        addDependencyQuestions()
     */
    public function clearDependencyQuestions()
    {
        $this->collDependencyQuestions = null; // important to set this to null since that means it is uninitialized
        $this->collDependencyQuestionsPartial = null;

        return $this;
    }

    /**
     * reset is the collDependencyQuestions collection loaded partially
     *
     * @return void
     */
    public function resetPartialDependencyQuestions($v = true)
    {
        $this->collDependencyQuestionsPartial = $v;
    }

    /**
     * Initializes the collDependencyQuestions collection.
     *
     * By default this just sets the collDependencyQuestions collection to an empty array (like clearcollDependencyQuestions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDependencyQuestions($overrideExisting = true)
    {
        if (null !== $this->collDependencyQuestions && !$overrideExisting) {
            return;
        }
        $this->collDependencyQuestions = new PropelObjectCollection();
        $this->collDependencyQuestions->setModel('DependencyQuestion');
    }

    /**
     * Gets an array of DependencyQuestion objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Dependency is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|DependencyQuestion[] List of DependencyQuestion objects
     * @throws PropelException
     */
    public function getDependencyQuestions($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collDependencyQuestionsPartial && !$this->isNew();
        if (null === $this->collDependencyQuestions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDependencyQuestions) {
                // return empty collection
                $this->initDependencyQuestions();
            } else {
                $collDependencyQuestions = DependencyQuestionQuery::create(null, $criteria)
                    ->filterByDemandingDependency($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collDependencyQuestionsPartial && count($collDependencyQuestions)) {
                      $this->initDependencyQuestions(false);

                      foreach ($collDependencyQuestions as $obj) {
                        if (false == $this->collDependencyQuestions->contains($obj)) {
                          $this->collDependencyQuestions->append($obj);
                        }
                      }

                      $this->collDependencyQuestionsPartial = true;
                    }

                    $collDependencyQuestions->getInternalIterator()->rewind();

                    return $collDependencyQuestions;
                }

                if ($partial && $this->collDependencyQuestions) {
                    foreach ($this->collDependencyQuestions as $obj) {
                        if ($obj->isNew()) {
                            $collDependencyQuestions[] = $obj;
                        }
                    }
                }

                $this->collDependencyQuestions = $collDependencyQuestions;
                $this->collDependencyQuestionsPartial = false;
            }
        }

        return $this->collDependencyQuestions;
    }

    /**
     * Sets a collection of DependencyQuestion objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $dependencyQuestions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Dependency The current object (for fluent API support)
     */
    public function setDependencyQuestions(PropelCollection $dependencyQuestions, PropelPDO $con = null)
    {
        $dependencyQuestionsToDelete = $this->getDependencyQuestions(new Criteria(), $con)->diff($dependencyQuestions);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->dependencyQuestionsScheduledForDeletion = clone $dependencyQuestionsToDelete;

        foreach ($dependencyQuestionsToDelete as $dependencyQuestionRemoved) {
            $dependencyQuestionRemoved->setDemandingDependency(null);
        }

        $this->collDependencyQuestions = null;
        foreach ($dependencyQuestions as $dependencyQuestion) {
            $this->addDependencyQuestion($dependencyQuestion);
        }

        $this->collDependencyQuestions = $dependencyQuestions;
        $this->collDependencyQuestionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related DependencyQuestion objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related DependencyQuestion objects.
     * @throws PropelException
     */
    public function countDependencyQuestions(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collDependencyQuestionsPartial && !$this->isNew();
        if (null === $this->collDependencyQuestions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDependencyQuestions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDependencyQuestions());
            }
            $query = DependencyQuestionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDemandingDependency($this)
                ->count($con);
        }

        return count($this->collDependencyQuestions);
    }

    /**
     * Method called to associate a DependencyQuestion object to this object
     * through the DependencyQuestion foreign key attribute.
     *
     * @param    DependencyQuestion $l DependencyQuestion
     * @return Dependency The current object (for fluent API support)
     */
    public function addDependencyQuestion(DependencyQuestion $l)
    {
        if ($this->collDependencyQuestions === null) {
            $this->initDependencyQuestions();
            $this->collDependencyQuestionsPartial = true;
        }

        if (!in_array($l, $this->collDependencyQuestions->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddDependencyQuestion($l);

            if ($this->dependencyQuestionsScheduledForDeletion and $this->dependencyQuestionsScheduledForDeletion->contains($l)) {
                $this->dependencyQuestionsScheduledForDeletion->remove($this->dependencyQuestionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	DependencyQuestion $dependencyQuestion The dependencyQuestion object to add.
     */
    protected function doAddDependencyQuestion($dependencyQuestion)
    {
        $this->collDependencyQuestions[]= $dependencyQuestion;
        $dependencyQuestion->setDemandingDependency($this);
    }

    /**
     * @param	DependencyQuestion $dependencyQuestion The dependencyQuestion object to remove.
     * @return Dependency The current object (for fluent API support)
     */
    public function removeDependencyQuestion($dependencyQuestion)
    {
        if ($this->getDependencyQuestions()->contains($dependencyQuestion)) {
            $this->collDependencyQuestions->remove($this->collDependencyQuestions->search($dependencyQuestion));
            if (null === $this->dependencyQuestionsScheduledForDeletion) {
                $this->dependencyQuestionsScheduledForDeletion = clone $this->collDependencyQuestions;
                $this->dependencyQuestionsScheduledForDeletion->clear();
            }
            $this->dependencyQuestionsScheduledForDeletion[]= clone $dependencyQuestion;
            $dependencyQuestion->setDemandingDependency(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Dependency is new, it will return
     * an empty collection; or if this Dependency has previously
     * been saved, it will retrieve related DependencyQuestions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Dependency.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|DependencyQuestion[] List of DependencyQuestion objects
     */
    public function getDependencyQuestionsJoinDependentQuestion($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = DependencyQuestionQuery::create(null, $criteria);
        $query->joinWith('DependentQuestion', $join_behavior);

        return $this->getDependencyQuestions($query, $con);
    }

    /**
     * Clears out the collDependentQuestions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Dependency The current object (for fluent API support)
     * @see        addDependentQuestions()
     */
    public function clearDependentQuestions()
    {
        $this->collDependentQuestions = null; // important to set this to null since that means it is uninitialized
        $this->collDependentQuestionsPartial = null;

        return $this;
    }

    /**
     * Initializes the collDependentQuestions collection.
     *
     * By default this just sets the collDependentQuestions collection to an empty collection (like clearDependentQuestions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initDependentQuestions()
    {
        $this->collDependentQuestions = new PropelObjectCollection();
        $this->collDependentQuestions->setModel('Question');
    }

    /**
     * Gets a collection of Question objects related by a many-to-many relationship
     * to the current object by way of the questionnaire_dependency_question cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Dependency is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param PropelPDO $con Optional connection object
     *
     * @return PropelObjectCollection|Question[] List of Question objects
     */
    public function getDependentQuestions($criteria = null, PropelPDO $con = null)
    {
        if (null === $this->collDependentQuestions || null !== $criteria) {
            if ($this->isNew() && null === $this->collDependentQuestions) {
                // return empty collection
                $this->initDependentQuestions();
            } else {
                $collDependentQuestions = QuestionQuery::create(null, $criteria)
                    ->filterByDemandingDependency($this)
                    ->find($con);
                if (null !== $criteria) {
                    return $collDependentQuestions;
                }
                $this->collDependentQuestions = $collDependentQuestions;
            }
        }

        return $this->collDependentQuestions;
    }

    /**
     * Sets a collection of Question objects related by a many-to-many relationship
     * to the current object by way of the questionnaire_dependency_question cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $dependentQuestions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Dependency The current object (for fluent API support)
     */
    public function setDependentQuestions(PropelCollection $dependentQuestions, PropelPDO $con = null)
    {
        $this->clearDependentQuestions();
        $currentDependentQuestions = $this->getDependentQuestions(null, $con);

        $this->dependentQuestionsScheduledForDeletion = $currentDependentQuestions->diff($dependentQuestions);

        foreach ($dependentQuestions as $dependentQuestion) {
            if (!$currentDependentQuestions->contains($dependentQuestion)) {
                $this->doAddDependentQuestion($dependentQuestion);
            }
        }

        $this->collDependentQuestions = $dependentQuestions;

        return $this;
    }

    /**
     * Gets the number of Question objects related by a many-to-many relationship
     * to the current object by way of the questionnaire_dependency_question cross-reference table.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param boolean $distinct Set to true to force count distinct
     * @param PropelPDO $con Optional connection object
     *
     * @return int the number of related Question objects
     */
    public function countDependentQuestions($criteria = null, $distinct = false, PropelPDO $con = null)
    {
        if (null === $this->collDependentQuestions || null !== $criteria) {
            if ($this->isNew() && null === $this->collDependentQuestions) {
                return 0;
            } else {
                $query = QuestionQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByDemandingDependency($this)
                    ->count($con);
            }
        } else {
            return count($this->collDependentQuestions);
        }
    }

    /**
     * Associate a Question object to this object
     * through the questionnaire_dependency_question cross reference table.
     *
     * @param  Question $question The DependencyQuestion object to relate
     * @return Dependency The current object (for fluent API support)
     */
    public function addDependentQuestion(Question $question)
    {
        if ($this->collDependentQuestions === null) {
            $this->initDependentQuestions();
        }

        if (!$this->collDependentQuestions->contains($question)) { // only add it if the **same** object is not already associated
            $this->doAddDependentQuestion($question);
            $this->collDependentQuestions[] = $question;

            if ($this->dependentQuestionsScheduledForDeletion and $this->dependentQuestionsScheduledForDeletion->contains($question)) {
                $this->dependentQuestionsScheduledForDeletion->remove($this->dependentQuestionsScheduledForDeletion->search($question));
            }
        }

        return $this;
    }

    /**
     * @param	DependentQuestion $dependentQuestion The dependentQuestion object to add.
     */
    protected function doAddDependentQuestion(Question $dependentQuestion)
    {
        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$dependentQuestion->getDemandingDependencies()->contains($this)) { $dependencyQuestion = new DependencyQuestion();
            $dependencyQuestion->setDependentQuestion($dependentQuestion);
            $this->addDependencyQuestion($dependencyQuestion);

            $foreignCollection = $dependentQuestion->getDemandingDependencies();
            $foreignCollection[] = $this;
        }
    }

    /**
     * Remove a Question object to this object
     * through the questionnaire_dependency_question cross reference table.
     *
     * @param Question $question The DependencyQuestion object to relate
     * @return Dependency The current object (for fluent API support)
     */
    public function removeDependentQuestion(Question $question)
    {
        if ($this->getDependentQuestions()->contains($question)) {
            $this->collDependentQuestions->remove($this->collDependentQuestions->search($question));
            if (null === $this->dependentQuestionsScheduledForDeletion) {
                $this->dependentQuestionsScheduledForDeletion = clone $this->collDependentQuestions;
                $this->dependentQuestionsScheduledForDeletion->clear();
            }
            $this->dependentQuestionsScheduledForDeletion[]= $question;
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->question_id = null;
        $this->value = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collDependencyQuestions) {
                foreach ($this->collDependencyQuestions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDependentQuestions) {
                foreach ($this->collDependentQuestions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aQuestion instanceof Persistent) {
              $this->aQuestion->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collDependencyQuestions instanceof PropelCollection) {
            $this->collDependencyQuestions->clearIterator();
        }
        $this->collDependencyQuestions = null;
        if ($this->collDependentQuestions instanceof PropelCollection) {
            $this->collDependentQuestions->clearIterator();
        }
        $this->collDependentQuestions = null;
        $this->aQuestion = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(DependencyPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}

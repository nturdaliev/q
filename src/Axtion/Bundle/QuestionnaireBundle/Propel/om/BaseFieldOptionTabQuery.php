<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOption;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionTab;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionTabPeer;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionTabQuery;

/**
 * @method FieldOptionTabQuery orderById($order = Criteria::ASC) Order by the id column
 * @method FieldOptionTabQuery orderByName($order = Criteria::ASC) Order by the name column
 *
 * @method FieldOptionTabQuery groupById() Group by the id column
 * @method FieldOptionTabQuery groupByName() Group by the name column
 *
 * @method FieldOptionTabQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method FieldOptionTabQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method FieldOptionTabQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method FieldOptionTabQuery leftJoinFieldOption($relationAlias = null) Adds a LEFT JOIN clause to the query using the FieldOption relation
 * @method FieldOptionTabQuery rightJoinFieldOption($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FieldOption relation
 * @method FieldOptionTabQuery innerJoinFieldOption($relationAlias = null) Adds a INNER JOIN clause to the query using the FieldOption relation
 *
 * @method FieldOptionTab findOne(PropelPDO $con = null) Return the first FieldOptionTab matching the query
 * @method FieldOptionTab findOneOrCreate(PropelPDO $con = null) Return the first FieldOptionTab matching the query, or a new FieldOptionTab object populated from the query conditions when no match is found
 *
 * @method FieldOptionTab findOneByName(string $name) Return the first FieldOptionTab filtered by the name column
 *
 * @method array findById(int $id) Return FieldOptionTab objects filtered by the id column
 * @method array findByName(string $name) Return FieldOptionTab objects filtered by the name column
 */
abstract class BaseFieldOptionTabQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseFieldOptionTabQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\FieldOptionTab';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new FieldOptionTabQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   FieldOptionTabQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return FieldOptionTabQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof FieldOptionTabQuery) {
            return $criteria;
        }
        $query = new FieldOptionTabQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   FieldOptionTab|FieldOptionTab[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FieldOptionTabPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(FieldOptionTabPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 FieldOptionTab A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 FieldOptionTab A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "id", "name" FROM "questionnaire_field_option_tab" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new FieldOptionTab();
            $obj->hydrate($row);
            FieldOptionTabPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return FieldOptionTab|FieldOptionTab[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|FieldOptionTab[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return FieldOptionTabQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FieldOptionTabPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return FieldOptionTabQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FieldOptionTabPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FieldOptionTabQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FieldOptionTabPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FieldOptionTabPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FieldOptionTabPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FieldOptionTabQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FieldOptionTabPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query by a related FieldOption object
     *
     * @param   FieldOption|PropelObjectCollection $fieldOption  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 FieldOptionTabQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByFieldOption($fieldOption, $comparison = null)
    {
        if ($fieldOption instanceof FieldOption) {
            return $this
                ->addUsingAlias(FieldOptionTabPeer::ID, $fieldOption->getFieldOptionTabId(), $comparison);
        } elseif ($fieldOption instanceof PropelObjectCollection) {
            return $this
                ->useFieldOptionQuery()
                ->filterByPrimaryKeys($fieldOption->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFieldOption() only accepts arguments of type FieldOption or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FieldOption relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return FieldOptionTabQuery The current query, for fluid interface
     */
    public function joinFieldOption($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FieldOption');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FieldOption');
        }

        return $this;
    }

    /**
     * Use the FieldOption relation FieldOption object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionQuery A secondary query class using the current class as primary query
     */
    public function useFieldOptionQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFieldOption($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FieldOption', '\Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   FieldOptionTab $fieldOptionTab Object to remove from the list of results
     *
     * @return FieldOptionTabQuery The current query, for fluid interface
     */
    public function prune($fieldOptionTab = null)
    {
        if ($fieldOptionTab) {
            $this->addUsingAlias(FieldOptionTabPeer::ID, $fieldOptionTab->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

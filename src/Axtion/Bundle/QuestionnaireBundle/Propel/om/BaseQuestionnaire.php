<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Axtion\Bundle\AssessmentBundle\Propel\Assessment;
use Axtion\Bundle\AssessmentBundle\Propel\AssessmentQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionGroup;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionGroupQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Questionnaire;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionnairePeer;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionnaireQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Status;
use Axtion\Bundle\QuestionnaireBundle\Propel\StatusQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Theme;
use Axtion\Bundle\QuestionnaireBundle\Propel\ThemeQuery;
use Axtion\Bundle\UserBundle\Propel\User;
use Axtion\Bundle\UserBundle\Propel\UserQuery;

abstract class BaseQuestionnaire extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\QuestionnairePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        QuestionnairePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the user_id field.
     * @var        int
     */
    protected $user_id;

    /**
     * The value for the status_id field.
     * @var        int
     */
    protected $status_id;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the welcome field.
     * @var        string
     */
    protected $welcome;

    /**
     * The value for the goodbye field.
     * @var        string
     */
    protected $goodbye;

    /**
     * The value for the theme_id field.
     * @var        int
     */
    protected $theme_id;

    /**
     * The value for the copyright field.
     * @var        string
     */
    protected $copyright;

    /**
     * The value for the created_at field.
     * @var        string
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     * @var        string
     */
    protected $updated_at;

    /**
     * @var        Status
     */
    protected $aStatus;

    /**
     * @var        User
     */
    protected $aUser;

    /**
     * @var        Theme
     */
    protected $aTheme;

    /**
     * @var        PropelObjectCollection|Assessment[] Collection to store aggregation of Assessment objects.
     */
    protected $collAssessments;
    protected $collAssessmentsPartial;

    /**
     * @var        PropelObjectCollection|Question[] Collection to store aggregation of Question objects.
     */
    protected $collQuestions;
    protected $collQuestionsPartial;

    /**
     * @var        PropelObjectCollection|QuestionGroup[] Collection to store aggregation of QuestionGroup objects.
     */
    protected $collQuestionGroups;
    protected $collQuestionGroupsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $assessmentsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $questionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $questionGroupsScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [user_id] column value.
     *
     * @return int
     */
    public function getUserId()
    {

        return $this->user_id;
    }

    /**
     * Get the [status_id] column value.
     *
     * @return int
     */
    public function getStatusId()
    {

        return $this->status_id;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * Get the [welcome] column value.
     *
     * @return string
     */
    public function getWelcome()
    {

        return $this->welcome;
    }

    /**
     * Get the [goodbye] column value.
     *
     * @return string
     */
    public function getGoodbye()
    {

        return $this->goodbye;
    }

    /**
     * Get the [theme_id] column value.
     *
     * @return int
     */
    public function getThemeId()
    {

        return $this->theme_id;
    }

    /**
     * Get the [copyright] column value.
     *
     * @return string
     */
    public function getCopyright()
    {

        return $this->copyright;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = null)
    {
        if ($this->created_at === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->created_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = null)
    {
        if ($this->updated_at === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->updated_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->updated_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return Questionnaire The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = QuestionnairePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [user_id] column.
     *
     * @param  int $v new value
     * @return Questionnaire The current object (for fluent API support)
     */
    public function setUserId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->user_id !== $v) {
            $this->user_id = $v;
            $this->modifiedColumns[] = QuestionnairePeer::USER_ID;
        }

        if ($this->aUser !== null && $this->aUser->getId() !== $v) {
            $this->aUser = null;
        }


        return $this;
    } // setUserId()

    /**
     * Set the value of [status_id] column.
     *
     * @param  int $v new value
     * @return Questionnaire The current object (for fluent API support)
     */
    public function setStatusId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->status_id !== $v) {
            $this->status_id = $v;
            $this->modifiedColumns[] = QuestionnairePeer::STATUS_ID;
        }

        if ($this->aStatus !== null && $this->aStatus->getId() !== $v) {
            $this->aStatus = null;
        }


        return $this;
    } // setStatusId()

    /**
     * Set the value of [name] column.
     *
     * @param  string $v new value
     * @return Questionnaire The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = QuestionnairePeer::NAME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [welcome] column.
     *
     * @param  string $v new value
     * @return Questionnaire The current object (for fluent API support)
     */
    public function setWelcome($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->welcome !== $v) {
            $this->welcome = $v;
            $this->modifiedColumns[] = QuestionnairePeer::WELCOME;
        }


        return $this;
    } // setWelcome()

    /**
     * Set the value of [goodbye] column.
     *
     * @param  string $v new value
     * @return Questionnaire The current object (for fluent API support)
     */
    public function setGoodbye($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->goodbye !== $v) {
            $this->goodbye = $v;
            $this->modifiedColumns[] = QuestionnairePeer::GOODBYE;
        }


        return $this;
    } // setGoodbye()

    /**
     * Set the value of [theme_id] column.
     *
     * @param  int $v new value
     * @return Questionnaire The current object (for fluent API support)
     */
    public function setThemeId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->theme_id !== $v) {
            $this->theme_id = $v;
            $this->modifiedColumns[] = QuestionnairePeer::THEME_ID;
        }

        if ($this->aTheme !== null && $this->aTheme->getId() !== $v) {
            $this->aTheme = null;
        }


        return $this;
    } // setThemeId()

    /**
     * Set the value of [copyright] column.
     *
     * @param  string $v new value
     * @return Questionnaire The current object (for fluent API support)
     */
    public function setCopyright($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->copyright !== $v) {
            $this->copyright = $v;
            $this->modifiedColumns[] = QuestionnairePeer::COPYRIGHT;
        }


        return $this;
    } // setCopyright()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Questionnaire The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            $currentDateAsString = ($this->created_at !== null && $tmpDt = new DateTime($this->created_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->created_at = $newDateAsString;
                $this->modifiedColumns[] = QuestionnairePeer::CREATED_AT;
            }
        } // if either are not null


        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Questionnaire The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            $currentDateAsString = ($this->updated_at !== null && $tmpDt = new DateTime($this->updated_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->updated_at = $newDateAsString;
                $this->modifiedColumns[] = QuestionnairePeer::UPDATED_AT;
            }
        } // if either are not null


        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->user_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->status_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->name = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->welcome = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->goodbye = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->theme_id = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->copyright = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->created_at = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->updated_at = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 10; // 10 = QuestionnairePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Questionnaire object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aUser !== null && $this->user_id !== $this->aUser->getId()) {
            $this->aUser = null;
        }
        if ($this->aStatus !== null && $this->status_id !== $this->aStatus->getId()) {
            $this->aStatus = null;
        }
        if ($this->aTheme !== null && $this->theme_id !== $this->aTheme->getId()) {
            $this->aTheme = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(QuestionnairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = QuestionnairePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aStatus = null;
            $this->aUser = null;
            $this->aTheme = null;
            $this->collAssessments = null;

            $this->collQuestions = null;

            $this->collQuestionGroups = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(QuestionnairePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = QuestionnaireQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(QuestionnairePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                if (!$this->isColumnModified(QuestionnairePeer::CREATED_AT)) {
                    $this->setCreatedAt(time());
                }
                if (!$this->isColumnModified(QuestionnairePeer::UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(QuestionnairePeer::UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                QuestionnairePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aStatus !== null) {
                if ($this->aStatus->isModified() || $this->aStatus->isNew()) {
                    $affectedRows += $this->aStatus->save($con);
                }
                $this->setStatus($this->aStatus);
            }

            if ($this->aUser !== null) {
                if ($this->aUser->isModified() || $this->aUser->isNew()) {
                    $affectedRows += $this->aUser->save($con);
                }
                $this->setUser($this->aUser);
            }

            if ($this->aTheme !== null) {
                if ($this->aTheme->isModified() || $this->aTheme->isNew()) {
                    $affectedRows += $this->aTheme->save($con);
                }
                $this->setTheme($this->aTheme);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->assessmentsScheduledForDeletion !== null) {
                if (!$this->assessmentsScheduledForDeletion->isEmpty()) {
                    AssessmentQuery::create()
                        ->filterByPrimaryKeys($this->assessmentsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->assessmentsScheduledForDeletion = null;
                }
            }

            if ($this->collAssessments !== null) {
                foreach ($this->collAssessments as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->questionsScheduledForDeletion !== null) {
                if (!$this->questionsScheduledForDeletion->isEmpty()) {
                    QuestionQuery::create()
                        ->filterByPrimaryKeys($this->questionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->questionsScheduledForDeletion = null;
                }
            }

            if ($this->collQuestions !== null) {
                foreach ($this->collQuestions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->questionGroupsScheduledForDeletion !== null) {
                if (!$this->questionGroupsScheduledForDeletion->isEmpty()) {
                    QuestionGroupQuery::create()
                        ->filterByPrimaryKeys($this->questionGroupsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->questionGroupsScheduledForDeletion = null;
                }
            }

            if ($this->collQuestionGroups !== null) {
                foreach ($this->collQuestionGroups as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = QuestionnairePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . QuestionnairePeer::ID . ')');
        }
        if (null === $this->id) {
            try {
                $stmt = $con->query("SELECT nextval('questionnaire_questionnaire_id_seq')");
                $row = $stmt->fetch(PDO::FETCH_NUM);
                $this->id = $row[0];
            } catch (Exception $e) {
                throw new PropelException('Unable to get sequence id.', $e);
            }
        }


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(QuestionnairePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '"id"';
        }
        if ($this->isColumnModified(QuestionnairePeer::USER_ID)) {
            $modifiedColumns[':p' . $index++]  = '"user_id"';
        }
        if ($this->isColumnModified(QuestionnairePeer::STATUS_ID)) {
            $modifiedColumns[':p' . $index++]  = '"status_id"';
        }
        if ($this->isColumnModified(QuestionnairePeer::NAME)) {
            $modifiedColumns[':p' . $index++]  = '"name"';
        }
        if ($this->isColumnModified(QuestionnairePeer::WELCOME)) {
            $modifiedColumns[':p' . $index++]  = '"welcome"';
        }
        if ($this->isColumnModified(QuestionnairePeer::GOODBYE)) {
            $modifiedColumns[':p' . $index++]  = '"goodbye"';
        }
        if ($this->isColumnModified(QuestionnairePeer::THEME_ID)) {
            $modifiedColumns[':p' . $index++]  = '"theme_id"';
        }
        if ($this->isColumnModified(QuestionnairePeer::COPYRIGHT)) {
            $modifiedColumns[':p' . $index++]  = '"copyright"';
        }
        if ($this->isColumnModified(QuestionnairePeer::CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '"created_at"';
        }
        if ($this->isColumnModified(QuestionnairePeer::UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '"updated_at"';
        }

        $sql = sprintf(
            'INSERT INTO "questionnaire_questionnaire" (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '"id"':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '"user_id"':
                        $stmt->bindValue($identifier, $this->user_id, PDO::PARAM_INT);
                        break;
                    case '"status_id"':
                        $stmt->bindValue($identifier, $this->status_id, PDO::PARAM_INT);
                        break;
                    case '"name"':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '"welcome"':
                        $stmt->bindValue($identifier, $this->welcome, PDO::PARAM_STR);
                        break;
                    case '"goodbye"':
                        $stmt->bindValue($identifier, $this->goodbye, PDO::PARAM_STR);
                        break;
                    case '"theme_id"':
                        $stmt->bindValue($identifier, $this->theme_id, PDO::PARAM_INT);
                        break;
                    case '"copyright"':
                        $stmt->bindValue($identifier, $this->copyright, PDO::PARAM_STR);
                        break;
                    case '"created_at"':
                        $stmt->bindValue($identifier, $this->created_at, PDO::PARAM_STR);
                        break;
                    case '"updated_at"':
                        $stmt->bindValue($identifier, $this->updated_at, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aStatus !== null) {
                if (!$this->aStatus->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aStatus->getValidationFailures());
                }
            }

            if ($this->aUser !== null) {
                if (!$this->aUser->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aUser->getValidationFailures());
                }
            }

            if ($this->aTheme !== null) {
                if (!$this->aTheme->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aTheme->getValidationFailures());
                }
            }


            if (($retval = QuestionnairePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collAssessments !== null) {
                    foreach ($this->collAssessments as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collQuestions !== null) {
                    foreach ($this->collQuestions as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collQuestionGroups !== null) {
                    foreach ($this->collQuestionGroups as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = QuestionnairePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getUserId();
                break;
            case 2:
                return $this->getStatusId();
                break;
            case 3:
                return $this->getName();
                break;
            case 4:
                return $this->getWelcome();
                break;
            case 5:
                return $this->getGoodbye();
                break;
            case 6:
                return $this->getThemeId();
                break;
            case 7:
                return $this->getCopyright();
                break;
            case 8:
                return $this->getCreatedAt();
                break;
            case 9:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Questionnaire'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Questionnaire'][$this->getPrimaryKey()] = true;
        $keys = QuestionnairePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getUserId(),
            $keys[2] => $this->getStatusId(),
            $keys[3] => $this->getName(),
            $keys[4] => $this->getWelcome(),
            $keys[5] => $this->getGoodbye(),
            $keys[6] => $this->getThemeId(),
            $keys[7] => $this->getCopyright(),
            $keys[8] => $this->getCreatedAt(),
            $keys[9] => $this->getUpdatedAt(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aStatus) {
                $result['Status'] = $this->aStatus->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUser) {
                $result['User'] = $this->aUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aTheme) {
                $result['Theme'] = $this->aTheme->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collAssessments) {
                $result['Assessments'] = $this->collAssessments->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collQuestions) {
                $result['Questions'] = $this->collQuestions->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collQuestionGroups) {
                $result['QuestionGroups'] = $this->collQuestionGroups->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = QuestionnairePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setUserId($value);
                break;
            case 2:
                $this->setStatusId($value);
                break;
            case 3:
                $this->setName($value);
                break;
            case 4:
                $this->setWelcome($value);
                break;
            case 5:
                $this->setGoodbye($value);
                break;
            case 6:
                $this->setThemeId($value);
                break;
            case 7:
                $this->setCopyright($value);
                break;
            case 8:
                $this->setCreatedAt($value);
                break;
            case 9:
                $this->setUpdatedAt($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = QuestionnairePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setUserId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setStatusId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setName($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setWelcome($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setGoodbye($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setThemeId($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setCopyright($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setCreatedAt($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setUpdatedAt($arr[$keys[9]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(QuestionnairePeer::DATABASE_NAME);

        if ($this->isColumnModified(QuestionnairePeer::ID)) $criteria->add(QuestionnairePeer::ID, $this->id);
        if ($this->isColumnModified(QuestionnairePeer::USER_ID)) $criteria->add(QuestionnairePeer::USER_ID, $this->user_id);
        if ($this->isColumnModified(QuestionnairePeer::STATUS_ID)) $criteria->add(QuestionnairePeer::STATUS_ID, $this->status_id);
        if ($this->isColumnModified(QuestionnairePeer::NAME)) $criteria->add(QuestionnairePeer::NAME, $this->name);
        if ($this->isColumnModified(QuestionnairePeer::WELCOME)) $criteria->add(QuestionnairePeer::WELCOME, $this->welcome);
        if ($this->isColumnModified(QuestionnairePeer::GOODBYE)) $criteria->add(QuestionnairePeer::GOODBYE, $this->goodbye);
        if ($this->isColumnModified(QuestionnairePeer::THEME_ID)) $criteria->add(QuestionnairePeer::THEME_ID, $this->theme_id);
        if ($this->isColumnModified(QuestionnairePeer::COPYRIGHT)) $criteria->add(QuestionnairePeer::COPYRIGHT, $this->copyright);
        if ($this->isColumnModified(QuestionnairePeer::CREATED_AT)) $criteria->add(QuestionnairePeer::CREATED_AT, $this->created_at);
        if ($this->isColumnModified(QuestionnairePeer::UPDATED_AT)) $criteria->add(QuestionnairePeer::UPDATED_AT, $this->updated_at);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(QuestionnairePeer::DATABASE_NAME);
        $criteria->add(QuestionnairePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Questionnaire (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUserId($this->getUserId());
        $copyObj->setStatusId($this->getStatusId());
        $copyObj->setName($this->getName());
        $copyObj->setWelcome($this->getWelcome());
        $copyObj->setGoodbye($this->getGoodbye());
        $copyObj->setThemeId($this->getThemeId());
        $copyObj->setCopyright($this->getCopyright());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getAssessments() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAssessment($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getQuestions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addQuestion($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getQuestionGroups() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addQuestionGroup($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Questionnaire Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return QuestionnairePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new QuestionnairePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Status object.
     *
     * @param                  Status $v
     * @return Questionnaire The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStatus(Status $v = null)
    {
        if ($v === null) {
            $this->setStatusId(NULL);
        } else {
            $this->setStatusId($v->getId());
        }

        $this->aStatus = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Status object, it will not be re-added.
        if ($v !== null) {
            $v->addQuestionnaire($this);
        }


        return $this;
    }


    /**
     * Get the associated Status object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Status The associated Status object.
     * @throws PropelException
     */
    public function getStatus(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aStatus === null && ($this->status_id !== null) && $doQuery) {
            $this->aStatus = StatusQuery::create()->findPk($this->status_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStatus->addQuestionnaires($this);
             */
        }

        return $this->aStatus;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param                  User $v
     * @return Questionnaire The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUser(User $v = null)
    {
        if ($v === null) {
            $this->setUserId(NULL);
        } else {
            $this->setUserId($v->getId());
        }

        $this->aUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addQuestionnaire($this);
        }


        return $this;
    }


    /**
     * Get the associated User object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return User The associated User object.
     * @throws PropelException
     */
    public function getUser(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aUser === null && ($this->user_id !== null) && $doQuery) {
            $this->aUser = UserQuery::create()->findPk($this->user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUser->addQuestionnaires($this);
             */
        }

        return $this->aUser;
    }

    /**
     * Declares an association between this object and a Theme object.
     *
     * @param                  Theme $v
     * @return Questionnaire The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTheme(Theme $v = null)
    {
        if ($v === null) {
            $this->setThemeId(NULL);
        } else {
            $this->setThemeId($v->getId());
        }

        $this->aTheme = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Theme object, it will not be re-added.
        if ($v !== null) {
            $v->addQuestionnaire($this);
        }


        return $this;
    }


    /**
     * Get the associated Theme object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Theme The associated Theme object.
     * @throws PropelException
     */
    public function getTheme(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aTheme === null && ($this->theme_id !== null) && $doQuery) {
            $this->aTheme = ThemeQuery::create()->findPk($this->theme_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTheme->addQuestionnaires($this);
             */
        }

        return $this->aTheme;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Assessment' == $relationName) {
            $this->initAssessments();
        }
        if ('Question' == $relationName) {
            $this->initQuestions();
        }
        if ('QuestionGroup' == $relationName) {
            $this->initQuestionGroups();
        }
    }

    /**
     * Clears out the collAssessments collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Questionnaire The current object (for fluent API support)
     * @see        addAssessments()
     */
    public function clearAssessments()
    {
        $this->collAssessments = null; // important to set this to null since that means it is uninitialized
        $this->collAssessmentsPartial = null;

        return $this;
    }

    /**
     * reset is the collAssessments collection loaded partially
     *
     * @return void
     */
    public function resetPartialAssessments($v = true)
    {
        $this->collAssessmentsPartial = $v;
    }

    /**
     * Initializes the collAssessments collection.
     *
     * By default this just sets the collAssessments collection to an empty array (like clearcollAssessments());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAssessments($overrideExisting = true)
    {
        if (null !== $this->collAssessments && !$overrideExisting) {
            return;
        }
        $this->collAssessments = new PropelObjectCollection();
        $this->collAssessments->setModel('Assessment');
    }

    /**
     * Gets an array of Assessment objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Questionnaire is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Assessment[] List of Assessment objects
     * @throws PropelException
     */
    public function getAssessments($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collAssessmentsPartial && !$this->isNew();
        if (null === $this->collAssessments || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAssessments) {
                // return empty collection
                $this->initAssessments();
            } else {
                $collAssessments = AssessmentQuery::create(null, $criteria)
                    ->filterByQuestionnaire($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collAssessmentsPartial && count($collAssessments)) {
                      $this->initAssessments(false);

                      foreach ($collAssessments as $obj) {
                        if (false == $this->collAssessments->contains($obj)) {
                          $this->collAssessments->append($obj);
                        }
                      }

                      $this->collAssessmentsPartial = true;
                    }

                    $collAssessments->getInternalIterator()->rewind();

                    return $collAssessments;
                }

                if ($partial && $this->collAssessments) {
                    foreach ($this->collAssessments as $obj) {
                        if ($obj->isNew()) {
                            $collAssessments[] = $obj;
                        }
                    }
                }

                $this->collAssessments = $collAssessments;
                $this->collAssessmentsPartial = false;
            }
        }

        return $this->collAssessments;
    }

    /**
     * Sets a collection of Assessment objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $assessments A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Questionnaire The current object (for fluent API support)
     */
    public function setAssessments(PropelCollection $assessments, PropelPDO $con = null)
    {
        $assessmentsToDelete = $this->getAssessments(new Criteria(), $con)->diff($assessments);


        $this->assessmentsScheduledForDeletion = $assessmentsToDelete;

        foreach ($assessmentsToDelete as $assessmentRemoved) {
            $assessmentRemoved->setQuestionnaire(null);
        }

        $this->collAssessments = null;
        foreach ($assessments as $assessment) {
            $this->addAssessment($assessment);
        }

        $this->collAssessments = $assessments;
        $this->collAssessmentsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Assessment objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Assessment objects.
     * @throws PropelException
     */
    public function countAssessments(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collAssessmentsPartial && !$this->isNew();
        if (null === $this->collAssessments || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAssessments) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAssessments());
            }
            $query = AssessmentQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByQuestionnaire($this)
                ->count($con);
        }

        return count($this->collAssessments);
    }

    /**
     * Method called to associate a Assessment object to this object
     * through the Assessment foreign key attribute.
     *
     * @param    Assessment $l Assessment
     * @return Questionnaire The current object (for fluent API support)
     */
    public function addAssessment(Assessment $l)
    {
        if ($this->collAssessments === null) {
            $this->initAssessments();
            $this->collAssessmentsPartial = true;
        }

        if (!in_array($l, $this->collAssessments->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddAssessment($l);

            if ($this->assessmentsScheduledForDeletion and $this->assessmentsScheduledForDeletion->contains($l)) {
                $this->assessmentsScheduledForDeletion->remove($this->assessmentsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Assessment $assessment The assessment object to add.
     */
    protected function doAddAssessment($assessment)
    {
        $this->collAssessments[]= $assessment;
        $assessment->setQuestionnaire($this);
    }

    /**
     * @param	Assessment $assessment The assessment object to remove.
     * @return Questionnaire The current object (for fluent API support)
     */
    public function removeAssessment($assessment)
    {
        if ($this->getAssessments()->contains($assessment)) {
            $this->collAssessments->remove($this->collAssessments->search($assessment));
            if (null === $this->assessmentsScheduledForDeletion) {
                $this->assessmentsScheduledForDeletion = clone $this->collAssessments;
                $this->assessmentsScheduledForDeletion->clear();
            }
            $this->assessmentsScheduledForDeletion[]= clone $assessment;
            $assessment->setQuestionnaire(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Questionnaire is new, it will return
     * an empty collection; or if this Questionnaire has previously
     * been saved, it will retrieve related Assessments from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Questionnaire.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Assessment[] List of Assessment objects
     */
    public function getAssessmentsJoinClient($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AssessmentQuery::create(null, $criteria);
        $query->joinWith('Client', $join_behavior);

        return $this->getAssessments($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Questionnaire is new, it will return
     * an empty collection; or if this Questionnaire has previously
     * been saved, it will retrieve related Assessments from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Questionnaire.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Assessment[] List of Assessment objects
     */
    public function getAssessmentsJoinStatus($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AssessmentQuery::create(null, $criteria);
        $query->joinWith('Status', $join_behavior);

        return $this->getAssessments($query, $con);
    }

    /**
     * Clears out the collQuestions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Questionnaire The current object (for fluent API support)
     * @see        addQuestions()
     */
    public function clearQuestions()
    {
        $this->collQuestions = null; // important to set this to null since that means it is uninitialized
        $this->collQuestionsPartial = null;

        return $this;
    }

    /**
     * reset is the collQuestions collection loaded partially
     *
     * @return void
     */
    public function resetPartialQuestions($v = true)
    {
        $this->collQuestionsPartial = $v;
    }

    /**
     * Initializes the collQuestions collection.
     *
     * By default this just sets the collQuestions collection to an empty array (like clearcollQuestions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initQuestions($overrideExisting = true)
    {
        if (null !== $this->collQuestions && !$overrideExisting) {
            return;
        }
        $this->collQuestions = new PropelObjectCollection();
        $this->collQuestions->setModel('Question');
    }

    /**
     * Gets an array of Question objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Questionnaire is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Question[] List of Question objects
     * @throws PropelException
     */
    public function getQuestions($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collQuestionsPartial && !$this->isNew();
        if (null === $this->collQuestions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collQuestions) {
                // return empty collection
                $this->initQuestions();
            } else {
                $collQuestions = QuestionQuery::create(null, $criteria)
                    ->filterByQuestionnaire($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collQuestionsPartial && count($collQuestions)) {
                      $this->initQuestions(false);

                      foreach ($collQuestions as $obj) {
                        if (false == $this->collQuestions->contains($obj)) {
                          $this->collQuestions->append($obj);
                        }
                      }

                      $this->collQuestionsPartial = true;
                    }

                    $collQuestions->getInternalIterator()->rewind();

                    return $collQuestions;
                }

                if ($partial && $this->collQuestions) {
                    foreach ($this->collQuestions as $obj) {
                        if ($obj->isNew()) {
                            $collQuestions[] = $obj;
                        }
                    }
                }

                $this->collQuestions = $collQuestions;
                $this->collQuestionsPartial = false;
            }
        }

        return $this->collQuestions;
    }

    /**
     * Sets a collection of Question objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $questions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Questionnaire The current object (for fluent API support)
     */
    public function setQuestions(PropelCollection $questions, PropelPDO $con = null)
    {
        $questionsToDelete = $this->getQuestions(new Criteria(), $con)->diff($questions);


        $this->questionsScheduledForDeletion = $questionsToDelete;

        foreach ($questionsToDelete as $questionRemoved) {
            $questionRemoved->setQuestionnaire(null);
        }

        $this->collQuestions = null;
        foreach ($questions as $question) {
            $this->addQuestion($question);
        }

        $this->collQuestions = $questions;
        $this->collQuestionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Question objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Question objects.
     * @throws PropelException
     */
    public function countQuestions(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collQuestionsPartial && !$this->isNew();
        if (null === $this->collQuestions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collQuestions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getQuestions());
            }
            $query = QuestionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByQuestionnaire($this)
                ->count($con);
        }

        return count($this->collQuestions);
    }

    /**
     * Method called to associate a Question object to this object
     * through the Question foreign key attribute.
     *
     * @param    Question $l Question
     * @return Questionnaire The current object (for fluent API support)
     */
    public function addQuestion(Question $l)
    {
        if ($this->collQuestions === null) {
            $this->initQuestions();
            $this->collQuestionsPartial = true;
        }

        if (!in_array($l, $this->collQuestions->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddQuestion($l);

            if ($this->questionsScheduledForDeletion and $this->questionsScheduledForDeletion->contains($l)) {
                $this->questionsScheduledForDeletion->remove($this->questionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Question $question The question object to add.
     */
    protected function doAddQuestion($question)
    {
        $this->collQuestions[]= $question;
        $question->setQuestionnaire($this);
    }

    /**
     * @param	Question $question The question object to remove.
     * @return Questionnaire The current object (for fluent API support)
     */
    public function removeQuestion($question)
    {
        if ($this->getQuestions()->contains($question)) {
            $this->collQuestions->remove($this->collQuestions->search($question));
            if (null === $this->questionsScheduledForDeletion) {
                $this->questionsScheduledForDeletion = clone $this->collQuestions;
                $this->questionsScheduledForDeletion->clear();
            }
            $this->questionsScheduledForDeletion[]= clone $question;
            $question->setQuestionnaire(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Questionnaire is new, it will return
     * an empty collection; or if this Questionnaire has previously
     * been saved, it will retrieve related Questions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Questionnaire.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Question[] List of Question objects
     */
    public function getQuestionsJoinQuestionType($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = QuestionQuery::create(null, $criteria);
        $query->joinWith('QuestionType', $join_behavior);

        return $this->getQuestions($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Questionnaire is new, it will return
     * an empty collection; or if this Questionnaire has previously
     * been saved, it will retrieve related Questions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Questionnaire.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Question[] List of Question objects
     */
    public function getQuestionsJoinParent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = QuestionQuery::create(null, $criteria);
        $query->joinWith('Parent', $join_behavior);

        return $this->getQuestions($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Questionnaire is new, it will return
     * an empty collection; or if this Questionnaire has previously
     * been saved, it will retrieve related Questions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Questionnaire.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Question[] List of Question objects
     */
    public function getQuestionsJoinQuestionGroup($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = QuestionQuery::create(null, $criteria);
        $query->joinWith('QuestionGroup', $join_behavior);

        return $this->getQuestions($query, $con);
    }

    /**
     * Clears out the collQuestionGroups collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Questionnaire The current object (for fluent API support)
     * @see        addQuestionGroups()
     */
    public function clearQuestionGroups()
    {
        $this->collQuestionGroups = null; // important to set this to null since that means it is uninitialized
        $this->collQuestionGroupsPartial = null;

        return $this;
    }

    /**
     * reset is the collQuestionGroups collection loaded partially
     *
     * @return void
     */
    public function resetPartialQuestionGroups($v = true)
    {
        $this->collQuestionGroupsPartial = $v;
    }

    /**
     * Initializes the collQuestionGroups collection.
     *
     * By default this just sets the collQuestionGroups collection to an empty array (like clearcollQuestionGroups());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initQuestionGroups($overrideExisting = true)
    {
        if (null !== $this->collQuestionGroups && !$overrideExisting) {
            return;
        }
        $this->collQuestionGroups = new PropelObjectCollection();
        $this->collQuestionGroups->setModel('QuestionGroup');
    }

    /**
     * Gets an array of QuestionGroup objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Questionnaire is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|QuestionGroup[] List of QuestionGroup objects
     * @throws PropelException
     */
    public function getQuestionGroups($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collQuestionGroupsPartial && !$this->isNew();
        if (null === $this->collQuestionGroups || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collQuestionGroups) {
                // return empty collection
                $this->initQuestionGroups();
            } else {
                $collQuestionGroups = QuestionGroupQuery::create(null, $criteria)
                    ->filterByQuestionnaire($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collQuestionGroupsPartial && count($collQuestionGroups)) {
                      $this->initQuestionGroups(false);

                      foreach ($collQuestionGroups as $obj) {
                        if (false == $this->collQuestionGroups->contains($obj)) {
                          $this->collQuestionGroups->append($obj);
                        }
                      }

                      $this->collQuestionGroupsPartial = true;
                    }

                    $collQuestionGroups->getInternalIterator()->rewind();

                    return $collQuestionGroups;
                }

                if ($partial && $this->collQuestionGroups) {
                    foreach ($this->collQuestionGroups as $obj) {
                        if ($obj->isNew()) {
                            $collQuestionGroups[] = $obj;
                        }
                    }
                }

                $this->collQuestionGroups = $collQuestionGroups;
                $this->collQuestionGroupsPartial = false;
            }
        }

        return $this->collQuestionGroups;
    }

    /**
     * Sets a collection of QuestionGroup objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $questionGroups A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Questionnaire The current object (for fluent API support)
     */
    public function setQuestionGroups(PropelCollection $questionGroups, PropelPDO $con = null)
    {
        $questionGroupsToDelete = $this->getQuestionGroups(new Criteria(), $con)->diff($questionGroups);


        $this->questionGroupsScheduledForDeletion = $questionGroupsToDelete;

        foreach ($questionGroupsToDelete as $questionGroupRemoved) {
            $questionGroupRemoved->setQuestionnaire(null);
        }

        $this->collQuestionGroups = null;
        foreach ($questionGroups as $questionGroup) {
            $this->addQuestionGroup($questionGroup);
        }

        $this->collQuestionGroups = $questionGroups;
        $this->collQuestionGroupsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related QuestionGroup objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related QuestionGroup objects.
     * @throws PropelException
     */
    public function countQuestionGroups(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collQuestionGroupsPartial && !$this->isNew();
        if (null === $this->collQuestionGroups || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collQuestionGroups) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getQuestionGroups());
            }
            $query = QuestionGroupQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByQuestionnaire($this)
                ->count($con);
        }

        return count($this->collQuestionGroups);
    }

    /**
     * Method called to associate a QuestionGroup object to this object
     * through the QuestionGroup foreign key attribute.
     *
     * @param    QuestionGroup $l QuestionGroup
     * @return Questionnaire The current object (for fluent API support)
     */
    public function addQuestionGroup(QuestionGroup $l)
    {
        if ($this->collQuestionGroups === null) {
            $this->initQuestionGroups();
            $this->collQuestionGroupsPartial = true;
        }

        if (!in_array($l, $this->collQuestionGroups->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddQuestionGroup($l);

            if ($this->questionGroupsScheduledForDeletion and $this->questionGroupsScheduledForDeletion->contains($l)) {
                $this->questionGroupsScheduledForDeletion->remove($this->questionGroupsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	QuestionGroup $questionGroup The questionGroup object to add.
     */
    protected function doAddQuestionGroup($questionGroup)
    {
        $this->collQuestionGroups[]= $questionGroup;
        $questionGroup->setQuestionnaire($this);
    }

    /**
     * @param	QuestionGroup $questionGroup The questionGroup object to remove.
     * @return Questionnaire The current object (for fluent API support)
     */
    public function removeQuestionGroup($questionGroup)
    {
        if ($this->getQuestionGroups()->contains($questionGroup)) {
            $this->collQuestionGroups->remove($this->collQuestionGroups->search($questionGroup));
            if (null === $this->questionGroupsScheduledForDeletion) {
                $this->questionGroupsScheduledForDeletion = clone $this->collQuestionGroups;
                $this->questionGroupsScheduledForDeletion->clear();
            }
            $this->questionGroupsScheduledForDeletion[]= clone $questionGroup;
            $questionGroup->setQuestionnaire(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->user_id = null;
        $this->status_id = null;
        $this->name = null;
        $this->welcome = null;
        $this->goodbye = null;
        $this->theme_id = null;
        $this->copyright = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collAssessments) {
                foreach ($this->collAssessments as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collQuestions) {
                foreach ($this->collQuestions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collQuestionGroups) {
                foreach ($this->collQuestionGroups as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aStatus instanceof Persistent) {
              $this->aStatus->clearAllReferences($deep);
            }
            if ($this->aUser instanceof Persistent) {
              $this->aUser->clearAllReferences($deep);
            }
            if ($this->aTheme instanceof Persistent) {
              $this->aTheme->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collAssessments instanceof PropelCollection) {
            $this->collAssessments->clearIterator();
        }
        $this->collAssessments = null;
        if ($this->collQuestions instanceof PropelCollection) {
            $this->collQuestions->clearIterator();
        }
        $this->collQuestions = null;
        if ($this->collQuestionGroups instanceof PropelCollection) {
            $this->collQuestionGroups->clearIterator();
        }
        $this->collQuestionGroups = null;
        $this->aStatus = null;
        $this->aUser = null;
        $this->aTheme = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string The value of the 'name' column
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     Questionnaire The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[] = QuestionnairePeer::UPDATED_AT;

        return $this;
    }

}

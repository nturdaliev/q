<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Axtion\Bundle\QuestionnaireBundle\Propel\Dependency;
use Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuestion;
use Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuestionPeer;
use Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuestionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Question;

/**
 * @method DependencyQuestionQuery orderByDependencyId($order = Criteria::ASC) Order by the dependency_id column
 * @method DependencyQuestionQuery orderByQuestionId($order = Criteria::ASC) Order by the question_id column
 *
 * @method DependencyQuestionQuery groupByDependencyId() Group by the dependency_id column
 * @method DependencyQuestionQuery groupByQuestionId() Group by the question_id column
 *
 * @method DependencyQuestionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method DependencyQuestionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method DependencyQuestionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method DependencyQuestionQuery leftJoinDependentQuestion($relationAlias = null) Adds a LEFT JOIN clause to the query using the DependentQuestion relation
 * @method DependencyQuestionQuery rightJoinDependentQuestion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DependentQuestion relation
 * @method DependencyQuestionQuery innerJoinDependentQuestion($relationAlias = null) Adds a INNER JOIN clause to the query using the DependentQuestion relation
 *
 * @method DependencyQuestionQuery leftJoinDemandingDependency($relationAlias = null) Adds a LEFT JOIN clause to the query using the DemandingDependency relation
 * @method DependencyQuestionQuery rightJoinDemandingDependency($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DemandingDependency relation
 * @method DependencyQuestionQuery innerJoinDemandingDependency($relationAlias = null) Adds a INNER JOIN clause to the query using the DemandingDependency relation
 *
 * @method DependencyQuestion findOne(PropelPDO $con = null) Return the first DependencyQuestion matching the query
 * @method DependencyQuestion findOneOrCreate(PropelPDO $con = null) Return the first DependencyQuestion matching the query, or a new DependencyQuestion object populated from the query conditions when no match is found
 *
 * @method DependencyQuestion findOneByDependencyId(int $dependency_id) Return the first DependencyQuestion filtered by the dependency_id column
 * @method DependencyQuestion findOneByQuestionId(int $question_id) Return the first DependencyQuestion filtered by the question_id column
 *
 * @method array findByDependencyId(int $dependency_id) Return DependencyQuestion objects filtered by the dependency_id column
 * @method array findByQuestionId(int $question_id) Return DependencyQuestion objects filtered by the question_id column
 */
abstract class BaseDependencyQuestionQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseDependencyQuestionQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\DependencyQuestion';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new DependencyQuestionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   DependencyQuestionQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return DependencyQuestionQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof DependencyQuestionQuery) {
            return $criteria;
        }
        $query = new DependencyQuestionQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query
                         A Primary key composition: [$dependency_id, $question_id]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   DependencyQuestion|DependencyQuestion[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = DependencyQuestionPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(DependencyQuestionPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 DependencyQuestion A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "dependency_id", "question_id" FROM "questionnaire_dependency_question" WHERE "dependency_id" = :p0 AND "question_id" = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new DependencyQuestion();
            $obj->hydrate($row);
            DependencyQuestionPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return DependencyQuestion|DependencyQuestion[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|DependencyQuestion[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return DependencyQuestionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(DependencyQuestionPeer::DEPENDENCY_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(DependencyQuestionPeer::QUESTION_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return DependencyQuestionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(DependencyQuestionPeer::DEPENDENCY_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(DependencyQuestionPeer::QUESTION_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the dependency_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDependencyId(1234); // WHERE dependency_id = 1234
     * $query->filterByDependencyId(array(12, 34)); // WHERE dependency_id IN (12, 34)
     * $query->filterByDependencyId(array('min' => 12)); // WHERE dependency_id >= 12
     * $query->filterByDependencyId(array('max' => 12)); // WHERE dependency_id <= 12
     * </code>
     *
     * @see       filterByDemandingDependency()
     *
     * @param     mixed $dependencyId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DependencyQuestionQuery The current query, for fluid interface
     */
    public function filterByDependencyId($dependencyId = null, $comparison = null)
    {
        if (is_array($dependencyId)) {
            $useMinMax = false;
            if (isset($dependencyId['min'])) {
                $this->addUsingAlias(DependencyQuestionPeer::DEPENDENCY_ID, $dependencyId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dependencyId['max'])) {
                $this->addUsingAlias(DependencyQuestionPeer::DEPENDENCY_ID, $dependencyId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DependencyQuestionPeer::DEPENDENCY_ID, $dependencyId, $comparison);
    }

    /**
     * Filter the query on the question_id column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestionId(1234); // WHERE question_id = 1234
     * $query->filterByQuestionId(array(12, 34)); // WHERE question_id IN (12, 34)
     * $query->filterByQuestionId(array('min' => 12)); // WHERE question_id >= 12
     * $query->filterByQuestionId(array('max' => 12)); // WHERE question_id <= 12
     * </code>
     *
     * @see       filterByDependentQuestion()
     *
     * @param     mixed $questionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DependencyQuestionQuery The current query, for fluid interface
     */
    public function filterByQuestionId($questionId = null, $comparison = null)
    {
        if (is_array($questionId)) {
            $useMinMax = false;
            if (isset($questionId['min'])) {
                $this->addUsingAlias(DependencyQuestionPeer::QUESTION_ID, $questionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($questionId['max'])) {
                $this->addUsingAlias(DependencyQuestionPeer::QUESTION_ID, $questionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DependencyQuestionPeer::QUESTION_ID, $questionId, $comparison);
    }

    /**
     * Filter the query by a related Question object
     *
     * @param   Question|PropelObjectCollection $question The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 DependencyQuestionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByDependentQuestion($question, $comparison = null)
    {
        if ($question instanceof Question) {
            return $this
                ->addUsingAlias(DependencyQuestionPeer::QUESTION_ID, $question->getId(), $comparison);
        } elseif ($question instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DependencyQuestionPeer::QUESTION_ID, $question->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDependentQuestion() only accepts arguments of type Question or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DependentQuestion relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return DependencyQuestionQuery The current query, for fluid interface
     */
    public function joinDependentQuestion($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DependentQuestion');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DependentQuestion');
        }

        return $this;
    }

    /**
     * Use the DependentQuestion relation Question object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery A secondary query class using the current class as primary query
     */
    public function useDependentQuestionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDependentQuestion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DependentQuestion', '\Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery');
    }

    /**
     * Filter the query by a related Dependency object
     *
     * @param   Dependency|PropelObjectCollection $dependency The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 DependencyQuestionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByDemandingDependency($dependency, $comparison = null)
    {
        if ($dependency instanceof Dependency) {
            return $this
                ->addUsingAlias(DependencyQuestionPeer::DEPENDENCY_ID, $dependency->getId(), $comparison);
        } elseif ($dependency instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DependencyQuestionPeer::DEPENDENCY_ID, $dependency->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDemandingDependency() only accepts arguments of type Dependency or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DemandingDependency relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return DependencyQuestionQuery The current query, for fluid interface
     */
    public function joinDemandingDependency($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DemandingDependency');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DemandingDependency');
        }

        return $this;
    }

    /**
     * Use the DemandingDependency relation Dependency object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuery A secondary query class using the current class as primary query
     */
    public function useDemandingDependencyQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDemandingDependency($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DemandingDependency', '\Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   DependencyQuestion $dependencyQuestion Object to remove from the list of results
     *
     * @return DependencyQuestionQuery The current query, for fluid interface
     */
    public function prune($dependencyQuestion = null)
    {
        if ($dependencyQuestion) {
            $this->addCond('pruneCond0', $this->getAliasedColName(DependencyQuestionPeer::DEPENDENCY_ID), $dependencyQuestion->getDependencyId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(DependencyQuestionPeer::QUESTION_ID), $dependencyQuestion->getQuestionId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}

<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Axtion\Bundle\AssessmentBundle\Propel\Response;
use Axtion\Bundle\AssessmentBundle\Propel\ResponseQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Dependency;
use Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuestion;
use Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuestionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOption;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionGroup;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionGroupQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionPeer;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionType;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Questionnaire;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionnaireQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\SelectedOption;
use Axtion\Bundle\QuestionnaireBundle\Propel\SelectedOptionQuery;

abstract class BaseQuestion extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\QuestionPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        QuestionPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the parent_id field.
     * @var        int
     */
    protected $parent_id;

    /**
     * The value for the questionnaire_id field.
     * @var        int
     */
    protected $questionnaire_id;

    /**
     * The value for the question_type_id field.
     * @var        int
     */
    protected $question_type_id;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the help field.
     * @var        string
     */
    protected $help;

    /**
     * The value for the group_id field.
     * @var        int
     */
    protected $group_id;

    /**
     * The value for the sortable_rank field.
     * @var        int
     */
    protected $sortable_rank;

    /**
     * The value for the created_at field.
     * @var        string
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     * @var        string
     */
    protected $updated_at;

    /**
     * @var        QuestionType
     */
    protected $aQuestionType;

    /**
     * @var        Questionnaire
     */
    protected $aQuestionnaire;

    /**
     * @var        Question
     */
    protected $aParent;

    /**
     * @var        QuestionGroup
     */
    protected $aQuestionGroup;

    /**
     * @var        PropelObjectCollection|Response[] Collection to store aggregation of Response objects.
     */
    protected $collResponses;
    protected $collResponsesPartial;

    /**
     * @var        PropelObjectCollection|Question[] Collection to store aggregation of Question objects.
     */
    protected $collSubQuestions;
    protected $collSubQuestionsPartial;

    /**
     * @var        PropelObjectCollection|SelectedOption[] Collection to store aggregation of SelectedOption objects.
     */
    protected $collSelectedOptions;
    protected $collSelectedOptionsPartial;

    /**
     * @var        PropelObjectCollection|Dependency[] Collection to store aggregation of Dependency objects.
     */
    protected $collDependencies;
    protected $collDependenciesPartial;

    /**
     * @var        PropelObjectCollection|DependencyQuestion[] Collection to store aggregation of DependencyQuestion objects.
     */
    protected $collDependencyQuestions;
    protected $collDependencyQuestionsPartial;

    /**
     * @var        PropelObjectCollection|FieldOption[] Collection to store aggregation of FieldOption objects.
     */
    protected $collFieldOptions;

    /**
     * @var        PropelObjectCollection|Dependency[] Collection to store aggregation of Dependency objects.
     */
    protected $collDemandingDependencies;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    // sortable behavior

    /**
     * Queries to be executed in the save transaction
     * @var        array
     */
    protected $sortableQueries = array();

    /**
     * The old scope value.
     * @var        int
     */
    protected $oldScope;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $fieldOptionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $demandingDependenciesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $responsesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $subQuestionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selectedOptionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $dependenciesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $dependencyQuestionsScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [parent_id] column value.
     *
     * @return int
     */
    public function getParentId()
    {

        return $this->parent_id;
    }

    /**
     * Get the [questionnaire_id] column value.
     *
     * @return int
     */
    public function getQuestionnaireId()
    {

        return $this->questionnaire_id;
    }

    /**
     * Get the [question_type_id] column value.
     *
     * @return int
     */
    public function getQuestionTypeId()
    {

        return $this->question_type_id;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * Get the [help] column value.
     *
     * @return string
     */
    public function getHelp()
    {

        return $this->help;
    }

    /**
     * Get the [group_id] column value.
     *
     * @return int
     */
    public function getGroupId()
    {

        return $this->group_id;
    }

    /**
     * Get the [sortable_rank] column value.
     *
     * @return int
     */
    public function getSortableRank()
    {

        return $this->sortable_rank;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = null)
    {
        if ($this->created_at === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->created_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = null)
    {
        if ($this->updated_at === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->updated_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->updated_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return Question The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = QuestionPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [parent_id] column.
     *
     * @param  int $v new value
     * @return Question The current object (for fluent API support)
     */
    public function setParentId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->parent_id !== $v) {
            $this->parent_id = $v;
            $this->modifiedColumns[] = QuestionPeer::PARENT_ID;
        }

        if ($this->aParent !== null && $this->aParent->getId() !== $v) {
            $this->aParent = null;
        }


        return $this;
    } // setParentId()

    /**
     * Set the value of [questionnaire_id] column.
     *
     * @param  int $v new value
     * @return Question The current object (for fluent API support)
     */
    public function setQuestionnaireId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->questionnaire_id !== $v) {
            $this->questionnaire_id = $v;
            $this->modifiedColumns[] = QuestionPeer::QUESTIONNAIRE_ID;
        }

        if ($this->aQuestionnaire !== null && $this->aQuestionnaire->getId() !== $v) {
            $this->aQuestionnaire = null;
        }


        return $this;
    } // setQuestionnaireId()

    /**
     * Set the value of [question_type_id] column.
     *
     * @param  int $v new value
     * @return Question The current object (for fluent API support)
     */
    public function setQuestionTypeId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->question_type_id !== $v) {
            $this->question_type_id = $v;
            $this->modifiedColumns[] = QuestionPeer::QUESTION_TYPE_ID;
        }

        if ($this->aQuestionType !== null && $this->aQuestionType->getId() !== $v) {
            $this->aQuestionType = null;
        }


        return $this;
    } // setQuestionTypeId()

    /**
     * Set the value of [name] column.
     *
     * @param  string $v new value
     * @return Question The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = QuestionPeer::NAME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [help] column.
     *
     * @param  string $v new value
     * @return Question The current object (for fluent API support)
     */
    public function setHelp($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->help !== $v) {
            $this->help = $v;
            $this->modifiedColumns[] = QuestionPeer::HELP;
        }


        return $this;
    } // setHelp()

    /**
     * Set the value of [group_id] column.
     *
     * @param  int $v new value
     * @return Question The current object (for fluent API support)
     */
    public function setGroupId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->group_id !== $v) {
            // sortable behavior
            $this->oldScope = $this->group_id;

            $this->group_id = $v;
            $this->modifiedColumns[] = QuestionPeer::GROUP_ID;
        }

        if ($this->aQuestionGroup !== null && $this->aQuestionGroup->getId() !== $v) {
            $this->aQuestionGroup = null;
        }


        return $this;
    } // setGroupId()

    /**
     * Set the value of [sortable_rank] column.
     *
     * @param  int $v new value
     * @return Question The current object (for fluent API support)
     */
    public function setSortableRank($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->sortable_rank !== $v) {
            $this->sortable_rank = $v;
            $this->modifiedColumns[] = QuestionPeer::SORTABLE_RANK;
        }


        return $this;
    } // setSortableRank()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Question The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            $currentDateAsString = ($this->created_at !== null && $tmpDt = new DateTime($this->created_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->created_at = $newDateAsString;
                $this->modifiedColumns[] = QuestionPeer::CREATED_AT;
            }
        } // if either are not null


        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Question The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            $currentDateAsString = ($this->updated_at !== null && $tmpDt = new DateTime($this->updated_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->updated_at = $newDateAsString;
                $this->modifiedColumns[] = QuestionPeer::UPDATED_AT;
            }
        } // if either are not null


        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->parent_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->questionnaire_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->question_type_id = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->name = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->help = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->group_id = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->sortable_rank = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->created_at = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->updated_at = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 10; // 10 = QuestionPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Question object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aParent !== null && $this->parent_id !== $this->aParent->getId()) {
            $this->aParent = null;
        }
        if ($this->aQuestionnaire !== null && $this->questionnaire_id !== $this->aQuestionnaire->getId()) {
            $this->aQuestionnaire = null;
        }
        if ($this->aQuestionType !== null && $this->question_type_id !== $this->aQuestionType->getId()) {
            $this->aQuestionType = null;
        }
        if ($this->aQuestionGroup !== null && $this->group_id !== $this->aQuestionGroup->getId()) {
            $this->aQuestionGroup = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(QuestionPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = QuestionPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aQuestionType = null;
            $this->aQuestionnaire = null;
            $this->aParent = null;
            $this->aQuestionGroup = null;
            $this->collResponses = null;

            $this->collSubQuestions = null;

            $this->collSelectedOptions = null;

            $this->collDependencies = null;

            $this->collDependencyQuestions = null;

            $this->collFieldOptions = null;
            $this->collDemandingDependencies = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(QuestionPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = QuestionQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // sortable behavior

            QuestionPeer::shiftRank(-1, $this->getSortableRank() + 1, null, $this->getScopeValue(), $con);
            QuestionPeer::clearInstancePool();

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(QuestionPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            // sortable behavior
            $this->processSortableQueries($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // sortable behavior
                if (!$this->isColumnModified(QuestionPeer::RANK_COL)) {
                    $this->setSortableRank(QuestionQuery::create()->getMaxRankArray($this->getScopeValue(), $con) + 1);
                }

                // timestampable behavior
                if (!$this->isColumnModified(QuestionPeer::CREATED_AT)) {
                    $this->setCreatedAt(time());
                }
                if (!$this->isColumnModified(QuestionPeer::UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // sortable behavior
                // if scope has changed and rank was not modified (if yes, assuming superior action)
                // insert object to the end of new scope and cleanup old one
                if (($this->isColumnModified(QuestionPeer::GROUP_ID)) && !$this->isColumnModified(QuestionPeer::RANK_COL)) { QuestionPeer::shiftRank(-1, $this->getSortableRank() + 1, null, $this->oldScope, $con);
                    $this->insertAtBottom($con);
                }

                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(QuestionPeer::UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                QuestionPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aQuestionType !== null) {
                if ($this->aQuestionType->isModified() || $this->aQuestionType->isNew()) {
                    $affectedRows += $this->aQuestionType->save($con);
                }
                $this->setQuestionType($this->aQuestionType);
            }

            if ($this->aQuestionnaire !== null) {
                if ($this->aQuestionnaire->isModified() || $this->aQuestionnaire->isNew()) {
                    $affectedRows += $this->aQuestionnaire->save($con);
                }
                $this->setQuestionnaire($this->aQuestionnaire);
            }

            if ($this->aParent !== null) {
                if ($this->aParent->isModified() || $this->aParent->isNew()) {
                    $affectedRows += $this->aParent->save($con);
                }
                $this->setParent($this->aParent);
            }

            if ($this->aQuestionGroup !== null) {
                if ($this->aQuestionGroup->isModified() || $this->aQuestionGroup->isNew()) {
                    $affectedRows += $this->aQuestionGroup->save($con);
                }
                $this->setQuestionGroup($this->aQuestionGroup);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->fieldOptionsScheduledForDeletion !== null) {
                if (!$this->fieldOptionsScheduledForDeletion->isEmpty()) {
                    $pks = array();
                    $pk = $this->getPrimaryKey();
                    foreach ($this->fieldOptionsScheduledForDeletion->getPrimaryKeys(false) as $remotePk) {
                        $pks[] = array($remotePk, $pk);
                    }
                    SelectedOptionQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);
                    $this->fieldOptionsScheduledForDeletion = null;
                }

                foreach ($this->getFieldOptions() as $fieldOption) {
                    if ($fieldOption->isModified()) {
                        $fieldOption->save($con);
                    }
                }
            } elseif ($this->collFieldOptions) {
                foreach ($this->collFieldOptions as $fieldOption) {
                    if ($fieldOption->isModified()) {
                        $fieldOption->save($con);
                    }
                }
            }

            if ($this->demandingDependenciesScheduledForDeletion !== null) {
                if (!$this->demandingDependenciesScheduledForDeletion->isEmpty()) {
                    $pks = array();
                    $pk = $this->getPrimaryKey();
                    foreach ($this->demandingDependenciesScheduledForDeletion->getPrimaryKeys(false) as $remotePk) {
                        $pks[] = array($pk, $remotePk);
                    }
                    DependencyQuestionQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);
                    $this->demandingDependenciesScheduledForDeletion = null;
                }

                foreach ($this->getDemandingDependencies() as $demandingDependency) {
                    if ($demandingDependency->isModified()) {
                        $demandingDependency->save($con);
                    }
                }
            } elseif ($this->collDemandingDependencies) {
                foreach ($this->collDemandingDependencies as $demandingDependency) {
                    if ($demandingDependency->isModified()) {
                        $demandingDependency->save($con);
                    }
                }
            }

            if ($this->responsesScheduledForDeletion !== null) {
                if (!$this->responsesScheduledForDeletion->isEmpty()) {
                    ResponseQuery::create()
                        ->filterByPrimaryKeys($this->responsesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->responsesScheduledForDeletion = null;
                }
            }

            if ($this->collResponses !== null) {
                foreach ($this->collResponses as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->subQuestionsScheduledForDeletion !== null) {
                if (!$this->subQuestionsScheduledForDeletion->isEmpty()) {
                    foreach ($this->subQuestionsScheduledForDeletion as $subQuestion) {
                        // need to save related object because we set the relation to null
                        $subQuestion->save($con);
                    }
                    $this->subQuestionsScheduledForDeletion = null;
                }
            }

            if ($this->collSubQuestions !== null) {
                foreach ($this->collSubQuestions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->selectedOptionsScheduledForDeletion !== null) {
                if (!$this->selectedOptionsScheduledForDeletion->isEmpty()) {
                    SelectedOptionQuery::create()
                        ->filterByPrimaryKeys($this->selectedOptionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selectedOptionsScheduledForDeletion = null;
                }
            }

            if ($this->collSelectedOptions !== null) {
                foreach ($this->collSelectedOptions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->dependenciesScheduledForDeletion !== null) {
                if (!$this->dependenciesScheduledForDeletion->isEmpty()) {
                    DependencyQuery::create()
                        ->filterByPrimaryKeys($this->dependenciesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->dependenciesScheduledForDeletion = null;
                }
            }

            if ($this->collDependencies !== null) {
                foreach ($this->collDependencies as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->dependencyQuestionsScheduledForDeletion !== null) {
                if (!$this->dependencyQuestionsScheduledForDeletion->isEmpty()) {
                    DependencyQuestionQuery::create()
                        ->filterByPrimaryKeys($this->dependencyQuestionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->dependencyQuestionsScheduledForDeletion = null;
                }
            }

            if ($this->collDependencyQuestions !== null) {
                foreach ($this->collDependencyQuestions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = QuestionPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . QuestionPeer::ID . ')');
        }
        if (null === $this->id) {
            try {
                $stmt = $con->query("SELECT nextval('questionnaire_question_id_seq')");
                $row = $stmt->fetch(PDO::FETCH_NUM);
                $this->id = $row[0];
            } catch (Exception $e) {
                throw new PropelException('Unable to get sequence id.', $e);
            }
        }


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(QuestionPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '"id"';
        }
        if ($this->isColumnModified(QuestionPeer::PARENT_ID)) {
            $modifiedColumns[':p' . $index++]  = '"parent_id"';
        }
        if ($this->isColumnModified(QuestionPeer::QUESTIONNAIRE_ID)) {
            $modifiedColumns[':p' . $index++]  = '"questionnaire_id"';
        }
        if ($this->isColumnModified(QuestionPeer::QUESTION_TYPE_ID)) {
            $modifiedColumns[':p' . $index++]  = '"question_type_id"';
        }
        if ($this->isColumnModified(QuestionPeer::NAME)) {
            $modifiedColumns[':p' . $index++]  = '"name"';
        }
        if ($this->isColumnModified(QuestionPeer::HELP)) {
            $modifiedColumns[':p' . $index++]  = '"help"';
        }
        if ($this->isColumnModified(QuestionPeer::GROUP_ID)) {
            $modifiedColumns[':p' . $index++]  = '"group_id"';
        }
        if ($this->isColumnModified(QuestionPeer::SORTABLE_RANK)) {
            $modifiedColumns[':p' . $index++]  = '"sortable_rank"';
        }
        if ($this->isColumnModified(QuestionPeer::CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '"created_at"';
        }
        if ($this->isColumnModified(QuestionPeer::UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '"updated_at"';
        }

        $sql = sprintf(
            'INSERT INTO "questionnaire_question" (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '"id"':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '"parent_id"':
                        $stmt->bindValue($identifier, $this->parent_id, PDO::PARAM_INT);
                        break;
                    case '"questionnaire_id"':
                        $stmt->bindValue($identifier, $this->questionnaire_id, PDO::PARAM_INT);
                        break;
                    case '"question_type_id"':
                        $stmt->bindValue($identifier, $this->question_type_id, PDO::PARAM_INT);
                        break;
                    case '"name"':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '"help"':
                        $stmt->bindValue($identifier, $this->help, PDO::PARAM_STR);
                        break;
                    case '"group_id"':
                        $stmt->bindValue($identifier, $this->group_id, PDO::PARAM_INT);
                        break;
                    case '"sortable_rank"':
                        $stmt->bindValue($identifier, $this->sortable_rank, PDO::PARAM_INT);
                        break;
                    case '"created_at"':
                        $stmt->bindValue($identifier, $this->created_at, PDO::PARAM_STR);
                        break;
                    case '"updated_at"':
                        $stmt->bindValue($identifier, $this->updated_at, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aQuestionType !== null) {
                if (!$this->aQuestionType->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aQuestionType->getValidationFailures());
                }
            }

            if ($this->aQuestionnaire !== null) {
                if (!$this->aQuestionnaire->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aQuestionnaire->getValidationFailures());
                }
            }

            if ($this->aParent !== null) {
                if (!$this->aParent->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aParent->getValidationFailures());
                }
            }

            if ($this->aQuestionGroup !== null) {
                if (!$this->aQuestionGroup->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aQuestionGroup->getValidationFailures());
                }
            }


            if (($retval = QuestionPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collResponses !== null) {
                    foreach ($this->collResponses as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSubQuestions !== null) {
                    foreach ($this->collSubQuestions as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSelectedOptions !== null) {
                    foreach ($this->collSelectedOptions as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collDependencies !== null) {
                    foreach ($this->collDependencies as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collDependencyQuestions !== null) {
                    foreach ($this->collDependencyQuestions as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = QuestionPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getParentId();
                break;
            case 2:
                return $this->getQuestionnaireId();
                break;
            case 3:
                return $this->getQuestionTypeId();
                break;
            case 4:
                return $this->getName();
                break;
            case 5:
                return $this->getHelp();
                break;
            case 6:
                return $this->getGroupId();
                break;
            case 7:
                return $this->getSortableRank();
                break;
            case 8:
                return $this->getCreatedAt();
                break;
            case 9:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Question'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Question'][$this->getPrimaryKey()] = true;
        $keys = QuestionPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getParentId(),
            $keys[2] => $this->getQuestionnaireId(),
            $keys[3] => $this->getQuestionTypeId(),
            $keys[4] => $this->getName(),
            $keys[5] => $this->getHelp(),
            $keys[6] => $this->getGroupId(),
            $keys[7] => $this->getSortableRank(),
            $keys[8] => $this->getCreatedAt(),
            $keys[9] => $this->getUpdatedAt(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aQuestionType) {
                $result['QuestionType'] = $this->aQuestionType->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aQuestionnaire) {
                $result['Questionnaire'] = $this->aQuestionnaire->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aParent) {
                $result['Parent'] = $this->aParent->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aQuestionGroup) {
                $result['QuestionGroup'] = $this->aQuestionGroup->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collResponses) {
                $result['Responses'] = $this->collResponses->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSubQuestions) {
                $result['SubQuestions'] = $this->collSubQuestions->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSelectedOptions) {
                $result['SelectedOptions'] = $this->collSelectedOptions->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDependencies) {
                $result['Dependencies'] = $this->collDependencies->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDependencyQuestions) {
                $result['DependencyQuestions'] = $this->collDependencyQuestions->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = QuestionPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setParentId($value);
                break;
            case 2:
                $this->setQuestionnaireId($value);
                break;
            case 3:
                $this->setQuestionTypeId($value);
                break;
            case 4:
                $this->setName($value);
                break;
            case 5:
                $this->setHelp($value);
                break;
            case 6:
                $this->setGroupId($value);
                break;
            case 7:
                $this->setSortableRank($value);
                break;
            case 8:
                $this->setCreatedAt($value);
                break;
            case 9:
                $this->setUpdatedAt($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = QuestionPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setParentId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setQuestionnaireId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setQuestionTypeId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setName($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setHelp($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setGroupId($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setSortableRank($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setCreatedAt($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setUpdatedAt($arr[$keys[9]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(QuestionPeer::DATABASE_NAME);

        if ($this->isColumnModified(QuestionPeer::ID)) $criteria->add(QuestionPeer::ID, $this->id);
        if ($this->isColumnModified(QuestionPeer::PARENT_ID)) $criteria->add(QuestionPeer::PARENT_ID, $this->parent_id);
        if ($this->isColumnModified(QuestionPeer::QUESTIONNAIRE_ID)) $criteria->add(QuestionPeer::QUESTIONNAIRE_ID, $this->questionnaire_id);
        if ($this->isColumnModified(QuestionPeer::QUESTION_TYPE_ID)) $criteria->add(QuestionPeer::QUESTION_TYPE_ID, $this->question_type_id);
        if ($this->isColumnModified(QuestionPeer::NAME)) $criteria->add(QuestionPeer::NAME, $this->name);
        if ($this->isColumnModified(QuestionPeer::HELP)) $criteria->add(QuestionPeer::HELP, $this->help);
        if ($this->isColumnModified(QuestionPeer::GROUP_ID)) $criteria->add(QuestionPeer::GROUP_ID, $this->group_id);
        if ($this->isColumnModified(QuestionPeer::SORTABLE_RANK)) $criteria->add(QuestionPeer::SORTABLE_RANK, $this->sortable_rank);
        if ($this->isColumnModified(QuestionPeer::CREATED_AT)) $criteria->add(QuestionPeer::CREATED_AT, $this->created_at);
        if ($this->isColumnModified(QuestionPeer::UPDATED_AT)) $criteria->add(QuestionPeer::UPDATED_AT, $this->updated_at);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(QuestionPeer::DATABASE_NAME);
        $criteria->add(QuestionPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Question (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setParentId($this->getParentId());
        $copyObj->setQuestionnaireId($this->getQuestionnaireId());
        $copyObj->setQuestionTypeId($this->getQuestionTypeId());
        $copyObj->setName($this->getName());
        $copyObj->setHelp($this->getHelp());
        $copyObj->setGroupId($this->getGroupId());
        $copyObj->setSortableRank($this->getSortableRank());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getResponses() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addResponse($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSubQuestions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSubQuestion($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSelectedOptions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelectedOption($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDependencies() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDependency($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDependencyQuestions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDependencyQuestion($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Question Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return QuestionPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new QuestionPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a QuestionType object.
     *
     * @param                  QuestionType $v
     * @return Question The current object (for fluent API support)
     * @throws PropelException
     */
    public function setQuestionType(QuestionType $v = null)
    {
        if ($v === null) {
            $this->setQuestionTypeId(NULL);
        } else {
            $this->setQuestionTypeId($v->getId());
        }

        $this->aQuestionType = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the QuestionType object, it will not be re-added.
        if ($v !== null) {
            $v->addQuestion($this);
        }


        return $this;
    }


    /**
     * Get the associated QuestionType object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return QuestionType The associated QuestionType object.
     * @throws PropelException
     */
    public function getQuestionType(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aQuestionType === null && ($this->question_type_id !== null) && $doQuery) {
            $this->aQuestionType = QuestionTypeQuery::create()->findPk($this->question_type_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aQuestionType->addQuestions($this);
             */
        }

        return $this->aQuestionType;
    }

    /**
     * Declares an association between this object and a Questionnaire object.
     *
     * @param                  Questionnaire $v
     * @return Question The current object (for fluent API support)
     * @throws PropelException
     */
    public function setQuestionnaire(Questionnaire $v = null)
    {
        if ($v === null) {
            $this->setQuestionnaireId(NULL);
        } else {
            $this->setQuestionnaireId($v->getId());
        }

        $this->aQuestionnaire = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Questionnaire object, it will not be re-added.
        if ($v !== null) {
            $v->addQuestion($this);
        }


        return $this;
    }


    /**
     * Get the associated Questionnaire object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Questionnaire The associated Questionnaire object.
     * @throws PropelException
     */
    public function getQuestionnaire(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aQuestionnaire === null && ($this->questionnaire_id !== null) && $doQuery) {
            $this->aQuestionnaire = QuestionnaireQuery::create()->findPk($this->questionnaire_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aQuestionnaire->addQuestions($this);
             */
        }

        return $this->aQuestionnaire;
    }

    /**
     * Declares an association between this object and a Question object.
     *
     * @param                  Question $v
     * @return Question The current object (for fluent API support)
     * @throws PropelException
     */
    public function setParent(Question $v = null)
    {
        if ($v === null) {
            $this->setParentId(NULL);
        } else {
            $this->setParentId($v->getId());
        }

        $this->aParent = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Question object, it will not be re-added.
        if ($v !== null) {
            $v->addSubQuestion($this);
        }


        return $this;
    }


    /**
     * Get the associated Question object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Question The associated Question object.
     * @throws PropelException
     */
    public function getParent(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aParent === null && ($this->parent_id !== null) && $doQuery) {
            $this->aParent = QuestionQuery::create()->findPk($this->parent_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aParent->addSubQuestions($this);
             */
        }

        return $this->aParent;
    }

    /**
     * Declares an association between this object and a QuestionGroup object.
     *
     * @param                  QuestionGroup $v
     * @return Question The current object (for fluent API support)
     * @throws PropelException
     */
    public function setQuestionGroup(QuestionGroup $v = null)
    {
        if ($v === null) {
            $this->setGroupId(NULL);
        } else {
            $this->setGroupId($v->getId());
        }

        $this->aQuestionGroup = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the QuestionGroup object, it will not be re-added.
        if ($v !== null) {
            $v->addQuestion($this);
        }


        return $this;
    }


    /**
     * Get the associated QuestionGroup object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return QuestionGroup The associated QuestionGroup object.
     * @throws PropelException
     */
    public function getQuestionGroup(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aQuestionGroup === null && ($this->group_id !== null) && $doQuery) {
            $this->aQuestionGroup = QuestionGroupQuery::create()->findPk($this->group_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aQuestionGroup->addQuestions($this);
             */
        }

        return $this->aQuestionGroup;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Response' == $relationName) {
            $this->initResponses();
        }
        if ('SubQuestion' == $relationName) {
            $this->initSubQuestions();
        }
        if ('SelectedOption' == $relationName) {
            $this->initSelectedOptions();
        }
        if ('Dependency' == $relationName) {
            $this->initDependencies();
        }
        if ('DependencyQuestion' == $relationName) {
            $this->initDependencyQuestions();
        }
    }

    /**
     * Clears out the collResponses collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Question The current object (for fluent API support)
     * @see        addResponses()
     */
    public function clearResponses()
    {
        $this->collResponses = null; // important to set this to null since that means it is uninitialized
        $this->collResponsesPartial = null;

        return $this;
    }

    /**
     * reset is the collResponses collection loaded partially
     *
     * @return void
     */
    public function resetPartialResponses($v = true)
    {
        $this->collResponsesPartial = $v;
    }

    /**
     * Initializes the collResponses collection.
     *
     * By default this just sets the collResponses collection to an empty array (like clearcollResponses());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initResponses($overrideExisting = true)
    {
        if (null !== $this->collResponses && !$overrideExisting) {
            return;
        }
        $this->collResponses = new PropelObjectCollection();
        $this->collResponses->setModel('Response');
    }

    /**
     * Gets an array of Response objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Question is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Response[] List of Response objects
     * @throws PropelException
     */
    public function getResponses($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collResponsesPartial && !$this->isNew();
        if (null === $this->collResponses || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collResponses) {
                // return empty collection
                $this->initResponses();
            } else {
                $collResponses = ResponseQuery::create(null, $criteria)
                    ->filterByQuestion($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collResponsesPartial && count($collResponses)) {
                      $this->initResponses(false);

                      foreach ($collResponses as $obj) {
                        if (false == $this->collResponses->contains($obj)) {
                          $this->collResponses->append($obj);
                        }
                      }

                      $this->collResponsesPartial = true;
                    }

                    $collResponses->getInternalIterator()->rewind();

                    return $collResponses;
                }

                if ($partial && $this->collResponses) {
                    foreach ($this->collResponses as $obj) {
                        if ($obj->isNew()) {
                            $collResponses[] = $obj;
                        }
                    }
                }

                $this->collResponses = $collResponses;
                $this->collResponsesPartial = false;
            }
        }

        return $this->collResponses;
    }

    /**
     * Sets a collection of Response objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $responses A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Question The current object (for fluent API support)
     */
    public function setResponses(PropelCollection $responses, PropelPDO $con = null)
    {
        $responsesToDelete = $this->getResponses(new Criteria(), $con)->diff($responses);


        $this->responsesScheduledForDeletion = $responsesToDelete;

        foreach ($responsesToDelete as $responseRemoved) {
            $responseRemoved->setQuestion(null);
        }

        $this->collResponses = null;
        foreach ($responses as $response) {
            $this->addResponse($response);
        }

        $this->collResponses = $responses;
        $this->collResponsesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Response objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Response objects.
     * @throws PropelException
     */
    public function countResponses(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collResponsesPartial && !$this->isNew();
        if (null === $this->collResponses || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collResponses) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getResponses());
            }
            $query = ResponseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByQuestion($this)
                ->count($con);
        }

        return count($this->collResponses);
    }

    /**
     * Method called to associate a Response object to this object
     * through the Response foreign key attribute.
     *
     * @param    Response $l Response
     * @return Question The current object (for fluent API support)
     */
    public function addResponse(Response $l)
    {
        if ($this->collResponses === null) {
            $this->initResponses();
            $this->collResponsesPartial = true;
        }

        if (!in_array($l, $this->collResponses->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddResponse($l);

            if ($this->responsesScheduledForDeletion and $this->responsesScheduledForDeletion->contains($l)) {
                $this->responsesScheduledForDeletion->remove($this->responsesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Response $response The response object to add.
     */
    protected function doAddResponse($response)
    {
        $this->collResponses[]= $response;
        $response->setQuestion($this);
    }

    /**
     * @param	Response $response The response object to remove.
     * @return Question The current object (for fluent API support)
     */
    public function removeResponse($response)
    {
        if ($this->getResponses()->contains($response)) {
            $this->collResponses->remove($this->collResponses->search($response));
            if (null === $this->responsesScheduledForDeletion) {
                $this->responsesScheduledForDeletion = clone $this->collResponses;
                $this->responsesScheduledForDeletion->clear();
            }
            $this->responsesScheduledForDeletion[]= clone $response;
            $response->setQuestion(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Question is new, it will return
     * an empty collection; or if this Question has previously
     * been saved, it will retrieve related Responses from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Question.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Response[] List of Response objects
     */
    public function getResponsesJoinAssessment($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = ResponseQuery::create(null, $criteria);
        $query->joinWith('Assessment', $join_behavior);

        return $this->getResponses($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Question is new, it will return
     * an empty collection; or if this Question has previously
     * been saved, it will retrieve related Responses from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Question.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Response[] List of Response objects
     */
    public function getResponsesJoinParent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = ResponseQuery::create(null, $criteria);
        $query->joinWith('Parent', $join_behavior);

        return $this->getResponses($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Question is new, it will return
     * an empty collection; or if this Question has previously
     * been saved, it will retrieve related Responses from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Question.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Response[] List of Response objects
     */
    public function getResponsesJoinResponseGroup($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = ResponseQuery::create(null, $criteria);
        $query->joinWith('ResponseGroup', $join_behavior);

        return $this->getResponses($query, $con);
    }

    /**
     * Clears out the collSubQuestions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Question The current object (for fluent API support)
     * @see        addSubQuestions()
     */
    public function clearSubQuestions()
    {
        $this->collSubQuestions = null; // important to set this to null since that means it is uninitialized
        $this->collSubQuestionsPartial = null;

        return $this;
    }

    /**
     * reset is the collSubQuestions collection loaded partially
     *
     * @return void
     */
    public function resetPartialSubQuestions($v = true)
    {
        $this->collSubQuestionsPartial = $v;
    }

    /**
     * Initializes the collSubQuestions collection.
     *
     * By default this just sets the collSubQuestions collection to an empty array (like clearcollSubQuestions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSubQuestions($overrideExisting = true)
    {
        if (null !== $this->collSubQuestions && !$overrideExisting) {
            return;
        }
        $this->collSubQuestions = new PropelObjectCollection();
        $this->collSubQuestions->setModel('Question');
    }

    /**
     * Gets an array of Question objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Question is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Question[] List of Question objects
     * @throws PropelException
     */
    public function getSubQuestions($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSubQuestionsPartial && !$this->isNew();
        if (null === $this->collSubQuestions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSubQuestions) {
                // return empty collection
                $this->initSubQuestions();
            } else {
                $collSubQuestions = QuestionQuery::create(null, $criteria)
                    ->filterByParent($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSubQuestionsPartial && count($collSubQuestions)) {
                      $this->initSubQuestions(false);

                      foreach ($collSubQuestions as $obj) {
                        if (false == $this->collSubQuestions->contains($obj)) {
                          $this->collSubQuestions->append($obj);
                        }
                      }

                      $this->collSubQuestionsPartial = true;
                    }

                    $collSubQuestions->getInternalIterator()->rewind();

                    return $collSubQuestions;
                }

                if ($partial && $this->collSubQuestions) {
                    foreach ($this->collSubQuestions as $obj) {
                        if ($obj->isNew()) {
                            $collSubQuestions[] = $obj;
                        }
                    }
                }

                $this->collSubQuestions = $collSubQuestions;
                $this->collSubQuestionsPartial = false;
            }
        }

        return $this->collSubQuestions;
    }

    /**
     * Sets a collection of SubQuestion objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $subQuestions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Question The current object (for fluent API support)
     */
    public function setSubQuestions(PropelCollection $subQuestions, PropelPDO $con = null)
    {
        $subQuestionsToDelete = $this->getSubQuestions(new Criteria(), $con)->diff($subQuestions);


        $this->subQuestionsScheduledForDeletion = $subQuestionsToDelete;

        foreach ($subQuestionsToDelete as $subQuestionRemoved) {
            $subQuestionRemoved->setParent(null);
        }

        $this->collSubQuestions = null;
        foreach ($subQuestions as $subQuestion) {
            $this->addSubQuestion($subQuestion);
        }

        $this->collSubQuestions = $subQuestions;
        $this->collSubQuestionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Question objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Question objects.
     * @throws PropelException
     */
    public function countSubQuestions(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSubQuestionsPartial && !$this->isNew();
        if (null === $this->collSubQuestions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSubQuestions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSubQuestions());
            }
            $query = QuestionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByParent($this)
                ->count($con);
        }

        return count($this->collSubQuestions);
    }

    /**
     * Method called to associate a Question object to this object
     * through the Question foreign key attribute.
     *
     * @param    Question $l Question
     * @return Question The current object (for fluent API support)
     */
    public function addSubQuestion(Question $l)
    {
        if ($this->collSubQuestions === null) {
            $this->initSubQuestions();
            $this->collSubQuestionsPartial = true;
        }

        if (!in_array($l, $this->collSubQuestions->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSubQuestion($l);

            if ($this->subQuestionsScheduledForDeletion and $this->subQuestionsScheduledForDeletion->contains($l)) {
                $this->subQuestionsScheduledForDeletion->remove($this->subQuestionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	SubQuestion $subQuestion The subQuestion object to add.
     */
    protected function doAddSubQuestion($subQuestion)
    {
        $this->collSubQuestions[]= $subQuestion;
        $subQuestion->setParent($this);
    }

    /**
     * @param	SubQuestion $subQuestion The subQuestion object to remove.
     * @return Question The current object (for fluent API support)
     */
    public function removeSubQuestion($subQuestion)
    {
        if ($this->getSubQuestions()->contains($subQuestion)) {
            $this->collSubQuestions->remove($this->collSubQuestions->search($subQuestion));
            if (null === $this->subQuestionsScheduledForDeletion) {
                $this->subQuestionsScheduledForDeletion = clone $this->collSubQuestions;
                $this->subQuestionsScheduledForDeletion->clear();
            }
            $this->subQuestionsScheduledForDeletion[]= $subQuestion;
            $subQuestion->setParent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Question is new, it will return
     * an empty collection; or if this Question has previously
     * been saved, it will retrieve related SubQuestions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Question.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Question[] List of Question objects
     */
    public function getSubQuestionsJoinQuestionType($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = QuestionQuery::create(null, $criteria);
        $query->joinWith('QuestionType', $join_behavior);

        return $this->getSubQuestions($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Question is new, it will return
     * an empty collection; or if this Question has previously
     * been saved, it will retrieve related SubQuestions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Question.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Question[] List of Question objects
     */
    public function getSubQuestionsJoinQuestionnaire($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = QuestionQuery::create(null, $criteria);
        $query->joinWith('Questionnaire', $join_behavior);

        return $this->getSubQuestions($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Question is new, it will return
     * an empty collection; or if this Question has previously
     * been saved, it will retrieve related SubQuestions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Question.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Question[] List of Question objects
     */
    public function getSubQuestionsJoinQuestionGroup($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = QuestionQuery::create(null, $criteria);
        $query->joinWith('QuestionGroup', $join_behavior);

        return $this->getSubQuestions($query, $con);
    }

    /**
     * Clears out the collSelectedOptions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Question The current object (for fluent API support)
     * @see        addSelectedOptions()
     */
    public function clearSelectedOptions()
    {
        $this->collSelectedOptions = null; // important to set this to null since that means it is uninitialized
        $this->collSelectedOptionsPartial = null;

        return $this;
    }

    /**
     * reset is the collSelectedOptions collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelectedOptions($v = true)
    {
        $this->collSelectedOptionsPartial = $v;
    }

    /**
     * Initializes the collSelectedOptions collection.
     *
     * By default this just sets the collSelectedOptions collection to an empty array (like clearcollSelectedOptions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelectedOptions($overrideExisting = true)
    {
        if (null !== $this->collSelectedOptions && !$overrideExisting) {
            return;
        }
        $this->collSelectedOptions = new PropelObjectCollection();
        $this->collSelectedOptions->setModel('SelectedOption');
    }

    /**
     * Gets an array of SelectedOption objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Question is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelectedOption[] List of SelectedOption objects
     * @throws PropelException
     */
    public function getSelectedOptions($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelectedOptionsPartial && !$this->isNew();
        if (null === $this->collSelectedOptions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelectedOptions) {
                // return empty collection
                $this->initSelectedOptions();
            } else {
                $collSelectedOptions = SelectedOptionQuery::create(null, $criteria)
                    ->filterByQuestion($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelectedOptionsPartial && count($collSelectedOptions)) {
                      $this->initSelectedOptions(false);

                      foreach ($collSelectedOptions as $obj) {
                        if (false == $this->collSelectedOptions->contains($obj)) {
                          $this->collSelectedOptions->append($obj);
                        }
                      }

                      $this->collSelectedOptionsPartial = true;
                    }

                    $collSelectedOptions->getInternalIterator()->rewind();

                    return $collSelectedOptions;
                }

                if ($partial && $this->collSelectedOptions) {
                    foreach ($this->collSelectedOptions as $obj) {
                        if ($obj->isNew()) {
                            $collSelectedOptions[] = $obj;
                        }
                    }
                }

                $this->collSelectedOptions = $collSelectedOptions;
                $this->collSelectedOptionsPartial = false;
            }
        }

        return $this->collSelectedOptions;
    }

    /**
     * Sets a collection of SelectedOption objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selectedOptions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Question The current object (for fluent API support)
     */
    public function setSelectedOptions(PropelCollection $selectedOptions, PropelPDO $con = null)
    {
        $selectedOptionsToDelete = $this->getSelectedOptions(new Criteria(), $con)->diff($selectedOptions);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->selectedOptionsScheduledForDeletion = clone $selectedOptionsToDelete;

        foreach ($selectedOptionsToDelete as $selectedOptionRemoved) {
            $selectedOptionRemoved->setQuestion(null);
        }

        $this->collSelectedOptions = null;
        foreach ($selectedOptions as $selectedOption) {
            $this->addSelectedOption($selectedOption);
        }

        $this->collSelectedOptions = $selectedOptions;
        $this->collSelectedOptionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelectedOption objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelectedOption objects.
     * @throws PropelException
     */
    public function countSelectedOptions(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelectedOptionsPartial && !$this->isNew();
        if (null === $this->collSelectedOptions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelectedOptions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSelectedOptions());
            }
            $query = SelectedOptionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByQuestion($this)
                ->count($con);
        }

        return count($this->collSelectedOptions);
    }

    /**
     * Method called to associate a SelectedOption object to this object
     * through the SelectedOption foreign key attribute.
     *
     * @param    SelectedOption $l SelectedOption
     * @return Question The current object (for fluent API support)
     */
    public function addSelectedOption(SelectedOption $l)
    {
        if ($this->collSelectedOptions === null) {
            $this->initSelectedOptions();
            $this->collSelectedOptionsPartial = true;
        }

        if (!in_array($l, $this->collSelectedOptions->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelectedOption($l);

            if ($this->selectedOptionsScheduledForDeletion and $this->selectedOptionsScheduledForDeletion->contains($l)) {
                $this->selectedOptionsScheduledForDeletion->remove($this->selectedOptionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	SelectedOption $selectedOption The selectedOption object to add.
     */
    protected function doAddSelectedOption($selectedOption)
    {
        $this->collSelectedOptions[]= $selectedOption;
        $selectedOption->setQuestion($this);
    }

    /**
     * @param	SelectedOption $selectedOption The selectedOption object to remove.
     * @return Question The current object (for fluent API support)
     */
    public function removeSelectedOption($selectedOption)
    {
        if ($this->getSelectedOptions()->contains($selectedOption)) {
            $this->collSelectedOptions->remove($this->collSelectedOptions->search($selectedOption));
            if (null === $this->selectedOptionsScheduledForDeletion) {
                $this->selectedOptionsScheduledForDeletion = clone $this->collSelectedOptions;
                $this->selectedOptionsScheduledForDeletion->clear();
            }
            $this->selectedOptionsScheduledForDeletion[]= clone $selectedOption;
            $selectedOption->setQuestion(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Question is new, it will return
     * an empty collection; or if this Question has previously
     * been saved, it will retrieve related SelectedOptions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Question.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelectedOption[] List of SelectedOption objects
     */
    public function getSelectedOptionsJoinFieldOption($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelectedOptionQuery::create(null, $criteria);
        $query->joinWith('FieldOption', $join_behavior);

        return $this->getSelectedOptions($query, $con);
    }

    /**
     * Clears out the collDependencies collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Question The current object (for fluent API support)
     * @see        addDependencies()
     */
    public function clearDependencies()
    {
        $this->collDependencies = null; // important to set this to null since that means it is uninitialized
        $this->collDependenciesPartial = null;

        return $this;
    }

    /**
     * reset is the collDependencies collection loaded partially
     *
     * @return void
     */
    public function resetPartialDependencies($v = true)
    {
        $this->collDependenciesPartial = $v;
    }

    /**
     * Initializes the collDependencies collection.
     *
     * By default this just sets the collDependencies collection to an empty array (like clearcollDependencies());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDependencies($overrideExisting = true)
    {
        if (null !== $this->collDependencies && !$overrideExisting) {
            return;
        }
        $this->collDependencies = new PropelObjectCollection();
        $this->collDependencies->setModel('Dependency');
    }

    /**
     * Gets an array of Dependency objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Question is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Dependency[] List of Dependency objects
     * @throws PropelException
     */
    public function getDependencies($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collDependenciesPartial && !$this->isNew();
        if (null === $this->collDependencies || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDependencies) {
                // return empty collection
                $this->initDependencies();
            } else {
                $collDependencies = DependencyQuery::create(null, $criteria)
                    ->filterByQuestion($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collDependenciesPartial && count($collDependencies)) {
                      $this->initDependencies(false);

                      foreach ($collDependencies as $obj) {
                        if (false == $this->collDependencies->contains($obj)) {
                          $this->collDependencies->append($obj);
                        }
                      }

                      $this->collDependenciesPartial = true;
                    }

                    $collDependencies->getInternalIterator()->rewind();

                    return $collDependencies;
                }

                if ($partial && $this->collDependencies) {
                    foreach ($this->collDependencies as $obj) {
                        if ($obj->isNew()) {
                            $collDependencies[] = $obj;
                        }
                    }
                }

                $this->collDependencies = $collDependencies;
                $this->collDependenciesPartial = false;
            }
        }

        return $this->collDependencies;
    }

    /**
     * Sets a collection of Dependency objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $dependencies A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Question The current object (for fluent API support)
     */
    public function setDependencies(PropelCollection $dependencies, PropelPDO $con = null)
    {
        $dependenciesToDelete = $this->getDependencies(new Criteria(), $con)->diff($dependencies);


        $this->dependenciesScheduledForDeletion = $dependenciesToDelete;

        foreach ($dependenciesToDelete as $dependencyRemoved) {
            $dependencyRemoved->setQuestion(null);
        }

        $this->collDependencies = null;
        foreach ($dependencies as $dependency) {
            $this->addDependency($dependency);
        }

        $this->collDependencies = $dependencies;
        $this->collDependenciesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Dependency objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Dependency objects.
     * @throws PropelException
     */
    public function countDependencies(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collDependenciesPartial && !$this->isNew();
        if (null === $this->collDependencies || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDependencies) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDependencies());
            }
            $query = DependencyQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByQuestion($this)
                ->count($con);
        }

        return count($this->collDependencies);
    }

    /**
     * Method called to associate a Dependency object to this object
     * through the Dependency foreign key attribute.
     *
     * @param    Dependency $l Dependency
     * @return Question The current object (for fluent API support)
     */
    public function addDependency(Dependency $l)
    {
        if ($this->collDependencies === null) {
            $this->initDependencies();
            $this->collDependenciesPartial = true;
        }

        if (!in_array($l, $this->collDependencies->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddDependency($l);

            if ($this->dependenciesScheduledForDeletion and $this->dependenciesScheduledForDeletion->contains($l)) {
                $this->dependenciesScheduledForDeletion->remove($this->dependenciesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Dependency $dependency The dependency object to add.
     */
    protected function doAddDependency($dependency)
    {
        $this->collDependencies[]= $dependency;
        $dependency->setQuestion($this);
    }

    /**
     * @param	Dependency $dependency The dependency object to remove.
     * @return Question The current object (for fluent API support)
     */
    public function removeDependency($dependency)
    {
        if ($this->getDependencies()->contains($dependency)) {
            $this->collDependencies->remove($this->collDependencies->search($dependency));
            if (null === $this->dependenciesScheduledForDeletion) {
                $this->dependenciesScheduledForDeletion = clone $this->collDependencies;
                $this->dependenciesScheduledForDeletion->clear();
            }
            $this->dependenciesScheduledForDeletion[]= clone $dependency;
            $dependency->setQuestion(null);
        }

        return $this;
    }

    /**
     * Clears out the collDependencyQuestions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Question The current object (for fluent API support)
     * @see        addDependencyQuestions()
     */
    public function clearDependencyQuestions()
    {
        $this->collDependencyQuestions = null; // important to set this to null since that means it is uninitialized
        $this->collDependencyQuestionsPartial = null;

        return $this;
    }

    /**
     * reset is the collDependencyQuestions collection loaded partially
     *
     * @return void
     */
    public function resetPartialDependencyQuestions($v = true)
    {
        $this->collDependencyQuestionsPartial = $v;
    }

    /**
     * Initializes the collDependencyQuestions collection.
     *
     * By default this just sets the collDependencyQuestions collection to an empty array (like clearcollDependencyQuestions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDependencyQuestions($overrideExisting = true)
    {
        if (null !== $this->collDependencyQuestions && !$overrideExisting) {
            return;
        }
        $this->collDependencyQuestions = new PropelObjectCollection();
        $this->collDependencyQuestions->setModel('DependencyQuestion');
    }

    /**
     * Gets an array of DependencyQuestion objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Question is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|DependencyQuestion[] List of DependencyQuestion objects
     * @throws PropelException
     */
    public function getDependencyQuestions($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collDependencyQuestionsPartial && !$this->isNew();
        if (null === $this->collDependencyQuestions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDependencyQuestions) {
                // return empty collection
                $this->initDependencyQuestions();
            } else {
                $collDependencyQuestions = DependencyQuestionQuery::create(null, $criteria)
                    ->filterByDependentQuestion($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collDependencyQuestionsPartial && count($collDependencyQuestions)) {
                      $this->initDependencyQuestions(false);

                      foreach ($collDependencyQuestions as $obj) {
                        if (false == $this->collDependencyQuestions->contains($obj)) {
                          $this->collDependencyQuestions->append($obj);
                        }
                      }

                      $this->collDependencyQuestionsPartial = true;
                    }

                    $collDependencyQuestions->getInternalIterator()->rewind();

                    return $collDependencyQuestions;
                }

                if ($partial && $this->collDependencyQuestions) {
                    foreach ($this->collDependencyQuestions as $obj) {
                        if ($obj->isNew()) {
                            $collDependencyQuestions[] = $obj;
                        }
                    }
                }

                $this->collDependencyQuestions = $collDependencyQuestions;
                $this->collDependencyQuestionsPartial = false;
            }
        }

        return $this->collDependencyQuestions;
    }

    /**
     * Sets a collection of DependencyQuestion objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $dependencyQuestions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Question The current object (for fluent API support)
     */
    public function setDependencyQuestions(PropelCollection $dependencyQuestions, PropelPDO $con = null)
    {
        $dependencyQuestionsToDelete = $this->getDependencyQuestions(new Criteria(), $con)->diff($dependencyQuestions);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->dependencyQuestionsScheduledForDeletion = clone $dependencyQuestionsToDelete;

        foreach ($dependencyQuestionsToDelete as $dependencyQuestionRemoved) {
            $dependencyQuestionRemoved->setDependentQuestion(null);
        }

        $this->collDependencyQuestions = null;
        foreach ($dependencyQuestions as $dependencyQuestion) {
            $this->addDependencyQuestion($dependencyQuestion);
        }

        $this->collDependencyQuestions = $dependencyQuestions;
        $this->collDependencyQuestionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related DependencyQuestion objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related DependencyQuestion objects.
     * @throws PropelException
     */
    public function countDependencyQuestions(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collDependencyQuestionsPartial && !$this->isNew();
        if (null === $this->collDependencyQuestions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDependencyQuestions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDependencyQuestions());
            }
            $query = DependencyQuestionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDependentQuestion($this)
                ->count($con);
        }

        return count($this->collDependencyQuestions);
    }

    /**
     * Method called to associate a DependencyQuestion object to this object
     * through the DependencyQuestion foreign key attribute.
     *
     * @param    DependencyQuestion $l DependencyQuestion
     * @return Question The current object (for fluent API support)
     */
    public function addDependencyQuestion(DependencyQuestion $l)
    {
        if ($this->collDependencyQuestions === null) {
            $this->initDependencyQuestions();
            $this->collDependencyQuestionsPartial = true;
        }

        if (!in_array($l, $this->collDependencyQuestions->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddDependencyQuestion($l);

            if ($this->dependencyQuestionsScheduledForDeletion and $this->dependencyQuestionsScheduledForDeletion->contains($l)) {
                $this->dependencyQuestionsScheduledForDeletion->remove($this->dependencyQuestionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	DependencyQuestion $dependencyQuestion The dependencyQuestion object to add.
     */
    protected function doAddDependencyQuestion($dependencyQuestion)
    {
        $this->collDependencyQuestions[]= $dependencyQuestion;
        $dependencyQuestion->setDependentQuestion($this);
    }

    /**
     * @param	DependencyQuestion $dependencyQuestion The dependencyQuestion object to remove.
     * @return Question The current object (for fluent API support)
     */
    public function removeDependencyQuestion($dependencyQuestion)
    {
        if ($this->getDependencyQuestions()->contains($dependencyQuestion)) {
            $this->collDependencyQuestions->remove($this->collDependencyQuestions->search($dependencyQuestion));
            if (null === $this->dependencyQuestionsScheduledForDeletion) {
                $this->dependencyQuestionsScheduledForDeletion = clone $this->collDependencyQuestions;
                $this->dependencyQuestionsScheduledForDeletion->clear();
            }
            $this->dependencyQuestionsScheduledForDeletion[]= clone $dependencyQuestion;
            $dependencyQuestion->setDependentQuestion(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Question is new, it will return
     * an empty collection; or if this Question has previously
     * been saved, it will retrieve related DependencyQuestions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Question.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|DependencyQuestion[] List of DependencyQuestion objects
     */
    public function getDependencyQuestionsJoinDemandingDependency($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = DependencyQuestionQuery::create(null, $criteria);
        $query->joinWith('DemandingDependency', $join_behavior);

        return $this->getDependencyQuestions($query, $con);
    }

    /**
     * Clears out the collFieldOptions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Question The current object (for fluent API support)
     * @see        addFieldOptions()
     */
    public function clearFieldOptions()
    {
        $this->collFieldOptions = null; // important to set this to null since that means it is uninitialized
        $this->collFieldOptionsPartial = null;

        return $this;
    }

    /**
     * Initializes the collFieldOptions collection.
     *
     * By default this just sets the collFieldOptions collection to an empty collection (like clearFieldOptions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initFieldOptions()
    {
        $this->collFieldOptions = new PropelObjectCollection();
        $this->collFieldOptions->setModel('FieldOption');
    }

    /**
     * Gets a collection of FieldOption objects related by a many-to-many relationship
     * to the current object by way of the questionnaire_selected_option cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Question is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param PropelPDO $con Optional connection object
     *
     * @return PropelObjectCollection|FieldOption[] List of FieldOption objects
     */
    public function getFieldOptions($criteria = null, PropelPDO $con = null)
    {
        if (null === $this->collFieldOptions || null !== $criteria) {
            if ($this->isNew() && null === $this->collFieldOptions) {
                // return empty collection
                $this->initFieldOptions();
            } else {
                $collFieldOptions = FieldOptionQuery::create(null, $criteria)
                    ->filterByQuestion($this)
                    ->find($con);
                if (null !== $criteria) {
                    return $collFieldOptions;
                }
                $this->collFieldOptions = $collFieldOptions;
            }
        }

        return $this->collFieldOptions;
    }

    /**
     * Sets a collection of FieldOption objects related by a many-to-many relationship
     * to the current object by way of the questionnaire_selected_option cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $fieldOptions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Question The current object (for fluent API support)
     */
    public function setFieldOptions(PropelCollection $fieldOptions, PropelPDO $con = null)
    {
        $this->clearFieldOptions();
        $currentFieldOptions = $this->getFieldOptions(null, $con);

        $this->fieldOptionsScheduledForDeletion = $currentFieldOptions->diff($fieldOptions);

        foreach ($fieldOptions as $fieldOption) {
            if (!$currentFieldOptions->contains($fieldOption)) {
                $this->doAddFieldOption($fieldOption);
            }
        }

        $this->collFieldOptions = $fieldOptions;

        return $this;
    }

    /**
     * Gets the number of FieldOption objects related by a many-to-many relationship
     * to the current object by way of the questionnaire_selected_option cross-reference table.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param boolean $distinct Set to true to force count distinct
     * @param PropelPDO $con Optional connection object
     *
     * @return int the number of related FieldOption objects
     */
    public function countFieldOptions($criteria = null, $distinct = false, PropelPDO $con = null)
    {
        if (null === $this->collFieldOptions || null !== $criteria) {
            if ($this->isNew() && null === $this->collFieldOptions) {
                return 0;
            } else {
                $query = FieldOptionQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByQuestion($this)
                    ->count($con);
            }
        } else {
            return count($this->collFieldOptions);
        }
    }

    /**
     * Associate a FieldOption object to this object
     * through the questionnaire_selected_option cross reference table.
     *
     * @param  FieldOption $fieldOption The SelectedOption object to relate
     * @return Question The current object (for fluent API support)
     */
    public function addFieldOption(FieldOption $fieldOption)
    {
        if ($this->collFieldOptions === null) {
            $this->initFieldOptions();
        }

        if (!$this->collFieldOptions->contains($fieldOption)) { // only add it if the **same** object is not already associated
            $this->doAddFieldOption($fieldOption);
            $this->collFieldOptions[] = $fieldOption;

            if ($this->fieldOptionsScheduledForDeletion and $this->fieldOptionsScheduledForDeletion->contains($fieldOption)) {
                $this->fieldOptionsScheduledForDeletion->remove($this->fieldOptionsScheduledForDeletion->search($fieldOption));
            }
        }

        return $this;
    }

    /**
     * @param	FieldOption $fieldOption The fieldOption object to add.
     */
    protected function doAddFieldOption(FieldOption $fieldOption)
    {
        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$fieldOption->getQuestions()->contains($this)) { $selectedOption = new SelectedOption();
            $selectedOption->setFieldOption($fieldOption);
            $this->addSelectedOption($selectedOption);

            $foreignCollection = $fieldOption->getQuestions();
            $foreignCollection[] = $this;
        }
    }

    /**
     * Remove a FieldOption object to this object
     * through the questionnaire_selected_option cross reference table.
     *
     * @param FieldOption $fieldOption The SelectedOption object to relate
     * @return Question The current object (for fluent API support)
     */
    public function removeFieldOption(FieldOption $fieldOption)
    {
        if ($this->getFieldOptions()->contains($fieldOption)) {
            $this->collFieldOptions->remove($this->collFieldOptions->search($fieldOption));
            if (null === $this->fieldOptionsScheduledForDeletion) {
                $this->fieldOptionsScheduledForDeletion = clone $this->collFieldOptions;
                $this->fieldOptionsScheduledForDeletion->clear();
            }
            $this->fieldOptionsScheduledForDeletion[]= $fieldOption;
        }

        return $this;
    }

    /**
     * Clears out the collDemandingDependencies collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Question The current object (for fluent API support)
     * @see        addDemandingDependencies()
     */
    public function clearDemandingDependencies()
    {
        $this->collDemandingDependencies = null; // important to set this to null since that means it is uninitialized
        $this->collDemandingDependenciesPartial = null;

        return $this;
    }

    /**
     * Initializes the collDemandingDependencies collection.
     *
     * By default this just sets the collDemandingDependencies collection to an empty collection (like clearDemandingDependencies());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initDemandingDependencies()
    {
        $this->collDemandingDependencies = new PropelObjectCollection();
        $this->collDemandingDependencies->setModel('Dependency');
    }

    /**
     * Gets a collection of Dependency objects related by a many-to-many relationship
     * to the current object by way of the questionnaire_dependency_question cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Question is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param PropelPDO $con Optional connection object
     *
     * @return PropelObjectCollection|Dependency[] List of Dependency objects
     */
    public function getDemandingDependencies($criteria = null, PropelPDO $con = null)
    {
        if (null === $this->collDemandingDependencies || null !== $criteria) {
            if ($this->isNew() && null === $this->collDemandingDependencies) {
                // return empty collection
                $this->initDemandingDependencies();
            } else {
                $collDemandingDependencies = DependencyQuery::create(null, $criteria)
                    ->filterByDependentQuestion($this)
                    ->find($con);
                if (null !== $criteria) {
                    return $collDemandingDependencies;
                }
                $this->collDemandingDependencies = $collDemandingDependencies;
            }
        }

        return $this->collDemandingDependencies;
    }

    /**
     * Sets a collection of Dependency objects related by a many-to-many relationship
     * to the current object by way of the questionnaire_dependency_question cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $demandingDependencies A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Question The current object (for fluent API support)
     */
    public function setDemandingDependencies(PropelCollection $demandingDependencies, PropelPDO $con = null)
    {
        $this->clearDemandingDependencies();
        $currentDemandingDependencies = $this->getDemandingDependencies(null, $con);

        $this->demandingDependenciesScheduledForDeletion = $currentDemandingDependencies->diff($demandingDependencies);

        foreach ($demandingDependencies as $demandingDependency) {
            if (!$currentDemandingDependencies->contains($demandingDependency)) {
                $this->doAddDemandingDependency($demandingDependency);
            }
        }

        $this->collDemandingDependencies = $demandingDependencies;

        return $this;
    }

    /**
     * Gets the number of Dependency objects related by a many-to-many relationship
     * to the current object by way of the questionnaire_dependency_question cross-reference table.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param boolean $distinct Set to true to force count distinct
     * @param PropelPDO $con Optional connection object
     *
     * @return int the number of related Dependency objects
     */
    public function countDemandingDependencies($criteria = null, $distinct = false, PropelPDO $con = null)
    {
        if (null === $this->collDemandingDependencies || null !== $criteria) {
            if ($this->isNew() && null === $this->collDemandingDependencies) {
                return 0;
            } else {
                $query = DependencyQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByDependentQuestion($this)
                    ->count($con);
            }
        } else {
            return count($this->collDemandingDependencies);
        }
    }

    /**
     * Associate a Dependency object to this object
     * through the questionnaire_dependency_question cross reference table.
     *
     * @param  Dependency $dependency The DependencyQuestion object to relate
     * @return Question The current object (for fluent API support)
     */
    public function addDemandingDependency(Dependency $dependency)
    {
        if ($this->collDemandingDependencies === null) {
            $this->initDemandingDependencies();
        }

        if (!$this->collDemandingDependencies->contains($dependency)) { // only add it if the **same** object is not already associated
            $this->doAddDemandingDependency($dependency);
            $this->collDemandingDependencies[] = $dependency;

            if ($this->demandingDependenciesScheduledForDeletion and $this->demandingDependenciesScheduledForDeletion->contains($dependency)) {
                $this->demandingDependenciesScheduledForDeletion->remove($this->demandingDependenciesScheduledForDeletion->search($dependency));
            }
        }

        return $this;
    }

    /**
     * @param	DemandingDependency $demandingDependency The demandingDependency object to add.
     */
    protected function doAddDemandingDependency(Dependency $demandingDependency)
    {
        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$demandingDependency->getDependentQuestions()->contains($this)) { $dependencyQuestion = new DependencyQuestion();
            $dependencyQuestion->setDemandingDependency($demandingDependency);
            $this->addDependencyQuestion($dependencyQuestion);

            $foreignCollection = $demandingDependency->getDependentQuestions();
            $foreignCollection[] = $this;
        }
    }

    /**
     * Remove a Dependency object to this object
     * through the questionnaire_dependency_question cross reference table.
     *
     * @param Dependency $dependency The DependencyQuestion object to relate
     * @return Question The current object (for fluent API support)
     */
    public function removeDemandingDependency(Dependency $dependency)
    {
        if ($this->getDemandingDependencies()->contains($dependency)) {
            $this->collDemandingDependencies->remove($this->collDemandingDependencies->search($dependency));
            if (null === $this->demandingDependenciesScheduledForDeletion) {
                $this->demandingDependenciesScheduledForDeletion = clone $this->collDemandingDependencies;
                $this->demandingDependenciesScheduledForDeletion->clear();
            }
            $this->demandingDependenciesScheduledForDeletion[]= $dependency;
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->parent_id = null;
        $this->questionnaire_id = null;
        $this->question_type_id = null;
        $this->name = null;
        $this->help = null;
        $this->group_id = null;
        $this->sortable_rank = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collResponses) {
                foreach ($this->collResponses as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSubQuestions) {
                foreach ($this->collSubQuestions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSelectedOptions) {
                foreach ($this->collSelectedOptions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDependencies) {
                foreach ($this->collDependencies as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDependencyQuestions) {
                foreach ($this->collDependencyQuestions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFieldOptions) {
                foreach ($this->collFieldOptions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDemandingDependencies) {
                foreach ($this->collDemandingDependencies as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aQuestionType instanceof Persistent) {
              $this->aQuestionType->clearAllReferences($deep);
            }
            if ($this->aQuestionnaire instanceof Persistent) {
              $this->aQuestionnaire->clearAllReferences($deep);
            }
            if ($this->aParent instanceof Persistent) {
              $this->aParent->clearAllReferences($deep);
            }
            if ($this->aQuestionGroup instanceof Persistent) {
              $this->aQuestionGroup->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collResponses instanceof PropelCollection) {
            $this->collResponses->clearIterator();
        }
        $this->collResponses = null;
        if ($this->collSubQuestions instanceof PropelCollection) {
            $this->collSubQuestions->clearIterator();
        }
        $this->collSubQuestions = null;
        if ($this->collSelectedOptions instanceof PropelCollection) {
            $this->collSelectedOptions->clearIterator();
        }
        $this->collSelectedOptions = null;
        if ($this->collDependencies instanceof PropelCollection) {
            $this->collDependencies->clearIterator();
        }
        $this->collDependencies = null;
        if ($this->collDependencyQuestions instanceof PropelCollection) {
            $this->collDependencyQuestions->clearIterator();
        }
        $this->collDependencyQuestions = null;
        if ($this->collFieldOptions instanceof PropelCollection) {
            $this->collFieldOptions->clearIterator();
        }
        $this->collFieldOptions = null;
        if ($this->collDemandingDependencies instanceof PropelCollection) {
            $this->collDemandingDependencies->clearIterator();
        }
        $this->collDemandingDependencies = null;
        $this->aQuestionType = null;
        $this->aQuestionnaire = null;
        $this->aParent = null;
        $this->aQuestionGroup = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string The value of the 'name' column
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

    // sortable behavior

    /**
     * Wrap the getter for rank value
     *
     * @return    int
     */
    public function getRank()
    {
        return $this->sortable_rank;
    }

    /**
     * Wrap the setter for rank value
     *
     * @param     int
     * @return    Question
     */
    public function setRank($v)
    {
        return $this->setSortableRank($v);
    }


    /**
     * Wrap the getter for scope value
     *
     * @param boolean $returnNulls If true and all scope values are null, this will return null instead of a array full with nulls
     *
     * @return    mixed A array or a native type
     */
    public function getScopeValue($returnNulls = true)
    {


        return $this->getGroupId();

    }

    /**
     * Wrap the setter for scope value
     *
     * @param     mixed A array or a native type
     * @return    Question
     */
    public function setScopeValue($v)
    {


        return $this->setGroupId($v);

    }

    /**
     * Check if the object is first in the list, i.e. if it has 1 for rank
     *
     * @return    boolean
     */
    public function isFirst()
    {
        return $this->getSortableRank() == 1;
    }

    /**
     * Check if the object is last in the list, i.e. if its rank is the highest rank
     *
     * @param     PropelPDO  $con      optional connection
     *
     * @return    boolean
     */
    public function isLast(PropelPDO $con = null)
    {
        return $this->getSortableRank() == QuestionQuery::create()->getMaxRankArray($this->getScopeValue(), $con);
    }

    /**
     * Get the next item in the list, i.e. the one for which rank is immediately higher
     *
     * @param     PropelPDO  $con      optional connection
     *
     * @return    Question
     */
    public function getNext(PropelPDO $con = null)
    {

        $query = QuestionQuery::create();

        $scope = $this->getScopeValue();

        $query->filterByRank($this->getSortableRank() + 1, $scope);


        return $query->findOne($con);
    }

    /**
     * Get the previous item in the list, i.e. the one for which rank is immediately lower
     *
     * @param     PropelPDO  $con      optional connection
     *
     * @return    Question
     */
    public function getPrevious(PropelPDO $con = null)
    {

        $query = QuestionQuery::create();

        $scope = $this->getScopeValue();

        $query->filterByRank($this->getSortableRank() - 1, $scope);


        return $query->findOne($con);
    }

    /**
     * Insert at specified rank
     * The modifications are not persisted until the object is saved.
     *
     * @param     integer    $rank rank value
     * @param     PropelPDO  $con      optional connection
     *
     * @return    Question the current object
     *
     * @throws    PropelException
     */
    public function insertAtRank($rank, PropelPDO $con = null)
    {
        $maxRank = QuestionQuery::create()->getMaxRankArray($this->getScopeValue(), $con);
        if ($rank < 1 || $rank > $maxRank + 1) {
            throw new PropelException('Invalid rank ' . $rank);
        }
        // move the object in the list, at the given rank
        $this->setSortableRank($rank);
        if ($rank != $maxRank + 1) {
            // Keep the list modification query for the save() transaction
            $this->sortableQueries []= array(
                'callable'  => array(self::PEER, 'shiftRank'),
                'arguments' => array(1, $rank, null, $this->getScopeValue())
            );
        }

        return $this;
    }

    /**
     * Insert in the last rank
     * The modifications are not persisted until the object is saved.
     *
     * @param PropelPDO $con optional connection
     *
     * @return    Question the current object
     *
     * @throws    PropelException
     */
    public function insertAtBottom(PropelPDO $con = null)
    {
        $this->setSortableRank(QuestionQuery::create()->getMaxRankArray($this->getScopeValue(), $con) + 1);

        return $this;
    }

    /**
     * Insert in the first rank
     * The modifications are not persisted until the object is saved.
     *
     * @return    Question the current object
     */
    public function insertAtTop()
    {
        return $this->insertAtRank(1);
    }

    /**
     * Move the object to a new rank, and shifts the rank
     * Of the objects inbetween the old and new rank accordingly
     *
     * @param     integer   $newRank rank value
     * @param     PropelPDO $con optional connection
     *
     * @return    Question the current object
     *
     * @throws    PropelException
     */
    public function moveToRank($newRank, PropelPDO $con = null)
    {
        if ($this->isNew()) {
            throw new PropelException('New objects cannot be moved. Please use insertAtRank() instead');
        }
        if ($con === null) {
            $con = Propel::getConnection(QuestionPeer::DATABASE_NAME);
        }
        if ($newRank < 1 || $newRank > QuestionQuery::create()->getMaxRankArray($this->getScopeValue(), $con)) {
            throw new PropelException('Invalid rank ' . $newRank);
        }

        $oldRank = $this->getSortableRank();
        if ($oldRank == $newRank) {
            return $this;
        }

        $con->beginTransaction();
        try {
            // shift the objects between the old and the new rank
            $delta = ($oldRank < $newRank) ? -1 : 1;
            QuestionPeer::shiftRank($delta, min($oldRank, $newRank), max($oldRank, $newRank), $this->getScopeValue(), $con);

            // move the object to its new rank
            $this->setSortableRank($newRank);
            $this->save($con);

            $con->commit();

            return $this;
        } catch (Exception $e) {
            $con->rollback();
            throw $e;
        }
    }

    /**
     * Exchange the rank of the object with the one passed as argument, and saves both objects
     *
     * @param     Question $object
     * @param     PropelPDO $con optional connection
     *
     * @return    Question the current object
     *
     * @throws Exception if the database cannot execute the two updates
     */
    public function swapWith($object, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(QuestionPeer::DATABASE_NAME);
        }
        $con->beginTransaction();
        try {
            $oldScope = $this->getScopeValue();
            $newScope = $object->getScopeValue();
            if ($oldScope != $newScope) {
                $this->setScopeValue($newScope);
                $object->setScopeValue($oldScope);
            }
            $oldRank = $this->getSortableRank();
            $newRank = $object->getSortableRank();
            $this->setSortableRank($newRank);
            $this->save($con);
            $object->setSortableRank($oldRank);
            $object->save($con);
            $con->commit();

            return $this;
        } catch (Exception $e) {
            $con->rollback();
            throw $e;
        }
    }

    /**
     * Move the object higher in the list, i.e. exchanges its rank with the one of the previous object
     *
     * @param     PropelPDO $con optional connection
     *
     * @return    Question the current object
     */
    public function moveUp(PropelPDO $con = null)
    {
        if ($this->isFirst()) {
            return $this;
        }
        if ($con === null) {
            $con = Propel::getConnection(QuestionPeer::DATABASE_NAME);
        }
        $con->beginTransaction();
        try {
            $prev = $this->getPrevious($con);
            $this->swapWith($prev, $con);
            $con->commit();

            return $this;
        } catch (Exception $e) {
            $con->rollback();
            throw $e;
        }
    }

    /**
     * Move the object higher in the list, i.e. exchanges its rank with the one of the next object
     *
     * @param     PropelPDO $con optional connection
     *
     * @return    Question the current object
     */
    public function moveDown(PropelPDO $con = null)
    {
        if ($this->isLast($con)) {
            return $this;
        }
        if ($con === null) {
            $con = Propel::getConnection(QuestionPeer::DATABASE_NAME);
        }
        $con->beginTransaction();
        try {
            $next = $this->getNext($con);
            $this->swapWith($next, $con);
            $con->commit();

            return $this;
        } catch (Exception $e) {
            $con->rollback();
            throw $e;
        }
    }

    /**
     * Move the object to the top of the list
     *
     * @param     PropelPDO $con optional connection
     *
     * @return    Question the current object
     */
    public function moveToTop(PropelPDO $con = null)
    {
        if ($this->isFirst()) {
            return $this;
        }

        return $this->moveToRank(1, $con);
    }

    /**
     * Move the object to the bottom of the list
     *
     * @param     PropelPDO $con optional connection
     *
     * @return integer the old object's rank
     */
    public function moveToBottom(PropelPDO $con = null)
    {
        if ($this->isLast($con)) {
            return false;
        }
        if ($con === null) {
            $con = Propel::getConnection(QuestionPeer::DATABASE_NAME);
        }
        $con->beginTransaction();
        try {
            $bottom = QuestionQuery::create()->getMaxRankArray($this->getScopeValue(), $con);
            $res = $this->moveToRank($bottom, $con);
            $con->commit();

            return $res;
        } catch (Exception $e) {
            $con->rollback();
            throw $e;
        }
    }

    /**
     * Removes the current object from the list (moves it to the null scope).
     * The modifications are not persisted until the object is saved.
     *
     * @param     PropelPDO $con optional connection
     *
     * @return    Question the current object
     */
    public function removeFromList(PropelPDO $con = null)
    {
        // check if object is already removed
        if ($this->getScopeValue() === null) {
            throw new PropelException('Object is already removed (has null scope)');
        }

        // move the object to the end of null scope
        $this->setScopeValue(null);
    //    $this->insertAtBottom($con);

        return $this;
    }

    /**
     * Execute queries that were saved to be run inside the save transaction
     */
    protected function processSortableQueries($con)
    {
        foreach ($this->sortableQueries as $query) {
            $query['arguments'][]= $con;
            call_user_func_array($query['callable'], $query['arguments']);
        }
        $this->sortableQueries = array();
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     Question The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[] = QuestionPeer::UPDATED_AT;

        return $this;
    }

}

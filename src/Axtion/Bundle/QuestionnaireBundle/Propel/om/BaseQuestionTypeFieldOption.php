<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelException;
use \PropelPDO;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOption;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionType;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeFieldOption;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeFieldOptionPeer;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeFieldOptionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeQuery;

abstract class BaseQuestionTypeFieldOption extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\QuestionTypeFieldOptionPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        QuestionTypeFieldOptionPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the question_type_id field.
     * @var        int
     */
    protected $question_type_id;

    /**
     * The value for the option_id field.
     * @var        int
     */
    protected $option_id;

    /**
     * The value for the form_options field.
     * @var        string
     */
    protected $form_options;

    /**
     * The value for the hidden field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $hidden;

    /**
     * The value for the default_value field.
     * @var        string
     */
    protected $default_value;

    /**
     * The value for the array_level field.
     * @var        string
     */
    protected $array_level;

    /**
     * @var        FieldOption
     */
    protected $aFieldOption;

    /**
     * @var        QuestionType
     */
    protected $aQuestionType;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->hidden = false;
    }

    /**
     * Initializes internal state of BaseQuestionTypeFieldOption object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [question_type_id] column value.
     *
     * @return int
     */
    public function getQuestionTypeId()
    {

        return $this->question_type_id;
    }

    /**
     * Get the [option_id] column value.
     *
     * @return int
     */
    public function getOptionId()
    {

        return $this->option_id;
    }

    /**
     * Get the [form_options] column value.
     *
     * @return string
     */
    public function getFormOptions()
    {

        return $this->form_options;
    }

    /**
     * Get the [hidden] column value.
     *
     * @return boolean
     */
    public function getHidden()
    {

        return $this->hidden;
    }

    /**
     * Get the [default_value] column value.
     *
     * @return string
     */
    public function getDefaultValue()
    {

        return $this->default_value;
    }

    /**
     * Get the [array_level] column value.
     *
     * @return string
     */
    public function getArrayLevel()
    {

        return $this->array_level;
    }

    /**
     * Set the value of [question_type_id] column.
     *
     * @param  int $v new value
     * @return QuestionTypeFieldOption The current object (for fluent API support)
     */
    public function setQuestionTypeId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->question_type_id !== $v) {
            $this->question_type_id = $v;
            $this->modifiedColumns[] = QuestionTypeFieldOptionPeer::QUESTION_TYPE_ID;
        }

        if ($this->aQuestionType !== null && $this->aQuestionType->getId() !== $v) {
            $this->aQuestionType = null;
        }


        return $this;
    } // setQuestionTypeId()

    /**
     * Set the value of [option_id] column.
     *
     * @param  int $v new value
     * @return QuestionTypeFieldOption The current object (for fluent API support)
     */
    public function setOptionId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->option_id !== $v) {
            $this->option_id = $v;
            $this->modifiedColumns[] = QuestionTypeFieldOptionPeer::OPTION_ID;
        }

        if ($this->aFieldOption !== null && $this->aFieldOption->getId() !== $v) {
            $this->aFieldOption = null;
        }


        return $this;
    } // setOptionId()

    /**
     * Set the value of [form_options] column.
     *
     * @param  string $v new value
     * @return QuestionTypeFieldOption The current object (for fluent API support)
     */
    public function setFormOptions($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->form_options !== $v) {
            $this->form_options = $v;
            $this->modifiedColumns[] = QuestionTypeFieldOptionPeer::FORM_OPTIONS;
        }


        return $this;
    } // setFormOptions()

    /**
     * Sets the value of the [hidden] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return QuestionTypeFieldOption The current object (for fluent API support)
     */
    public function setHidden($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->hidden !== $v) {
            $this->hidden = $v;
            $this->modifiedColumns[] = QuestionTypeFieldOptionPeer::HIDDEN;
        }


        return $this;
    } // setHidden()

    /**
     * Set the value of [default_value] column.
     *
     * @param  string $v new value
     * @return QuestionTypeFieldOption The current object (for fluent API support)
     */
    public function setDefaultValue($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->default_value !== $v) {
            $this->default_value = $v;
            $this->modifiedColumns[] = QuestionTypeFieldOptionPeer::DEFAULT_VALUE;
        }


        return $this;
    } // setDefaultValue()

    /**
     * Set the value of [array_level] column.
     *
     * @param  string $v new value
     * @return QuestionTypeFieldOption The current object (for fluent API support)
     */
    public function setArrayLevel($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->array_level !== $v) {
            $this->array_level = $v;
            $this->modifiedColumns[] = QuestionTypeFieldOptionPeer::ARRAY_LEVEL;
        }


        return $this;
    } // setArrayLevel()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->hidden !== false) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->question_type_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->option_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->form_options = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->hidden = ($row[$startcol + 3] !== null) ? (boolean) $row[$startcol + 3] : null;
            $this->default_value = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->array_level = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 6; // 6 = QuestionTypeFieldOptionPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating QuestionTypeFieldOption object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aQuestionType !== null && $this->question_type_id !== $this->aQuestionType->getId()) {
            $this->aQuestionType = null;
        }
        if ($this->aFieldOption !== null && $this->option_id !== $this->aFieldOption->getId()) {
            $this->aFieldOption = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(QuestionTypeFieldOptionPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = QuestionTypeFieldOptionPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aFieldOption = null;
            $this->aQuestionType = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(QuestionTypeFieldOptionPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = QuestionTypeFieldOptionQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(QuestionTypeFieldOptionPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                QuestionTypeFieldOptionPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aFieldOption !== null) {
                if ($this->aFieldOption->isModified() || $this->aFieldOption->isNew()) {
                    $affectedRows += $this->aFieldOption->save($con);
                }
                $this->setFieldOption($this->aFieldOption);
            }

            if ($this->aQuestionType !== null) {
                if ($this->aQuestionType->isModified() || $this->aQuestionType->isNew()) {
                    $affectedRows += $this->aQuestionType->save($con);
                }
                $this->setQuestionType($this->aQuestionType);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(QuestionTypeFieldOptionPeer::QUESTION_TYPE_ID)) {
            $modifiedColumns[':p' . $index++]  = '"question_type_id"';
        }
        if ($this->isColumnModified(QuestionTypeFieldOptionPeer::OPTION_ID)) {
            $modifiedColumns[':p' . $index++]  = '"option_id"';
        }
        if ($this->isColumnModified(QuestionTypeFieldOptionPeer::FORM_OPTIONS)) {
            $modifiedColumns[':p' . $index++]  = '"form_options"';
        }
        if ($this->isColumnModified(QuestionTypeFieldOptionPeer::HIDDEN)) {
            $modifiedColumns[':p' . $index++]  = '"hidden"';
        }
        if ($this->isColumnModified(QuestionTypeFieldOptionPeer::DEFAULT_VALUE)) {
            $modifiedColumns[':p' . $index++]  = '"default_value"';
        }
        if ($this->isColumnModified(QuestionTypeFieldOptionPeer::ARRAY_LEVEL)) {
            $modifiedColumns[':p' . $index++]  = '"array_level"';
        }

        $sql = sprintf(
            'INSERT INTO "questionnaire_question_type_field_option" (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '"question_type_id"':
                        $stmt->bindValue($identifier, $this->question_type_id, PDO::PARAM_INT);
                        break;
                    case '"option_id"':
                        $stmt->bindValue($identifier, $this->option_id, PDO::PARAM_INT);
                        break;
                    case '"form_options"':
                        $stmt->bindValue($identifier, $this->form_options, PDO::PARAM_STR);
                        break;
                    case '"hidden"':
                        $stmt->bindValue($identifier, $this->hidden, PDO::PARAM_BOOL);
                        break;
                    case '"default_value"':
                        $stmt->bindValue($identifier, $this->default_value, PDO::PARAM_STR);
                        break;
                    case '"array_level"':
                        $stmt->bindValue($identifier, $this->array_level, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aFieldOption !== null) {
                if (!$this->aFieldOption->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aFieldOption->getValidationFailures());
                }
            }

            if ($this->aQuestionType !== null) {
                if (!$this->aQuestionType->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aQuestionType->getValidationFailures());
                }
            }


            if (($retval = QuestionTypeFieldOptionPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = QuestionTypeFieldOptionPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getQuestionTypeId();
                break;
            case 1:
                return $this->getOptionId();
                break;
            case 2:
                return $this->getFormOptions();
                break;
            case 3:
                return $this->getHidden();
                break;
            case 4:
                return $this->getDefaultValue();
                break;
            case 5:
                return $this->getArrayLevel();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['QuestionTypeFieldOption'][serialize($this->getPrimaryKey())])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['QuestionTypeFieldOption'][serialize($this->getPrimaryKey())] = true;
        $keys = QuestionTypeFieldOptionPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getQuestionTypeId(),
            $keys[1] => $this->getOptionId(),
            $keys[2] => $this->getFormOptions(),
            $keys[3] => $this->getHidden(),
            $keys[4] => $this->getDefaultValue(),
            $keys[5] => $this->getArrayLevel(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aFieldOption) {
                $result['FieldOption'] = $this->aFieldOption->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aQuestionType) {
                $result['QuestionType'] = $this->aQuestionType->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = QuestionTypeFieldOptionPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setQuestionTypeId($value);
                break;
            case 1:
                $this->setOptionId($value);
                break;
            case 2:
                $this->setFormOptions($value);
                break;
            case 3:
                $this->setHidden($value);
                break;
            case 4:
                $this->setDefaultValue($value);
                break;
            case 5:
                $this->setArrayLevel($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = QuestionTypeFieldOptionPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setQuestionTypeId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setOptionId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setFormOptions($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setHidden($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setDefaultValue($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setArrayLevel($arr[$keys[5]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(QuestionTypeFieldOptionPeer::DATABASE_NAME);

        if ($this->isColumnModified(QuestionTypeFieldOptionPeer::QUESTION_TYPE_ID)) $criteria->add(QuestionTypeFieldOptionPeer::QUESTION_TYPE_ID, $this->question_type_id);
        if ($this->isColumnModified(QuestionTypeFieldOptionPeer::OPTION_ID)) $criteria->add(QuestionTypeFieldOptionPeer::OPTION_ID, $this->option_id);
        if ($this->isColumnModified(QuestionTypeFieldOptionPeer::FORM_OPTIONS)) $criteria->add(QuestionTypeFieldOptionPeer::FORM_OPTIONS, $this->form_options);
        if ($this->isColumnModified(QuestionTypeFieldOptionPeer::HIDDEN)) $criteria->add(QuestionTypeFieldOptionPeer::HIDDEN, $this->hidden);
        if ($this->isColumnModified(QuestionTypeFieldOptionPeer::DEFAULT_VALUE)) $criteria->add(QuestionTypeFieldOptionPeer::DEFAULT_VALUE, $this->default_value);
        if ($this->isColumnModified(QuestionTypeFieldOptionPeer::ARRAY_LEVEL)) $criteria->add(QuestionTypeFieldOptionPeer::ARRAY_LEVEL, $this->array_level);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(QuestionTypeFieldOptionPeer::DATABASE_NAME);
        $criteria->add(QuestionTypeFieldOptionPeer::QUESTION_TYPE_ID, $this->question_type_id);
        $criteria->add(QuestionTypeFieldOptionPeer::OPTION_ID, $this->option_id);

        return $criteria;
    }

    /**
     * Returns the composite primary key for this object.
     * The array elements will be in same order as specified in XML.
     * @return array
     */
    public function getPrimaryKey()
    {
        $pks = array();
        $pks[0] = $this->getQuestionTypeId();
        $pks[1] = $this->getOptionId();

        return $pks;
    }

    /**
     * Set the [composite] primary key.
     *
     * @param array $keys The elements of the composite key (order must match the order in XML file).
     * @return void
     */
    public function setPrimaryKey($keys)
    {
        $this->setQuestionTypeId($keys[0]);
        $this->setOptionId($keys[1]);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return (null === $this->getQuestionTypeId()) && (null === $this->getOptionId());
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of QuestionTypeFieldOption (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setQuestionTypeId($this->getQuestionTypeId());
        $copyObj->setOptionId($this->getOptionId());
        $copyObj->setFormOptions($this->getFormOptions());
        $copyObj->setHidden($this->getHidden());
        $copyObj->setDefaultValue($this->getDefaultValue());
        $copyObj->setArrayLevel($this->getArrayLevel());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return QuestionTypeFieldOption Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return QuestionTypeFieldOptionPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new QuestionTypeFieldOptionPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a FieldOption object.
     *
     * @param                  FieldOption $v
     * @return QuestionTypeFieldOption The current object (for fluent API support)
     * @throws PropelException
     */
    public function setFieldOption(FieldOption $v = null)
    {
        if ($v === null) {
            $this->setOptionId(NULL);
        } else {
            $this->setOptionId($v->getId());
        }

        $this->aFieldOption = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the FieldOption object, it will not be re-added.
        if ($v !== null) {
            $v->addQuestionTypeFieldOption($this);
        }


        return $this;
    }


    /**
     * Get the associated FieldOption object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return FieldOption The associated FieldOption object.
     * @throws PropelException
     */
    public function getFieldOption(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aFieldOption === null && ($this->option_id !== null) && $doQuery) {
            $this->aFieldOption = FieldOptionQuery::create()->findPk($this->option_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aFieldOption->addQuestionTypeFieldOptions($this);
             */
        }

        return $this->aFieldOption;
    }

    /**
     * Declares an association between this object and a QuestionType object.
     *
     * @param                  QuestionType $v
     * @return QuestionTypeFieldOption The current object (for fluent API support)
     * @throws PropelException
     */
    public function setQuestionType(QuestionType $v = null)
    {
        if ($v === null) {
            $this->setQuestionTypeId(NULL);
        } else {
            $this->setQuestionTypeId($v->getId());
        }

        $this->aQuestionType = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the QuestionType object, it will not be re-added.
        if ($v !== null) {
            $v->addQuestionTypeFieldOption($this);
        }


        return $this;
    }


    /**
     * Get the associated QuestionType object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return QuestionType The associated QuestionType object.
     * @throws PropelException
     */
    public function getQuestionType(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aQuestionType === null && ($this->question_type_id !== null) && $doQuery) {
            $this->aQuestionType = QuestionTypeQuery::create()->findPk($this->question_type_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aQuestionType->addQuestionTypeFieldOptions($this);
             */
        }

        return $this->aQuestionType;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->question_type_id = null;
        $this->option_id = null;
        $this->form_options = null;
        $this->hidden = null;
        $this->default_value = null;
        $this->array_level = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aFieldOption instanceof Persistent) {
              $this->aFieldOption->clearAllReferences($deep);
            }
            if ($this->aQuestionType instanceof Persistent) {
              $this->aQuestionType->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aFieldOption = null;
        $this->aQuestionType = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(QuestionTypeFieldOptionPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}

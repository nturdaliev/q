<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOption;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionType;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeFieldOption;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeFieldOptionPeer;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeFieldOptionQuery;

/**
 * @method QuestionTypeFieldOptionQuery orderByQuestionTypeId($order = Criteria::ASC) Order by the question_type_id column
 * @method QuestionTypeFieldOptionQuery orderByOptionId($order = Criteria::ASC) Order by the option_id column
 * @method QuestionTypeFieldOptionQuery orderByFormOptions($order = Criteria::ASC) Order by the form_options column
 * @method QuestionTypeFieldOptionQuery orderByHidden($order = Criteria::ASC) Order by the hidden column
 * @method QuestionTypeFieldOptionQuery orderByDefaultValue($order = Criteria::ASC) Order by the default_value column
 * @method QuestionTypeFieldOptionQuery orderByArrayLevel($order = Criteria::ASC) Order by the array_level column
 *
 * @method QuestionTypeFieldOptionQuery groupByQuestionTypeId() Group by the question_type_id column
 * @method QuestionTypeFieldOptionQuery groupByOptionId() Group by the option_id column
 * @method QuestionTypeFieldOptionQuery groupByFormOptions() Group by the form_options column
 * @method QuestionTypeFieldOptionQuery groupByHidden() Group by the hidden column
 * @method QuestionTypeFieldOptionQuery groupByDefaultValue() Group by the default_value column
 * @method QuestionTypeFieldOptionQuery groupByArrayLevel() Group by the array_level column
 *
 * @method QuestionTypeFieldOptionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method QuestionTypeFieldOptionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method QuestionTypeFieldOptionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method QuestionTypeFieldOptionQuery leftJoinFieldOption($relationAlias = null) Adds a LEFT JOIN clause to the query using the FieldOption relation
 * @method QuestionTypeFieldOptionQuery rightJoinFieldOption($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FieldOption relation
 * @method QuestionTypeFieldOptionQuery innerJoinFieldOption($relationAlias = null) Adds a INNER JOIN clause to the query using the FieldOption relation
 *
 * @method QuestionTypeFieldOptionQuery leftJoinQuestionType($relationAlias = null) Adds a LEFT JOIN clause to the query using the QuestionType relation
 * @method QuestionTypeFieldOptionQuery rightJoinQuestionType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the QuestionType relation
 * @method QuestionTypeFieldOptionQuery innerJoinQuestionType($relationAlias = null) Adds a INNER JOIN clause to the query using the QuestionType relation
 *
 * @method QuestionTypeFieldOption findOne(PropelPDO $con = null) Return the first QuestionTypeFieldOption matching the query
 * @method QuestionTypeFieldOption findOneOrCreate(PropelPDO $con = null) Return the first QuestionTypeFieldOption matching the query, or a new QuestionTypeFieldOption object populated from the query conditions when no match is found
 *
 * @method QuestionTypeFieldOption findOneByQuestionTypeId(int $question_type_id) Return the first QuestionTypeFieldOption filtered by the question_type_id column
 * @method QuestionTypeFieldOption findOneByOptionId(int $option_id) Return the first QuestionTypeFieldOption filtered by the option_id column
 * @method QuestionTypeFieldOption findOneByFormOptions(string $form_options) Return the first QuestionTypeFieldOption filtered by the form_options column
 * @method QuestionTypeFieldOption findOneByHidden(boolean $hidden) Return the first QuestionTypeFieldOption filtered by the hidden column
 * @method QuestionTypeFieldOption findOneByDefaultValue(string $default_value) Return the first QuestionTypeFieldOption filtered by the default_value column
 * @method QuestionTypeFieldOption findOneByArrayLevel(string $array_level) Return the first QuestionTypeFieldOption filtered by the array_level column
 *
 * @method array findByQuestionTypeId(int $question_type_id) Return QuestionTypeFieldOption objects filtered by the question_type_id column
 * @method array findByOptionId(int $option_id) Return QuestionTypeFieldOption objects filtered by the option_id column
 * @method array findByFormOptions(string $form_options) Return QuestionTypeFieldOption objects filtered by the form_options column
 * @method array findByHidden(boolean $hidden) Return QuestionTypeFieldOption objects filtered by the hidden column
 * @method array findByDefaultValue(string $default_value) Return QuestionTypeFieldOption objects filtered by the default_value column
 * @method array findByArrayLevel(string $array_level) Return QuestionTypeFieldOption objects filtered by the array_level column
 */
abstract class BaseQuestionTypeFieldOptionQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseQuestionTypeFieldOptionQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\QuestionTypeFieldOption';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new QuestionTypeFieldOptionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   QuestionTypeFieldOptionQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return QuestionTypeFieldOptionQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof QuestionTypeFieldOptionQuery) {
            return $criteria;
        }
        $query = new QuestionTypeFieldOptionQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query
                         A Primary key composition: [$question_type_id, $option_id]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   QuestionTypeFieldOption|QuestionTypeFieldOption[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = QuestionTypeFieldOptionPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(QuestionTypeFieldOptionPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 QuestionTypeFieldOption A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "question_type_id", "option_id", "form_options", "hidden", "default_value", "array_level" FROM "questionnaire_question_type_field_option" WHERE "question_type_id" = :p0 AND "option_id" = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new QuestionTypeFieldOption();
            $obj->hydrate($row);
            QuestionTypeFieldOptionPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return QuestionTypeFieldOption|QuestionTypeFieldOption[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|QuestionTypeFieldOption[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return QuestionTypeFieldOptionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(QuestionTypeFieldOptionPeer::QUESTION_TYPE_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(QuestionTypeFieldOptionPeer::OPTION_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return QuestionTypeFieldOptionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(QuestionTypeFieldOptionPeer::QUESTION_TYPE_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(QuestionTypeFieldOptionPeer::OPTION_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the question_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestionTypeId(1234); // WHERE question_type_id = 1234
     * $query->filterByQuestionTypeId(array(12, 34)); // WHERE question_type_id IN (12, 34)
     * $query->filterByQuestionTypeId(array('min' => 12)); // WHERE question_type_id >= 12
     * $query->filterByQuestionTypeId(array('max' => 12)); // WHERE question_type_id <= 12
     * </code>
     *
     * @see       filterByQuestionType()
     *
     * @param     mixed $questionTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionTypeFieldOptionQuery The current query, for fluid interface
     */
    public function filterByQuestionTypeId($questionTypeId = null, $comparison = null)
    {
        if (is_array($questionTypeId)) {
            $useMinMax = false;
            if (isset($questionTypeId['min'])) {
                $this->addUsingAlias(QuestionTypeFieldOptionPeer::QUESTION_TYPE_ID, $questionTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($questionTypeId['max'])) {
                $this->addUsingAlias(QuestionTypeFieldOptionPeer::QUESTION_TYPE_ID, $questionTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionTypeFieldOptionPeer::QUESTION_TYPE_ID, $questionTypeId, $comparison);
    }

    /**
     * Filter the query on the option_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOptionId(1234); // WHERE option_id = 1234
     * $query->filterByOptionId(array(12, 34)); // WHERE option_id IN (12, 34)
     * $query->filterByOptionId(array('min' => 12)); // WHERE option_id >= 12
     * $query->filterByOptionId(array('max' => 12)); // WHERE option_id <= 12
     * </code>
     *
     * @see       filterByFieldOption()
     *
     * @param     mixed $optionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionTypeFieldOptionQuery The current query, for fluid interface
     */
    public function filterByOptionId($optionId = null, $comparison = null)
    {
        if (is_array($optionId)) {
            $useMinMax = false;
            if (isset($optionId['min'])) {
                $this->addUsingAlias(QuestionTypeFieldOptionPeer::OPTION_ID, $optionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($optionId['max'])) {
                $this->addUsingAlias(QuestionTypeFieldOptionPeer::OPTION_ID, $optionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionTypeFieldOptionPeer::OPTION_ID, $optionId, $comparison);
    }

    /**
     * Filter the query on the form_options column
     *
     * Example usage:
     * <code>
     * $query->filterByFormOptions('fooValue');   // WHERE form_options = 'fooValue'
     * $query->filterByFormOptions('%fooValue%'); // WHERE form_options LIKE '%fooValue%'
     * </code>
     *
     * @param     string $formOptions The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionTypeFieldOptionQuery The current query, for fluid interface
     */
    public function filterByFormOptions($formOptions = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($formOptions)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $formOptions)) {
                $formOptions = str_replace('*', '%', $formOptions);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionTypeFieldOptionPeer::FORM_OPTIONS, $formOptions, $comparison);
    }

    /**
     * Filter the query on the hidden column
     *
     * Example usage:
     * <code>
     * $query->filterByHidden(true); // WHERE hidden = true
     * $query->filterByHidden('yes'); // WHERE hidden = true
     * </code>
     *
     * @param     boolean|string $hidden The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionTypeFieldOptionQuery The current query, for fluid interface
     */
    public function filterByHidden($hidden = null, $comparison = null)
    {
        if (is_string($hidden)) {
            $hidden = in_array(strtolower($hidden), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(QuestionTypeFieldOptionPeer::HIDDEN, $hidden, $comparison);
    }

    /**
     * Filter the query on the default_value column
     *
     * Example usage:
     * <code>
     * $query->filterByDefaultValue('fooValue');   // WHERE default_value = 'fooValue'
     * $query->filterByDefaultValue('%fooValue%'); // WHERE default_value LIKE '%fooValue%'
     * </code>
     *
     * @param     string $defaultValue The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionTypeFieldOptionQuery The current query, for fluid interface
     */
    public function filterByDefaultValue($defaultValue = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($defaultValue)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $defaultValue)) {
                $defaultValue = str_replace('*', '%', $defaultValue);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionTypeFieldOptionPeer::DEFAULT_VALUE, $defaultValue, $comparison);
    }

    /**
     * Filter the query on the array_level column
     *
     * Example usage:
     * <code>
     * $query->filterByArrayLevel('fooValue');   // WHERE array_level = 'fooValue'
     * $query->filterByArrayLevel('%fooValue%'); // WHERE array_level LIKE '%fooValue%'
     * </code>
     *
     * @param     string $arrayLevel The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionTypeFieldOptionQuery The current query, for fluid interface
     */
    public function filterByArrayLevel($arrayLevel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($arrayLevel)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $arrayLevel)) {
                $arrayLevel = str_replace('*', '%', $arrayLevel);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionTypeFieldOptionPeer::ARRAY_LEVEL, $arrayLevel, $comparison);
    }

    /**
     * Filter the query by a related FieldOption object
     *
     * @param   FieldOption|PropelObjectCollection $fieldOption The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 QuestionTypeFieldOptionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByFieldOption($fieldOption, $comparison = null)
    {
        if ($fieldOption instanceof FieldOption) {
            return $this
                ->addUsingAlias(QuestionTypeFieldOptionPeer::OPTION_ID, $fieldOption->getId(), $comparison);
        } elseif ($fieldOption instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(QuestionTypeFieldOptionPeer::OPTION_ID, $fieldOption->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFieldOption() only accepts arguments of type FieldOption or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FieldOption relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return QuestionTypeFieldOptionQuery The current query, for fluid interface
     */
    public function joinFieldOption($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FieldOption');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FieldOption');
        }

        return $this;
    }

    /**
     * Use the FieldOption relation FieldOption object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionQuery A secondary query class using the current class as primary query
     */
    public function useFieldOptionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFieldOption($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FieldOption', '\Axtion\Bundle\QuestionnaireBundle\Propel\FieldOptionQuery');
    }

    /**
     * Filter the query by a related QuestionType object
     *
     * @param   QuestionType|PropelObjectCollection $questionType The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 QuestionTypeFieldOptionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByQuestionType($questionType, $comparison = null)
    {
        if ($questionType instanceof QuestionType) {
            return $this
                ->addUsingAlias(QuestionTypeFieldOptionPeer::QUESTION_TYPE_ID, $questionType->getId(), $comparison);
        } elseif ($questionType instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(QuestionTypeFieldOptionPeer::QUESTION_TYPE_ID, $questionType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByQuestionType() only accepts arguments of type QuestionType or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the QuestionType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return QuestionTypeFieldOptionQuery The current query, for fluid interface
     */
    public function joinQuestionType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('QuestionType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'QuestionType');
        }

        return $this;
    }

    /**
     * Use the QuestionType relation QuestionType object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeQuery A secondary query class using the current class as primary query
     */
    public function useQuestionTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinQuestionType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'QuestionType', '\Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   QuestionTypeFieldOption $questionTypeFieldOption Object to remove from the list of results
     *
     * @return QuestionTypeFieldOptionQuery The current query, for fluid interface
     */
    public function prune($questionTypeFieldOption = null)
    {
        if ($questionTypeFieldOption) {
            $this->addCond('pruneCond0', $this->getAliasedColName(QuestionTypeFieldOptionPeer::QUESTION_TYPE_ID), $questionTypeFieldOption->getQuestionTypeId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(QuestionTypeFieldOptionPeer::OPTION_ID), $questionTypeFieldOption->getOptionId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}

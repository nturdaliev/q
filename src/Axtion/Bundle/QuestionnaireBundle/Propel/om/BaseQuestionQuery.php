<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Axtion\Bundle\AssessmentBundle\Propel\Response;
use Axtion\Bundle\QuestionnaireBundle\Propel\Dependency;
use Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuestion;
use Axtion\Bundle\QuestionnaireBundle\Propel\FieldOption;
use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionGroup;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionPeer;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionType;
use Axtion\Bundle\QuestionnaireBundle\Propel\Questionnaire;
use Axtion\Bundle\QuestionnaireBundle\Propel\SelectedOption;

/**
 * @method QuestionQuery orderById($order = Criteria::ASC) Order by the id column
 * @method QuestionQuery orderByParentId($order = Criteria::ASC) Order by the parent_id column
 * @method QuestionQuery orderByQuestionnaireId($order = Criteria::ASC) Order by the questionnaire_id column
 * @method QuestionQuery orderByQuestionTypeId($order = Criteria::ASC) Order by the question_type_id column
 * @method QuestionQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method QuestionQuery orderByHelp($order = Criteria::ASC) Order by the help column
 * @method QuestionQuery orderByGroupId($order = Criteria::ASC) Order by the group_id column
 * @method QuestionQuery orderBySortableRank($order = Criteria::ASC) Order by the sortable_rank column
 * @method QuestionQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method QuestionQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method QuestionQuery groupById() Group by the id column
 * @method QuestionQuery groupByParentId() Group by the parent_id column
 * @method QuestionQuery groupByQuestionnaireId() Group by the questionnaire_id column
 * @method QuestionQuery groupByQuestionTypeId() Group by the question_type_id column
 * @method QuestionQuery groupByName() Group by the name column
 * @method QuestionQuery groupByHelp() Group by the help column
 * @method QuestionQuery groupByGroupId() Group by the group_id column
 * @method QuestionQuery groupBySortableRank() Group by the sortable_rank column
 * @method QuestionQuery groupByCreatedAt() Group by the created_at column
 * @method QuestionQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method QuestionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method QuestionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method QuestionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method QuestionQuery leftJoinQuestionType($relationAlias = null) Adds a LEFT JOIN clause to the query using the QuestionType relation
 * @method QuestionQuery rightJoinQuestionType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the QuestionType relation
 * @method QuestionQuery innerJoinQuestionType($relationAlias = null) Adds a INNER JOIN clause to the query using the QuestionType relation
 *
 * @method QuestionQuery leftJoinQuestionnaire($relationAlias = null) Adds a LEFT JOIN clause to the query using the Questionnaire relation
 * @method QuestionQuery rightJoinQuestionnaire($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Questionnaire relation
 * @method QuestionQuery innerJoinQuestionnaire($relationAlias = null) Adds a INNER JOIN clause to the query using the Questionnaire relation
 *
 * @method QuestionQuery leftJoinParent($relationAlias = null) Adds a LEFT JOIN clause to the query using the Parent relation
 * @method QuestionQuery rightJoinParent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Parent relation
 * @method QuestionQuery innerJoinParent($relationAlias = null) Adds a INNER JOIN clause to the query using the Parent relation
 *
 * @method QuestionQuery leftJoinQuestionGroup($relationAlias = null) Adds a LEFT JOIN clause to the query using the QuestionGroup relation
 * @method QuestionQuery rightJoinQuestionGroup($relationAlias = null) Adds a RIGHT JOIN clause to the query using the QuestionGroup relation
 * @method QuestionQuery innerJoinQuestionGroup($relationAlias = null) Adds a INNER JOIN clause to the query using the QuestionGroup relation
 *
 * @method QuestionQuery leftJoinResponse($relationAlias = null) Adds a LEFT JOIN clause to the query using the Response relation
 * @method QuestionQuery rightJoinResponse($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Response relation
 * @method QuestionQuery innerJoinResponse($relationAlias = null) Adds a INNER JOIN clause to the query using the Response relation
 *
 * @method QuestionQuery leftJoinSubQuestion($relationAlias = null) Adds a LEFT JOIN clause to the query using the SubQuestion relation
 * @method QuestionQuery rightJoinSubQuestion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SubQuestion relation
 * @method QuestionQuery innerJoinSubQuestion($relationAlias = null) Adds a INNER JOIN clause to the query using the SubQuestion relation
 *
 * @method QuestionQuery leftJoinSelectedOption($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelectedOption relation
 * @method QuestionQuery rightJoinSelectedOption($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelectedOption relation
 * @method QuestionQuery innerJoinSelectedOption($relationAlias = null) Adds a INNER JOIN clause to the query using the SelectedOption relation
 *
 * @method QuestionQuery leftJoinDependency($relationAlias = null) Adds a LEFT JOIN clause to the query using the Dependency relation
 * @method QuestionQuery rightJoinDependency($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Dependency relation
 * @method QuestionQuery innerJoinDependency($relationAlias = null) Adds a INNER JOIN clause to the query using the Dependency relation
 *
 * @method QuestionQuery leftJoinDependencyQuestion($relationAlias = null) Adds a LEFT JOIN clause to the query using the DependencyQuestion relation
 * @method QuestionQuery rightJoinDependencyQuestion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DependencyQuestion relation
 * @method QuestionQuery innerJoinDependencyQuestion($relationAlias = null) Adds a INNER JOIN clause to the query using the DependencyQuestion relation
 *
 * @method Question findOne(PropelPDO $con = null) Return the first Question matching the query
 * @method Question findOneOrCreate(PropelPDO $con = null) Return the first Question matching the query, or a new Question object populated from the query conditions when no match is found
 *
 * @method Question findOneByParentId(int $parent_id) Return the first Question filtered by the parent_id column
 * @method Question findOneByQuestionnaireId(int $questionnaire_id) Return the first Question filtered by the questionnaire_id column
 * @method Question findOneByQuestionTypeId(int $question_type_id) Return the first Question filtered by the question_type_id column
 * @method Question findOneByName(string $name) Return the first Question filtered by the name column
 * @method Question findOneByHelp(string $help) Return the first Question filtered by the help column
 * @method Question findOneByGroupId(int $group_id) Return the first Question filtered by the group_id column
 * @method Question findOneBySortableRank(int $sortable_rank) Return the first Question filtered by the sortable_rank column
 * @method Question findOneByCreatedAt(string $created_at) Return the first Question filtered by the created_at column
 * @method Question findOneByUpdatedAt(string $updated_at) Return the first Question filtered by the updated_at column
 *
 * @method array findById(int $id) Return Question objects filtered by the id column
 * @method array findByParentId(int $parent_id) Return Question objects filtered by the parent_id column
 * @method array findByQuestionnaireId(int $questionnaire_id) Return Question objects filtered by the questionnaire_id column
 * @method array findByQuestionTypeId(int $question_type_id) Return Question objects filtered by the question_type_id column
 * @method array findByName(string $name) Return Question objects filtered by the name column
 * @method array findByHelp(string $help) Return Question objects filtered by the help column
 * @method array findByGroupId(int $group_id) Return Question objects filtered by the group_id column
 * @method array findBySortableRank(int $sortable_rank) Return Question objects filtered by the sortable_rank column
 * @method array findByCreatedAt(string $created_at) Return Question objects filtered by the created_at column
 * @method array findByUpdatedAt(string $updated_at) Return Question objects filtered by the updated_at column
 */
abstract class BaseQuestionQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseQuestionQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Question';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new QuestionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   QuestionQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return QuestionQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof QuestionQuery) {
            return $criteria;
        }
        $query = new QuestionQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Question|Question[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = QuestionPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(QuestionPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Question A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Question A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "id", "parent_id", "questionnaire_id", "question_type_id", "name", "help", "group_id", "sortable_rank", "created_at", "updated_at" FROM "questionnaire_question" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Question();
            $obj->hydrate($row);
            QuestionPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Question|Question[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Question[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(QuestionPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(QuestionPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(QuestionPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(QuestionPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the parent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByParentId(1234); // WHERE parent_id = 1234
     * $query->filterByParentId(array(12, 34)); // WHERE parent_id IN (12, 34)
     * $query->filterByParentId(array('min' => 12)); // WHERE parent_id >= 12
     * $query->filterByParentId(array('max' => 12)); // WHERE parent_id <= 12
     * </code>
     *
     * @see       filterByParent()
     *
     * @param     mixed $parentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function filterByParentId($parentId = null, $comparison = null)
    {
        if (is_array($parentId)) {
            $useMinMax = false;
            if (isset($parentId['min'])) {
                $this->addUsingAlias(QuestionPeer::PARENT_ID, $parentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($parentId['max'])) {
                $this->addUsingAlias(QuestionPeer::PARENT_ID, $parentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionPeer::PARENT_ID, $parentId, $comparison);
    }

    /**
     * Filter the query on the questionnaire_id column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestionnaireId(1234); // WHERE questionnaire_id = 1234
     * $query->filterByQuestionnaireId(array(12, 34)); // WHERE questionnaire_id IN (12, 34)
     * $query->filterByQuestionnaireId(array('min' => 12)); // WHERE questionnaire_id >= 12
     * $query->filterByQuestionnaireId(array('max' => 12)); // WHERE questionnaire_id <= 12
     * </code>
     *
     * @see       filterByQuestionnaire()
     *
     * @param     mixed $questionnaireId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function filterByQuestionnaireId($questionnaireId = null, $comparison = null)
    {
        if (is_array($questionnaireId)) {
            $useMinMax = false;
            if (isset($questionnaireId['min'])) {
                $this->addUsingAlias(QuestionPeer::QUESTIONNAIRE_ID, $questionnaireId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($questionnaireId['max'])) {
                $this->addUsingAlias(QuestionPeer::QUESTIONNAIRE_ID, $questionnaireId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionPeer::QUESTIONNAIRE_ID, $questionnaireId, $comparison);
    }

    /**
     * Filter the query on the question_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestionTypeId(1234); // WHERE question_type_id = 1234
     * $query->filterByQuestionTypeId(array(12, 34)); // WHERE question_type_id IN (12, 34)
     * $query->filterByQuestionTypeId(array('min' => 12)); // WHERE question_type_id >= 12
     * $query->filterByQuestionTypeId(array('max' => 12)); // WHERE question_type_id <= 12
     * </code>
     *
     * @see       filterByQuestionType()
     *
     * @param     mixed $questionTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function filterByQuestionTypeId($questionTypeId = null, $comparison = null)
    {
        if (is_array($questionTypeId)) {
            $useMinMax = false;
            if (isset($questionTypeId['min'])) {
                $this->addUsingAlias(QuestionPeer::QUESTION_TYPE_ID, $questionTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($questionTypeId['max'])) {
                $this->addUsingAlias(QuestionPeer::QUESTION_TYPE_ID, $questionTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionPeer::QUESTION_TYPE_ID, $questionTypeId, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the help column
     *
     * Example usage:
     * <code>
     * $query->filterByHelp('fooValue');   // WHERE help = 'fooValue'
     * $query->filterByHelp('%fooValue%'); // WHERE help LIKE '%fooValue%'
     * </code>
     *
     * @param     string $help The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function filterByHelp($help = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($help)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $help)) {
                $help = str_replace('*', '%', $help);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionPeer::HELP, $help, $comparison);
    }

    /**
     * Filter the query on the group_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGroupId(1234); // WHERE group_id = 1234
     * $query->filterByGroupId(array(12, 34)); // WHERE group_id IN (12, 34)
     * $query->filterByGroupId(array('min' => 12)); // WHERE group_id >= 12
     * $query->filterByGroupId(array('max' => 12)); // WHERE group_id <= 12
     * </code>
     *
     * @see       filterByQuestionGroup()
     *
     * @param     mixed $groupId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function filterByGroupId($groupId = null, $comparison = null)
    {
        if (is_array($groupId)) {
            $useMinMax = false;
            if (isset($groupId['min'])) {
                $this->addUsingAlias(QuestionPeer::GROUP_ID, $groupId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($groupId['max'])) {
                $this->addUsingAlias(QuestionPeer::GROUP_ID, $groupId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionPeer::GROUP_ID, $groupId, $comparison);
    }

    /**
     * Filter the query on the sortable_rank column
     *
     * Example usage:
     * <code>
     * $query->filterBySortableRank(1234); // WHERE sortable_rank = 1234
     * $query->filterBySortableRank(array(12, 34)); // WHERE sortable_rank IN (12, 34)
     * $query->filterBySortableRank(array('min' => 12)); // WHERE sortable_rank >= 12
     * $query->filterBySortableRank(array('max' => 12)); // WHERE sortable_rank <= 12
     * </code>
     *
     * @param     mixed $sortableRank The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function filterBySortableRank($sortableRank = null, $comparison = null)
    {
        if (is_array($sortableRank)) {
            $useMinMax = false;
            if (isset($sortableRank['min'])) {
                $this->addUsingAlias(QuestionPeer::SORTABLE_RANK, $sortableRank['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sortableRank['max'])) {
                $this->addUsingAlias(QuestionPeer::SORTABLE_RANK, $sortableRank['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionPeer::SORTABLE_RANK, $sortableRank, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at < '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(QuestionPeer::CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(QuestionPeer::CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionPeer::CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at < '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(QuestionPeer::UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(QuestionPeer::UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionPeer::UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related QuestionType object
     *
     * @param   QuestionType|PropelObjectCollection $questionType The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 QuestionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByQuestionType($questionType, $comparison = null)
    {
        if ($questionType instanceof QuestionType) {
            return $this
                ->addUsingAlias(QuestionPeer::QUESTION_TYPE_ID, $questionType->getId(), $comparison);
        } elseif ($questionType instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(QuestionPeer::QUESTION_TYPE_ID, $questionType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByQuestionType() only accepts arguments of type QuestionType or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the QuestionType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function joinQuestionType($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('QuestionType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'QuestionType');
        }

        return $this;
    }

    /**
     * Use the QuestionType relation QuestionType object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeQuery A secondary query class using the current class as primary query
     */
    public function useQuestionTypeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinQuestionType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'QuestionType', '\Axtion\Bundle\QuestionnaireBundle\Propel\QuestionTypeQuery');
    }

    /**
     * Filter the query by a related Questionnaire object
     *
     * @param   Questionnaire|PropelObjectCollection $questionnaire The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 QuestionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByQuestionnaire($questionnaire, $comparison = null)
    {
        if ($questionnaire instanceof Questionnaire) {
            return $this
                ->addUsingAlias(QuestionPeer::QUESTIONNAIRE_ID, $questionnaire->getId(), $comparison);
        } elseif ($questionnaire instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(QuestionPeer::QUESTIONNAIRE_ID, $questionnaire->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByQuestionnaire() only accepts arguments of type Questionnaire or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Questionnaire relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function joinQuestionnaire($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Questionnaire');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Questionnaire');
        }

        return $this;
    }

    /**
     * Use the Questionnaire relation Questionnaire object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\QuestionnaireQuery A secondary query class using the current class as primary query
     */
    public function useQuestionnaireQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinQuestionnaire($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Questionnaire', '\Axtion\Bundle\QuestionnaireBundle\Propel\QuestionnaireQuery');
    }

    /**
     * Filter the query by a related Question object
     *
     * @param   Question|PropelObjectCollection $question The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 QuestionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByParent($question, $comparison = null)
    {
        if ($question instanceof Question) {
            return $this
                ->addUsingAlias(QuestionPeer::PARENT_ID, $question->getId(), $comparison);
        } elseif ($question instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(QuestionPeer::PARENT_ID, $question->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByParent() only accepts arguments of type Question or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Parent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function joinParent($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Parent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Parent');
        }

        return $this;
    }

    /**
     * Use the Parent relation Question object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery A secondary query class using the current class as primary query
     */
    public function useParentQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinParent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Parent', '\Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery');
    }

    /**
     * Filter the query by a related QuestionGroup object
     *
     * @param   QuestionGroup|PropelObjectCollection $questionGroup The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 QuestionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByQuestionGroup($questionGroup, $comparison = null)
    {
        if ($questionGroup instanceof QuestionGroup) {
            return $this
                ->addUsingAlias(QuestionPeer::GROUP_ID, $questionGroup->getId(), $comparison);
        } elseif ($questionGroup instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(QuestionPeer::GROUP_ID, $questionGroup->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByQuestionGroup() only accepts arguments of type QuestionGroup or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the QuestionGroup relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function joinQuestionGroup($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('QuestionGroup');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'QuestionGroup');
        }

        return $this;
    }

    /**
     * Use the QuestionGroup relation QuestionGroup object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\QuestionGroupQuery A secondary query class using the current class as primary query
     */
    public function useQuestionGroupQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinQuestionGroup($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'QuestionGroup', '\Axtion\Bundle\QuestionnaireBundle\Propel\QuestionGroupQuery');
    }

    /**
     * Filter the query by a related Response object
     *
     * @param   Response|PropelObjectCollection $response  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 QuestionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByResponse($response, $comparison = null)
    {
        if ($response instanceof Response) {
            return $this
                ->addUsingAlias(QuestionPeer::ID, $response->getQuestionId(), $comparison);
        } elseif ($response instanceof PropelObjectCollection) {
            return $this
                ->useResponseQuery()
                ->filterByPrimaryKeys($response->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByResponse() only accepts arguments of type Response or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Response relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function joinResponse($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Response');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Response');
        }

        return $this;
    }

    /**
     * Use the Response relation Response object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\AssessmentBundle\Propel\ResponseQuery A secondary query class using the current class as primary query
     */
    public function useResponseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinResponse($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Response', '\Axtion\Bundle\AssessmentBundle\Propel\ResponseQuery');
    }

    /**
     * Filter the query by a related Question object
     *
     * @param   Question|PropelObjectCollection $question  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 QuestionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySubQuestion($question, $comparison = null)
    {
        if ($question instanceof Question) {
            return $this
                ->addUsingAlias(QuestionPeer::ID, $question->getParentId(), $comparison);
        } elseif ($question instanceof PropelObjectCollection) {
            return $this
                ->useSubQuestionQuery()
                ->filterByPrimaryKeys($question->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySubQuestion() only accepts arguments of type Question or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SubQuestion relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function joinSubQuestion($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SubQuestion');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SubQuestion');
        }

        return $this;
    }

    /**
     * Use the SubQuestion relation Question object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery A secondary query class using the current class as primary query
     */
    public function useSubQuestionQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSubQuestion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SubQuestion', '\Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery');
    }

    /**
     * Filter the query by a related SelectedOption object
     *
     * @param   SelectedOption|PropelObjectCollection $selectedOption  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 QuestionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySelectedOption($selectedOption, $comparison = null)
    {
        if ($selectedOption instanceof SelectedOption) {
            return $this
                ->addUsingAlias(QuestionPeer::ID, $selectedOption->getQuestionId(), $comparison);
        } elseif ($selectedOption instanceof PropelObjectCollection) {
            return $this
                ->useSelectedOptionQuery()
                ->filterByPrimaryKeys($selectedOption->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelectedOption() only accepts arguments of type SelectedOption or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelectedOption relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function joinSelectedOption($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelectedOption');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelectedOption');
        }

        return $this;
    }

    /**
     * Use the SelectedOption relation SelectedOption object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\SelectedOptionQuery A secondary query class using the current class as primary query
     */
    public function useSelectedOptionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelectedOption($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelectedOption', '\Axtion\Bundle\QuestionnaireBundle\Propel\SelectedOptionQuery');
    }

    /**
     * Filter the query by a related Dependency object
     *
     * @param   Dependency|PropelObjectCollection $dependency  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 QuestionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByDependency($dependency, $comparison = null)
    {
        if ($dependency instanceof Dependency) {
            return $this
                ->addUsingAlias(QuestionPeer::ID, $dependency->getQuestionId(), $comparison);
        } elseif ($dependency instanceof PropelObjectCollection) {
            return $this
                ->useDependencyQuery()
                ->filterByPrimaryKeys($dependency->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDependency() only accepts arguments of type Dependency or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Dependency relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function joinDependency($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Dependency');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Dependency');
        }

        return $this;
    }

    /**
     * Use the Dependency relation Dependency object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuery A secondary query class using the current class as primary query
     */
    public function useDependencyQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDependency($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Dependency', '\Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuery');
    }

    /**
     * Filter the query by a related DependencyQuestion object
     *
     * @param   DependencyQuestion|PropelObjectCollection $dependencyQuestion  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 QuestionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByDependencyQuestion($dependencyQuestion, $comparison = null)
    {
        if ($dependencyQuestion instanceof DependencyQuestion) {
            return $this
                ->addUsingAlias(QuestionPeer::ID, $dependencyQuestion->getQuestionId(), $comparison);
        } elseif ($dependencyQuestion instanceof PropelObjectCollection) {
            return $this
                ->useDependencyQuestionQuery()
                ->filterByPrimaryKeys($dependencyQuestion->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDependencyQuestion() only accepts arguments of type DependencyQuestion or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DependencyQuestion relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function joinDependencyQuestion($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DependencyQuestion');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DependencyQuestion');
        }

        return $this;
    }

    /**
     * Use the DependencyQuestion relation DependencyQuestion object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuestionQuery A secondary query class using the current class as primary query
     */
    public function useDependencyQuestionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDependencyQuestion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DependencyQuestion', '\Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuestionQuery');
    }

    /**
     * Filter the query by a related FieldOption object
     * using the questionnaire_selected_option table as cross reference
     *
     * @param   FieldOption $fieldOption the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   QuestionQuery The current query, for fluid interface
     */
    public function filterByFieldOption($fieldOption, $comparison = Criteria::EQUAL)
    {
        return $this
            ->useSelectedOptionQuery()
            ->filterByFieldOption($fieldOption, $comparison)
            ->endUse();
    }

    /**
     * Filter the query by a related Dependency object
     * using the questionnaire_dependency_question table as cross reference
     *
     * @param   Dependency $dependency the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   QuestionQuery The current query, for fluid interface
     */
    public function filterByDemandingDependency($dependency, $comparison = Criteria::EQUAL)
    {
        return $this
            ->useDependencyQuestionQuery()
            ->filterByDemandingDependency($dependency, $comparison)
            ->endUse();
    }

    /**
     * Exclude object from result
     *
     * @param   Question $question Object to remove from the list of results
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function prune($question = null)
    {
        if ($question) {
            $this->addUsingAlias(QuestionPeer::ID, $question->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    // sortable behavior

    /**
     * Returns the objects in a certain list, from the list scope
     *
     * @param int $scope Scope to determine which objects node to return
     *
     * @return QuestionQuery The current query, for fluid interface
     */
    public function inList($scope = null)
    {

        QuestionPeer::sortableApplyScopeCriteria($this, $scope, 'addUsingAlias');

        return $this;
    }

    /**
     * Filter the query based on a rank in the list
     *
     * @param     integer   $rank rank
     * @param int $scope Scope to determine which objects node to return

     *
     * @return    QuestionQuery The current query, for fluid interface
     */
    public function filterByRank($rank, $scope = null)
    {


        return $this
            ->inList($scope)
            ->addUsingAlias(QuestionPeer::RANK_COL, $rank, Criteria::EQUAL);
    }

    /**
     * Order the query based on the rank in the list.
     * Using the default $order, returns the item with the lowest rank first
     *
     * @param     string $order either Criteria::ASC (default) or Criteria::DESC
     *
     * @return    QuestionQuery The current query, for fluid interface
     */
    public function orderByRank($order = Criteria::ASC)
    {
        $order = strtoupper($order);
        switch ($order) {
            case Criteria::ASC:
                return $this->addAscendingOrderByColumn($this->getAliasedColName(QuestionPeer::RANK_COL));
                break;
            case Criteria::DESC:
                return $this->addDescendingOrderByColumn($this->getAliasedColName(QuestionPeer::RANK_COL));
                break;
            default:
                throw new PropelException('QuestionQuery::orderBy() only accepts "asc" or "desc" as argument');
        }
    }

    /**
     * Get an item from the list based on its rank
     *
     * @param     integer   $rank rank
     * @param int $scope Scope to determine which objects node to return
     * @param     PropelPDO $con optional connection
     *
     * @return    Question
     */
    public function findOneByRank($rank, $scope = null, PropelPDO $con = null)
    {

        return $this
            ->filterByRank($rank, $scope)
            ->findOne($con);
    }

    /**
     * Returns a list of objects
     *
     * @param int $scope Scope to determine which objects node to return

     * @param      PropelPDO $con	Connection to use.
     *
     * @return     mixed the list of results, formatted by the current formatter
     */
    public function findList($scope = null, $con = null)
    {


        return $this
            ->inList($scope)
            ->orderByRank()
            ->find($con);
    }

    /**
     * Get the highest rank
     *
     * @param int $scope Scope to determine which objects node to return

     * @param     PropelPDO optional connection
     *
     * @return    integer highest position
     */
    public function getMaxRank($scope = null, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(QuestionPeer::DATABASE_NAME);
        }
        // shift the objects with a position lower than the one of object
        $this->addSelectColumn('MAX(' . QuestionPeer::RANK_COL . ')');

        QuestionPeer::sortableApplyScopeCriteria($this, $scope);
        $stmt = $this->doSelect($con);

        return $stmt->fetchColumn();
    }

    /**
     * Get the highest rank by a scope with a array format.
     *
     * @param     int $scope		The scope value as scalar type or array($value1, ...).

     * @param     PropelPDO optional connection
     *
     * @return    integer highest position
     */
    public function getMaxRankArray($scope, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(QuestionPeer::DATABASE_NAME);
        }
        // shift the objects with a position lower than the one of object
        $this->addSelectColumn('MAX(' . QuestionPeer::RANK_COL . ')');
        QuestionPeer::sortableApplyScopeCriteria($this, $scope);
        $stmt = $this->doSelect($con);

        return $stmt->fetchColumn();
    }

    /**
     * Reorder a set of sortable objects based on a list of id/position
     * Beware that there is no check made on the positions passed
     * So incoherent positions will result in an incoherent list
     *
     * @param     array     $order id => rank pairs
     * @param     PropelPDO $con   optional connection
     *
     * @return    boolean true if the reordering took place, false if a database problem prevented it
     */
    public function reorder(array $order, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(QuestionPeer::DATABASE_NAME);
        }

        $con->beginTransaction();
        try {
            $ids = array_keys($order);
            $objects = $this->findPks($ids, $con);
            foreach ($objects as $object) {
                $pk = $object->getPrimaryKey();
                if ($object->getSortableRank() != $order[$pk]) {
                    $object->setSortableRank($order[$pk]);
                    $object->save($con);
                }
            }
            $con->commit();

            return true;
        } catch (Exception $e) {
            $con->rollback();
            throw $e;
        }
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     QuestionQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(QuestionPeer::UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     QuestionQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(QuestionPeer::UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     QuestionQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(QuestionPeer::UPDATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     QuestionQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(QuestionPeer::CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date desc
     *
     * @return     QuestionQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(QuestionPeer::CREATED_AT);
    }

    /**
     * Order by create date asc
     *
     * @return     QuestionQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(QuestionPeer::CREATED_AT);
    }
}

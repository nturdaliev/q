<?php

namespace Axtion\Bundle\QuestionnaireBundle\Propel;

use Axtion\Bundle\QuestionnaireBundle\Propel\om\BaseQuestionnaire;
use Axtion\Bundle\UserBundle\Propel\User;
use PropelPDO;

/**
 * Class Questionnaire
 * @package Axtion\Bundle\QuestionnaireBundle\Propel
 */
class Questionnaire extends BaseQuestionnaire
{
    /**
     * @param User $user
     * @return bool
     */
    public function ofUser(User $user)
    {
        return $this->getUser() === $user;
    }

    /**
     * Get main questions
     *
     * @return Question[] Questions
     */
    public function getMainQuestions()
    {
        return $this->getQuestions(QuestionQuery::create()->filterByParentId(null, \Criteria::ISNULL));
    }

    /**
     * Code to be run before deleting the object in database
     *
     * @param PropelPDO $con
     *
     * @return boolean
     */
    public function preDelete(PropelPDO $con = null)
    {
        $this->getQuestions()->delete();

        return parent::preDelete($con);
    }
}
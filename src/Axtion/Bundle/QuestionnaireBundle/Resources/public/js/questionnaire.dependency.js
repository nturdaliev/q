/**
 * Created by tunu on 9/4/2015.
 */
/*jslint browser: true*/
/*global $, jQuery, alert,Translator, console,confirm*/
jQuery(function () {
    "use strict";
    (function ($) {
        $.widget('axtion.dependency', {
            fresh: true,
            _init: function () {
                this.questionGroup = this.element.find('select[name="dependency_form[questionGroup]"]');
                this.question = this.element.find('input[name="dependency_form[question]"]');
                this._value = this.element.find('input[name="dependency_form[value]"]');
                this.questions = this.element.find('input[name="dependency_form[dependentQuestions][]"]');
                this.spinner = this.element.find('#spinner');
            },
            _addEventListenerTo: function (object) {
                var self = this;
                if (object.length > 0) {
                    $(object).on('change', self.element, function () {
                        if (self.fresh) {
                            self._ajaxSubmit(this);
                        } else {
                            if (window.confirm("Your changes will be discarded!")) {
                                self._ajaxSubmit(this);
                            } else {
                                $(this).val($.data(window, $(this).attr('name')));
                            }
                        }
                    });
                    $.data(window, $(object).attr('name'), $(object).val());
                }
            },
            _preSubmit: function (object) {
                switch ($(object).attr('name')) {
                case this.questionGroup.attr('name'):
                    this._removeElement(this.question);
                    this._removeElement(this._value);
                    this._removeElement(this.questions);
                    break;
                case this.question.attr('name'):
                    this._removeElement(this._value);
                    this._removeElement(this.questions);
                    break;
                case this._value.attr('name'):
                    this._removeElement(this.questions);
                    break;
                default:
                    break;
                }
            },
            _removeElement: function (object) {
                if (object instanceof jQuery && object.attr('name')) {
                    object.attr('name', '');
                }
            },
            _ajaxSubmit: function (object) {
                var self = this, questionnaireId = this.element.data('id');
                this._preSubmit(object);
                $.ajax({
                    data: this.element.serialize(),
                    method: "POST",
                    url: Routing.generate("questionnaire_dependency_form", {id: questionnaireId}),
                    beforeSend: function () {
                        self.spinner.removeClass('sr-only');
                    }
                }).done(function (data) {
                    $('.dependency-form').html(data);
                    $('form[name="dependency_form"]').dependency();
                });

            },
            _addEventListeners: function () {
                var self = this;
                self._addEventListenerTo(self.questionGroup);
                self._addEventListenerTo(self.question);
                self._addEventListenerTo(self._value);
                self.questions.on('change', self.element, function () {
                    self.fresh = false;
                });
            },
            _create: function () {
                this._init();
                this._addEventListeners();
            }
        });
        //Plugin Inline Call
        $('form[name="dependency_form"]').dependency();
    }(jQuery));
});

/**
 * Created by tunu on 9/14/2015.
 */
/*global jQuery,console */
jQuery(function () {
    "use strict";
    (function ($) {
        $('.sortable').sortable({
            helper: function (event, ui) {
                ui.children().each(function () {
                    $(this).width($(this).width());
                });
                return ui;
            },
            stop: function () {
                var self = this,
                    ranks = $(self).find('.sortable_rank'),
                    data = [];
                $.each(ranks, function (index) {
                    $(this).html(index + 1);
                    data.push({
                        id: $(this).attr('data-id'),
                        sortable_rank: index + 1
                    });
                });
                $.ajax({
                    url: Routing.generate('questionnaire_questions_update_sortable_ranks'),
                    method: 'POST',
                    data: {'questions': data}
                }).done(function (response) {
                    console.log(response);
                });

            }
        });
    }(jQuery));
});
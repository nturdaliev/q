/**
 * Created by tunu on 10/8/2015.
 */
/*global jQuery,console */
jQuery(function () {
    "use strict";
    (function ($) {
        $('#group-sortable').sortable({
            helper: function (event, ui) {
                ui.children().each(function () {
                    $(this).width($(this).width());
                });
                return ui;
            },
            stop: function () {
                this.getData = function () {
                    var id, self = this, data = [];
                    $(self).find('>li.ui-sortable-handle').each(function (index, element) {
                        id = $(element).attr('data-id');
                        data.push({'id': id, rank: index + 1});
                    });
                    return data;
                };

                $.ajax({
                    url: Routing.generate('questionnaire_question_group_sortable'),
                    method: 'POST',
                    data: {ranks: this.getData()}
                });

            }
        });
    }(jQuery));
});
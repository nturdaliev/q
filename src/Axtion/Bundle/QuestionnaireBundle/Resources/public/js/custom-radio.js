/**
 * Created by tunu on 1/28/2016.
 */
/*global jQuery,console */
jQuery(function () {
    "use strict";
    (function ($) {
        $.widget('questionnaire.customRadio', {
            _create: function () {
                $(':radio').each(function () {
                    $(this)
                        .addClass('custom-radio')
                        .wrap('<label class="radio"></label>')
                        .radiocheck();
                });
            }
        });
    }(jQuery));
    jQuery('.goodbye-message, .welcome-message').customRadio();
});
/**
 * Created by tunu on 8/11/2015.
 */

/*jshint strict: true */
/*jslint browser: true*/
/*global $, jQuery, alert, Translator, console*/
jQuery(function () {
    "use strict";
    $.widget('axtion.manageClients', {
        options: {
            questionnaire_id: null
        },
        _create: function () {
            this.options.questionnaire_id = this.element.data('questionnaire-id');
            this._configureFilter();
        },
        _configureFilter: function () {
            var self = this;
            self.element.find('#client-filter').on('click', document, function (event) {
                event.preventDefault();
                $.ajax({
                    data: $(this).closest('form').serialize(),
                    method: 'POST',
                    url: Routing.generate("questionnaire_questionnaire_filter_clients", {id: self.options.questionnaire_id})
                }).done(function (data) {
                    $('#clients').html(data);
                    return $('#filterModal') ? $('#filterModal').modal('hide') : true;
                });
            });
        }
    });
    $('#manageClients').manageClients();
});
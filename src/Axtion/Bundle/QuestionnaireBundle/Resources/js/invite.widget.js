/**
 * Created by tunu on 4/20/2016.
 */
/*jshint strict: true */
/*jslint browser: true*/
/*global $, jQuery, alert, Translator, console*/

(function ($) {
    "use strict";
    $.widget('axtion.invite', {
        participants: [],
        _create: function () {
            this.configCheckboxes();
            this.configSubmit();
            this.configResend();
            this.submitBtn = this.element.find('input[type="submit"]')[0];
            this.submitBtn.disabled = true;
        },
        configCheckboxes: function () {
            var self = this;
            $('.participating').on('change', document, function () {
                self.toggle(this.value, this.checked);
                self.toggleSubmitBtn();
            });
        },
        toggleSubmitBtn: function (resendChecked) {
            var checkboxes = $('input[name*=participating_]:checked');
            this.submitBtn.disabled = !(this.participants.length > 0 || (resendChecked && checkboxes && checkboxes.length > 0));
        },
        configResend: function () {
            var self = this;
            self.element.find('#questionnaire_invite_resend').on('change', function () {
                self.toggleSubmitBtn(this.checked);
            });
        },
        configSubmit: function () {
            var self = this, clients;
            this.element.submit(function () {
                if (self.participants.length > 0) {
                    clients = $('#questionnaire_invite_clients');
                    $.each(self.participants, function (index) {
                        clients.append('<option value="' + self.participants[index] + '" selected>' + self.participants[index] + '</option>');
                    });
                }
                return true;
            });
        },
        toggle: function (value, checked) {
            var index;
            if (checked) {
                this.participants.push(value);
            } else {
                index = this.participants.indexOf(value);
                if (index > -1) {
                    this.participants.splice(index, 1);
                }
            }
        }
    });
}(jQuery));
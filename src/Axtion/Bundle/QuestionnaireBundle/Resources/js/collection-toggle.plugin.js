/**
 * Created by tunu on 7/14/2015.
 */
/*jslint browser: true*/
/*global $, jQuery, alert,Translator, console*/
(function ($) {
    "use strict";
    $.widget('axtion.collectionToggle', {
        options: {
            title: Translator.trans('field_option.new_choice')
        },
        _create: function () {
            var self = this;
            self.element.append(this._createAddButton());
            self.element.data('index', $(self.element).find(':input').length);
            $(self.element).on('click', '.add_link', function (e) {
                e.preventDefault();
                self._addForm(self.element);
            });
            self.element.children('.form-group').each(function () {
                self._createRemoveLink($(this));
            });
        },
        _addForm: function (self) {
            var prototype = self.data('prototype'),
                index = self.data('index'),
                newForm = $(prototype.replace(/__name__/g, index));
            self.data('index', index + 1);
            $(this.element).find('.link_container').before(newForm);
            this._createRemoveLink(newForm);
            $(this).configureQuestion().configureQuestion("load", "registerObserver");
        },
        _createRemoveLink: function (self) {
            var remove = $('<a href="#" class="text-danger"><span class="fa fa-lg fui-cross"></span></a><br/>');
            remove.on('click', function (e) {
                e.preventDefault();
                self.remove();
                $('form[name="question_form"]').configureQuestion().configureQuestion("load", "preview");
            });
            return self.append(remove);
        },
        _createAddButton: function () {
            var container = $('<div/>', {
                    'class': 'link_container'
                }),
                addBtn = $('<a/>', {
                    href: '#',
                    'class': 'add_link btn btn-primary fa fa-plus'
                }).text(this.options.title);
            return container.append(addBtn);
        }
    });
}(jQuery));
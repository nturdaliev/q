/*jshint strict: true */
/*jslint browser: true*/
/*global $, jQuery, alert,confirm, window*/
jQuery(function ($) {
    "use strict";
    $(".link-delete").on('click', function (event) {
        if (!confirm(($(this).data("message")))) {
            event.preventDefault();
        }
    });
    $('[data-toggle="tooltip"]').tooltip();
});
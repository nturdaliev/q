/**
 * Created by tunu on 7/28/2015.
 */
/*global $, jQuery, alert,Translator, console*/
(function ($) {
    "use strict";
    $.widget('axtion.gridChoice', {
        options: {},
        _create: function () {
            this._hover();
        },
        _hover: function () {
            var self = this;
            self.element.find('.choice').hover(function () {
                $(this).addClass('btn-primary');
            }, function () {
                $(this).data('selected') != "1" && $(this).removeClass('btn-primary');
            });
            self.element.find('.choice').on('click', function () {
                self._removeSelected();
                $(this).addClass('btn-primary').data('selected', '1');
                $(self.options.hiddenId).val($(this).data('id')).change();
            });
        },
        _removeSelected: function () {
            this.element.find('.choice').each(function () {
                $(this).data('selected', null).removeClass('btn-primary');
            });
        }
    });

}(jQuery));
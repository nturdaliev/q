/**
 * Created by tunu on 7/16/2015.
 */
/*jshint strict: true */
/*jslint browser: true*/
/*global $, jQuery, alert, Translator, console,CKEDITOR*/
(function ($) {
    "use strict";
    $.widget('axtion.configureQuestion', {
        _create: function () {
            this._initialize();
            this._registerValidator();
        },
        _initialize: function () {
            var self = this;
            self.element.find('#question_form_questionType').on('change', function () {
                self._loadTemplate();
            });
        },
        _loadTemplate: function () {
            var self = this;
            $.ajax({
                url: Routing.generate("questionnaire_question_template", {id: self._getId()}),
                data: self._getSerializedData(),
                method: 'POST',
                success: function (data) {
                    $('#question').html(data.template);
                    self._registerObserver();
                    self._loadPreviewTemplate();
                    self._initCollectionToggle();
                }
            });
        },
        load: function (type) {
            switch (type) {
            case "preview":
                this._loadPreviewTemplate();
                break;
            case "registerObserver":
                this._registerObserver();
                break;
            case "initCollectionToggle":
                this._initCollectionToggle();
                break;
            default:
                break;
            }
        },
        _loadPreviewTemplate: function () {
            var self = this;
            if (self._validateFormFields()) {
                $('.question-alert').addClass('sr-only');
                $.ajax({
                    url: Routing.generate("questionnaire_question_preview", {id: this._getId()}),
                    data: this._getSerializedData(),
                    method: 'POST',
                    beforeSend: function () {
                        $('#preview').html('<i class="fa fa-spinner fa-pulse fa-3x icon-center"></i>');
                    },
                    success: function (data) {
                        $('#preview').html(data.preview);
                        self._initCollectionToggle();
                        self._loadPopover();
                        $('.question-preview input').attr('required', false);
                    }
                });
            } else {
                $('.question-alert').removeClass('sr-only');
            }
        },
        _getSerializedData: function () {
            this._updateCKEditor();
            return $('form[name="question_form"]').serialize();
        },
        _getId: function getId() {
            var id = $('#question_form_id').val();
            if (id === undefined) {
                id = 0;
            }
            return id;
        },
        _initCollectionToggle: function () {
            $('.choices').collectionToggle();
            $('.sub_questions').collectionToggle({
                title: Translator.trans('question.new_subquestion')
            });
        },
        _registerObserver: function () {
            var self = this;
            $('#question').find('form').observe(function () {
                self._loadPreviewTemplate();
            });
        },
        _removePreviewFormNames: function () {
            this.element.find('[name*="question_preview_form"]').removeAttr('name');
        },
        _validateFormFields: function () {
            var result = true;
            $('form input[type="text"][id*="question_form_option"]').each(function () {
                if ($(this).val().trim().length < 1) {
                    result = false;
                }
            });
            return result;
        },
        _updateCKEditor: function () {
            var instance;
            for (instance in CKEDITOR.instances) {
                if (CKEDITOR.instances.hasOwnProperty(instance)) {
                    CKEDITOR.instances[instance].updateElement();
                }
            }
        },
        _registerValidator: function () {
            var self = this;
            $('form[name="question_form"]').on("submit", function (e) {
                if (!isNameValid() || !isQuestionTypeSelected()) {
                    e.preventDefault();
                    $('.question-alert').removeClass('sr-only');
                }
                self._removePreviewFormNames();
            });

            function isNameValid() {
                var data = CKEDITOR.instances && CKEDITOR.instances.question_form_name && CKEDITOR.instances.question_form_name.getData();
                data = data.replace(/(<([^>]+)>)|(&nbsp;)+|(\n)|( )+/ig, "");
                return !!data.length;
            }

            function isQuestionTypeSelected() {
                return !!$('#question_form_questionType').val();
            }
        },
        _loadPopover: function () {
            $('[data-toggle="popover"]').popover();
        }
    });
}(jQuery));
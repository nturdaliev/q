<?php

namespace Axtion\Bundle\QuestionnaireBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class FormPass
 * @package Axtion\Bundle\QuestionnaireBundle\DependencyInjection\Compiler
 */
class FormPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->hasParameter('twig.form.resources')) {
            $parameters = $container->getParameter('twig.form.resources');
            $parameters[] = 'AxtionQuestionnaireBundle:Form:fields.html.twig';
            $container->setParameter('twig.form.resources', $parameters);
        }

    }
}

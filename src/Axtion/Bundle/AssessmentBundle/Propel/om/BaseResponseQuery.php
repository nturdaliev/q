<?php

namespace Axtion\Bundle\AssessmentBundle\Propel\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Axtion\Bundle\AssessmentBundle\Propel\Assessment;
use Axtion\Bundle\AssessmentBundle\Propel\Response;
use Axtion\Bundle\AssessmentBundle\Propel\ResponseGroup;
use Axtion\Bundle\AssessmentBundle\Propel\ResponsePeer;
use Axtion\Bundle\AssessmentBundle\Propel\ResponseQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Question;

/**
 * @method ResponseQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ResponseQuery orderByQuestionId($order = Criteria::ASC) Order by the question_id column
 * @method ResponseQuery orderByAssessmentId($order = Criteria::ASC) Order by the assessment_id column
 * @method ResponseQuery orderByValue($order = Criteria::ASC) Order by the value column
 * @method ResponseQuery orderByParentId($order = Criteria::ASC) Order by the parent_id column
 * @method ResponseQuery orderByResponseGroupId($order = Criteria::ASC) Order by the response_group_id column
 *
 * @method ResponseQuery groupById() Group by the id column
 * @method ResponseQuery groupByQuestionId() Group by the question_id column
 * @method ResponseQuery groupByAssessmentId() Group by the assessment_id column
 * @method ResponseQuery groupByValue() Group by the value column
 * @method ResponseQuery groupByParentId() Group by the parent_id column
 * @method ResponseQuery groupByResponseGroupId() Group by the response_group_id column
 *
 * @method ResponseQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ResponseQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ResponseQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ResponseQuery leftJoinQuestion($relationAlias = null) Adds a LEFT JOIN clause to the query using the Question relation
 * @method ResponseQuery rightJoinQuestion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Question relation
 * @method ResponseQuery innerJoinQuestion($relationAlias = null) Adds a INNER JOIN clause to the query using the Question relation
 *
 * @method ResponseQuery leftJoinAssessment($relationAlias = null) Adds a LEFT JOIN clause to the query using the Assessment relation
 * @method ResponseQuery rightJoinAssessment($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Assessment relation
 * @method ResponseQuery innerJoinAssessment($relationAlias = null) Adds a INNER JOIN clause to the query using the Assessment relation
 *
 * @method ResponseQuery leftJoinParent($relationAlias = null) Adds a LEFT JOIN clause to the query using the Parent relation
 * @method ResponseQuery rightJoinParent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Parent relation
 * @method ResponseQuery innerJoinParent($relationAlias = null) Adds a INNER JOIN clause to the query using the Parent relation
 *
 * @method ResponseQuery leftJoinResponseGroup($relationAlias = null) Adds a LEFT JOIN clause to the query using the ResponseGroup relation
 * @method ResponseQuery rightJoinResponseGroup($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ResponseGroup relation
 * @method ResponseQuery innerJoinResponseGroup($relationAlias = null) Adds a INNER JOIN clause to the query using the ResponseGroup relation
 *
 * @method ResponseQuery leftJoinSubResponse($relationAlias = null) Adds a LEFT JOIN clause to the query using the SubResponse relation
 * @method ResponseQuery rightJoinSubResponse($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SubResponse relation
 * @method ResponseQuery innerJoinSubResponse($relationAlias = null) Adds a INNER JOIN clause to the query using the SubResponse relation
 *
 * @method Response findOne(PropelPDO $con = null) Return the first Response matching the query
 * @method Response findOneOrCreate(PropelPDO $con = null) Return the first Response matching the query, or a new Response object populated from the query conditions when no match is found
 *
 * @method Response findOneByQuestionId(int $question_id) Return the first Response filtered by the question_id column
 * @method Response findOneByAssessmentId(int $assessment_id) Return the first Response filtered by the assessment_id column
 * @method Response findOneByValue(string $value) Return the first Response filtered by the value column
 * @method Response findOneByParentId(int $parent_id) Return the first Response filtered by the parent_id column
 * @method Response findOneByResponseGroupId(int $response_group_id) Return the first Response filtered by the response_group_id column
 *
 * @method array findById(int $id) Return Response objects filtered by the id column
 * @method array findByQuestionId(int $question_id) Return Response objects filtered by the question_id column
 * @method array findByAssessmentId(int $assessment_id) Return Response objects filtered by the assessment_id column
 * @method array findByValue(string $value) Return Response objects filtered by the value column
 * @method array findByParentId(int $parent_id) Return Response objects filtered by the parent_id column
 * @method array findByResponseGroupId(int $response_group_id) Return Response objects filtered by the response_group_id column
 */
abstract class BaseResponseQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseResponseQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'Axtion\\Bundle\\AssessmentBundle\\Propel\\Response';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ResponseQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ResponseQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ResponseQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ResponseQuery) {
            return $criteria;
        }
        $query = new ResponseQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Response|Response[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ResponsePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ResponsePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Response A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Response A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "id", "question_id", "assessment_id", "value", "parent_id", "response_group_id" FROM "assessment_response" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Response();
            $obj->hydrate($row);
            ResponsePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Response|Response[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Response[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ResponseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ResponsePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ResponseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ResponsePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResponseQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ResponsePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ResponsePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResponsePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the question_id column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestionId(1234); // WHERE question_id = 1234
     * $query->filterByQuestionId(array(12, 34)); // WHERE question_id IN (12, 34)
     * $query->filterByQuestionId(array('min' => 12)); // WHERE question_id >= 12
     * $query->filterByQuestionId(array('max' => 12)); // WHERE question_id <= 12
     * </code>
     *
     * @see       filterByQuestion()
     *
     * @param     mixed $questionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResponseQuery The current query, for fluid interface
     */
    public function filterByQuestionId($questionId = null, $comparison = null)
    {
        if (is_array($questionId)) {
            $useMinMax = false;
            if (isset($questionId['min'])) {
                $this->addUsingAlias(ResponsePeer::QUESTION_ID, $questionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($questionId['max'])) {
                $this->addUsingAlias(ResponsePeer::QUESTION_ID, $questionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResponsePeer::QUESTION_ID, $questionId, $comparison);
    }

    /**
     * Filter the query on the assessment_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAssessmentId(1234); // WHERE assessment_id = 1234
     * $query->filterByAssessmentId(array(12, 34)); // WHERE assessment_id IN (12, 34)
     * $query->filterByAssessmentId(array('min' => 12)); // WHERE assessment_id >= 12
     * $query->filterByAssessmentId(array('max' => 12)); // WHERE assessment_id <= 12
     * </code>
     *
     * @see       filterByAssessment()
     *
     * @param     mixed $assessmentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResponseQuery The current query, for fluid interface
     */
    public function filterByAssessmentId($assessmentId = null, $comparison = null)
    {
        if (is_array($assessmentId)) {
            $useMinMax = false;
            if (isset($assessmentId['min'])) {
                $this->addUsingAlias(ResponsePeer::ASSESSMENT_ID, $assessmentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($assessmentId['max'])) {
                $this->addUsingAlias(ResponsePeer::ASSESSMENT_ID, $assessmentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResponsePeer::ASSESSMENT_ID, $assessmentId, $comparison);
    }

    /**
     * Filter the query on the value column
     *
     * Example usage:
     * <code>
     * $query->filterByValue('fooValue');   // WHERE value = 'fooValue'
     * $query->filterByValue('%fooValue%'); // WHERE value LIKE '%fooValue%'
     * </code>
     *
     * @param     string $value The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResponseQuery The current query, for fluid interface
     */
    public function filterByValue($value = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($value)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $value)) {
                $value = str_replace('*', '%', $value);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ResponsePeer::VALUE, $value, $comparison);
    }

    /**
     * Filter the query on the parent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByParentId(1234); // WHERE parent_id = 1234
     * $query->filterByParentId(array(12, 34)); // WHERE parent_id IN (12, 34)
     * $query->filterByParentId(array('min' => 12)); // WHERE parent_id >= 12
     * $query->filterByParentId(array('max' => 12)); // WHERE parent_id <= 12
     * </code>
     *
     * @see       filterByParent()
     *
     * @param     mixed $parentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResponseQuery The current query, for fluid interface
     */
    public function filterByParentId($parentId = null, $comparison = null)
    {
        if (is_array($parentId)) {
            $useMinMax = false;
            if (isset($parentId['min'])) {
                $this->addUsingAlias(ResponsePeer::PARENT_ID, $parentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($parentId['max'])) {
                $this->addUsingAlias(ResponsePeer::PARENT_ID, $parentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResponsePeer::PARENT_ID, $parentId, $comparison);
    }

    /**
     * Filter the query on the response_group_id column
     *
     * Example usage:
     * <code>
     * $query->filterByResponseGroupId(1234); // WHERE response_group_id = 1234
     * $query->filterByResponseGroupId(array(12, 34)); // WHERE response_group_id IN (12, 34)
     * $query->filterByResponseGroupId(array('min' => 12)); // WHERE response_group_id >= 12
     * $query->filterByResponseGroupId(array('max' => 12)); // WHERE response_group_id <= 12
     * </code>
     *
     * @see       filterByResponseGroup()
     *
     * @param     mixed $responseGroupId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ResponseQuery The current query, for fluid interface
     */
    public function filterByResponseGroupId($responseGroupId = null, $comparison = null)
    {
        if (is_array($responseGroupId)) {
            $useMinMax = false;
            if (isset($responseGroupId['min'])) {
                $this->addUsingAlias(ResponsePeer::RESPONSE_GROUP_ID, $responseGroupId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($responseGroupId['max'])) {
                $this->addUsingAlias(ResponsePeer::RESPONSE_GROUP_ID, $responseGroupId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResponsePeer::RESPONSE_GROUP_ID, $responseGroupId, $comparison);
    }

    /**
     * Filter the query by a related Question object
     *
     * @param   Question|PropelObjectCollection $question The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ResponseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByQuestion($question, $comparison = null)
    {
        if ($question instanceof Question) {
            return $this
                ->addUsingAlias(ResponsePeer::QUESTION_ID, $question->getId(), $comparison);
        } elseif ($question instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ResponsePeer::QUESTION_ID, $question->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByQuestion() only accepts arguments of type Question or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Question relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ResponseQuery The current query, for fluid interface
     */
    public function joinQuestion($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Question');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Question');
        }

        return $this;
    }

    /**
     * Use the Question relation Question object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery A secondary query class using the current class as primary query
     */
    public function useQuestionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinQuestion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Question', '\Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery');
    }

    /**
     * Filter the query by a related Assessment object
     *
     * @param   Assessment|PropelObjectCollection $assessment The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ResponseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAssessment($assessment, $comparison = null)
    {
        if ($assessment instanceof Assessment) {
            return $this
                ->addUsingAlias(ResponsePeer::ASSESSMENT_ID, $assessment->getId(), $comparison);
        } elseif ($assessment instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ResponsePeer::ASSESSMENT_ID, $assessment->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByAssessment() only accepts arguments of type Assessment or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Assessment relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ResponseQuery The current query, for fluid interface
     */
    public function joinAssessment($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Assessment');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Assessment');
        }

        return $this;
    }

    /**
     * Use the Assessment relation Assessment object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\AssessmentBundle\Propel\AssessmentQuery A secondary query class using the current class as primary query
     */
    public function useAssessmentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAssessment($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Assessment', '\Axtion\Bundle\AssessmentBundle\Propel\AssessmentQuery');
    }

    /**
     * Filter the query by a related Response object
     *
     * @param   Response|PropelObjectCollection $response The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ResponseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByParent($response, $comparison = null)
    {
        if ($response instanceof Response) {
            return $this
                ->addUsingAlias(ResponsePeer::PARENT_ID, $response->getId(), $comparison);
        } elseif ($response instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ResponsePeer::PARENT_ID, $response->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByParent() only accepts arguments of type Response or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Parent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ResponseQuery The current query, for fluid interface
     */
    public function joinParent($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Parent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Parent');
        }

        return $this;
    }

    /**
     * Use the Parent relation Response object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\AssessmentBundle\Propel\ResponseQuery A secondary query class using the current class as primary query
     */
    public function useParentQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinParent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Parent', '\Axtion\Bundle\AssessmentBundle\Propel\ResponseQuery');
    }

    /**
     * Filter the query by a related ResponseGroup object
     *
     * @param   ResponseGroup|PropelObjectCollection $responseGroup The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ResponseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByResponseGroup($responseGroup, $comparison = null)
    {
        if ($responseGroup instanceof ResponseGroup) {
            return $this
                ->addUsingAlias(ResponsePeer::RESPONSE_GROUP_ID, $responseGroup->getId(), $comparison);
        } elseif ($responseGroup instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ResponsePeer::RESPONSE_GROUP_ID, $responseGroup->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByResponseGroup() only accepts arguments of type ResponseGroup or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ResponseGroup relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ResponseQuery The current query, for fluid interface
     */
    public function joinResponseGroup($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ResponseGroup');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ResponseGroup');
        }

        return $this;
    }

    /**
     * Use the ResponseGroup relation ResponseGroup object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\AssessmentBundle\Propel\ResponseGroupQuery A secondary query class using the current class as primary query
     */
    public function useResponseGroupQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinResponseGroup($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ResponseGroup', '\Axtion\Bundle\AssessmentBundle\Propel\ResponseGroupQuery');
    }

    /**
     * Filter the query by a related Response object
     *
     * @param   Response|PropelObjectCollection $response  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ResponseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySubResponse($response, $comparison = null)
    {
        if ($response instanceof Response) {
            return $this
                ->addUsingAlias(ResponsePeer::ID, $response->getParentId(), $comparison);
        } elseif ($response instanceof PropelObjectCollection) {
            return $this
                ->useSubResponseQuery()
                ->filterByPrimaryKeys($response->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySubResponse() only accepts arguments of type Response or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SubResponse relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ResponseQuery The current query, for fluid interface
     */
    public function joinSubResponse($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SubResponse');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SubResponse');
        }

        return $this;
    }

    /**
     * Use the SubResponse relation Response object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Axtion\Bundle\AssessmentBundle\Propel\ResponseQuery A secondary query class using the current class as primary query
     */
    public function useSubResponseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSubResponse($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SubResponse', '\Axtion\Bundle\AssessmentBundle\Propel\ResponseQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Response $response Object to remove from the list of results
     *
     * @return ResponseQuery The current query, for fluid interface
     */
    public function prune($response = null)
    {
        if ($response) {
            $this->addUsingAlias(ResponsePeer::ID, $response->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

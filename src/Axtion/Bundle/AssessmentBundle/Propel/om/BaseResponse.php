<?php

namespace Axtion\Bundle\AssessmentBundle\Propel\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Axtion\Bundle\AssessmentBundle\Propel\Assessment;
use Axtion\Bundle\AssessmentBundle\Propel\AssessmentQuery;
use Axtion\Bundle\AssessmentBundle\Propel\Response;
use Axtion\Bundle\AssessmentBundle\Propel\ResponseGroup;
use Axtion\Bundle\AssessmentBundle\Propel\ResponseGroupQuery;
use Axtion\Bundle\AssessmentBundle\Propel\ResponsePeer;
use Axtion\Bundle\AssessmentBundle\Propel\ResponseQuery;
use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionQuery;

abstract class BaseResponse extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Axtion\\Bundle\\AssessmentBundle\\Propel\\ResponsePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        ResponsePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the question_id field.
     * @var        int
     */
    protected $question_id;

    /**
     * The value for the assessment_id field.
     * @var        int
     */
    protected $assessment_id;

    /**
     * The value for the value field.
     * @var        string
     */
    protected $value;

    /**
     * The value for the parent_id field.
     * @var        int
     */
    protected $parent_id;

    /**
     * The value for the response_group_id field.
     * @var        int
     */
    protected $response_group_id;

    /**
     * @var        Question
     */
    protected $aQuestion;

    /**
     * @var        Assessment
     */
    protected $aAssessment;

    /**
     * @var        Response
     */
    protected $aParent;

    /**
     * @var        ResponseGroup
     */
    protected $aResponseGroup;

    /**
     * @var        PropelObjectCollection|Response[] Collection to store aggregation of Response objects.
     */
    protected $collSubResponses;
    protected $collSubResponsesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $subResponsesScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [question_id] column value.
     *
     * @return int
     */
    public function getQuestionId()
    {

        return $this->question_id;
    }

    /**
     * Get the [assessment_id] column value.
     *
     * @return int
     */
    public function getAssessmentId()
    {

        return $this->assessment_id;
    }

    /**
     * Get the [value] column value.
     *
     * @return string
     */
    public function getValue()
    {

        return $this->value;
    }

    /**
     * Get the [parent_id] column value.
     *
     * @return int
     */
    public function getParentId()
    {

        return $this->parent_id;
    }

    /**
     * Get the [response_group_id] column value.
     *
     * @return int
     */
    public function getResponseGroupId()
    {

        return $this->response_group_id;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return Response The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = ResponsePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [question_id] column.
     *
     * @param  int $v new value
     * @return Response The current object (for fluent API support)
     */
    public function setQuestionId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->question_id !== $v) {
            $this->question_id = $v;
            $this->modifiedColumns[] = ResponsePeer::QUESTION_ID;
        }

        if ($this->aQuestion !== null && $this->aQuestion->getId() !== $v) {
            $this->aQuestion = null;
        }


        return $this;
    } // setQuestionId()

    /**
     * Set the value of [assessment_id] column.
     *
     * @param  int $v new value
     * @return Response The current object (for fluent API support)
     */
    public function setAssessmentId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->assessment_id !== $v) {
            $this->assessment_id = $v;
            $this->modifiedColumns[] = ResponsePeer::ASSESSMENT_ID;
        }

        if ($this->aAssessment !== null && $this->aAssessment->getId() !== $v) {
            $this->aAssessment = null;
        }


        return $this;
    } // setAssessmentId()

    /**
     * Set the value of [value] column.
     *
     * @param  string $v new value
     * @return Response The current object (for fluent API support)
     */
    public function setValue($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->value !== $v) {
            $this->value = $v;
            $this->modifiedColumns[] = ResponsePeer::VALUE;
        }


        return $this;
    } // setValue()

    /**
     * Set the value of [parent_id] column.
     *
     * @param  int $v new value
     * @return Response The current object (for fluent API support)
     */
    public function setParentId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->parent_id !== $v) {
            $this->parent_id = $v;
            $this->modifiedColumns[] = ResponsePeer::PARENT_ID;
        }

        if ($this->aParent !== null && $this->aParent->getId() !== $v) {
            $this->aParent = null;
        }


        return $this;
    } // setParentId()

    /**
     * Set the value of [response_group_id] column.
     *
     * @param  int $v new value
     * @return Response The current object (for fluent API support)
     */
    public function setResponseGroupId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->response_group_id !== $v) {
            $this->response_group_id = $v;
            $this->modifiedColumns[] = ResponsePeer::RESPONSE_GROUP_ID;
        }

        if ($this->aResponseGroup !== null && $this->aResponseGroup->getId() !== $v) {
            $this->aResponseGroup = null;
        }


        return $this;
    } // setResponseGroupId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->question_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->assessment_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->value = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->parent_id = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->response_group_id = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 6; // 6 = ResponsePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Response object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aQuestion !== null && $this->question_id !== $this->aQuestion->getId()) {
            $this->aQuestion = null;
        }
        if ($this->aAssessment !== null && $this->assessment_id !== $this->aAssessment->getId()) {
            $this->aAssessment = null;
        }
        if ($this->aParent !== null && $this->parent_id !== $this->aParent->getId()) {
            $this->aParent = null;
        }
        if ($this->aResponseGroup !== null && $this->response_group_id !== $this->aResponseGroup->getId()) {
            $this->aResponseGroup = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ResponsePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = ResponsePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aQuestion = null;
            $this->aAssessment = null;
            $this->aParent = null;
            $this->aResponseGroup = null;
            $this->collSubResponses = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ResponsePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = ResponseQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ResponsePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ResponsePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aQuestion !== null) {
                if ($this->aQuestion->isModified() || $this->aQuestion->isNew()) {
                    $affectedRows += $this->aQuestion->save($con);
                }
                $this->setQuestion($this->aQuestion);
            }

            if ($this->aAssessment !== null) {
                if ($this->aAssessment->isModified() || $this->aAssessment->isNew()) {
                    $affectedRows += $this->aAssessment->save($con);
                }
                $this->setAssessment($this->aAssessment);
            }

            if ($this->aParent !== null) {
                if ($this->aParent->isModified() || $this->aParent->isNew()) {
                    $affectedRows += $this->aParent->save($con);
                }
                $this->setParent($this->aParent);
            }

            if ($this->aResponseGroup !== null) {
                if ($this->aResponseGroup->isModified() || $this->aResponseGroup->isNew()) {
                    $affectedRows += $this->aResponseGroup->save($con);
                }
                $this->setResponseGroup($this->aResponseGroup);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->subResponsesScheduledForDeletion !== null) {
                if (!$this->subResponsesScheduledForDeletion->isEmpty()) {
                    foreach ($this->subResponsesScheduledForDeletion as $subResponse) {
                        // need to save related object because we set the relation to null
                        $subResponse->save($con);
                    }
                    $this->subResponsesScheduledForDeletion = null;
                }
            }

            if ($this->collSubResponses !== null) {
                foreach ($this->collSubResponses as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = ResponsePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ResponsePeer::ID . ')');
        }
        if (null === $this->id) {
            try {
                $stmt = $con->query("SELECT nextval('assessment_response_id_seq')");
                $row = $stmt->fetch(PDO::FETCH_NUM);
                $this->id = $row[0];
            } catch (Exception $e) {
                throw new PropelException('Unable to get sequence id.', $e);
            }
        }


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ResponsePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '"id"';
        }
        if ($this->isColumnModified(ResponsePeer::QUESTION_ID)) {
            $modifiedColumns[':p' . $index++]  = '"question_id"';
        }
        if ($this->isColumnModified(ResponsePeer::ASSESSMENT_ID)) {
            $modifiedColumns[':p' . $index++]  = '"assessment_id"';
        }
        if ($this->isColumnModified(ResponsePeer::VALUE)) {
            $modifiedColumns[':p' . $index++]  = '"value"';
        }
        if ($this->isColumnModified(ResponsePeer::PARENT_ID)) {
            $modifiedColumns[':p' . $index++]  = '"parent_id"';
        }
        if ($this->isColumnModified(ResponsePeer::RESPONSE_GROUP_ID)) {
            $modifiedColumns[':p' . $index++]  = '"response_group_id"';
        }

        $sql = sprintf(
            'INSERT INTO "assessment_response" (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '"id"':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '"question_id"':
                        $stmt->bindValue($identifier, $this->question_id, PDO::PARAM_INT);
                        break;
                    case '"assessment_id"':
                        $stmt->bindValue($identifier, $this->assessment_id, PDO::PARAM_INT);
                        break;
                    case '"value"':
                        $stmt->bindValue($identifier, $this->value, PDO::PARAM_STR);
                        break;
                    case '"parent_id"':
                        $stmt->bindValue($identifier, $this->parent_id, PDO::PARAM_INT);
                        break;
                    case '"response_group_id"':
                        $stmt->bindValue($identifier, $this->response_group_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aQuestion !== null) {
                if (!$this->aQuestion->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aQuestion->getValidationFailures());
                }
            }

            if ($this->aAssessment !== null) {
                if (!$this->aAssessment->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aAssessment->getValidationFailures());
                }
            }

            if ($this->aParent !== null) {
                if (!$this->aParent->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aParent->getValidationFailures());
                }
            }

            if ($this->aResponseGroup !== null) {
                if (!$this->aResponseGroup->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aResponseGroup->getValidationFailures());
                }
            }


            if (($retval = ResponsePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collSubResponses !== null) {
                    foreach ($this->collSubResponses as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ResponsePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getQuestionId();
                break;
            case 2:
                return $this->getAssessmentId();
                break;
            case 3:
                return $this->getValue();
                break;
            case 4:
                return $this->getParentId();
                break;
            case 5:
                return $this->getResponseGroupId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Response'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Response'][$this->getPrimaryKey()] = true;
        $keys = ResponsePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getQuestionId(),
            $keys[2] => $this->getAssessmentId(),
            $keys[3] => $this->getValue(),
            $keys[4] => $this->getParentId(),
            $keys[5] => $this->getResponseGroupId(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aQuestion) {
                $result['Question'] = $this->aQuestion->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aAssessment) {
                $result['Assessment'] = $this->aAssessment->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aParent) {
                $result['Parent'] = $this->aParent->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aResponseGroup) {
                $result['ResponseGroup'] = $this->aResponseGroup->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSubResponses) {
                $result['SubResponses'] = $this->collSubResponses->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ResponsePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setQuestionId($value);
                break;
            case 2:
                $this->setAssessmentId($value);
                break;
            case 3:
                $this->setValue($value);
                break;
            case 4:
                $this->setParentId($value);
                break;
            case 5:
                $this->setResponseGroupId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = ResponsePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setQuestionId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setAssessmentId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setValue($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setParentId($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setResponseGroupId($arr[$keys[5]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ResponsePeer::DATABASE_NAME);

        if ($this->isColumnModified(ResponsePeer::ID)) $criteria->add(ResponsePeer::ID, $this->id);
        if ($this->isColumnModified(ResponsePeer::QUESTION_ID)) $criteria->add(ResponsePeer::QUESTION_ID, $this->question_id);
        if ($this->isColumnModified(ResponsePeer::ASSESSMENT_ID)) $criteria->add(ResponsePeer::ASSESSMENT_ID, $this->assessment_id);
        if ($this->isColumnModified(ResponsePeer::VALUE)) $criteria->add(ResponsePeer::VALUE, $this->value);
        if ($this->isColumnModified(ResponsePeer::PARENT_ID)) $criteria->add(ResponsePeer::PARENT_ID, $this->parent_id);
        if ($this->isColumnModified(ResponsePeer::RESPONSE_GROUP_ID)) $criteria->add(ResponsePeer::RESPONSE_GROUP_ID, $this->response_group_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(ResponsePeer::DATABASE_NAME);
        $criteria->add(ResponsePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Response (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setQuestionId($this->getQuestionId());
        $copyObj->setAssessmentId($this->getAssessmentId());
        $copyObj->setValue($this->getValue());
        $copyObj->setParentId($this->getParentId());
        $copyObj->setResponseGroupId($this->getResponseGroupId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getSubResponses() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSubResponse($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Response Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return ResponsePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new ResponsePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Question object.
     *
     * @param                  Question $v
     * @return Response The current object (for fluent API support)
     * @throws PropelException
     */
    public function setQuestion(Question $v = null)
    {
        if ($v === null) {
            $this->setQuestionId(NULL);
        } else {
            $this->setQuestionId($v->getId());
        }

        $this->aQuestion = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Question object, it will not be re-added.
        if ($v !== null) {
            $v->addResponse($this);
        }


        return $this;
    }


    /**
     * Get the associated Question object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Question The associated Question object.
     * @throws PropelException
     */
    public function getQuestion(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aQuestion === null && ($this->question_id !== null) && $doQuery) {
            $this->aQuestion = QuestionQuery::create()->findPk($this->question_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aQuestion->addResponses($this);
             */
        }

        return $this->aQuestion;
    }

    /**
     * Declares an association between this object and a Assessment object.
     *
     * @param                  Assessment $v
     * @return Response The current object (for fluent API support)
     * @throws PropelException
     */
    public function setAssessment(Assessment $v = null)
    {
        if ($v === null) {
            $this->setAssessmentId(NULL);
        } else {
            $this->setAssessmentId($v->getId());
        }

        $this->aAssessment = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Assessment object, it will not be re-added.
        if ($v !== null) {
            $v->addResponse($this);
        }


        return $this;
    }


    /**
     * Get the associated Assessment object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Assessment The associated Assessment object.
     * @throws PropelException
     */
    public function getAssessment(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aAssessment === null && ($this->assessment_id !== null) && $doQuery) {
            $this->aAssessment = AssessmentQuery::create()->findPk($this->assessment_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aAssessment->addResponses($this);
             */
        }

        return $this->aAssessment;
    }

    /**
     * Declares an association between this object and a Response object.
     *
     * @param                  Response $v
     * @return Response The current object (for fluent API support)
     * @throws PropelException
     */
    public function setParent(Response $v = null)
    {
        if ($v === null) {
            $this->setParentId(NULL);
        } else {
            $this->setParentId($v->getId());
        }

        $this->aParent = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Response object, it will not be re-added.
        if ($v !== null) {
            $v->addSubResponse($this);
        }


        return $this;
    }


    /**
     * Get the associated Response object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Response The associated Response object.
     * @throws PropelException
     */
    public function getParent(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aParent === null && ($this->parent_id !== null) && $doQuery) {
            $this->aParent = ResponseQuery::create()->findPk($this->parent_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aParent->addSubResponses($this);
             */
        }

        return $this->aParent;
    }

    /**
     * Declares an association between this object and a ResponseGroup object.
     *
     * @param                  ResponseGroup $v
     * @return Response The current object (for fluent API support)
     * @throws PropelException
     */
    public function setResponseGroup(ResponseGroup $v = null)
    {
        if ($v === null) {
            $this->setResponseGroupId(NULL);
        } else {
            $this->setResponseGroupId($v->getId());
        }

        $this->aResponseGroup = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ResponseGroup object, it will not be re-added.
        if ($v !== null) {
            $v->addResponse($this);
        }


        return $this;
    }


    /**
     * Get the associated ResponseGroup object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return ResponseGroup The associated ResponseGroup object.
     * @throws PropelException
     */
    public function getResponseGroup(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aResponseGroup === null && ($this->response_group_id !== null) && $doQuery) {
            $this->aResponseGroup = ResponseGroupQuery::create()->findPk($this->response_group_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aResponseGroup->addResponses($this);
             */
        }

        return $this->aResponseGroup;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SubResponse' == $relationName) {
            $this->initSubResponses();
        }
    }

    /**
     * Clears out the collSubResponses collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Response The current object (for fluent API support)
     * @see        addSubResponses()
     */
    public function clearSubResponses()
    {
        $this->collSubResponses = null; // important to set this to null since that means it is uninitialized
        $this->collSubResponsesPartial = null;

        return $this;
    }

    /**
     * reset is the collSubResponses collection loaded partially
     *
     * @return void
     */
    public function resetPartialSubResponses($v = true)
    {
        $this->collSubResponsesPartial = $v;
    }

    /**
     * Initializes the collSubResponses collection.
     *
     * By default this just sets the collSubResponses collection to an empty array (like clearcollSubResponses());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSubResponses($overrideExisting = true)
    {
        if (null !== $this->collSubResponses && !$overrideExisting) {
            return;
        }
        $this->collSubResponses = new PropelObjectCollection();
        $this->collSubResponses->setModel('Response');
    }

    /**
     * Gets an array of Response objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Response is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Response[] List of Response objects
     * @throws PropelException
     */
    public function getSubResponses($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSubResponsesPartial && !$this->isNew();
        if (null === $this->collSubResponses || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSubResponses) {
                // return empty collection
                $this->initSubResponses();
            } else {
                $collSubResponses = ResponseQuery::create(null, $criteria)
                    ->filterByParent($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSubResponsesPartial && count($collSubResponses)) {
                      $this->initSubResponses(false);

                      foreach ($collSubResponses as $obj) {
                        if (false == $this->collSubResponses->contains($obj)) {
                          $this->collSubResponses->append($obj);
                        }
                      }

                      $this->collSubResponsesPartial = true;
                    }

                    $collSubResponses->getInternalIterator()->rewind();

                    return $collSubResponses;
                }

                if ($partial && $this->collSubResponses) {
                    foreach ($this->collSubResponses as $obj) {
                        if ($obj->isNew()) {
                            $collSubResponses[] = $obj;
                        }
                    }
                }

                $this->collSubResponses = $collSubResponses;
                $this->collSubResponsesPartial = false;
            }
        }

        return $this->collSubResponses;
    }

    /**
     * Sets a collection of SubResponse objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $subResponses A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Response The current object (for fluent API support)
     */
    public function setSubResponses(PropelCollection $subResponses, PropelPDO $con = null)
    {
        $subResponsesToDelete = $this->getSubResponses(new Criteria(), $con)->diff($subResponses);


        $this->subResponsesScheduledForDeletion = $subResponsesToDelete;

        foreach ($subResponsesToDelete as $subResponseRemoved) {
            $subResponseRemoved->setParent(null);
        }

        $this->collSubResponses = null;
        foreach ($subResponses as $subResponse) {
            $this->addSubResponse($subResponse);
        }

        $this->collSubResponses = $subResponses;
        $this->collSubResponsesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Response objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Response objects.
     * @throws PropelException
     */
    public function countSubResponses(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSubResponsesPartial && !$this->isNew();
        if (null === $this->collSubResponses || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSubResponses) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSubResponses());
            }
            $query = ResponseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByParent($this)
                ->count($con);
        }

        return count($this->collSubResponses);
    }

    /**
     * Method called to associate a Response object to this object
     * through the Response foreign key attribute.
     *
     * @param    Response $l Response
     * @return Response The current object (for fluent API support)
     */
    public function addSubResponse(Response $l)
    {
        if ($this->collSubResponses === null) {
            $this->initSubResponses();
            $this->collSubResponsesPartial = true;
        }

        if (!in_array($l, $this->collSubResponses->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSubResponse($l);

            if ($this->subResponsesScheduledForDeletion and $this->subResponsesScheduledForDeletion->contains($l)) {
                $this->subResponsesScheduledForDeletion->remove($this->subResponsesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	SubResponse $subResponse The subResponse object to add.
     */
    protected function doAddSubResponse($subResponse)
    {
        $this->collSubResponses[]= $subResponse;
        $subResponse->setParent($this);
    }

    /**
     * @param	SubResponse $subResponse The subResponse object to remove.
     * @return Response The current object (for fluent API support)
     */
    public function removeSubResponse($subResponse)
    {
        if ($this->getSubResponses()->contains($subResponse)) {
            $this->collSubResponses->remove($this->collSubResponses->search($subResponse));
            if (null === $this->subResponsesScheduledForDeletion) {
                $this->subResponsesScheduledForDeletion = clone $this->collSubResponses;
                $this->subResponsesScheduledForDeletion->clear();
            }
            $this->subResponsesScheduledForDeletion[]= $subResponse;
            $subResponse->setParent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Response is new, it will return
     * an empty collection; or if this Response has previously
     * been saved, it will retrieve related SubResponses from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Response.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Response[] List of Response objects
     */
    public function getSubResponsesJoinQuestion($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = ResponseQuery::create(null, $criteria);
        $query->joinWith('Question', $join_behavior);

        return $this->getSubResponses($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Response is new, it will return
     * an empty collection; or if this Response has previously
     * been saved, it will retrieve related SubResponses from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Response.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Response[] List of Response objects
     */
    public function getSubResponsesJoinAssessment($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = ResponseQuery::create(null, $criteria);
        $query->joinWith('Assessment', $join_behavior);

        return $this->getSubResponses($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Response is new, it will return
     * an empty collection; or if this Response has previously
     * been saved, it will retrieve related SubResponses from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Response.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Response[] List of Response objects
     */
    public function getSubResponsesJoinResponseGroup($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = ResponseQuery::create(null, $criteria);
        $query->joinWith('ResponseGroup', $join_behavior);

        return $this->getSubResponses($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->question_id = null;
        $this->assessment_id = null;
        $this->value = null;
        $this->parent_id = null;
        $this->response_group_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collSubResponses) {
                foreach ($this->collSubResponses as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aQuestion instanceof Persistent) {
              $this->aQuestion->clearAllReferences($deep);
            }
            if ($this->aAssessment instanceof Persistent) {
              $this->aAssessment->clearAllReferences($deep);
            }
            if ($this->aParent instanceof Persistent) {
              $this->aParent->clearAllReferences($deep);
            }
            if ($this->aResponseGroup instanceof Persistent) {
              $this->aResponseGroup->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collSubResponses instanceof PropelCollection) {
            $this->collSubResponses->clearIterator();
        }
        $this->collSubResponses = null;
        $this->aQuestion = null;
        $this->aAssessment = null;
        $this->aParent = null;
        $this->aResponseGroup = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string The value of the 'value' column
     */
    public function __toString()
    {
        return (string) $this->getValue();
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}

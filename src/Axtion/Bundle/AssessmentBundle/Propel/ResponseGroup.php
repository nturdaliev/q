<?php

namespace Axtion\Bundle\AssessmentBundle\Propel;

use Axtion\Bundle\AssessmentBundle\Propel\om\BaseResponseGroup;
use PropelPDO;

/**
 * Class ResponseGroup
 * @package Axtion\Bundle\AssessmentBundle\Propel
 */
class ResponseGroup extends BaseResponseGroup
{
    /**
     * @inheritDoc
     */
    public function getResponses($criteria = null, PropelPDO $con = null)
    {
        $criteria = ResponseQuery::create()
            ->useQuestionQuery()
                ->orderBySortableRank()
            ->endUse()
            ->filterByParentId(null);

        return parent::getResponses($criteria, $con);
    }
}

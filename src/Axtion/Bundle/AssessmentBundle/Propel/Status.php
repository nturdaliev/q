<?php

namespace Axtion\Bundle\AssessmentBundle\Propel;

use Axtion\Bundle\AssessmentBundle\Propel\om\BaseStatus;

class Status extends BaseStatus
{
    const IN_PROGRESS = "in_progress";
    const OPEN = "open";
    const FINISHED = "finished";
}

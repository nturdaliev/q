<?php

namespace Axtion\Bundle\AssessmentBundle\Propel\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'assessment_status' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Axtion.Bundle.AssessmentBundle.Propel.map
 */
class StatusTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Axtion.Bundle.AssessmentBundle.Propel.map.StatusTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('assessment_status');
        $this->setPhpName('Status');
        $this->setClassname('Axtion\\Bundle\\AssessmentBundle\\Propel\\Status');
        $this->setPackage('src.Axtion.Bundle.AssessmentBundle.Propel');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('assessment_status_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, null, null);
        $this->getColumn('name', false)->setPrimaryString(true);
        $this->addColumn('slug', 'Slug', 'VARCHAR', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Assessment', 'Axtion\\Bundle\\AssessmentBundle\\Propel\\Assessment', RelationMap::ONE_TO_MANY, array('id' => 'status_id', ), null, null, 'Assessments');
    } // buildRelations()

} // StatusTableMap

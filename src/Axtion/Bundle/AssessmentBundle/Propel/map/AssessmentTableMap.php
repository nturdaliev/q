<?php

namespace Axtion\Bundle\AssessmentBundle\Propel\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'assessment_assessment' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Axtion.Bundle.AssessmentBundle.Propel.map
 */
class AssessmentTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Axtion.Bundle.AssessmentBundle.Propel.map.AssessmentTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('assessment_assessment');
        $this->setPhpName('Assessment');
        $this->setClassname('Axtion\\Bundle\\AssessmentBundle\\Propel\\Assessment');
        $this->setPackage('src.Axtion.Bundle.AssessmentBundle.Propel');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('assessment_assessment_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('client_id', 'ClientId', 'INTEGER', 'assessment_client', 'id', true, null, null);
        $this->addForeignKey('questionnaire_id', 'QuestionnaireId', 'INTEGER', 'questionnaire_questionnaire', 'id', true, null, null);
        $this->addForeignKey('status_id', 'StatusId', 'INTEGER', 'assessment_status', 'id', true, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Client', 'Axtion\\Bundle\\AssessmentBundle\\Propel\\Client', RelationMap::MANY_TO_ONE, array('client_id' => 'id', ), null, null);
        $this->addRelation('Questionnaire', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Questionnaire', RelationMap::MANY_TO_ONE, array('questionnaire_id' => 'id', ), null, null);
        $this->addRelation('Status', 'Axtion\\Bundle\\AssessmentBundle\\Propel\\Status', RelationMap::MANY_TO_ONE, array('status_id' => 'id', ), null, null);
        $this->addRelation('Response', 'Axtion\\Bundle\\AssessmentBundle\\Propel\\Response', RelationMap::ONE_TO_MANY, array('id' => 'assessment_id', ), null, null, 'Responses');
        $this->addRelation('ResponseGroup', 'Axtion\\Bundle\\AssessmentBundle\\Propel\\ResponseGroup', RelationMap::ONE_TO_MANY, array('id' => 'assessment_id', ), null, null, 'ResponseGroups');
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' =>  array (
  'create_column' => 'created_at',
  'update_column' => 'updated_at',
  'disable_updated_at' => 'false',
),
        );
    } // getBehaviors()

} // AssessmentTableMap

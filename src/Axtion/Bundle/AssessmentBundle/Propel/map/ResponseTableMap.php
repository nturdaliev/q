<?php

namespace Axtion\Bundle\AssessmentBundle\Propel\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'assessment_response' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Axtion.Bundle.AssessmentBundle.Propel.map
 */
class ResponseTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Axtion.Bundle.AssessmentBundle.Propel.map.ResponseTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('assessment_response');
        $this->setPhpName('Response');
        $this->setClassname('Axtion\\Bundle\\AssessmentBundle\\Propel\\Response');
        $this->setPackage('src.Axtion.Bundle.AssessmentBundle.Propel');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('assessment_response_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('question_id', 'QuestionId', 'INTEGER', 'questionnaire_question', 'id', true, null, null);
        $this->addForeignKey('assessment_id', 'AssessmentId', 'INTEGER', 'assessment_assessment', 'id', true, null, null);
        $this->addColumn('value', 'Value', 'LONGVARCHAR', false, null, null);
        $this->getColumn('value', false)->setPrimaryString(true);
        $this->addForeignKey('parent_id', 'ParentId', 'INTEGER', 'assessment_response', 'id', false, null, null);
        $this->addForeignKey('response_group_id', 'ResponseGroupId', 'INTEGER', 'assessment_response_group', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Question', 'Axtion\\Bundle\\QuestionnaireBundle\\Propel\\Question', RelationMap::MANY_TO_ONE, array('question_id' => 'id', ), null, null);
        $this->addRelation('Assessment', 'Axtion\\Bundle\\AssessmentBundle\\Propel\\Assessment', RelationMap::MANY_TO_ONE, array('assessment_id' => 'id', ), null, null);
        $this->addRelation('Parent', 'Axtion\\Bundle\\AssessmentBundle\\Propel\\Response', RelationMap::MANY_TO_ONE, array('parent_id' => 'id', ), null, null);
        $this->addRelation('ResponseGroup', 'Axtion\\Bundle\\AssessmentBundle\\Propel\\ResponseGroup', RelationMap::MANY_TO_ONE, array('response_group_id' => 'id', ), null, null);
        $this->addRelation('SubResponse', 'Axtion\\Bundle\\AssessmentBundle\\Propel\\Response', RelationMap::ONE_TO_MANY, array('id' => 'parent_id', ), null, null, 'SubResponses');
    } // buildRelations()

} // ResponseTableMap

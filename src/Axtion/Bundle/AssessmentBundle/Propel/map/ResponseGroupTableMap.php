<?php

namespace Axtion\Bundle\AssessmentBundle\Propel\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'assessment_response_group' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Axtion.Bundle.AssessmentBundle.Propel.map
 */
class ResponseGroupTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Axtion.Bundle.AssessmentBundle.Propel.map.ResponseGroupTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('assessment_response_group');
        $this->setPhpName('ResponseGroup');
        $this->setClassname('Axtion\\Bundle\\AssessmentBundle\\Propel\\ResponseGroup');
        $this->setPackage('src.Axtion.Bundle.AssessmentBundle.Propel');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('assessment_response_group_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, null, null);
        $this->getColumn('name', false)->setPrimaryString(true);
        $this->addForeignKey('assessment_id', 'AssessmentId', 'INTEGER', 'assessment_assessment', 'id', false, null, null);
        $this->addColumn('sortable_rank', 'SortableRank', 'INTEGER', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Assessment', 'Axtion\\Bundle\\AssessmentBundle\\Propel\\Assessment', RelationMap::MANY_TO_ONE, array('assessment_id' => 'id', ), null, null);
        $this->addRelation('Response', 'Axtion\\Bundle\\AssessmentBundle\\Propel\\Response', RelationMap::ONE_TO_MANY, array('id' => 'response_group_id', ), null, null, 'Responses');
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'sortable' =>  array (
  'rank_column' => 'sortable_rank',
  'use_scope' => 'true',
  'scope_column' => 'assessment_id',
),
        );
    } // getBehaviors()

} // ResponseGroupTableMap

<?php

namespace Axtion\Bundle\AssessmentBundle\Propel\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'assessment_client' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Axtion.Bundle.AssessmentBundle.Propel.map
 */
class ClientTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Axtion.Bundle.AssessmentBundle.Propel.map.ClientTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('assessment_client');
        $this->setPhpName('Client');
        $this->setClassname('Axtion\\Bundle\\AssessmentBundle\\Propel\\Client');
        $this->setPackage('src.Axtion.Bundle.AssessmentBundle.Propel');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('assessment_client_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('firstname', 'Firstname', 'VARCHAR', true, 50, null);
        $this->addColumn('lastname', 'Lastname', 'VARCHAR', true, 50, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 100, null);
        $this->addColumn('access_code', 'AccessCode', 'VARCHAR', true, 255, null);
        $this->addColumn('is_active', 'IsActive', 'BOOLEAN', false, null, false);
        $this->addForeignKey('user_id', 'UserId', 'INTEGER', 'user_user', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('User', 'Axtion\\Bundle\\UserBundle\\Propel\\User', RelationMap::MANY_TO_ONE, array('user_id' => 'id', ), null, null);
        $this->addRelation('Assessment', 'Axtion\\Bundle\\AssessmentBundle\\Propel\\Assessment', RelationMap::ONE_TO_MANY, array('id' => 'client_id', ), null, null, 'Assessments');
    } // buildRelations()

} // ClientTableMap

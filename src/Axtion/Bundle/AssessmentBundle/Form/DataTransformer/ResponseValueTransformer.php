<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/20/2015
 * Time: 11:47 AM
 */

namespace Axtion\Bundle\AssessmentBundle\Form\DataTransformer;


use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class ResponseValueTransformer
 * @package Axtion\Bundle\AssessmentBundle\Form\DataTransformer
 */
class ResponseValueTransformer implements DataTransformerInterface
{


    /**
     * @param mixed $value
     * @return mixed|void
     */
    public function transform($value)
    {
        if ($value === "") {
            return $value = null;
        }
        if ($value instanceof \DateTime || is_int($value)) {
            return $value;
        }
        if (is_string($value) && ($d = \DateTime::createFromFormat('m/d/Y G:i:s', $value)) !== false) {
            $value = $d;
        } elseif ($this->isJson($value)) {
            $value = json_decode($value);
        }

        return $value;
    }

    /**
     * @param $string
     * @return bool
     */
    public function isJson($string)
    {
        json_decode($string);

        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * @param mixed $value
     * @return mixed|void
     */
    public function reverseTransform($value)
    {
        if (is_array($value)) {
            $value = json_encode($value);
        }
        if ($value instanceof \DateTime) {
            $value = $value->format("m/d/Y G:i:s");
        }

        return $value;
    }
}
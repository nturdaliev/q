<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 10/2/2015
 * Time: 8:57 AM
 */

namespace Axtion\Bundle\AssessmentBundle\Form\Type\Theme;


use Axtion\Bundle\AssessmentBundle\Form\Type\BaseAssessmentType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class SingleAssessmentType
 * @package Axtion\Bundle\AssessmentBundle\Form\Type\Theme
 */
class SingleAssessmentType extends BaseAssessmentType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('responses','responses_form');
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return 'single_assessment_form';
    }


}
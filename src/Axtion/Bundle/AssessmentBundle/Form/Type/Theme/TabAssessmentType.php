<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/18/2015
 * Time: 2:24 PM
 */

namespace Axtion\Bundle\AssessmentBundle\Form\Type\Theme;

use Axtion\Bundle\AssessmentBundle\Form\Type\BaseAssessmentType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class TabAssessmentType
 * @package Axtion\Bundle\AssessmentBundle\Form\Type\Theme
 */
class TabAssessmentType extends BaseAssessmentType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('responseGroups', 'response_groups_form');

        parent::buildForm($builder, $options);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'tab_assessment_form';
    }
}

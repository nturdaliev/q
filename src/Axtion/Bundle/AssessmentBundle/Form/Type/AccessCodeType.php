<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/10/2015
 * Time: 3:31 PM
 */

namespace Axtion\Bundle\AssessmentBundle\Form\Type;


use Symfony\Component\Form\AbstractType;

/**
 * Class AccessCodeType
 * @package Axtion\Bundle\AssessmentBundle\Form\Type
 */
class AccessCodeType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function getParent()
    {
        return 'text';
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'access_code';
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/10/2015
 * Time: 2:35 PM
 */

namespace Axtion\Bundle\AssessmentBundle\Form\Type;

use Axtion\Bundle\AssessmentBundle\Form\Subscriber\ClientSubscriber;
use Axtion\Bundle\QuestionnaireBundle\Form\EventListener\AddButtonSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class ClientType
 * @package Axtion\Bundle\AssessmentBundle\Form\Type
 */
class ClientType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add(
                'is_active',
                'choice',
                array(
                    'choices' => array(
                        0 => 'client.inactive',
                        1 => 'client.active',
                    ),
                )
            )
            ->add('access_code', 'access_code')
            ->addEventSubscriber(new AddButtonSubscriber());
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'   => 'Axtion\Bundle\AssessmentBundle\Propel\Client',
                'label_format' => 'client.%name%',
            )
        );
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'client_form';
    }
}
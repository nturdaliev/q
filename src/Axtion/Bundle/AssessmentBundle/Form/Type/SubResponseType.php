<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/19/2015
 * Time: 12:03 PM
 */

namespace Axtion\Bundle\AssessmentBundle\Form\Type;


use Axtion\Bundle\AssessmentBundle\Constraint\BaseConstraint;
use Axtion\Bundle\AssessmentBundle\Form\DataTransformer\ResponseValueTransformer;
use Axtion\Bundle\AssessmentBundle\Propel\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SubResponseType
 * @package Axtion\Bundle\AssessmentBundle\Form\Type
 */
class SubResponseType extends AbstractType
{
    private $container;

    /**
     * @inheritDoc
     */
    function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }


    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($builder) {
                $form = $event->getForm();

                /*/** @var Question $subResponse */
                $response = $event->getData();

                /** @var \PropelObjectCollection|Response[] $subResponses */
                if (!($response instanceof Response)) {
                    return;
                }

                $options = $response->getParent()->getQuestion()->createOptions();
                $question = $response->getQuestion();

                // get clicked button
                // Bad code!
                $request = $this->container->get('request');
                $assessmentForm = $request->get('assessment_form');
                $required = $assessmentForm['required'];

                $constraintOptions = array(
                    'choices'  => array('value' => array_keys($options['choices'])),
                    'multiple' => array('value' => false),
                    'required' => $options['required'],
                );
                if ($required === 'false') {
                    $constraintOptions['required'] = false;
                }
                /** @var BaseConstraint $constraint */
                $constraint = $this->container->get('axtion_assessment.constraints.choice', ContainerInterface::NULL_ON_INVALID_REFERENCE);

                $constraint->setOptions($constraintOptions);
                $constraint->configureConstraint();

                $form->add(
                    $builder->create(
                        'value',
                        'choice',
                        array(
                            'choices'         => $options['choices'],
                            'expanded'        => true,
                            'label'           => $question->getName(),
                            'auto_initialize' => false,
                            'constraints'     => $constraint->getConstraint(),
                            'required'        => $constraintOptions['required'],
                        )
                    )
                        ->addModelTransformer(new ResponseValueTransformer())->getForm()
                );
            }
        );

    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Axtion\Bundle\AssessmentBundle\Propel\Response',
                'label'      => false,
            )
        );
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'sub_response_form';
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 10/2/2015
 * Time: 8:53 AM
 */

namespace Axtion\Bundle\AssessmentBundle\Form\Type;


use Axtion\Bundle\AssessmentBundle\Propel\Assessment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BaseAssessmentType
 * @package Axtion\Bundle\AssessmentBundle\Form\Type\Theme
 */
class BaseAssessmentType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('required', 'hidden', array('data' => "true"))
            ->add('save', 'submit')
            ->add('submit', 'submit')
            ->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) {
                    /** @var Assessment $assessment */
                    if (is_null($assessment = $event->getData()) || !$assessment instanceof Assessment) {
                        return;
                    }
                    if ($assessment->countResponses() === 0) {
                        $assessment->convertQuestionsToResponses();
                        $assessment->save();
                        $event->setData($assessment);
                    }
                }
            );
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Axtion\Bundle\AssessmentBundle\Propel\Assessment',
//                'attr'=>array('novalidate'=>'novalidate')
            )
        );
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'base_assessment_form';
    }
}
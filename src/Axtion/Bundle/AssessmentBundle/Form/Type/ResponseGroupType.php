<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/21/2015
 * Time: 9:41 AM
 */

namespace Axtion\Bundle\AssessmentBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ResponseGroupType
 * @package Axtion\Bundle\AssessmentBundle\Form\Type
 */
class ResponseGroupType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'responses',
            'responses_form'
        );
    }


    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Axtion\Bundle\AssessmentBundle\Propel\ResponseGroup',
            )
        );
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'response_group_form';
    }
}
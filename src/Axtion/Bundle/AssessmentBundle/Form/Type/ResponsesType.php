<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/18/2015
 * Time: 3:20 PM
 */

namespace Axtion\Bundle\AssessmentBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ResponsesType
 * @package Axtion\Bundle\AssessmentBundle\Form\Type
 */
class ResponsesType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'type' => 'response_form',
            )
        );
    }


    /**
     * @inheritDoc
     */
    public function getParent()
    {
        return 'collection';
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'responses_form';
    }
}
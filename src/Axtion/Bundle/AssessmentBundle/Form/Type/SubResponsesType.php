<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/18/2015
 * Time: 4:54 PM
 */

namespace Axtion\Bundle\AssessmentBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ResponseGridType
 * @package Axtion\Bundle\AssessmentBundle\Form\Type
 */
class SubResponsesType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'type'     => 'sub_response_form',
                'required' => false,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'collection';
    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'sub_responses_form';
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/21/2015
 * Time: 9:40 AM
 */

namespace Axtion\Bundle\AssessmentBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ResponseGroups
 * @package Axtion\Bundle\AssessmentBundle\Form\Type
 */
class ResponseGroupsType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'type' => 'response_group_form',
            )
        );
    }


    /**
     * @inheritDoc
     */
    public function getParent()
    {
        return 'collection';
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'response_groups_form';
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/18/2015
 * Time: 3:22 PM
 */

namespace Axtion\Bundle\AssessmentBundle\Form\Type;


use Axtion\Bundle\AssessmentBundle\Constraint\BaseConstraint;
use Axtion\Bundle\AssessmentBundle\Form\DataTransformer\ResponseValueTransformer;
use Axtion\Bundle\AssessmentBundle\Propel\Response;
use Axtion\Bundle\QuestionnaireBundle\Propel\QuestionType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ResponseType
 * @package Axtion\Bundle\AssessmentBundle\Form\Type
 */
class ResponseType extends AbstractType
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }


    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event) use ($builder) {
                $form = $event->getForm();
                /** @var Response $response */
                $response = $event->getData();
                if (!$response instanceof Response) {
                    return;
                }

                $question = $response->getQuestion();
                $questionType = $question->getQuestionType();
                if ($questionType instanceof QuestionType) {
                    $fieldType = $questionType->getFieldType()->getName();

                    $defaultOptions = array('auto_initialize' => false, 'error_bubbling' => false);
                    $constraints = $this->prepareOption($fieldType, $response);

                    $options = array_merge_recursive($question->createOptions(), $defaultOptions, $constraints);
                    if ($questionType->getHasSubquestions()) {
                        $form->add('subResponses', 'sub_responses_form', array('required' => $options['required'],'label'=>$question->getName(),'help'=>$question->getHelp()));
                    } else {
                        $builder->add('value', $fieldType, $options);

                        $field = $builder->get('value');
                        $field->addModelTransformer(new ResponseValueTransformer());
                        $form->add($field->getForm());
                    }
                }

            }
        );
    }

    /**
     * @param $fieldType
     * @param Response $response
     * @return array
     */
    function prepareOption($fieldType, Response $response)
    {
        // get clicked button
        // Bad code!
        $request = $this->container->get('request');
        $assessmentForm = $request->get('assessment_form');
        $required = $assessmentForm['required'];

        /** @var BaseConstraint $constraint */
        $constraint = $this->container->get('axtion_assessment.constraints.'.$fieldType, ContainerInterface::NULL_ON_INVALID_REFERENCE);
        if (!$constraint instanceof BaseConstraint) {
            $constraint = $this->container->get('axtion_assessment.constraints.base');
        }
        $constraintOptions = $response->getQuestion()->getOptions();

        if ($required === 'false') {
            $constraintOptions['required'] = false;
        }

        $constraint->setOptions($constraintOptions);
        $constraint->configureConstraint();
        $constraints = array('constraints' => $constraint->getConstraint());

        return $constraints;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'        => 'Axtion\Bundle\AssessmentBundle\Propel\Response',
                'validation_groups' => array('Default'),
                'error_bubbling'    => false,
            )
        );
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'response_form';
    }
}
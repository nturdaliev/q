<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/11/2015
 * Time: 10:08 AM
 */

namespace Axtion\Bundle\AssessmentBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ClientFilterType
 * @package Axtion\Bundle\AssessmentBundle\Form\Type
 */
class ClientFilterType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'label_format' => 'client.%name%',
            )
        );
    }


    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'search',
                'text',
                array(
                    'required' => false,
                    'attr'     => array(
                        'placeholder' => 'client.search_placeholder',
                    ),
                )
            )
            ->add(
                'is_active',
                'choice',
                array(
                    'required' => false,
                    'choices'  => array(
                        '1' => 'client.yes',
                        '0' => 'client.no',
                    ),
                )
            );
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'client_filter_form';
    }
}
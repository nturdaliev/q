<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/27/2015
 * Time: 3:41 PM
 */

namespace Axtion\Bundle\AssessmentBundle\Form\Handler;


use Axtion\Bundle\AssessmentBundle\Propel\Assessment;
use Axtion\Bundle\AssessmentBundle\Propel\Response;
use Axtion\Bundle\AssessmentBundle\Propel\ResponseGroup;
use Axtion\Bundle\QuestionnaireBundle\Propel\Dependency;
use Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuery;
use Axtion\Bundle\UtilitiesBundle\Form\Handler\SaveHandler;
use Persistent;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ResponseHandler
 * @package Axtion\Bundle\AssessmentBundle\Form\Handler
 */
class ResponseHandler extends SaveHandler
{
    private $validator;

    /**
     * @param FormInterface $form
     * @param Request $request
     * @param ValidatorInterface $validator
     */
    public function __construct(FormInterface $form, Request $request, ValidatorInterface $validator)
    {
        $this->validator = $validator;
        parent::__construct($form, $request);
    }

    /**
     * @param Persistent | Assessment $obj
     * @return bool
     */
    public function process(Persistent $obj)
    {
        $this->form->setData($obj);

        $this->form->handleRequest($this->request);


        if ($this->isValidForm() || $this->form->isValid()) {
            $this->onSuccess($obj);

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    private function isValidForm()
    {
        if (!$this->form->isSubmitted()) {
            return false;
        }

        if ($this->form->isDisabled()) {
            return true;
        }

        /** @var FormErrorIterator $formErrors */
        $formErrors = $this->form->getErrors(true, true);
        /** @var Assessment $assessment */
        $assessment = $this->form->getData();

        if ($assessment->getRequired() === 'false') {
            return true;
        }
        $dependentQuestionIds = $this->getDependentQuestionIds($assessment);
        $errorIds = $this->getErrorQuestionIds($formErrors);
        $mergedIds = array_unique(array_merge($dependentQuestionIds, $errorIds), SORT_REGULAR);

        return sizeof($mergedIds) === sizeof($dependentQuestionIds);
    }

    /**
     * @param Persistent | Assessment $assessment
     * @return array
     * @throws \PropelException
     */
    private function getDependentQuestionIds(Assessment $assessment)
    {
        $questionIds = array();
        /** @var ResponseGroup $responseGroup */
        foreach ($assessment->getResponseGroups() as $responseGroup) {
            /** @var Response $response */
            foreach ($responseGroup->getResponses() as $response) {
                $question = $response->getQuestion();
                $value = $response->getValue();
                if (!is_null($value) && $value !== "") {
                    $dependency = DependencyQuery::create()
                        ->filterByQuestion($question)
                        ->filterByValue($value)
                        ->findOne();
                    if ($dependency instanceof Dependency) {
                        foreach ($dependency->getDependentQuestions() as $dependentQuestion) {
                            $questionIds[] = $dependentQuestion->getId();
                        }
                    }
                }
            }
        }

        return $questionIds;
    }

    /**
     * @param $formErrors
     * @return array
     */
    private function getErrorQuestionIds($formErrors)
    {
        $ids = array();
        /** @var FormError $error */
        foreach ($formErrors as $key => $error) {
            $form = $error->getOrigin()->getParent();
            if (!$form) {
                continue;
            }
            /** @var Response $response */
            $response = $form->getData();
            $id = $response->getQuestion()->getId();
            $ids[] = $id;
        }

        return $ids;
    }
}
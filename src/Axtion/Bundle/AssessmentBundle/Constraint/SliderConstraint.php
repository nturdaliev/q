<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 9/1/2015
 * Time: 10:17 AM
 */

namespace Axtion\Bundle\AssessmentBundle\Constraint;


use Axtion\Bundle\AssessmentBundle\Validator\Constraints\DivisibleBy;
use Axtion\Bundle\AssessmentBundle\Validator\Constraints\SnapBounds;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;

/**
 * Class Slider
 * @package Axtion\Bundle\AssessmentBundle\Constraint
 */
class SliderConstraint extends BaseConstraint
{


    /**
     * @return array
     */
    public function configureConstraint()
    {
        parent::configureConstraint();

        if (array_key_exists('data-slider-min', $this->options)) {
            $this->constraints[] = new GreaterThanOrEqual(
                array(
                    'value' => $this->options['data-slider-min']['value'],
                )
            );

            $this->constraints[] = new LessThanOrEqual(
                array(
                    'value' => $this->options['data-slider-max']['value'],
                )
            );
            $this->constraints[] = new DivisibleBy(
                array(
                    'value' => $this->options['data-slider-step']['value'],
                )
            );
        } else {
            $ticks = $this->options['data-slider-ticks']['value'];
            $snapBound = $this->options['data-slider-ticks-snap-bounds']['value'];
            $this->constraints[] = new SnapBounds(
                array(
                    'value' => $ticks.', '.$snapBound,
                )
            );
        }
    }
}
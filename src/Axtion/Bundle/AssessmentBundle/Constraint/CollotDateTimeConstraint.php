<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 9/1/2015
 * Time: 10:32 AM
 */

namespace Axtion\Bundle\AssessmentBundle\Constraint;


use Axtion\Bundle\AssessmentBundle\Validator\Constraints\CollotDate;
use Axtion\Bundle\AssessmentBundle\Validator\Constraints\CollotDateTime;
use Axtion\Bundle\AssessmentBundle\Validator\Constraints\CollotTime;

/**
 * Class CollotDateTime
 * @package Axtion\Bundle\AssessmentBundle\Constraint
 */
class CollotDateTimeConstraint extends BaseConstraint
{

    /**
     * @return array
     */
    public function configureConstraint()
    {
        parent::configureConstraint();

        if (!$this->options['format']['value']) {
            $this->options['format']['value'] = 'dd/mm/yyyy hh:ii';
        }
        switch ($this->options['format']['value']) {
            case 'mm/dd/yyyy':
                $this->constraints[] = new CollotDate();
                break;
            case 'dd/mm/yyyy hh:ii':
                $this->constraints[] = new CollotDateTime();
                break;
            case 'hh:ii':
                $this->constraints[] = new CollotTime();
                break;
        };
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 9/1/2015
 * Time: 10:24 AM
 */

namespace Axtion\Bundle\AssessmentBundle\Constraint;

use Symfony\Component\Validator\Constraints\NotBlank;


/**
 * Class BaseConstraint
 * @package Axtion\Bundle\AssessmentBundle\Constraint
 */
class BaseConstraint
{
    protected $options;

    protected $constraints;

    /**
     * @inheritDoc
     */
    function __construct()
    {
        $this->constraints = array();
    }

    /**
     * @return array
     */
    public function getConstraint()
    {
        return $this->constraints;
    }

    /**
     * @param mixed $options
     * @return $this
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    public function configureConstraint()
    {
        $this->setConstraints(array());
        if (array_key_exists('required', $this->options)) {
            if (is_array($this->options['required']) && array_key_exists('value', $this->options['required'])) {
                $this->options['required'] = $this->options['required']['value'];
            }
        }
        if (array_key_exists('required', $this->options) && ($this->options['required'] === true)) {
            $this->constraints[] = new NotBlank();
        }
    }

    /**
     * @param mixed $constraints
     * @return BaseConstraint
     */
    public function setConstraints($constraints)
    {
        $this->constraints = $constraints;

        return $this;
    }


}
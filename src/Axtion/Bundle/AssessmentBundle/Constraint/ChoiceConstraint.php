<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 9/1/2015
 * Time: 10:47 AM
 */

namespace Axtion\Bundle\AssessmentBundle\Constraint;


use Axtion\Bundle\AssessmentBundle\Validator\Constraints\CustomChoice;

/**
 * Class ChoiceConstraint
 * @package Axtion\Bundle\AssessmentBundle\Constraint
 */
class ChoiceConstraint extends BaseConstraint
{
    /**
     * @return array
     */
    public function configureConstraint()
    {
        parent::configureConstraint();

        $this->constraints[] = new CustomChoice(
            array(
                'choices'  => array_keys($this->options['choices']['value']),
                'multiple' => $this->options['multiple']['value'],
            )
        );
    }
}
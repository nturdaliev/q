<?php

namespace Axtion\Bundle\AssessmentBundle\Controller;


use Axtion\Bundle\AssessmentBundle\Propel\Client;
use Axtion\Bundle\AssessmentBundle\Propel\ClientQuery;
use Axtion\Bundle\UserBundle\Propel\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class ClientController
 * @package Axtion\Bundle\AssessmentBundle\Controller
 * @Route("/panel/client")
 * @Security("has_role('ROLE_USER')")
 */
class ClientController extends Controller
{
    /**
     * @Route("/", name="assessment_client_index")
     * @Template()
     */
    public function indexAction()
    {
        /** @var User $user */
        $user = $this->getUser();
        $query = ClientQuery::create();
        if (!$this->isGranted('ROLE_ADMIN')) {
            $query->filterByUser($user);
        }
        $clients = $query->find();

        return array('clients' => $clients);
    }

    /**
     * @Route("/new", name="assessment_client_new")
     * @Template()
     */
    public function newAction()
    {
        $client = new Client();
        $client->setUser($this->getUser());
        $handler = $this->get('axtion_assessment.form.handler.client');
        if ($handler->process($client)) {
            $this->addFlash('success', $this->get('translator')->trans('client.success.new', array('%client_name%' => $client->getFirstname())));
            if ($handler->getForm()->get('create_return_list')->isClicked()) {
                return $this->redirectToRoute('assessment_client_index');
            } else {
                return $this->redirectToRoute('assessment_client_new');
            }
        }

        return array('client' => $client, 'form' => $handler->getForm()->createView());
    }

    /**
     * @Route("/{id}/edit", name="assessment_client_edit")
     * @ParamConverter("client", options={""})
     * @Security("is_granted('ROLE_ADMIN') or (client.getUserId() == user.getId())")
     * @Template()
     * @param Client $client
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Client $client)
    {
        $handler = $this->get('axtion_assessment.form.handler.client');
        if ($handler->process($client)) {
            $this->addFlash('success', $this->get('translator')->trans('client.success.edit', array('%client_name%' => $client->getFirstname())));

            return $this->redirectToRoute('assessment_client_index');
        }

        return array('form' => $handler->getForm()->createView(), 'client' => $client);
    }

    /**
     * @Route("/{id}/delete", name="assessment_client_delete")
     * @ParamConverter("client", options={""})
     * @Security("client.getUserId() == user.getId()")
     * @param Client $client
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws AccessDeniedException
     */
    public function deleteAction(Client $client)
    {
        if ($client->getUserId() !== $this->getUser()->getId()) {
            throw new AccessDeniedException();
        }
        try {
            $client->delete();
            $this->addFlash('success', $this->get('translator')->trans('client.success.delete', array('%client_name%' => $client->getFirstname())));
        } catch (\Exception $e) {
            $this->addFlash('danger', $this->get('translator')->trans('client.failure.delete', array('%client_name%' => $client->getFirstname())));
        }

        return $this->redirectToRoute('assessment_client_index');
    }

    /**
     * @Route("/{id}/show", name="assessment_client_show")
     * @ParamConverter("client", options={""})
     * @Security("client.getUserId() == user.getId()")
     * @Template()
     * @param Client $client
     * @return array
     */
    public function showAction(Client $client)
    {
        return array('client' => $client);
    }
}

<?php

namespace Axtion\Bundle\AssessmentBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class UserController
 * @package Axtion\Bundle\AssessmentBundle\Controller
 *
 * @Route("/assessment")
 */
class SecurityController extends Controller
{

    /**
     * @Route("/check", name="assessment_security_check")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function checkAction()
    {

    }

    /**
     * @Route("/login", name="assessment_security_login")
     * @Template()
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     */
    public function loginAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'AxtionAssessmentBundle:Security:login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $lastUsername,
                'error'         => $error,
            )
        );
    }

    /**
     * @Route("/logout", name="assessment_security_logout")
     */
    public function logoutAction()
    {
        return new BadRequestHttpException("Please configure the firewall!");
    }
}

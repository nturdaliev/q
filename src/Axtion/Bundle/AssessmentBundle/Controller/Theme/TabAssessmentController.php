<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 10/1/2015
 * Time: 3:48 PM
 */

namespace Axtion\Bundle\AssessmentBundle\Controller\Theme;


use Axtion\Bundle\AssessmentBundle\Propel\Assessment;
use Axtion\Bundle\AssessmentBundle\Propel\Status;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/assessment/tab")
 * Class TabAssessmentController
 * @package Axtion\Bundle\AssessmentBundle\Controller\Theme
 */
class TabAssessmentController extends Controller
{
    /**
     * @Route("/{id}/fill", name="assessment_tab_assessment_fill", requirements={"id"="\d+"})
     * @ParamConverter("assessment", options={""})
     * @Security("assessment.getClient() == user and assessment.getQuestionnaire().getTheme().getIdentifier() == 'tab'")
     * @Template()
     * @param Assessment $assessment
     * @return array
     * @throws \Exception
     */
    public function fillAction(Assessment $assessment)
    {
        if ($assessment->getStatus()->getSlug() === Status::FINISHED) {
            return $this->redirectToRoute('assessment_assessment_index');
        }
        $handler = $this->get('axtion_assessment.form.handler.tab_assessment');
        if ($handler->process($assessment)) {
            if ($assessment->getStatus() == Status::IN_PROGRESS) {
                $this->addFlash('success', $this->get('translator')->trans('client.questionnaire.save', array('%name%' => $assessment->getQuestionnaire()->getName())));
            } elseif ($assessment->getStatus() == Status::FINISHED) {
                $this->addFlash('success', $this->get('translator')->trans('client.questionnaire.success', array('%name%' => $assessment->getQuestionnaire()->getName())));
            }

            return $this->redirectToRoute('assessment_assessment_goodbye', array('id' => $assessment->getId()));
        }

        return array('form' => $handler->getForm()->createView(), 'assessment' => $assessment);
    }
}
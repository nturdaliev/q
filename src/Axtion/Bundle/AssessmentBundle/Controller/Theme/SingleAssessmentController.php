<?php

namespace Axtion\Bundle\AssessmentBundle\Controller\Theme;

use Axtion\Bundle\AssessmentBundle\Propel\Assessment;
use Axtion\Bundle\AssessmentBundle\Propel\Status;
use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/assessment/single")
 * Class SingleController
 * @package Axtion\Bundle\AssessmentBundle\Controller
 */
class SingleAssessmentController extends Controller
{

    /**
     * @Route("/{id}/fill", name="assessment_single_assessment_fill", requirements={"id"="\d+"})
     * @ParamConverter("assessment", options={""})
     * @Security("assessment.getClient() == user and assessment.getQuestionnaire().getTheme().getIdentifier() == 'single'")
     * @Template()
     * @param Assessment $assessment
     * @return array
     * @throws \Exception
     */
    public function fillAction(Assessment $assessment)
    {
        $handler = $this->get('axtion_assessment.form.handler.single_assessment');
        if ($handler->process($assessment)) {
            if ($assessment->getStatus() == Status::IN_PROGRESS) {
                $this->addFlash('success', $this->get('translator')->trans('client.questionnaire.save', array('%name%' => $assessment->getQuestionnaire()->getName())));
            } elseif ($assessment->getStatus() == Status::FINISHED) {
                $this->addFlash('success', $this->get('translator')->trans('client.questionnaire.success', array('%name%' => $assessment->getQuestionnaire()->getName())));
            }

            return $this->redirectToRoute('assessment_assessment_goodbye', array('id' => $assessment->getId()));
        }

        return array('form' => $handler->getForm()->createView(), 'assessment' => $assessment);
    }
}

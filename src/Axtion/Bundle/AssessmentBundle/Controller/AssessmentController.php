<?php

namespace Axtion\Bundle\AssessmentBundle\Controller;

use Axtion\Bundle\AssessmentBundle\Propel\Assessment;
use Axtion\Bundle\AssessmentBundle\Propel\AssessmentQuery;
use Axtion\Bundle\AssessmentBundle\Propel\Client;
use Axtion\Bundle\AssessmentBundle\Propel\Status;
use Axtion\Bundle\QuestionnaireBundle\Propel\DependencyQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/assessment")
 * @Security("has_role('ROLE_CLIENT')")
 * Class PatientController
 * @package Axtion\Bundle\QuestionnaireBundle\Controller
 */
class AssessmentController extends Controller
{
    /**
     * @Route("/", name="assessment_assessment_index")
     * @Template()
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        /** @var Client $client */
        $client = $this->getUser();
        $paginator = $this->get('knp_paginator');
        $page = $this->get('request')->query->getInt('page', 1);
        $query = AssessmentQuery::create()
            ->filterByClient($client)
            ->useQuestionnaireQuery()
            ->endUse()
            ->useStatusQuery()
            ->endUse()
            ->withColumn('Questionnaire.Name', 'qname')
            ->withColumn('Questionnaire.CreatedAt', 'created_at')
            ->withColumn('Status.Name', 'sname');

        $assessments = $paginator->paginate($query, $page);

        return array('assessments' => $assessments);
    }

    /**
     * @Route("/{id}/welcome", name="assessment_assessment_welcome", requirements={"id"="\d+"})
     * @ParamConverter("assessment", options={""})
     * @Template()
     * @param Assessment $assessment
     * @return array
     */
    public function welcomeAction(Assessment $assessment)
    {
        return array('assessment' => $assessment);
    }

    /**
     * @Route("/{id}/goodbye", name="assessment_assessment_goodbye", requirements={"id"="\d+"})
     * @ParamConverter("assessment", options={""})
     * @Template()
     * @param Assessment $assessment
     * @return array
     * @internal param Questionnaire $questionnaire
     */
    public function goodbyeAction(Assessment $assessment)
    {
        if ($assessment->getStatus()->getSlug() === 'finished') {
            return $this->redirectToRoute('assessment_assessment_index');
        }

        return array('clientQ' => $assessment);
    }

    /**
     * @Route("/{id}/show", name="assessment_assessment_show", requirements={"id"="\d+"})
     * @ParamConverter("assessment", options={""})
     * @Security("assessment.getClient() == user")
     * @Template()
     * @param Assessment $assessment
     * @return array
     */
    public function showAction(Assessment $assessment)
    {
        return array('assessment' => $assessment);
    }

    /**
     * @Route("/{id}/dependency", name="assessment_assessment_dependency", requirements={"id"="\d+"}, options={"expose"=true})
     * @ParamConverter("assessment", options={""})
     * @Security("assessment.getClient() == user")
     * @param Assessment $assessment
     * @return \Symfony\Component\HttpFoundation\Response|static
     */
    public function dependencyAction(Assessment $assessment)
    {
        $questions = $assessment->getQuestionnaire()->getQuestions();
        $dependencies = DependencyQuery::create()
            ->joinDependencyQuestion("DP")
            ->select(array('Value', 'QuestionId', 'DP.QuestionId'))
            ->filterByQuestion($questions, \Criteria::IN)
            ->find()->toArray();

        return JsonResponse::create($dependencies);
    }
}

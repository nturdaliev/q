<?php

namespace Axtion\Bundle\AssessmentBundle\Controller;

use Axtion\Bundle\AssessmentBundle\Propel\Assessment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/response")
 * Class ResponseController
 * @package Axtion\Bundle\AssessmentBundle\Controller
 */
class ResponseController extends Controller
{
    /**
     * @Route("/{id}/assessment", name="assessment_response_index")
     * @ParamConverter("assessment", options={""})
     * @Template()
     * @param \Axtion\Bundle\AssessmentBundle\Propel\Assessment $assessment
     * @return array
     */
    public function indexAction(Assessment $assessment)
    {
        return array('assessment' => $assessment);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 7/31/2015
 * Time: 1:57 PM
 */

namespace Axtion\Bundle\AssessmentBundle\Security;

use Axtion\Bundle\AssessmentBundle\Propel\Client;
use Axtion\Bundle\AssessmentBundle\Propel\ClientQuery;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class UserProvider
 * @package Axtion\Bundle\AssessmentBundle\Security
 */
class UserProvider implements UserProviderInterface
{

    /**
     * Refreshes the user for the account interface.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     *
     * @param UserInterface $user
     *
     * @return UserInterface
     *
     * @throws UnsupportedUserException if the account is not supported
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof Client) {
            throw new UnsupportedUserException(
                sprintf('Instance of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getEmail());
    }

    /**
     * Loads the user for the given username.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @param string $email
     * @return UserInterface
     * @internal param string $username The username
     *
     * @see UsernameNotFoundException
     *
     */
    public function loadUserByUsername($email)
    {
        $user = ClientQuery::create()->findOneByEmail($email);

        if (!$user) {
            throw new UsernameNotFoundException(sprintf('Access Code "%s" does not exist.', $email));
        }

        return $user;

    }

    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return $class === 'Axtion\Bundle\AssessmentBundle\Propel\Client';
    }
}
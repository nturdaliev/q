<?php

namespace Axtion\Bundle\AssessmentBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class ResponseValid
 * @package Axtion\Bundle\AssessmentBundle\Validator\Constraints
 */
class ResponseValid extends Constraint
{
  /**
   * {@inheritdoc}
   */
  public function getTargets()
  {
    return self::CLASS_CONSTRAINT;
  }

  /**
   * {@inheritdoc}
   */
  public function validatedBy()
  {
    return 'response_valid';
  }
}
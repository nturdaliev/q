<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Axtion\Bundle\AssessmentBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


/**
 * Class CollotDateTime
 * @package Axtion\Bundle\AssessmentBundle\Validator\Constraints
 */
class CollotDateTime extends Constraint
{
    const INVALID_FORMAT_ERROR = 1;
    const INVALID_DATE_ERROR = 2;
    const INVALID_TIME_ERROR = 3;

    protected static $errorNames = array(
        self::INVALID_FORMAT_ERROR => 'INVALID_FORMAT_ERROR',
        self::INVALID_DATE_ERROR => 'INVALID_DATE_ERROR',
        self::INVALID_TIME_ERROR => 'INVALID_TIME_ERROR',
    );

    public $message = 'validation.datetime';
}

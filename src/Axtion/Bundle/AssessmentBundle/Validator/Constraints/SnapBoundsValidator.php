<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/31/2015
 * Time: 2:36 PM
 */

namespace Axtion\Bundle\AssessmentBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class SnapBoundsValidator
 * @package Axtion\Bundle\AssessmentBundle\Validator\Constraints
 */
class SnapBoundsValidator extends ConstraintValidator
{

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     *
     * @api
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof SnapBounds) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\SnapBounds');
        }

        $ticks = explode(', ', $constraint->value);
        $bound = $ticks[sizeof($ticks) - 1];
        $lastIndex = sizeof($ticks) - 2;
        $violation = ($value < $ticks[0] || ($value > $ticks[$lastIndex]));
        if (!$violation) {
            for ($i = 0; $i <= $lastIndex; $i++) {
                if ($value != $ticks[$i]) {
                    if ($value < $ticks[$i]) {
                        if ($ticks[$i] - $bound < $value) {
                            $violation = true;
                            break;
                        }
                    } elseif ($value > $ticks[$i]) {
                        if ($value < $ticks[$i] + $bound) {
                            $violation = true;
                            break;
                        }
                    }
                }
            }
        }
        if ($violation && ($this->context instanceof ExecutionContextInterface)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
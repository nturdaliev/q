<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/31/2015
 * Time: 2:30 PM
 */

namespace Axtion\Bundle\AssessmentBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;

/**
 * Class SnapBounds
 * @package Axtion\Bundle\AssessmentBundle\Validator\Constraints
 */
class SnapBounds extends Constraint
{
    public $message = 'validation.snap_bounds';
    public $value;

    /**
     * {@inheritdoc}
     */
    public function __construct($options = null)
    {
        if (is_array($options) && !isset($options['value'])) {
            throw new ConstraintDefinitionException(
                sprintf(
                    'The %s constraint requires the "ticks" option to be set.',
                    get_class($this)
                )
            );
        }

        parent::__construct($options);
    }


    /**
     * {@inheritdoc}
     */
    public function getDefaultOption()
    {
        return 'ticks';
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/31/2015
 * Time: 11:32 AM
 */

namespace Axtion\Bundle\AssessmentBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraints\AbstractComparison;

/**
 * Class DisibleBy
 * @package Axtion\Bundle\AssessmentBundle\Validator\Constraints
 */
class DivisibleBy extends AbstractComparison
{
    public $message = 'validation.divisible_by';
}
<?php

namespace Axtion\Bundle\AssessmentBundle\Validator\Constraints;

use Axtion\Bundle\AssessmentBundle\Constraint\BaseConstraint;
use Axtion\Bundle\AssessmentBundle\Propel\Assessment;
use Axtion\Bundle\QuestionnaireBundle\Propel\Question;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ResponseValidValidator
 * @package Axtion\Bundle\AssessmentBundle\Validator\Constraints
 */
class ResponseValidValidator extends ConstraintValidator
{
    /**
     * @var ContainerInterface Container
     */
    protected $container;

    /**
     * @var  ValidatorInterface Validator
     */
    protected $validator;

    /**
     * Constructor
     *
     * @param ContainerInterface $container Container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->validator = $container->get('validator');
    }

    /**
     * Checks if the response is valid.
     *
     * @param mixed $response
     * @param Constraint $constraint The constraint for the validation
     * @internal param Assessment $assessment Assessment
     */
    public function validate($response, Constraint $constraint)
    {
        /** @var Assessment $assessment */
        $assessment = $response->getAssessment();
        /** @var ExecutionContextInterface $context */
        $context = $this->context;
        /** @var Question $question */
        $question = $response->getQuestion();
        $constraintOptions = $question->getOptions();
        $questionType = ($question->getQuestionType()) ? $question->getQuestionType() : $question->getParent()->getQuestionType();
        $fieldType = $questionType->getFieldType()->getName();

        /** @var BaseConstraint $constraint */
        $constraint = $this->container->get('axtion_assessment.constraints.'.$fieldType, ContainerInterface::NULL_ON_INVALID_REFERENCE);

        if (!$constraint instanceof BaseConstraint) {
            $constraint = $this->container->get('axtion_assessment.constraints.base');
        }

        if (!$assessment->getRequired()) {
            $constraintOptions['required'] = false;
        }

        $constraint->setOptions($constraintOptions);
        $constraint->configureConstraint();

        foreach ($constraint->getConstraint() as $c) {
            foreach ($this->validator->validate($response->getValue(), $c) as $violation/** @var ConstraintViolation $violation */) {
                $context
                    ->buildViolation($violation->getMessage())
                    ->addViolation();
            }
        }
    }
}
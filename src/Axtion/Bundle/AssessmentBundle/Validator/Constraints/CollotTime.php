<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Axtion\Bundle\AssessmentBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


/**
 * Class CollotTime
 * @package Axtion\Bundle\AssessmentBundle\Validator\Constraints
 */
class CollotTime extends Constraint
{
    const INVALID_FORMAT_ERROR = 1;
    const INVALID_TIME_ERROR = 2;

    protected static $errorNames = array(
        self::INVALID_FORMAT_ERROR => 'INVALID_FORMAT_ERROR',
        self::INVALID_TIME_ERROR => 'INVALID_TIME_ERROR',
    );

    public $message = 'validation.time';
}

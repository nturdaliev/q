<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/31/2015
 * Time: 1:07 PM
 */

namespace Axtion\Bundle\AssessmentBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\ChoiceValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class CustomChoiceValidator
 * @package Axtion\Bundle\AssessmentBundle\Validator\Constraints
 */
class CustomChoiceValidator extends ChoiceValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof CustomChoice) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Choice');
        }

        if (!is_array($constraint->choices) && !$constraint->callback) {
            throw new ConstraintDefinitionException('Either "choices" or "callback" must be specified on constraint Choice');
        }

        if (null === $value) {
            return;
        }

        $tmpValue = json_decode($value);
        if (!is_null($tmpValue)) {
            $value = $tmpValue;
        }

        if ($constraint->multiple && !is_array($value)) {
            throw new UnexpectedTypeException($value, 'array');
        }

        if ($constraint->callback) {
            if (!is_callable($choices = array($this->context->getClassName(), $constraint->callback))
                && !is_callable($choices = $constraint->callback)
            ) {
                throw new ConstraintDefinitionException('The Choice constraint expects a valid callback');
            }
            $choices = call_user_func($choices);
        } else {
            $choices = $constraint->choices;
        }

        if ($constraint->multiple) {
            foreach ($value as $_value) {
                if (!in_array($_value, $choices, $constraint->strict)) {
                    if ($this->context instanceof ExecutionContextInterface) {
                        $this->context->buildViolation($constraint->multipleMessage)
                            ->setParameter('{{ value }}', $this->formatValue($_value))
                            ->setCode(CustomChoice::NO_SUCH_CHOICE_ERROR)
                            ->setInvalidValue($_value)
                            ->addViolation();
                    } else {
                        $this->buildViolation($constraint->multipleMessage)
                            ->setParameter('{{ value }}', $this->formatValue($_value))
                            ->setCode(CustomChoice::NO_SUCH_CHOICE_ERROR)
                            ->setInvalidValue($_value)
                            ->addViolation();
                    }

                    return;
                }
            }

            $count = count($value);

            if ($constraint->min !== null && $count < $constraint->min) {
                if ($this->context instanceof ExecutionContextInterface) {
                    $this->context->buildViolation($constraint->minMessage)
                        ->setParameter('{{ limit }}', $constraint->min)
                        ->setPlural((int)$constraint->min)
                        ->setCode(CustomChoice::TOO_FEW_ERROR)
                        ->addViolation();
                } else {
                    $this->buildViolation($constraint->minMessage)
                        ->setParameter('{{ limit }}', $constraint->min)
                        ->setPlural((int)$constraint->min)
                        ->setCode(CustomChoice::TOO_FEW_ERROR)
                        ->addViolation();
                }

                return;
            }

            if ($constraint->max !== null && $count > $constraint->max) {
                if ($this->context instanceof ExecutionContextInterface) {
                    $this->context->buildViolation($constraint->maxMessage)
                        ->setParameter('{{ limit }}', $constraint->max)
                        ->setPlural((int)$constraint->max)
                        ->setCode(CustomChoice::TOO_MANY_ERROR)
                        ->addViolation();
                } else {
                    $this->buildViolation($constraint->maxMessage)
                        ->setParameter('{{ limit }}', $constraint->max)
                        ->setPlural((int)$constraint->max)
                        ->setCode(CustomChoice::TOO_MANY_ERROR)
                        ->addViolation();
                }

                return;
            }
        } elseif (!in_array($value, $choices, $constraint->strict)) {
            if ($this->context instanceof ExecutionContextInterface) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ value }}', $this->formatValue($value))
                    ->setCode(CustomChoice::NO_SUCH_CHOICE_ERROR)
                    ->addViolation();
            } else {
                $this->buildViolation($constraint->message)
                    ->setParameter('{{ value }}', $this->formatValue($value))
                    ->setCode(CustomChoice::NO_SUCH_CHOICE_ERROR)
                    ->addViolation();
            }
        }
    }
}
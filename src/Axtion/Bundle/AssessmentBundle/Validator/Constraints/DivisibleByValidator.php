<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/31/2015
 * Time: 11:35 AM
 */

namespace Axtion\Bundle\AssessmentBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraints\AbstractComparisonValidator;

/**
 * Class DivisibleByValidator
 * @package Axtion\Bundle\AssessmentBundle\Validator\Constraints
 */
class DivisibleByValidator extends AbstractComparisonValidator
{

    /**
     * Compares the two given values to find if their relationship is valid.
     *
     * @param mixed $value1 The first value to compare
     * @param mixed $value2 The second value to compare
     *
     * @return bool true if the relationship is valid, false otherwise
     */
    protected function compareValues($value1, $value2)
    {
        return $value1 % $value2 == 0;
    }
}
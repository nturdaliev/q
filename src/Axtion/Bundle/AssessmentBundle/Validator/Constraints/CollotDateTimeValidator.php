<?php

namespace Axtion\Bundle\AssessmentBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraints\DateValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;


/**
 * Class CollotDateTimeValidator
 * @package Axtion\Bundle\AssessmentBundle\Validator\Constraints
 */
class CollotDateTimeValidator extends DateValidator
{
    const PATTERN = '/^(\d{2})\/(\d{2})\/(\d{4}) ((\d{2})|((\d{1}))):(\d{2}):(\d{2})$/';

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof CollotDateTime) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\CollotDateTime');
        }

        if (null === $value || '' === $value || $value instanceof \DateTime) {
            return;
        }

        if (!is_scalar($value) && !(is_object($value) && method_exists($value, '__toString'))) {
            throw new UnexpectedTypeException($value, 'string');
        }

        $value = (string) $value;

        if (!preg_match(static::PATTERN, $value, $matches)) {
            if ($this->context instanceof ExecutionContextInterface) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ value }}', $this->formatValue($value))
                    ->setCode(CollotDateTime::INVALID_FORMAT_ERROR)
                    ->addViolation();
            } else {
                $this->buildViolation($constraint->message)
                    ->setParameter('{{ value }}', $this->formatValue($value))
                    ->setCode(CollotDateTime::INVALID_FORMAT_ERROR)
                    ->addViolation();
            }

            return;
        }

        if (!CollotDateValidator::checkDate($matches[1], $matches[2], $matches[3])) {
            if ($this->context instanceof ExecutionContextInterface) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ value }}', $this->formatValue($value))
                    ->setCode(CollotDateTime::INVALID_DATE_ERROR)
                    ->addViolation();
            } else {
                $this->buildViolation($constraint->message)
                    ->setParameter('{{ value }}', $this->formatValue($value))
                    ->setCode(CollotDateTime::INVALID_DATE_ERROR)
                    ->addViolation();
            }
        }

        if (!CollotTimeValidator::checkTime($matches[4], $matches[5], $matches[6])) {
            if ($this->context instanceof ExecutionContextInterface) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ value }}', $this->formatValue($value))
                    ->setCode(CollotDateTime::INVALID_TIME_ERROR)
                    ->addViolation();
            } else {
                $this->buildViolation($constraint->message)
                    ->setParameter('{{ value }}', $this->formatValue($value))
                    ->setCode(CollotDateTime::INVALID_TIME_ERROR)
                    ->addViolation();
            }
        }
    }
}

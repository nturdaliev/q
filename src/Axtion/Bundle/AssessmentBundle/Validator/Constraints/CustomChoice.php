<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 8/31/2015
 * Time: 1:07 PM
 */

namespace Axtion\Bundle\AssessmentBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraints\Choice;

/**
 * Class CustomChoice
 * @package Axtion\Bundle\AssessmentBundle\Validator\Constraints
 */
class CustomChoice extends Choice
{

}
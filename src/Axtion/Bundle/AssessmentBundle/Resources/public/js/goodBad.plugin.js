/**
 * Created by tunu on 10/7/2015.
 */
/*global jQuery*/
(function ($) {
    "use strict";
    $.widget('axtion.goodBadSlider', {
        _create: function () {
            this._configOrientation();
            this._initSlider();
            this._registerOnResize();
        },
        _configOrientation: function () {
            this.orientation = (screen.width < screen.height) ? 'vertical' : 'horizontal';
            if (this.orientation === 'vertical') {
                this.element.parent().addClass('col-xs-offset-5 col-sm-offset-0');
            } else {
                this.element.parent().removeClass('col-xs-offset-5 col-sm-offset-0');
            }
        },
        _registerOnResize: function () {
            (function (self) {
                $(window).on('resize', function () {
                    self._configOrientation();
                    self.element.bootstrapSlider('setAttribute', 'orientation', self.orientation).bootstrapSlider('refresh');
                });
            }(this));
        },
        _initSlider: function () {
            var options, val;
            options = {tooltip: 'hide', orientation: this.orientation};
            val = parseFloat($(this.element).val());
            if (!isNaN(val) && val !== 0) {
                options.value = val;
            }
            this.element.bootstrapSlider(options);
        }
    });
}(jQuery));
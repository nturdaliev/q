/**
 * Created by tunu on 10/2/2015.
 */
/*global jQuery, console,window */
jQuery(function () {
    "use strict";
    (function ($) {
        $.widget('assessment.navigation', {
            _create: function () {
                this._init();
                this._configButtons();
                this._getDependencies();
            },
            _init: function () {
                this.assessmentId = this.element.data('assessment-id');
                this.inputFilter = "input[type='radio'][checked='checked'], input[type='checkbox'][checked='checked']";
                this.prevButton = $('#prevButton', this.element);
                this.nextButton = $('#nextButton', this.element);
                this.submitButton = $('#single_assessment_form_submit', this.element);
                this.saveButton = $('#single_assessment_form_save', this.element);
                this.requiredField = $('#single_assessment_form_required', this.element);
                this.responseGroups = $('.breadcrumb > li');
                //Question index
                this.first = 0;
                this.current = 0;
                this.last = 0;
            },
            _configButtons: function () {
                var self = this;
                self.nextButton.on('click', function () {
                    self._findEnabledQuestion(1);
                });
                self.prevButton.on('click', function () {
                    self._findEnabledQuestion(-1);

                });
            },
            _findEnabledQuestion: function (direction) {
                var oldQuestionIndex = this.current;
                do {
                    this.current += direction;
                    if (this.current < 0) {
                        this.current = 0;
                    }
                    if (this.current > this.last) {
                        this.current = this.last;
                    }
                } while ($(this.questions[this.current]).attr('data-disabled') === "true" && this.current > 0 && this.current < this.last);
                this._hideAndShowQuestion(oldQuestionIndex);
                this._toggleRequired();
            },
            _btnDisableEnable: function () {
                var input, selected, currentQuestion;
                if (this.current >= this.last || $(this.questions[this.current]).attr('data-required') === 'true') {
                    this._setDisabled(this.nextButton);
                } else {
                    this._setEnabled(this.nextButton);
                    this._setDisabled(this.submitButton);
                }
                if (this.current <= this.first) {
                    this._setDisabled(this.prevButton);
                } else {
                    this._setEnabled(this.prevButton);
                }
                if (this.current >= this.last) {
                    currentQuestion = $(this.questions[this.current]);
                    selected = currentQuestion.find('option:selected');
                    if (currentQuestion.find(this.inputFilter).length > 0 || (selected && selected.val() && selected.val().length > 0)) {
                        this._setEnabled(this.submitButton);
                    }
                    input = currentQuestion.find('input[type="text"]').val();
                    if (input && input.length > 0) {
                        this._setEnabled(this.submitButton);
                    }
                }
            },
            _hideAndShowQuestion: function (direction) {
                var currentQuestion, groupId;
                currentQuestion = $(this.questions[this.current]);
                groupId = currentQuestion.attr('data-group-id');
                currentQuestion.removeClass('sr-only');
                this._executeJavaScripts();
                $(this.questions[direction]).addClass('sr-only');
                this._configResponseGroup(groupId);
            },
            _executeJavaScripts: function () {
                $(this.questions[this.current]).find('.slider-input').sliderWrapper();
                $(this.questions[this.current]).find('.good-bad-slider').goodBadSlider();
                $('[data-toggle="popover"]').popover();
            },
            _configResponseGroup: function (groupId) {
                this.responseGroups.filter('.text-primary').removeClass('text-primary');
                this.responseGroups.filter("[data-id='" + groupId + "']").addClass('text-primary');
            },
            _setEnabled: function (element) {
                element.removeAttr('disabled');
            },
            _setDisabled: function (element) {
                element.attr('disabled', 'disabled');
            },
            _getDependencies: function () {
                var self = this;
                $.ajax({
                    method: "GET",
                    url: Routing.generate('assessment_assessment_dependency', {'id': self.assessmentId})

                }).done(function (data) {
                    self.dependencies = data;
                    self._registerEventListeners();
                    self._finish();
                });
            },
            _registerEventListeners: function () {
                var self = this;
                $.each(this.element.find('.question'), function () {
                    if (this instanceof Object) {
                        self._registerEventListener(this);
                    }
                });
            },
            _registerEventListener: function (question) {
                var self = this, inputValue, inputs, questionId, type;
                questionId = parseInt($(question).attr('data-id'), 10);
                inputs = $('select, input', question);
                inputs.on('change', function () {
                    inputValue = $(this).val();
                    type = $(this).attr('type');
                    if (type === 'radio' || type === 'checkbox') {
                        if (type === 'radio') {
                            $(this).closest('.form-group').find('[checked="checked"]').removeAttr('checked');
                        }
                        $(this).attr('checked', 'checked');
                    }
                    self._toggleRequired(question);
                    self._configDependents(questionId, inputValue);
                });
                inputs.filter(self.inputFilter).each(function () {
                    inputValue = $(this).val();
                    self._configDependents(questionId, inputValue);
                });
                inputs = $(question).find('input[type="text"]');
                inputs.on('keyup, change', function () {
                    inputValue = $(this).val();
                    self._toggleRequired(question);
                    self._configDependents(questionId, inputValue);
                });
            },
            _toggleRequired: function () {
                var currentQuestion, filledInputsCount, self = this, tmpLength, input;
                currentQuestion = $(this.questions[this.current]);
                filledInputsCount = 1;
                $.each(currentQuestion.find('.form-group.response'), function () {
                    tmpLength = $(this).find(self.inputFilter).length;
                    if (tmpLength < filledInputsCount) {
                        filledInputsCount = tmpLength;
                    }
                    input = $(this).find('input[type="text"]').val();
                    if (input && input.length > 0) {
                        filledInputsCount = input.length;
                    }
                });
                currentQuestion.attr('data-required', filledInputsCount === 0);
                if (currentQuestion.find('option:selected').length > 0 && currentQuestion.find('option:selected').val().length > 0) {
                    currentQuestion.attr('data-required', false);
                }
                this._btnDisableEnable();
            },
            _configDependents: function (questionId, inputValue) {
                var dependents, self = this, elementFoundToDisable;
                dependents = this._getDependents(questionId, inputValue);
                elementFoundToDisable = self._disableDependents(dependents);
                if (!elementFoundToDisable) {
                    dependents = self._getDependents(questionId, null);
                    self._enableDependents(dependents);
                }
            },
            _disableDependents: function (dependents) {
                var index, dependent, self = this, elementFound = false, inputs;
                for (index = 0; index < dependents.length; index += 1) {
                    dependent = $('[data-id="' + dependents[index] + '"]', self.element).first();
                    if (dependent instanceof Object) {
                        inputs = $('input,select, textarea', dependent);
                        self._disableDependent(inputs);
                        dependent.attr('data-disabled', true);
                        elementFound = true;
                    }
                    dependent.filter('.has-error').removeClass('has-error');
                    dependent.filter('.help-block').remove();
                }
                return elementFound;
            },
            _disableDependent: function (inputs) {
                inputs.attr('disabled', 'disabled')
                    .attr('checked', false)
                    .removeAttr('selected');
                if ((inputs.attr('type') !== typeof undefined && (inputs.attr('type') === 'text')) || inputs.is('textarea')) {
                    inputs.val('');
                }
            },
            _enableDependents: function (dependents) {
                var index, dependent, self = this, inputs;
                for (index = 0; index < dependents.length; index += 1) {
                    dependent = $('[data-id="' + dependents[index] + '"]', self.element).first();
                    if (dependent instanceof Object) {
                        inputs = $('input,select, textarea', dependent);
                        self._enableDependent(inputs);
                        dependent.removeAttr('data-disabled');
                    }
                }
            },
            _enableDependent: function (inputs) {
                $(inputs).removeAttr('disabled');
            },
            _getDependents: function (questionId, inputValue) {
                var depenents = [];
                $.each(this.dependencies, function (index, dependency) {
                    if (dependency.QuestionId === questionId && (dependency.Value === inputValue || inputValue === null)) {
                        depenents.push(this["DP.QuestionId"]);
                    }
                });
                return depenents;
            },
            _hideQuestions: function () {
                var self = this;
                self.questions = self.element.find('.question');
                this.last = self.questions.length - 1;
                self.questions.each(function (index) {
                    if (index === 0) {
                        self.currentQuestion = $(this);
                    } else {
                        $(this).addClass('sr-only');
                    }
                });
            },
            _configureSave: function () {
                var self = this;
                self.saveButton.on('click', function (event) {
                    event.preventDefault();
                    self.element.attr('novalidate', true);
                    self.requiredField.val(false);

                    self.element.submit();
                });
            },
            _configureSubmit: function () {
                var self = this;
                self.submitButton.on('click', function (event) {
                    event.preventDefault();
                    self.element.submit();
                });
                self._setDisabled(self.submitButton);
            },
            _finish: function () {
                this._hideQuestions();
                this._configureSave();
                this._configureSubmit();
                this._btnDisableEnable();
                this._toggleRequired();
                this.element.removeClass('sr-only');
            }
        });
    }(jQuery));
});
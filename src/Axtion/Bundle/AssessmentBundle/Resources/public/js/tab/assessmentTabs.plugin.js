/**
 * Created by tunu on 8/25/2015.
 */
/*jshint strict: true */
/*jslint browser: true*/
/*global $, jQuery, alert,confirm, window, console, Translator*/
jQuery(function () {
    "use strict";
    (function ($) {
        $.widget('axtion.assessmentTabs', {
            options: {},
            _create: function () {
                this._configEventListeners();
                this._configActiveTab();
                this._configTabsWithErrors();
            },
            _configActiveTab: function () {
                var tab = this.element.next().children('.active');
                this._configAll(tab);
            },
            _configEventListeners: function () {
                var self = this;
                this.element.find('li:not(.active) a').on('shown.bs.tab', function (event) {
                    var id = $(event.target).attr('href'),
                        tab = $(id);
                    self._configAll(tab);
                    $(this).off('shown.bs.tab');
                });
            },
            _configTabsWithErrors: function () {
                $('.tab-content .tab-pane').each(function () {
                    var errorCount = $(this).find('.has-error, .help-block>.text-danger').length, id;
                    if (errorCount > 0) {
                        id = $(this).attr('id');
                        $('a[href="#' + id + '"').addClass('text-danger');
                    }
                });
            },
            _configAll: function (tab) {
                tab.find('.slider-input').sliderWrapper();
                tab.find('.good-bad-slider').goodBadSlider();
                tab.find('.form_datetime').each(function () {
                    var pickerOptions = $(this).data('options');
                    $(this).datetimepicker(pickerOptions);
                });
                tab.find('[data-toggle="popover"]').popover();
            }
        });
    }(jQuery));
});
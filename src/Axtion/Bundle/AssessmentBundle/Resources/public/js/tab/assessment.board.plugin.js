/*jshint strict: true */
/*jslint browser: true*/
/*global $, jQuery, alert,confirm, window, console, Translator*/
jQuery(function () {
    "use strict";
    (function ($) {
        $.widget('axtion.assessmentBoard', {
            options: {
                target: '#board'
            },
            _create: function () {
                this.inputs = this.element.find(':input:not(button)');
                this._updateBoard();
            },
            _updateBoard: function () {
                this.board = $(this.options.target);
                this.board.html('');
                this.board.append(this._getNumberOfQuestions());
            },
            _getNumberOfQuestions: function () {
                var amount = this.element.find('.question').length,
                    amountRequired = this.element.find('.fa-asterisk').length;
                this.board.append('<p><i class="fa fa-check"></i> ' + Translator.trans('board.total') + ': <strong class="pull-right">' + amount + '</strong></p>');
                this.board.append('<p><i class="fa fa-check"></i> ' + Translator.trans('board.total_required') + ': <strong class="pull-right">' + amountRequired + '</strong></p>');
            }
        });
    }(jQuery));
});
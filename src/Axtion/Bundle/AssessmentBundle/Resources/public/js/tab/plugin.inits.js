/**
 * Created by tunu on 9/9/2015.
 */
/*global jQuery */
jQuery(function () {
    "use strict";
    (function ($) {
        var assessmentForm = $('form[name="tab_assessment_form"]');
        assessmentForm.assessmentDependency();
        assessmentForm.assessmentBoard();
        $('.paginated-tab').paginatedTab();
    }(jQuery));
});

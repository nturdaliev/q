/**
 * Created by tunu on 9/7/2015.
 */
/*global jQuery,window, console*/
jQuery(function () {
    "use strict";
    (function ($) {
        $.widget('axtion.assessmentDependency', {

            _create: function () {
                this.assessmentId = this.element.data('assessment-id');
                this._init();
                this._getDependencies();
                this._configureSave();
                this._configureSubmit();
            },
            _init: function () {
                this.submitButton = this.element.find('#tab_assessment_form_submit');
                this.saveButton = this.element.find('#tab_assessment_form_save');
                this.requiredField = this.element.find('#tab_assessment_form_required');
                this.tabs = this.element.find('#assessment-tab');
                this.perTab = this.tabs.attr('data-per-tab');
            },
            _getDependencies: function () {
                var self = this;
                $.ajax({
                    method: "GET",
                    url: Routing.generate('assessment_assessment_dependency', {'id': this.assessmentId})

                }).done(function (data) {
                    self.dependencies = data;
                    self._config();
                    self._deviceDependentConfig();
                    self._finish();
                    self.tabs.assessmentTabs();
                });
            },
            _config: function () {
                var self = this,
                    container,
                    dependentQuestions,
                    dependencyTree = self._getDependencyTree();
                $.each(dependencyTree, function (index, object) {
                    var owner = $('[data-id="' + object.owner.QuestionId + '"]'),
                        questions = $('.question');
                    dependentQuestions = questions.filter(function () {
                        var id = parseInt($(this).attr('data-id'), 10);
                        return $.inArray(id, object.dependents) >= 0;
                    });
                    owner.attr('data-value', object.owner.Value);
                    container = $('<div/>').attr('id', 'dependent-questions-' + object.owner.QuestionId + object.owner.Value);
                    container.hide();
                    container.append(dependentQuestions);
                    owner.append(container);
                    self._registerEventListener(owner, object.owner.Value);
                });
            },
            _deviceDependentConfig: function () {
                var self = this, tabContent, tabElements, questions, numberOfTabs, tabElement;
                tabContent = self.element.find('.tab-content');
                tabElements = tabContent.children('.tab-pane');
                $.each(tabElements, function () {
                    tabElement = $(this);
                    questions = $(this).children('.form-group').children().children('.well');
                    if (questions.length > 0) {
                        numberOfTabs = questions.length / self.perTab;
                        if (numberOfTabs === Math.floor(questions.length / self.perTab)) {
                            numberOfTabs -= 1;
                        }
                        numberOfTabs = Math.floor(numberOfTabs);
                        if (numberOfTabs) {
                            self._createTabs(tabElement, numberOfTabs, questions);
                        }
                    }
                });
            },
            _createTabs: function (tabElement, numberOfTabs, questions) {
                var innerDiv, index, id, link, title, element, newId, tabPane, self, tabPanes, elements = [];
                self = this;
                id = tabElement.attr('id');
                link = self.tabs.find('a[href="#' + id + '"]');
                title = link.html();
                tabPanes = [];
                for (index = 0; index < numberOfTabs; index += 1) {
                    newId = id + '_' + index;
                    element = '<li><a href="#' + newId + '" role="tab" data-toggle="tab">' + title + ' (' + (index + 1) + ')</a></li>';
                    elements.push(element);
                    //prepare tab pane
                    innerDiv = $('<div/>').html(questions.slice((index + 1) * self.perTab, (index + 1) * self.perTab + self.perTab));
                    tabPane = $('<div/>').attr('class', 'tab-pane fade').attr('id', newId).html($('<div/>').addClass('form-group').html(innerDiv));
                    tabPanes.push(tabPane);
                }
                link.closest('li').after(elements);
                $('#' + id).after(tabPanes);
            },
            _registerEventListener: function (owner, value) {
                var self = this, inputValue, inputs;
                inputs = owner.children('.form-group').first().find('select, input[type="radio"]');
                inputs.on('change', function () {
                    inputValue = $(this).val();
                    self._disable(owner, inputValue, value);
                });
                inputs.filter('select[selected="selected"], input[type="radio"][checked="checked"], input[type="text"]').each(function () {
                    inputValue = $(this).val();
                    self._disable(owner, inputValue, value);
                });

                inputs = owner.children('.form-group').first().find('input[type="text"]');
                inputs.on('keyup', function () {
                    inputValue = $(this).val();
                    self._disable(owner, inputValue, value);
                });

            },
            _getDependencyTree: function () {
                var dependents,
                    dependencyTree = [],
                    self = this,
                    value,
                    owners = $.map(self.dependencies, function (dependency) {
                        return {QuestionId: dependency.QuestionId, Value: dependency.Value};
                    });
                owners = this._filterOwners(owners);
                $.each(owners, function (index) {
                    dependents = $.map(self.dependencies, function (dependency) {
                        if (dependency.QuestionId === owners[index].QuestionId && dependency.Value == owners[index].Value) {
                            return dependency["DP.QuestionId"];
                        }
                    });
                    dependents = $.unique(dependents);
                    value = self._getValueOfQuestion(owners[index]);
                    dependencyTree[index] = {owner: owners[index], value: value, dependents: dependents};
                });
                return dependencyTree;
            },
            _filterOwners: function (owners) {
                var filteredOwners = [], i, j, found;
                if (owners.length === 0) {
                    return [];
                }
                filteredOwners.push(owners[0]);
                for (i = 1; i < owners.length; i += 1) {
                    found = false;
                    for (j = 0; j < filteredOwners.length; j += 1) {
                        if (filteredOwners[j].QuestionId === owners[i].QuestionId && filteredOwners[j].Value === owners[i].Value) {
                            found = true;
                        }
                    }
                    if (!found) {
                        filteredOwners.push(owners[i]);
                    }
                }
                return filteredOwners;
            },
            //Used inside _getDependencyTree
            _getValueOfQuestion: function (questionId) {
                var value, self = this, index;
                for (index = 0; index < self.dependencies.length; index += 1) {
                    if (self.dependencies[index].QuestionId === questionId) {
                        value = self.dependencies[index].Value;
                        if (value === 'null') {
                            value = '';
                        }
                        return value;
                    }
                }
            },
            _disable: function (owner, inputValue, value) {
                var dependentsContainer = owner.find('[id="dependent-questions-' + owner.attr('data-id') + value + '"]'),
                    dependents = dependentsContainer.find('input,select, textarea');
                if (inputValue === value) {
                    this._showDependents(dependentsContainer, dependents);
                } else {
                    this._hideDependents(dependentsContainer, dependents);
                }
            },
            _showDependents: function (dependentsContainer, dependents) {
                dependentsContainer.slideDown('slow');
                $.each(dependents, function () {
                    $(this).removeAttr('disabled');
                });
            },
            _hideDependents: function (dependentsContainer, dependents) {
                dependentsContainer.slideUp('slow');
                $.each(dependents, function () {
                    var input = $(this);
                    input.attr('disabled', 'disabled')
                        .attr('checked', false)
                        .removeAttr('selected');
                    if ((input.attr('type') !== typeof undefined && (input.attr('type') === 'text')) || input.is('textarea')) {
                        input.val('');
                    }
                });
                dependentsContainer.find('.has-error').removeClass('has-error');
                dependentsContainer.find('.help-block').remove();
            },
            _configureSave: function () {
                var self = this;
                self.saveButton.on('click', function (event) {
                    event.preventDefault();
                    self.element.attr('novalidate', true);
                    self.requiredField.val(false);

                    self.element.submit();
                });
            },
            _configureSubmit: function () {
                var self = this;
                self.submitButton.on('click', function (event) {
                    event.preventDefault();
                    self.requiredField.val(true);
                    self.element.submit();
                });
            },
            _finish: function () {
                this.element.removeClass('sr-only');
            }
        });
    }(jQuery));
});
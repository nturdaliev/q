<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 10/12/2015
 * Time: 2:14 PM
 */

namespace Axtion\Bundle\HelpBundle\Controller;


use Axtion\Bundle\HelpBundle\Form\CategoryType;
use Axtion\Bundle\HelpBundle\Propel\Category;
use Axtion\Bundle\HelpBundle\Propel\CategoryQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/panel/category")
 * @Security("has_role('ROLE_DEVELOPER')")
 * Class CategoryController
 * @package Axtion\Bundle\HelpBundle\Controller
 */
class CategoryAdminController extends Controller
{

    /**
     * @Route("/", name="admin_help_category_index")
     * @Template()
     *
     */
    public function indexAction()
    {
        $categories = CategoryQuery::create()->find();

        return array('categories' => $categories);

    }

    /**
     * @Route("/new", name="admin_help_category_new")
     * @Template()
     * @throws \Exception
     * @throws \PropelException
     */
    public function newAction()
    {
        $category = new Category();
        $form = $this->createForm(new CategoryType(), $category);
        $form->handleRequest($this->get('request'));
        if ($form->isValid()) {
            $category->save();
            return $this->redirectToRoute('admin_help_category_index');
        }

        return array('form' => $form->createView());

    }

    /**
     * @Route("/{id}/edit", name="admin_help_category_edit", requirements={"id"="\d+"})
     * @ParamConverter("category", options={""})
     * @Template()
     * @param Category $category
     * @return array
     * @throws \Exception
     * @throws \PropelException
     */
    public function editAction(Category $category)
    {
        $form = $this->createForm(new CategoryType(), $category);
        $form->handleRequest($this->get('request'));
        if ($form->isValid()) {
            $category->save();
           return $this->redirectToRoute('admin_help_category_index');
        }

        return array('form' => $form->createView());

    }

    /**
     * @Route("{id}/delete", name="admin_help_category_delete", requirements={"id"="\d+"})
     * @ParamConverter("category", options={""})
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Category $category)
    {
        $category->delete();

        return $this->redirectToRoute('admin_help_category_index');
    }
}

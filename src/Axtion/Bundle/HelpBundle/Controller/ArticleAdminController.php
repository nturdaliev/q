<?php

namespace Axtion\Bundle\HelpBundle\Controller;

use Axtion\Bundle\HelpBundle\Form\ArticleType;
use Axtion\Bundle\HelpBundle\Propel\Article;
use Axtion\Bundle\HelpBundle\Propel\ArticleQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/panel/article")
 * Class Article
 * @package Axtion\Bundle\ArticleBundle\Controller
 */
class ArticleAdminController extends Controller
{
    /**
     * @Route("/", name="admin_help_article_index")
     * @Template()
     */
    public function indexAction()
    {
        $articles = ArticleQuery::create()->find();

        return array('articles' => $articles);
    }

    /**
     * @Route("/new", name="admin_help_article_new")
     * @Template()
     */
    public function newAction()
    {
        $article = new Article();
        $form = $this->createForm(new ArticleType(), $article);
        $form->handleRequest($this->get('request'));
        if ($form->isValid()) {
            $article->save();

            return $this->redirectToRoute('admin_help_article_index');
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/{id}/edit", name="admin_help_article_edit")
     * @ParamConverter("article", options={""})
     * @Template()
     * @param Article $article
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     * @throws \PropelException
     */
    public function editAction(Article $article)
    {
        $form = $this->createForm(new ArticleType(), $article);
        $form->handleRequest($this->get('request'));
        if ($form->isValid()) {
            $article->save();

            return $this->redirectToRoute('admin_help_article_index');
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/{id}/delete", name="admin_help_article_delete")
     * @ParamConverter("article", options={""})
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Article $article)
    {

        $article->delete();

        return $this->redirectToRoute('admin_help_article_index');
    }

    /**
     * @Route("/{id}/show", name="admin_help_article_show")
     * @ParamConverter("article", options={""})
     * @Template()
     * @param Article $article
     * @return array
     */
    public function showAction(Article $article)
    {
        return array('article' => $article);
    }
}

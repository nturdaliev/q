<?php

namespace Axtion\Bundle\HelpBundle\Controller;

use Axtion\Bundle\HelpBundle\Propel\CategoryQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * * @Route("/article/category")
 * Class HelpController
 * @package Axtion\Bundle\HelpBundle\Controller
 */
class CategoryController extends Controller
{
    /**
     * @Route("/", name="help_category_index")
     * @Template()
     */
    public function indexAction()
    {
        $categories = CategoryQuery::create()->find();

        return array('categories' => $categories);
    }
}

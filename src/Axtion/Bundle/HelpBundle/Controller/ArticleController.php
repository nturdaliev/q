<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 10/12/2015
 * Time: 3:57 PM
 */

namespace Axtion\Bundle\HelpBundle\Controller;


use Axtion\Bundle\HelpBundle\Propel\Article;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/article")
 * Class ArticleController
 * @package Axtion\Bundle\HelpBundle\Controller
 */
class ArticleController extends Controller
{

    /**
     * @Route("/{id}/show", name="help_article_show")
     * @ParamConverter("article", options={""})
     * @Template()
     * @param Article $article
     */
    public function showAction(Article $article)
    {
        array('article' => $article);
    }
}
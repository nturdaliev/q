<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 10/12/2015
 * Time: 4:32 PM
 */

namespace Axtion\Bundle\HelpBundle\Menu;


use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Translation\DataCollectorTranslator;

/**
 * Class Builder
 * @package Axtion\Bundle\HelpBundle\Menu
 */
class Builder extends ContainerAware
{
    /**
     * @var DataCollectorTranslator
     */
    protected $translator;

    /**
     * @var \Knp\Menu\ItemInterface;
     */
    protected $menu;

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     */
    public function breadcrumb(FactoryInterface $factory, array $options)
    {
        $this->menu = $factory->createItem(
            'root',
            array(
                'childrenAttributes' => array(
                    'class' => 'breadcrumb',
                ),
            )
        );
        $this->translator = $this->container->get('translator');

        $request = $this->container->get('request');
        $route = $request->get('_route');
        $this->configure($route);

        return $this->menu;
    }

    /**
     * @param $route
     */
    public function configure($route)
    {
        $this->addHome($route);
        switch ($route) {
            case 'help_article_show':
                $this->addArticleShow('');
                break;
            default:
                break;
        }
    }

    /**
     * @param string $route
     * @return \Knp\Menu\ItemInterface
     */
    private function addHome($route)
    {
        if ($route !== 'help_category_index') {
            $route = 'help_category_index';
        }

        return $this->menu->addChild(
            $this->translator->trans('home'),
            array(
                'route' => $route,
            )
        );
    }

    /**
     * @param string $route
     * @return \Knp\Menu\ItemInterface
     */
    private function addArticleShow($route = 'help_article_show')
    {
        return $this->menu->addChild(
            $this->translator->trans('article.article'),
            array(
                'route' => $route,
            )
        );
    }
}
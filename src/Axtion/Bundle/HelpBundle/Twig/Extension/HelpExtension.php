<?php

namespace Axtion\Bundle\HelpBundle\Twig\Extension;

use Axtion\Bundle\HelpBundle\Propel\ArticleQuery;
use Axtion\Bundle\HelpBundle\Propel\HelpQuery;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class DeviceExtension
 * @package Axtion\Bundle\QuestionnaireBundle\Twig\Extension
 */
class HelpExtension extends \Twig_Extension
{
    private $requestStack;

    /**
     * @inheritDoc
     */
    function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @inheritDoc
     */
    public function getGlobals()
    {
        $article = null;
        /** @var Request $currentRequest */
        $currentRequest = $this->requestStack->getCurrentRequest();

        if (!$currentRequest instanceof Request || !$currentRequest->isXmlHttpRequest()) {
            $routeName = $currentRequest->get('_route');
            $article = ArticleQuery::create()->findOneByRouteName($routeName);
        }

        return array('articleGlobal' => $article);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'articleGlobal';
    }
}

<?php

namespace Axtion\Bundle\HelpBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ArticleType
 * @package Axtion\Bundle\HelpBundle\Form
 */
class ArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title')
            ->add('category', null, array('required' => true))
            ->add('route_name')
            ->add(
                'content',
                'ckeditor',
                array(
                    'config' => array(
                        'filebrowserBrowseRoute'           => 'elfinder',
                        'filebrowserBrowseRouteParameters' => array('instance' => 'default'),
                    ),
                )
            );
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            array(
                'data_class'   => 'Axtion\Bundle\HelpBundle\Propel\Article',
                'label_format' => 'article.%name%',
            )
        );
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'axtion_help_article_type';
    }
}

<?php

namespace Axtion\Bundle\HelpBundle\Propel\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'help_article' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Axtion.Bundle.HelpBundle.Propel.map
 */
class ArticleTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Axtion.Bundle.HelpBundle.Propel.map.ArticleTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('help_article');
        $this->setPhpName('Article');
        $this->setClassname('Axtion\\Bundle\\HelpBundle\\Propel\\Article');
        $this->setPackage('src.Axtion.Bundle.HelpBundle.Propel');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('help_article_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', true, 255, null);
        $this->getColumn('title', false)->setPrimaryString(true);
        $this->addColumn('content', 'Content', 'LONGVARCHAR', true, null, null);
        $this->addColumn('route_name', 'RouteName', 'VARCHAR', true, 255, null);
        $this->addForeignKey('category_id', 'CategoryId', 'INTEGER', 'help_category', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Category', 'Axtion\\Bundle\\HelpBundle\\Propel\\Category', RelationMap::MANY_TO_ONE, array('category_id' => 'id', ), null, null);
    } // buildRelations()

} // ArticleTableMap

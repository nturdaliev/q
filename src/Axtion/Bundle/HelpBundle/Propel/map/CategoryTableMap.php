<?php

namespace Axtion\Bundle\HelpBundle\Propel\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'help_category' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Axtion.Bundle.HelpBundle.Propel.map
 */
class CategoryTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Axtion.Bundle.HelpBundle.Propel.map.CategoryTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('help_category');
        $this->setPhpName('Category');
        $this->setClassname('Axtion\\Bundle\\HelpBundle\\Propel\\Category');
        $this->setPackage('src.Axtion.Bundle.HelpBundle.Propel');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('help_category_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Article', 'Axtion\\Bundle\\HelpBundle\\Propel\\Article', RelationMap::ONE_TO_MANY, array('id' => 'category_id', ), null, null, 'Articles');
    } // buildRelations()

} // CategoryTableMap

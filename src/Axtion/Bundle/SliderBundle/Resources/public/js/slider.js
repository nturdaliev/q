/**
 * Created by tunu on 7/7/2015.
 */
/*jslint browser: true*/
/*global $, jQuery, alert,Translator, Slider*/
(function ($) {
    "use strict";
    $.widget('axtion.sliderWrapper', {
        options: {
            tooltip: 'always'
        },
        _create: function () {
            var opt, val;
            this._setTick();
            this._setLabel();
            this._setBounds();

            opt = this.options;
            val = parseInt($(this.element).val(), 10);
            if (!isNaN(val) && val !== 0) {
                opt = $.extend(opt, {value: val});
            }
            this.element.bootstrapSlider(opt);
        },
        _setTick: function () {
            var data = this.element.data('slider-ticks');
            if (data) {
                data = data.split(",");
                data = $.map(data, function (value) {
                    return parseInt(value, 10);
                });
                this.options.ticks = data;
            }
        },
        _setLabel: function () {
            var data = this.element.data('slider-ticks-labels');
            if (data) {
                data = data.split(',');
                data = $.map(data, function (value) {
                    return value.trim();
                });
                this.options.ticks_labels = data;
            }
        },
        _setBounds: function () {
            var data = this.element.data('slider-ticks-snap-bounds');
            if (data) {
                this.options.ticks_snap_bounds = parseInt(data, 10);
            }
        }
    });
}(jQuery));
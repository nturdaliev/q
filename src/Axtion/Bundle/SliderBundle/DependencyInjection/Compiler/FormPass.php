<?php
/**
 * Created by PhpStorm.
 * User: tunu
 * Date: 7/7/2015
 * Time: 11:41 AM
 */

namespace Axtion\Bundle\SliderBundle\DependencyInjection\Compiler;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class FormPass
 * @package Axtion\Bundle\SliderBundle\DependencyInjection\Compiler
 */
class FormPass implements CompilerPassInterface
{

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @api
     */
    public function process(ContainerBuilder $container)
    {

        if ($container->hasParameter('twig.form.resources')) {
            $parameters = $container->getParameter('twig.form.resources');
            $parameters[] = 'AxtionSliderBundle:Form:fields.html.twig';
            $container->setParameter('twig.form.resources', $parameters);
        }

    }
}